## 本例基于Create-react-app脚手架

### 包含：
1. [UI组件Antd](https://ant.design/index-cn)
2. [Electron桌面应用](https://electron.org.cn/)
3. [路由React Router4.0](http://reacttraining.cn/)
4. [Mobx(更易入手，代替了Redux)](http://mobxjs.github.io/mobx/)

### `npm install yarn -g`

install golbal yarn, then you can use yarn instead of npm

### `yarn(npm install)`

install node_modules

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](#deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

It will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

### `yarn electron`

Runs the app in the development mode. You can view it in desktop app.

### `yarn packager`

Builds the app for production to the `myDemo` folder, it's a win x64 desktop app,
you can run it when you click app.exe, and you can change package version by modify package.json.
