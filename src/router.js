//import components
import React from 'react'
import Bundle from './bundle.js'
import Login from './components/Login/Login'
import Home from 'bundle-loader?lazy&name=Home!./components/Home/Home'
import UserInfo from 'bundle-loader?lazy&name=UserInfo!./components/UserInfo/UserInfo'
import UserAuth from 'bundle-loader?lazy&name=UserAuth!./components/UserAuth/UserAuth'
import Module from 'bundle-loader?lazy&name=Module!./components/Module/Module'
import Shop from 'bundle-loader?lazy&name=Shop!./components/Shop/index'
import Classify from 'bundle-loader?lazy&name=Classify!./components/Classify/Classify'
import ItemSchedule from 'bundle-loader?lazy&name=ItemSchedule!./components/ItemSchedule/ItemSchedule'
import LockPrice from 'bundle-loader?lazy&name=LockPrice!./components/LockPrice/LockPrice'
import ItemFilter from 'bundle-loader?lazy&name=ItemFilter!./components/ItemFilter/ItemFilter'
import ItemBind from 'bundle-loader?lazy&name=ItemBind!./components/ItemBind/ItemBind'
import ItemModify from 'bundle-loader?lazy&name=ItemModify!./components/ItemModify/ItemModify'
import StoreItem from 'bundle-loader?lazy&name=StoreItem!./components/StoreItem/index'
import StoreRelease from 'bundle-loader?lazy&name=StoreRelease!./components/StoreRelease/StoreRelease'
import Order from 'bundle-loader?lazy&name=Order!./components/Order/Order'
import ReportSales from 'bundle-loader?lazy&name=ReportSales!./components/ReportSales/ReportSales'
import ExportRecord from 'bundle-loader?lazy&name=ExportRecord!./components/ExportRecord/index'
import ImportRecord from 'bundle-loader?lazy&name=ImportRecord!./components/ImportRecord/index'
import CategoryLog from 'bundle-loader?lazy&name=CategoryLog!./components/LogQuery/CategoryLog/index'
import ShopLog from 'bundle-loader?lazy&name=ShopLog!./components/LogQuery/ShopLog/index'
import ShopItemLog from 'bundle-loader?lazy&name=ShopItemLog!./components/LogQuery/ShopItemLog/index'
import InventoryLog from 'bundle-loader?lazy&name=InventoryLog!./components/LogQuery/InventoryLog/index'
import P4InventoryLog from 'bundle-loader?lazy&name=P4InventoryLog!./components/LogQuery/P4InventoryLog/index'
import ShopPrice from 'bundle-loader?lazy&name=ShopPrice!./components/LogQuery/ShopPrice/index'
import ItemReport from 'bundle-loader?lazy&name=ItemReport!.//components/ItemReport/index'
import Fence from 'bundle-loader?lazy&name=Fence!.//components/Fence/index'
import Warehouse from 'bundle-loader?lazy&name=Warehouse!./components/Warehouse/warehouse'
import Charge from 'bundle-loader?lazy&name=Charge!./components/Logistics/charge'
import LogisticsType from 'bundle-loader?lazy&name=LogisticsType!./components/Logistics/logisticsType'
import ShopLogisticsList from 'bundle-loader?lazy&name=ShopLogisticsList!./components/Logistics/shopList'
import AddShopLogisticsItem from 'bundle-loader?lazy&name=AddShopLogisticsItem!./components/Logistics/addShopLogisticsItem'
import ztManageClassify from 'bundle-loader?lazy&name=ztManageClassify!./components/Ztmanage/Classify'
import ztProduction from 'bundle-loader?lazy&name=ztProduction!./components/Ztmanage/Production'
import ztItemModify from 'bundle-loader?lazy&name=ztItemModify!./components/Ztmanage/ItemModify'
import TagsManage from 'bundle-loader?lazy&name=TagsManage!./components/Ztmanage/TagsManage'
import TagBindProduction from 'bundle-loader?lazy&name=TagBindProduction!./components/Ztmanage/TagBindProduction'
import ztSku from 'bundle-loader?lazy&name=ztSku!./components/Ztmanage/index'


// Each logical "route" has two components, one for
// the sidebar and one for the main area. We want to
// render both of them in different places when the
// path matches the current URL.

const bundleComponent = (Component) => {
  return (props) => (
      <Bundle load={Component}>
        {(Container) => <Container {...props}/>}
      </Bundle>
  )
}

    const routes = [
      // { path: '/',
      //   exact: true,
      //   component: Login
      // },
      {
        path: '/login',
        exact: true,
        component: Login
      },
      {
        path: '/index',
        exact: true,
        component: bundleComponent(Home)
      },
      {
        path: '/user/info',
        exact: true,
        component: bundleComponent(UserInfo)
      },
      {
        path: '/user/auth',
        exact: true,
        component: bundleComponent(UserAuth)
      },
      {
        path: '/user/module',
        exact: true,
        component: bundleComponent(Module)
      },
      {
        path: '/shop',
        component: bundleComponent(Shop)
      },
      {
        path: '/classify',
        component: bundleComponent(Classify)
      },
      {
        path: '/item/schedule',
        component: bundleComponent(ItemSchedule)
      },
      {
        path: '/lockPrice',
        component: bundleComponent(LockPrice)
      },
      {
        path: '/item/filter',
        component: bundleComponent(ItemFilter)
      },
      {
        path: '/item/bind',
        component: bundleComponent(ItemBind)
      },
      {
        path: '/item/modify/:itemCode/:retailFormatId',
        component: bundleComponent(ItemModify)
      },
      {
        path: '/store/item',
        component: bundleComponent(StoreItem)
      },
      {
        path: '/store/release',
        component: bundleComponent(StoreRelease)
      },
      {
        path: '/order',
        component: bundleComponent(Order)
      },
      {
        path: '/report/sales',
        component: bundleComponent(ReportSales)
      },
      {
        path: '/record/export',
        component: bundleComponent(ExportRecord)
      },
      {
        path: '/record/import',
        component: bundleComponent(ImportRecord)
      },
      {
        path: '/log/category',
        component: bundleComponent(CategoryLog)
      },
      {
        path: '/log/shopItem',
        component: bundleComponent(ShopItemLog)
      },
      {
        path: '/log/shop',
        component: bundleComponent(ShopLog)
      },
      {
        path: '/log/inventory',
        component: bundleComponent(InventoryLog)
      },
      {
        path: '/log/p4Inventory',
        component: bundleComponent(P4InventoryLog)
      },
      {
        path: '/log/price',
        component: bundleComponent(ShopPrice)
      },
      {
        path: '/report/items',
        component: bundleComponent(ItemReport)
      },
      {
        path: '/fence',
        component: bundleComponent(Fence)
      },
      {
        path: '/warehouse', //Warehouse
        component: bundleComponent(Warehouse)
      },
      {
        path: '/logistics/charge', //Warehouse
        component: bundleComponent(Charge)
      },
      {
        path: '/logistics/type', //Warehouse
        component: bundleComponent(LogisticsType)
      },
      {
        path: '/logistics/shop', //Warehouse
        component: bundleComponent(ShopLogisticsList)
      },
      {
        path: '/logistics/add/:shopId/:logisticsType', //Warehouse
        component: bundleComponent(AddShopLogisticsItem)
      },
      {
        path: '/logistics/add', //Warehouse
        component: bundleComponent(AddShopLogisticsItem)
      },
      {
        path: '/ztmanage/classify', //Warehouse
        component: bundleComponent(ztManageClassify)
      },
      {
        path: '/ztmanage/production', //Warehouse
        component: bundleComponent(ztProduction)
      },
      {
        path: '/ztmanage/modify/:itemId',
        component: bundleComponent(ztItemModify)
      },
      {
          path: '/ztmanage/ztSku',
          component: bundleComponent(ztSku)
      },
      {
          path: '/ztmanage/tagsManage',
          component: bundleComponent(TagsManage)
      },
      {
          path: '/ztmanage/tagbindproduction/:labelId',
          component: bundleComponent(TagBindProduction)
      },
      // {
      //   path: '/warehouse', //Warehouse
      //   component: bundleComponent(() =>
      //     import ('./components/Warehouse/warehouse'))
      // },
      {
        path: 'notMatch',
      },
    ]


    export default routes