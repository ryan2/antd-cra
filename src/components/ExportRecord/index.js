import React, {
    Component
} from 'react'
import ExportRecordListForm from './ExportRecordList'
import {
    observer,
    inject
} from 'mobx-react'
import RecordTable from '../RecordTable/index'

@inject('store') @observer
class ExportRecord extends Component {
    state = {
        page: undefined,
        pageSize: undefined,
        formList: {}
    }

    handleSubmit = (page, pageSize, type, operator, startDate, endDate) => {
        this.setState({
            page: this.refs.RecordTable.wrappedInstance.state.page,
            pageSize: this.refs.RecordTable.wrappedInstance.state.pageSize
        })
        this.refs.RecordTable.wrappedInstance.changeMsgData(this.refs.RecordTable.wrappedInstance.state.page, this.refs.RecordTable.wrappedInstance.state.pageSize, type, operator, startDate, endDate)
    }

    getFormList = formList => {
        this.setState({
            formList
        })
    }
    render () {
        let title
        if (this.props.store.collapse) {
          title = <h2 style={{ marginBottom: 16 ,color: '#108ee9'}}>导出记录</h2>
        } else {
          title = ''
        }
        return (
            <div>
            {title}
            <ExportRecordListForm 
                handleSubmit = {this.handleSubmit}
                page = {this.state.page}
                pageSize = {this.state.pageSize}
                getFormList = {this.getFormList}
            />
            <RecordTable 
                type = 'export'
                ref = 'RecordTable'
                formList = {this.state.formList}
            />
            </div>
        )
    }
}

export default ExportRecord