import React, { Component } from 'react';
import { Modal, Form, Checkbox, Spin, Button } from 'antd';
const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;

class UserAuthModule extends Component {
  constructor (props) {
    super (props);
    this.state  = {
      confirmDirty: false,
      autoExpandParent: true,
      checkedKeys: [],
      selectedKeys: [],
    }
  }

  //取消
  handleCancel = () => {
    const { onCancel, form } = this.props;
    /*this.setState({
     checkedKeys: []
     })*/
    form.resetFields();
    onCancel();
  }

  //提交修改、创建
  handleSubmit = () => {
    const { onCreate, form, userInfo } = this.props;
    const { validateFields } = form;
    validateFields((err, values) => {
      if ( !err) {
        values.userId = userInfo.userId;
        onCreate(values).then(() => {
          form.resetFields();
        });
      }
    });
  }

  render () {
    const { visible, defaultValues, form, roleList, userInfo, loading, submitLoading } = this.props;
    const { getFieldDecorator } = form;
    const formTailLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 18 },
    };
    const checkOptions = roleList.map(item => ({value: item.roleId, label: item.roleName}))
    return (
        <Modal
          title="设置用户角色"
          maskClosable={false}
          visible={visible}
          onCancel={this.handleCancel}
          onOk={this.handleSubmit}
          footer={[
            <Button key='back' size="large" onClick={this.handleCancel}>取消</Button>,
            <Button key='submit' type="primary" size="large" loading={submitLoading} onClick={this.handleSubmit}>
              保存
            </Button>,
          ]}
            >
          <Spin spinning={loading}>
            <Form style={{margin: 20}}>
              <FormItem label="当前用户" {...formTailLayout}>
                <span style={{paddingLeft: 6}}>{userInfo.userName}</span>
              </FormItem>
              <FormItem label="设置角色" {...formTailLayout}>
                {getFieldDecorator('roleList', {
                  initialValue: defaultValues,
                  rules: [{ required: true, message: '请至少选择一个角色' }],
                })(
                   <CheckboxGroup options={checkOptions}/>
                )}
              </FormItem>
            </Form>
          </Spin>
        </Modal>
    )
  }
}

export default Form.create()(UserAuthModule);