
图片上传
====
该组件用于图片上传

#### 用法
  ```js

  render() {
  	return (
  	<PhotoUpload fileList = { fileList }
                 limit = { 5 }
                 fileSize = { 52000 }
                 actionUrl = { `${ apiHost }${ goodAPI.upload }` }
                 data = {{
                     token: token,
                 }}
                 showRemoveIcon={true|false}
                 buttonText={'上传'}
                 errMessage = { "上传出错! 请重新上传，或稍后上传！" }
                 upload = { this.handleItemClick }
                 />
  	);
  }

  ```
#### Props ( 属性 )

  - `fileList`: PropTypes.array   已上传的图片数组
  - `limit`: PropTypes.number     限制上传个数
  - `fileSize`: PropTypes.string,  上传图片一张的大小
  - `actionUrl`: PropTypes.string, 上传url－－ action
  - `data`: PropTypes.func          url的参数
  - `errMessage`: PropTypes.func    上传错误提示
  - `upload`: PropTypes.func        上传图片数组的回调
  - `showRemoveIcon`                是否在图片上显示删除按钮，false不显示，true显示。默认显示
  - `buttonText`                    上传按钮上显示的文字
  - `onRemove`                      点击删除的回调函数，返回false则不删除
  - `callBack`                      图片上传成功的回调函数
