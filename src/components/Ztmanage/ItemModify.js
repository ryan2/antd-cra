import React, {
  Component
} from 'react'
import {
  observer,
  inject
} from 'mobx-react'
import {
  Tabs,
  Spin,
  message,
  Modal
} from 'antd';
import ItemModifyForm from './ItemModifyForm'
import TagsPagiationn from './Tags'
import Pictures from './Pictures'
import Describe from './Describe'
import '../ItemModify/itemModify.css'
const TabPane = Tabs.TabPane;

@inject('ztItemStore', 'store') @observer
class ItemModify extends Component {
  constructor(props) {
    super(props)
    this.state = {
      itemId :props.match.params.itemId
    }
  }
  componentWillMount() {
    const {
      getZtlabelList,
      getProductItem,
      initData
    } = this.props.ztItemStore;
    initData()
    getProductItem(this.props.match.params)
    getZtlabelList(this.props.match.params)
  }
  handleSubmit = (params) => {
    params.itemId = this.state.itemId;
    this.props.ztItemStore.updateProductItem(params).then(res => {
        if (res.status === 200) {
          message.success('操作成功')
          this.props.history.push('/ztmanage/production')
        } else {
          Modal.error({
            title: '操作失败',
            content: res.err
          })
        }
      })
      .catch(err => {
        message.error('操作失败')
      })
  }

  handleCancel = () => {
    this.props.history.push('/ztmanage/production')
  }

  render() {
      const {
        productItem,
        loading,
        handleCategoryData,
        categoryList,
        productionTagsList,
        filterTags,
        filterTagList,
        filterLabelList,
        temporary,
        editProductionTags,
        getPictures,
        pictures,
        getDescribes,
        describes
      } = this.props.ztItemStore
      // const {
      //   categoryList
      // } = this.props.classifyStore
    return (
      <Spin spinning={loading}>
        <Tabs>
          <TabPane tab="基本资料" key="1">
            <ItemModifyForm defaultData={productItem}
                            categoryList={handleCategoryData(categoryList, true)}
                            onSubmit={this.handleSubmit}
                            onCancel={this.handleCancel}
                            loading={loading}/>
          </TabPane>
          <TabPane tab="标签属性" key="2">
            <TagsPagiationn tagsList = { productionTagsList } 
                            filterTags = { filterTags } 
                            filterTagList = { filterTagList } 
                            temporary = { temporary }
                            itemId = { this.state.itemId }
                            editProductionTags = { editProductionTags }
                            loading = { loading }
                            filterLabelList = { filterLabelList } />
          </TabPane>
          <TabPane tab="图片" key="3">
            <Pictures getPictures = { getPictures } 
                     loading = { loading }
                     itemId = { this.state.itemId }
                     pictures = {pictures} />
          </TabPane>
          <TabPane tab="文描" key="4">
            <Describe getDescribes = { getDescribes } 
                     loading = { loading }
                     itemId = { this.state.itemId }
                     describes = {describes} />
          </TabPane>
        </Tabs>
      </Spin>
    )
  }
}

export default ItemModify
