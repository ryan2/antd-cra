import React, {
    Component
} from 'react'
import {
    Tag,
    Button,
    Card,
    Row,
    Col,
    Input,
    Table,
    Modal,
    Form,
    // Cascader,
    Tooltip,
    Spin,
    message
} from 'antd';
import {
    toJS
} from 'mobx'
import {
    observer,
    inject
} from 'mobx-react'
import '../ItemModify/itemModify.css'
const FormItem = Form.Item;
let interval = null;

class TagModal extends Component {
    addNewTag = () => {
        const {
            form,
            defaultValue,
            modalCancle,
            addNewTag
        } = this.props
        form.validateFields((err, values) => {
            if (!err) {
                addNewTag(Object.assign({}, defaultValue, values))
                modalCancle()
                form.resetFields()
            }
        });
    }
    render(){
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            }
        };
        const {
            modalVisible,
            loading,
            labelTypeId,
            labelTypeName,
            modalCancle,
            defaultValue,
            clearForm
            // categoryList
        } = this.props
        if(clearForm) this.props.form.resetFields()
        return (
            <Modal title="新增标签"
                style = {{ top: '10%', padding:0 }}
                visible = { modalVisible }
                onOk = { this.addNewTag }
                confirmLoading = { loading }
                onCancel = { modalCancle  }>
                <Form>
                    <FormItem label="标签类别ID" {...formItemLayout}>
                        {getFieldDecorator('labelTypeId', {
                            initialValue: labelTypeId
                        })(
                            <Input readOnly />
                        )}
                    </FormItem>
                    <FormItem label="标签类别名称" {...formItemLayout}>
                        {getFieldDecorator('labelTypeName', {
                            initialValue: labelTypeName
                        })(
                            <Input readOnly />
                        )}
                    </FormItem>
                    <FormItem label="标签名称(中)" {...formItemLayout}>
                        {getFieldDecorator('labelName', {
                            rules: [
                                { required: true, message: '必须填写标签名称(中)' },
                                { max: 20, message: '最大长度不超过30' }],
                            initialValue: defaultValue.labelName
                        })(
                            <Input placeholder="请填写标签名称"/>
                        )}
                    </FormItem>
                    <FormItem label="标签名称(英)" {...formItemLayout}>
                        {getFieldDecorator('labelNameEn', {
                            rules: [
                                { required: true, message: '必须填写标签名称(英)' },
                                { max: 20, message: '最大长度不超过30' }],
                            initialValue: defaultValue.labelNameEn
                        })(
                            <Input placeholder="请填写标签名称"/>
                        )}
                    </FormItem>
                    <FormItem label="排序权重" {...formItemLayout}>
                        <Tooltip title="数值越小排序越靠前">
                            {getFieldDecorator('seqNo', {
                                initialValue: defaultValue.seqNo==undefined?'100': defaultValue.seqNo.toString()
                            })(
                                    <Input type="text" placeholder="请填写排序权重"/>
                                )}
                        </Tooltip>
                    </FormItem>
                    {/* <FormItem label="商品分类" {...formItemLayout}>
                        {getFieldDecorator('categoryId', {
                            rules: [{
                                required: true, message: '必须选择商品类别',
                            }]
                        })(
                            <Cascader placeholder="请选择商品分类" options={categoryList} />
                        )}
                    </FormItem> */}
                </Form>
            </Modal>
        )
    }
}
const TagModalDom = Form.create()(TagModal)
@inject('ztItemStore', 'classifyStore') @observer
class TagsManage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            containerHeight: 0,
            showTagsList: [],
            labelTypeName: '',
            labelTypeId: '',
            modalVisible: false,
            defaultValue: {},
            clearForm: false,
            iptValue: props.ztItemStore.filterKeyValue
        }
    }
    componentDidMount() {
        const {
            getZtlabelList,
        } = this.props.ztItemStore
        this.getContainerHight()
        window.onresize = this.getContainerHight;
        getZtlabelList()
    }
    componentWillUnmount(props) {
        window.onresize = null
        clearInterval(interval)
    }
    getContainerHight = () => {
        this.setState({
            containerHeight: document.body.offsetHeight - 82 + 'px'
        })
    }
    routerGo = record => {
        if (record.flag) {
            window.localStorage.setItem('tagDetail', JSON.stringify(record))
            this.props.history.push(`/ztmanage/tagbindproduction/${record.labelId}`)
        } else message.error('已禁用标签无法绑定商品')
    }
    editTag = params => {
        const {
            addZtTag,
            editZtTag
        } = this.props.ztItemStore;
        if (params.labelId === undefined) addZtTag(params)
        else editZtTag(params)
        clearInterval(interval)
        interval = setInterval(() => {
            const {
                labelTypeId
            } = this.state;
            if (labelTypeId.toString().length > 0) this.chosenList(this.props.ztItemStore.filterLabelList.filter(p => p.labelTypeId === labelTypeId)[0])
        }, 1000)
    }
    chosenList = tagType => {
        if (tagType) this.setState({
            labelTypeName: tagType.labelTypeName,
            labelTypeId: tagType.labelTypeId,
            showTagsList: tagType.ztLabelList
        })
    }
    filterListBykey = () => {
        const {
            filterLabelList, //已经筛选过的标签组
            filterTagList //进行筛选标签操作的方法
        } = this.props.ztItemStore;
        filterTagList()
        this.setState({
            showTagsList: [],
            labelTypeName: '',
            labelTypeId: ''
        })
    }
    modalCancle = () => {
        this.setState({
            modalVisible: false
        })
    }
    render() {
        const {
            loading,
            // categoryList,
            addZtTag,
            // handleCategoryData,
            deleteZtTag,
            filterLabelList //已经筛选过的标签组
        } = this.props.ztItemStore;
        const {
            labelTypeName,
            labelTypeId,
            containerHeight,
            showTagsList,
            iptValue,
            modalVisible,
            defaultValue,
            clearForm
        } = this.state;
        const tableHeader = [{
                //     title: '标签ID',
                //     dataIndex: 'labelId',
                // },{
                title: '标签名(中)',
                dataIndex: 'labelName',
            }, {
                title: '标签名(英)',
                dataIndex: 'labelNameEn',
            }, {
                title: '操作',
                key: 'action',
                render:(text,record,index) => <span>
                    {/* <Link to={`/ztmanage/tagbindproduction/${record.labelId}`}><Tag color = 'purple'>绑定商品</Tag></Link> */}
                    <Tag color = 'purple' onClick = { e => this.routerGo(record) } >绑定商品</Tag>
                    <Tag color = 'blue' onClick={ e => this.setState({modalVisible:true,defaultValue:record,clearForm:true}) } style = {{margin:'0 15px'}} >编辑</Tag>
                    {/* onClick = { e => this.editTag(record.labelId) } */}
                    <Tooltip title={record.flag?'点击禁用此标签':'点击启用此标签'}>
                        <Tag color = {record.flag?'green':'red'} onClick = { e => this.editTag({labelId:record.labelId,flag:record.flag?0:1}) } >{record.flag?'启用':'禁用'}</Tag>
                    </Tooltip>
                </span>
            }]
        return  (
            <Spin spinning={loading}>
                <section style = {{ height: containerHeight }}>
                    <Row style = {{ marginBottom:'15px' }} >
                        <Col span={4} style = {{margin:'0 15px'}} >
                            <Input placeholder="请输入标签名或类别名筛选" defaultValue = { iptValue } onKeyDown = { e => e.keyCode===13&&this.filterListBykey() } onChange = { e => this.props.ztItemStore.filterKeyValue=e.target.value }/>
                        </Col>
                        <Col span={4}>
                            <Button type="primary" onClick={ this.filterListBykey } size="default">搜索</Button>
                        </Col>
                        {
                            labelTypeName.length>0?
                                <Col span={15} style = {{textAlign:'right'}}>
                                    <Button type="primary" onClick={ e => this.setState({modalVisible:true,defaultValue:{},clearForm:false}) } size="default">新增标签</Button>
                                </Col>
                            :null
                        }
                    </Row>
                    <section className = "tagFlexContain" >
                        <Card.Grid className = 'tagTypeContain'>
                            {
                                toJS(filterLabelList).map((tagType,i) => 
                                    <section key = {i} onClick = { e => this.chosenList(tagType) } className = 'tagType' >{tagType.labelTypeId+tagType.labelTypeName}</section>)
                            }
                        </Card.Grid>
                        {
                            labelTypeName.length>0?
                                <Card.Grid style = {{flex:1,overflow:'auto'}} >
                                    <h4 style = {{ lineHeight:'30px',height:'30px',fontSize:'14px' }} >{labelTypeName}</h4>
                                    <Table columns={tableHeader} dataSource={ showTagsList.slice()} size="small" pagination = {false} rowKey = 'labelId' />
                                </Card.Grid>
                            :
                                <Card.Grid className='noData' style = {{flex:1}} >
                                    <span>
                                        <i className='anticon anticon-frown-o'></i>
                                        点击左侧选择分类
                                    </span>
                                </Card.Grid>
                        }
                    </section>
                    <TagModalDom loading = { loading }
                                labelTypeName = { labelTypeName }
                                labelTypeId = { labelTypeId }
                                modalCancle = { this.modalCancle }
                                addNewTag = { value => this.editTag(value) }
                                defaultValue = { defaultValue }
                                // categoryList = { handleCategoryData(categoryList, true) }
                                modalVisible = { modalVisible } />
                </section>
            </Spin>
        )
        
    }
}

export default TagsManage