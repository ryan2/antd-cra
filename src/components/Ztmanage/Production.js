import React, {
  Component
} from 'react'
import {
  Link
} from 'react-router-dom'
import {
  observer,
  inject
} from 'mobx-react'
import {
  Table,
  Tooltip,
  // Menu,
  // Tag
} from 'antd'
import ProductionForm from './ProductionForm'
import '../ItemSchedule/itemSchedule.css'

// Global Component
@inject('ztProductionStore', 'store') @observer
class ProductionComponent extends Component {
  constructor(props) {
    super(props)
    const {
      cacheData
    } = props.ztProductionStore;
    this.state = {
      pagination: {
        defaultCurrent: props.ztProductionStore.page || 1,
        defaultPageSize: props.ztProductionStore.pageSize || 10,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '50', '100'],
        showTotal: () => `共${this.props.ztProductionStore.total}条数据`,
      },
      searchData: props.ztProductionStore.cacheData,
      widowWidth: document.body.offsetWidth,
      widowHeight: document.body.offsetHeight,
      categoryId: cacheData.categoryId || '',
      // menuOpenKeys: props.ztProductionStore.menuOpenKeys.slice(),
      // menuSelectedKeys: cacheData.categoryId ? [cacheData.categoryId] : [],
    }
  }
  componentDidMount() {
    const {
      // getCategoryListMenu,
      cacheData,
      // categoryListMenu,
      productList,
    } = this.props.ztProductionStore;
    let _cacheData = { ...cacheData
    }
    delete _cacheData.categoryId
    window.addEventListener('resize', this.onWindowResize)
    // if (categoryListMenu.length === 0) {
    //   getCategoryListMenu(_cacheData)
    // }
    if (productList.length === 0) {
      //getProductList(cacheData)
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onWindowResize)
  }

  onWindowResize = () => {
    this.setState({
      widowWidth: document.body.offsetWidth,
      widowHeight: document.body.offsetHeight
    })
  }

  handlePageChange = (pagination) => {
    const {
      searchData,
      categoryId
    } = this.state;
    this.state.pagination.current = pagination.current;
    this.props.ztProductionStore.getProductList({
      ...searchData,
      categoryId: categoryId,
      page: pagination.current,
      pageSize: pagination.pageSize,
    })
    this.setState({
      pagination: this.state.pagination
    })
  }

  handleSearchSubmit = (params) => {
    this.props.ztProductionStore.getProductList({
      categoryId: this.state.categoryId,
      ...params
    })
    // this.props.ztProductionStore.getCategoryListMenu(params)
    this.state.pagination.current = 1;
    this.setState({
      searchData: params,
      pagination: this.state.pagination
    })
  }

  // handleCategorySelect = (e) => {
  //   //const { searchData } = this.state;
  //   this.state.pagination.current = 1;
  //   let searchData = this.formRef.props.form.getFieldsValue();
  //   if (searchData.status === 'all') {
  //     delete searchData.status
  //   }
  //   this.setState({
  //     categoryId: e.key,
  //     menuSelectedKeys: [e.key],
  //     pagination: this.state.pagination
  //   })
  //   this.props.ztProductionStore.getProductList({ ...searchData,
  //     categoryId: e.key
  //   })
  //   // this.props.ztProductionStore.getCategoryListMenu(searchData)
  // }

  // handleMenuOpen = (e) => {
  //   this.setState({
  //     menuOpenKeys: e
  //   })
  //   this.props.ztProductionStore.saveCacheData({
  //     menuOpenKeys: e
  //   })
  // }

  handleFormReset = () => {
    // this.props.ztProductionStore.getProductList({})
    // this.props.ztProductionStore.getCategoryListMenu({})
    this.state.pagination.current = 1;
    this.setState({
      searchData: {},
      categoryId: "",
      // menuOpenKeys: [],
      // menuSelectedKeys: [],
      pagination: this.state.pagination
    })
    this.props.ztProductionStore.saveCacheData({
      categoryId: '',
      // menuOpenKeys: []
    })
  }

  //分类长度超过限制，显示提示文字
  // handleLabelTooltip = (label, limit, total) => {
  //     if (label.replace(/[^\u0000-\u00ff]/g, "aa").length / 2 > limit + 1) {
  //       let bytesCount = 0,
  //         subNum = 0;
  //       for (let i = 0; i < label.length; i++) {
  //         if (/^[\u0000-\u00ff]$/.test(label.charAt(i))) { //匹配双字节
  //           bytesCount += 1;
  //         } else {
  //           bytesCount += 2;
  //         }
  //         if (bytesCount > limit * 2) {
  //           break;
  //         }
  //         subNum += 1;
  //       }
  //       return (
  //         <Tooltip placement="right" title={label}>
  //           {
  //             `${label.substring(0,subNum)}...${total !== undefined ? ' (' + total + ')' : ''}`
  //           }
  //         </Tooltip>
  //       )
  //   } else {
  //     return `${label}${total !== undefined ? ' (' + total + ')' : ''}`;
  //   }
  // }

  render() {
    const textStyle = {
      display: 'inline-block',
      width: '150px',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap'
    }
    const canEdit = this.props.store.hasUserAuth('edit');
    const columns = [{
          title: '商品编码',
          dataIndex: 'itemCode',
          key: 'itemCode',
          width: 80,
        }, {
          title: '商品名称',
          dataIndex: 'goodsName',
          key: 'goodsName',
          width: 220,
          render: (text, record) => (
            text.length>18?
              <Tooltip title={text}>
                <span style = { textStyle } >{text}</span>
              </Tooltip>
            :<span>{text}</span>
          )
        }, {
          title: '英文名称',
          dataIndex: 'goodsNameEn',
          key: 'goodsNameEn',
          width: 220,
          render: (text, record) => (
            text.length>26?
              <Tooltip title={text}>
                <span style = { textStyle } >{text}</span>
              </Tooltip>
            :<span>{text}</span>
          )
        }, {
          title: '商品条码',
          dataIndex: 'barcode',
          key: 'barcode',
          width: 120,
        }, {
          title: '销售规格',
          dataIndex: 'specUnit',
          key: 'specUnit',
          width: 80,
          render: (text, record) => <span>{`${record.spec}/${record.unit}`}</span>
        }, {
          title: '重量(g)',
          dataIndex: 'weight',
          key: 'weight',
          width: 80
        },{
          title: '操作',
          key: 'action',
          width: 100,
          render: (text, record) => (
            <span>
              <Link to={`/ztmanage/modify/${record.itemId}`}>修改信息</Link>
            </span>
          ),
        }]
    const {
      loading,
      productList,
      total,
      // handleCategoryData,
      // categoryListMenu,
      cacheData
    } = this.props.ztProductionStore
    const {
      categoryId,
      // menuSelectedKeys,
      // menuOpenKeys,
      widowHeight
    } = this.state
    const pagination = Object.assign({
      total: total
    }, this.state.pagination)
    return (
      <div >
        {
          this.props.store.collapse ?
            <h2 style={{marginBottom: 16, flex: '0 0 auto'}}>
              <Link to="/item/schedule">商品主档管理</Link>
            </h2> : null
        }
        <ProductionForm wrappedComponentRef={(inst) => this.formRef  = inst}
                        history={this.props.history}
                        categoryId={categoryId}
                        defaultValues={cacheData}
                        onSearch={this.handleSearchSubmit}
                        onReset={this.handleFormReset}
                        canEdit={canEdit}/>
        {/* <div style={{display: 'flex', position: 'relative'}}> */}
          {/* <div style={{flex: '0 0 auto', width: 190, maxHeight: widowHeight - 175, overflowY: 'scroll'}}>
            <Menu mode="inline" onSelect={this.handleCategorySelect} onOpenChange={this.handleMenuOpen}
                  openKeys={menuOpenKeys} selectedKeys={menuSelectedKeys}>
              {handleCategoryData(categoryListMenu).map(item=>
                <Menu.SubMenu key={item.value}
                              title={`${this.handleLabelTooltip(item.label, 7)}`}>
                    {item.children.map(_item =>
                      <Menu.Item key={_item.value}>
                        {this.handleLabelTooltip(_item.label, 5, _item.total)}
                      </Menu.Item>)}
                </Menu.SubMenu>
              )}
            </Menu>
          </div> */}
          {/* <div ref='tableRapper' className="tableRapper"> */}
            <Table style={{width: '100%', minHeight: 450}} scroll={{y: widowHeight - 305}}
                   loading={loading}
                   columns={columns}
                   dataSource={productList.slice()}
                   pagination={pagination}
                   rowKey = 'itemId'
                   className = 'onlyYscroll'
                   onChange={(pagination) => this.handlePageChange(pagination)}/>
          {/* </div> */}
        {/* </div> */}
      </div>
    )
  }
}
export default ProductionComponent
