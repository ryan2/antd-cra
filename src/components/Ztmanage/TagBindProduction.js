import React, {
    Component
} from 'react'
import {
    Button,
    Card,
    Row,
    Col,
    Modal,
    Spin,
    message,
    Pagination,
    Checkbox,
    Input
} from 'antd';
import {
    toJS
} from 'mobx'
import {
    observer,
    inject
} from 'mobx-react'
import '../ItemModify/itemModify.css'
import FileOperation from '../FileOperation/FileOperation'

@inject('ztItemStore', 'classifyStore') @observer
class TagsManage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            containerHeight: 0,
            modalVisible: false,
            tagDetail: {},
            exList: [], //当前分页下已选已绑定商品
            exindeterminate: false,
            exCheckAll: false,
            exCurrent:1,
            noList: [], //当前分页下已选未绑定商品
            noindeterminate: false,
            noCheckAll: false,
            noCurrent:1
        }
    }
    componentDidMount() {
        //获取缓存标签详情,若与路由的labelID不一致,强制跳回标签管理页
        const tag = JSON.parse(window.localStorage.getItem('tagDetail'))
        if (tag !== undefined) {
            if (tag.labelId == this.props.match.params.labelId) {
                this.setState({
                    tagDetail: tag
                })
                this.getContainerHight()
                window.onresize = this.getContainerHight;
                const {
                    getExProductions,
                    getNoProductions,
                    getLabelId
                } = this.props.ztItemStore;
                getLabelId(tag.labelId)
                getNoProductions(1);
                getExProductions(1);
            } else {
                message.error('请从此页面进入标签绑定商品页面')
                this.props.history.push('/ztmanage/tagsManage')
            }
        }
    }
    componentWillUnmount(props) {
        window.onresize = null
    }
    getContainerHight = () => {
        this.setState({
            containerHeight: document.body.offsetHeight - 82 + 'px'
        })
    }

    exCheckAllChange = e => {
        const newList = []
        if (e.target.checked) {
            this.props.ztItemStore.exProductions.forEach(p => newList.push(p.itemId))
        }
        this.setState({
            exList: e.target.checked ? newList : [],
            exindeterminate: false,
            exCheckAll: e.target.checked,
        });
    }
    exChange = (checked, itemId) => {
        const {
            exProductions
        } = this.props.ztItemStore;
        const {
            exList
        } = this.state;
        const newList = []
        if (checked) newList.push(...exList, itemId);
        else newList.push(...exList.filter(p => p !== itemId));
        this.setState({
            exList: newList,
            exindeterminate: !!newList.length && (newList.length < exProductions.length),
            exCheckAll: newList.length === exProductions.length,
        });
    }
    exPageChange = page => {
        this.props.ztItemStore.getExProductions(page, () => {
            this.setState({
                exList: [],
                exindeterminate: false,
                exCheckAll: false,
                exCurrent:page
            })
        })
    }
    unbindProductions = () => {
        this.props.ztItemStore.unbindProductions(this.state.exList, () => {
            this.exPageChange(1)
            this.noPageChange(1)
        })
    }
    noCheckAllChange = e => {
        const newList = []
        if (e.target.checked) {
            this.props.ztItemStore.noProductions.forEach(p => newList.push(p.itemId))
        }
        this.setState({
            noList: e.target.checked ? newList : [],
            noindeterminate: false,
            noCheckAll: e.target.checked,
        });
    }
    noChange = (checked, itemId) => {
        const {
            noProductions
        } = this.props.ztItemStore;
        const {
            noList
        } = this.state;
        const newList = []
        if (checked) newList.push(...noList, itemId);
        else newList.push(...noList.filter(p => p !== itemId));
        this.setState({
            noList: newList,
            noindeterminate: !!newList.length && (newList.length < noProductions.length),
            noCheckAll: newList.length === noProductions.length,
        });
    }
    noPageChange = page => {
        this.props.ztItemStore.getNoProductions(page, () => {
            this.setState({
                noList: [],
                noindeterminate: false,
                noCheckAll: false,
                noCurrent:page
            })
        })
    }
    bindProductions = () => {
        this.props.ztItemStore.bindProductions(this.state.noList, () => {
            this.exPageChange(1)
            this.noPageChange(1)
        })
    }
    render() {
        const {
            loading,
            noProductions,
            exProductions,
            exTotal,
            noTotal,
            labelId,
            getKeyWords
        } = this.props.ztItemStore;
        const {
            containerHeight,
            tagDetail,
            exList,
            exindeterminate,
            exCheckAll,
            exCurrent,
            noList,
            noindeterminate,
            noCheckAll,
            noCurrent
        } = this.state
        // const modalStyle = { top: '10%', padding:0 }
        return  (
            <Spin spinning={loading}>
                <section style = {{ height: containerHeight, display:'flex', justifyContent:'space-between' }}>
                    {/* <Row style = {{ marginBottom:'15px' }} >
                        <Col span={4} style = {{margin:'0 15px'}} >
                            <Input placeholder="请输入标签名或类别名筛选" defaultValue = { iptValue } onKeyDown = { e => e.keyCode===13&&this.filterListBykey() } onChange = { e => this.props.ztItemStore.filterKeyValue=e.target.value }/>
                        </Col>
                        <Col span={4}>
                            <Button type="primary" onClick={ this.filterListBykey } size="default">搜索</Button>
                        </Col>
                    </Row> */}
                    <section className = "verticalContain" >
                        <Card.Grid className = "verticalTop" >
                           {/* <section className = 'tagType'>标签ID:<span>{tagDetail.labelId}</span></section> */}
                           <section className = 'tagType'>标签名(中):<span>{tagDetail.labelName}</span></section>
                           <section className = 'tagType'>标签名(英):<span>{tagDetail.labelNameEn}</span></section>
                           <section className = 'tagType'>状态:<span>{tagDetail.flag?'已启用':'已禁用'}</span></section>
                        </Card.Grid>
                        <Card.Grid className = 'verticalBottom' >
                            <Row style = {{width:'100%',marginBottom:'15px',paddingLeft:'16px'}} >
                                <Col span ={8} >
                                    <Input placeholder="请输入商品编码或名称" size = 'small' onChange = { e => getKeyWords('exKeyWords',e.target.value) } />
                                </Col>
                                <Col span ={8}>
                                    <Button size="small" type = 'primary' onClick = { e => this.exPageChange(1) } style = {{marginLeft:'16px'}} >筛选</Button>
                                </Col>
                            </Row>
                            <Checkbox
                                indeterminate={exindeterminate}
                                onChange={this.exCheckAllChange}
                                checked={exCheckAll}
                                style = {{marginBottom:'10px',paddingLeft:'16px'}}
                            >全选</Checkbox>
                            <Button size="small" type = 'primary' onClick = { this.unbindProductions }>解绑</Button>
                            {
                                exProductions.length>0?
                                    toJS(exProductions).map((production,i)=>
                                        <section className = 'tagType' style = {{width:'100%'}} key = {i} >
                                            <Checkbox checked = { exList.indexOf(production.itemId)!==-1 } onChange={ e => this.exChange(e.target.checked,production.itemId)}>{production.itemCode} {production.goodsName}</Checkbox>
                                            <span>{production.categoryName}</span>
                                        </section>)
                                    // toJS(exProductions).map((production,i)=>
                                    //     <Tooltip title={production.categoryName} key = {i} >
                                    //         <Checkbox checked = { exList.indexOf(production.itemId)!==-1  } onChange={e => this.exChange(e.target.checked,production.itemId)}>{production.itemCode} {production.goodsName}</Checkbox>
                                    //     </Tooltip>)
                                :
                                <section className='noData'>
                                    <span>
                                        <i className='anticon anticon-frown-o'></i>
                                        还未绑定商品
                                    </span>
                                </section>
                            }
                            {/* <CheckboxGroup options={exProductions.slice()} value={exList.slice()} onChange={this.exChange} /> */}
                        </Card.Grid>
                        <div className = 'absolutPagenation' >
                            <Pagination size="small" total={exTotal} onChange = { this.exPageChange } current = {exCurrent} />
                        </div>
                    </section>
                    <section className = "verticalContain" style = {{width:'56%'}} >
                        <Card.Grid className = 'verticalBottom'>
                            <Row style = {{width:'100%',marginBottom:'15px',paddingLeft:'16px'}} >
                                <Col span ={8} >
                                    <Input placeholder="请输入商品编码或名称" size = 'small' onChange = { e => getKeyWords('noKeyWords',e.target.value) } />
                                </Col>
                                <Col span ={8}>
                                    <Button size="small" type = 'primary' onClick = { e => this.noPageChange(1) } style = {{marginLeft:'16px'}} >筛选</Button>
                                </Col>
                            </Row>
                            <Checkbox
                                indeterminate={noindeterminate}
                                onChange={this.noCheckAllChange}
                                checked={noCheckAll}
                                style = {{marginBottom:'10px',paddingLeft:'16px'}}
                            >全选</Checkbox>
                            <Button size="small" type = "primary" onClick = { this.bindProductions } >绑定</Button>
                            <FileOperation type="template" buttonProps={{type: 'primary',size:'small'}}
                                templateUrl="/file/exportProductions" style = {{float:'right',marginLeft:'20px'}} />
                            <FileOperation history={this.props.history} type="ImportProductionsByLabelId"
                                buttonText="批量绑定" buttonProps={{type: 'primary',size:'small'}}
                                style = {{float:'right'}} queryData = {{labelId:labelId}}
                                fileType={["xlsx", "xls"]}/>
                            {/* <Button type="primary" size="small" style= {{float:'right'}} >批量绑定</Button> */}
                            {
                                noProductions.length>0?
                                <section style = {{display:'flex',flexWrap:'wrap',justifyContent:'space-between'}}>
                                        {
                                            toJS(noProductions).map((production,i)=>
                                            <section className = 'tagType' style = {{width:'100%'}} key = {i} >
                                                    <Checkbox checked = { noList.indexOf(production.itemId)!==-1 } onChange={ e => this.noChange(e.target.checked,production.itemId)}>{production.itemCode} {production.goodsName}</Checkbox>
                                                    <span>{production.categoryName}</span>
                                            </section>)
                                        }
                                    </section>
                                :
                                    <section className='noData'>
                                        <span>
                                            <i className='anticon anticon-frown-o'></i>
                                            已无未绑定商品
                                        </span>
                                    </section>
                            }
                        </Card.Grid>
                        <div className = 'absolutPagenation' >
                            <Pagination size="small" total={noTotal} onChange = { this.noPageChange } current = {noCurrent} />
                        </div>
                    </section>
                </section>
            </Spin>
        )
        
    }
}

export default TagsManage