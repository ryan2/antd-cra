import React from 'react'
import { Button, Modal, Form, Input, Radio, Select } from 'antd';
import { inject, observer } from 'mobx-react'
import * as mobx from 'mobx'
const FormItem = Form.Item;
const Option = Select.Option;

@inject('ztManageStore')@observer
class ClassifyForm extends React.Component {
    state = {
    };
   
    componentDidMount() {
        this.props.ztManageStore.getRetailFormatList()
    }
    
    handleCreate = () => {
        const form = this.props.form;
        form.validateFields((err, values) => {
            if (!err) {
                this.props.onSubmit(values).then(res => {
                    if(res.status === 200) {
                        form.resetFields();
                    }
                });
            }
        });
    }

    handleValidateName = (rule, value, callback) => {
        const { validateName, status } = this.props;
        if(status === 'insert' || (status === 'update' && value !== this.props.nodeInfo.categoryName)){
            if ( validateName(value) ) {
                callback('类别名称不能重复');
            }
        }
        callback();
    }
    handleValidateEnName = (rule, value, callback) => {
        const { validateName, status } = this.props;
        if(status === 'insert' || (status === 'update' && value !== this.props.nodeInfo.categoryEnName)){
            if ( validateName(value) ) {
                callback('英文类别名称不能重复');
            }
        }
        callback();
    }

    handleCancel = () => {
        this.props.form.resetFields();
        this.props.onCancel()
    }

    render() {
        const retailFormatList = mobx.toJS(this.props.ztManageStore.retailFormatList)
        const retailFormatOption = retailFormatList.map( item => <Option key = {item.retailFormatId}>{item.retailFormatName}</Option>)
        const { getFieldDecorator } = this.props.form;
        const { visible, loading, status, nodeInfo } = this.props;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        return (
            <Modal
                visible={visible}
                maskClosable={false}
                title={status === 'insert' ? "新增分类" : "修改分类"}
                okText="确认"
                onCancel={this.handleCancel}
                onOk={this.handleCreate}
                footer={[
                    <Button key="back" size="large" onClick={this.handleCancel}>取消</Button>,
                    <Button key="submit" type="primary" size="large"
                            loading={loading} onClick={this.handleCreate}>确认</Button>,
                ]}
                >
                <Form>
                    <FormItem label="类别名称" {...formItemLayout}>
                        {getFieldDecorator('categoryName', {
                            rules: [
                                { required: true, message: '必须填写类别名称' },
                                { max: 20, message: '最大长度不超过20' },
                                { validator: this.handleValidateName}],
                            initialValue: status === 'update' ? nodeInfo.categoryName : ''
                        })(
                            <Input placeholder="请填写类别名称"/>
                        )}
                    </FormItem>
                    <FormItem label="英文类别名称" {...formItemLayout}>
                        {getFieldDecorator('categoryEnName', {
                            rules: [
                                { required: true, message: '必须填写类别名称' },
                                { max: 10, message: '最大长度不超过10' },
                                { validator: this.handleValidateEnName}],
                            initialValue: status === 'update' ? nodeInfo.categoryEnName : ''
                        })(
                            <Input placeholder="请填写英文类别名称"/>
                        )}
                    </FormItem>
                    {/* {status === 'insert' ?
                        <FormItem label="业态" {...formItemLayout}>
                            {getFieldDecorator('retailFormatId', {
                                rules: [{ required: true, message: '必须选择业态' }],
                                initialValue: nodeInfo.retailFormatId === undefined ? undefined : nodeInfo.retailFormatId+''
                            })(
                            <Select placeholder = '请选择业态' disabled = {Object.keys(nodeInfo).length!==0}>
                               {retailFormatOption}
                              </Select>
                            )}
                        </FormItem> : null} */}
                    <FormItem label="备注" {...formItemLayout}>
                        {getFieldDecorator('memo', {
                            rules: [
                                { required: false, message: '必须填写类别名称' },
                                { max: 20, message: '最大长度不超过20'}],
                            initialValue: status === 'update' ? nodeInfo.memo : ''
                        })(
                            <Input placeholder="请填写备注"/>
                        )}
                    </FormItem>
                    <FormItem label="状态" {...formItemLayout}
                        className="collection-create-form_last-form-item">
                        {getFieldDecorator('flag', {
                            rules: [{ required: true, message: '必须选择状态' }],
                            initialValue: '1',
                        })(
                            <Radio.Group>
                                <Radio value="1">启用</Radio>
                                <Radio value="0">禁用</Radio>
                            </Radio.Group>
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}

export default Form.create()(ClassifyForm)

