import React, {
  Component
} from 'react'
import {
  Row,
  Col,
  Form,
  Select,
  Input,
  Button
} from 'antd'
import {
  observer,
  inject
} from 'mobx-react'
import * as mobx from 'mobx'
import FileOperation from '../FileOperation/FileOperation'
const FormItem = Form.Item
const Option = Select.Option

@inject('storeItemStoreZt', 'shopStore', 'store') @observer
class StoreItemList extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount = async () => {
    const shopId = window.location.hash.split('#')[1].split('/')[3]
    const channelId = window.location.hash.split('#')[1].split('/')[4]
    if (shopId !== undefined && channelId !== undefined) {
      await this.props.storeItemStoreZt.getShopNameById(shopId)
      this.props.form.setFieldsValue({
        shopId: mobx.toJS(this.props.storeItemStoreZt.shopName),
        channelId: channelId
      })
    }
    this.props.shopStore.getCityList()
    this.props.store.getChannelList()
  }

  componentWillUnmount = () => {
    this.props.shopStore.clearShopList()
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.props.onReset(1, 10)
    const page = this.props.pagination.page
    const pageSize = this.props.pagination.pageSize
    this.props.form.validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        let channelId = values.channelId;
        const o2oSkuId = values.o2oSkuId
        const goodsName = values.goodsName
        const barcode = values.barcode
        const itemCode = values.itemCode
        const shelved = values.shelved
        const status = values.status
        const isSelf = values.isSelf
        const city = values.city === '全部' ? '' : values.city
        const shopId = typeof (values.shopId) === 'string' ? [this.props.storeItemStoreZt.shopId] : values.shopId
        const formList = {
          shopId: shopId,
          channelId: channelId,
          o2oSkuId: o2oSkuId,
          goodsName: goodsName,
          barcode: barcode,
          itemCode: itemCode,
          shelved: shelved,
          status: status,
          isSelf: isSelf,
          city: city,
        }
        this.props.changeFormList(formList)
        this.props.changeMsgData(page, pageSize, shopId, channelId, o2oSkuId, goodsName, barcode, itemCode, shelved, status, this.props.categoryId, isSelf, city)
        // const result = await this.props.initCategoryList(formList)
        // this.props.changeCategoryList(result)              
      }
    })
  }

  handleReset = async () => {
    this.props.form.resetFields()
    this.props.onReset(1, 10)
    // this.props.changeMsgData()
    this.props.clearCategoryId()
    this.props.clearUnable()
    // const result = await this.props.initCategoryList({})
    // this.props.changeCategoryList(result)  
  }

  queryData = () => {
    let result = this.props.form.getFieldsValue()
    const categoryId = this.props.categoryId
    let queryData
    if (typeof (result.shopId) === 'string') {
      let shopId = [this.props.storeItemStoreZt.shopId]
      queryData = {
        ...queryData,
        shopId,
        categoryId
      }
      return queryData
    } else {
      queryData = {
        ...result,
        categoryId
      }
      return queryData
    }
  }

  cityChange = (value) => {
    this.props.storeItemStoreZt.getShopListByCityList({
      city: value
    })
    if (value !== undefined) {
      this.props.form.setFieldsValue({
        shopId: []
      })
    }
  }

  exportList = (e) => {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        let shopId = values.shopId === undefined ? window.location.pathname.split('/')[3] : values.shopId
        let channelId = values.channelId === undefined ? window.location.pathname.split('/')[4] : values.channelId
        const o2oSkuId = values.o2oSkuId
        const goodsName = values.goodsName
        const barcode = values.barcode
        const itemCode = values.itemCode
        const shelved = values.shelved
        const status = values.status
        await this.props.storeItemStoreZt.exportO2oGoodsList(shopId, channelId, o2oSkuId, goodsName, barcode, itemCode, shelved, status)
      }
    })
  }

  render() {
      const channelList = mobx.toJS(this.props.store.channelList)
      const channelOptions = channelList.map( item => <Option key = {item.channelId}>{item.channelName}</Option>)
      const formItemLayout = {
              labelCol: {      
                 xs: { span: 24 },
          
                 sm: { span: 10 },
          
                md: { span: 8 },
          
                 lg: { span: 7 },
          
                 xl: { span: 5 },
          
               },
               wrapperCol: {         
                 xs: { span: 24 },
          
                 sm: { span: 14 },
          
                 md: { span: 16 },
          
                 lg: { span: 16 },
          
                 xl: { span: 14 },
               },
      }
      const { getFieldDecorator } = this.props.form
      const selectWidth = {
        width: '100%',
      }
      return (
        <div className="searchForm">
          <Form
            onSubmit={this.handleSubmit} className="ant-advanced-custom-form"
          >
            <Row gutter={30}>
            <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'商品编码'}>
                  {getFieldDecorator('itemCode')(
                    <Input placeholder = '请输入商品编码' style={selectWidth} size = 'default' />
                  )}
                </FormItem>
              </Col>
            <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'商品名称'}>
                  {getFieldDecorator('goodsName')(
                    <Input placeholder = '请输入商品名称' style={selectWidth} size = 'default'/>
                  )}
                </FormItem>
              </Col>

            <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'城市'}>
                  {getFieldDecorator('city', {
                    rules: [{ required: false, message: 'Please input city!' }],
                  })(
                    <Select onChange = {this.cityChange} style={selectWidth} size = 'default' mode="multiple" placeholder="请选择城市">
                      {mobx.toJS(this.props.shopStore.cityList).map(data =><Option key={data.key}>{data.value}</Option>)}
                    </Select>
                    )}
                </FormItem>
              </Col>
              <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'家乐福门店'}>
                  {getFieldDecorator('shopId', {
                    rules: [{ required: false, message: 'Please input store!' }],
                  })(
                    <Select  style={selectWidth} size = 'default'  placeholder="请选择门店" mode="multiple">
                      {mobx.toJS(this.props.storeItemStoreZt.shopList).map(data =><Option key={data.shopId}>{data.shopNameAlias}</Option>)}
                    </Select>
                    )}
                </FormItem>
              </Col>
              <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'上架状态'}>
                  {getFieldDecorator('shelved')(
                    <Select placeholder = '请选择上架状态' style={selectWidth} size = 'default'>
                      <Option value="">全部</Option> 
                      <Option value="0">下架</Option>
                      <Option value="1">上架</Option>                  
                    </Select>
                  )}
                </FormItem>
              </Col>
              {/* <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'O2O编码'}>
                  {getFieldDecorator('o2oSkuId')(
                    <Input placeholder = '请输入O2O编码' style={selectWidth} size = 'default'/>
                  )}
                </FormItem>
              </Col> */}
            </Row>
            {/* <Row gutter={16} > */}
            {/* <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'商品条码'}>
                  {getFieldDecorator('barcode')(
                    <Input placeholder = '请输入商品条码' style={selectWidth} size = 'default'/>
                  )}
                </FormItem>
              </Col>
              <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'商品编码'}>
                  {getFieldDecorator('itemCode')(
                    <Input placeholder = '请输入商品编码' style={selectWidth} size = 'default' />
                  )}
                </FormItem>
              </Col> */}
              {/* <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'商品渠道'}>
                  {getFieldDecorator('channelId',{
                  })(
                    <Select placeholder = '请选择商品渠道' style={selectWidth} size = 'default'>
                      <Option value="">全部</Option> 
                      {channelOptions}
                    </Select>
                  )}
                </FormItem>
              </Col>
              
            </Row> */}
            {/* <Row gutter={16}>
            <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'启用状态'}>
                  {getFieldDecorator('status')(
                    <Select placeholder = '请选择启用状态' style={selectWidth} size = 'default'>
                      <Option value="">全部</Option> 
                      <Option value="-1">禁用</Option>                    
                      <Option value="0">未启用</Option>
                      <Option value="1">启用</Option>                   
                    </Select>
                  )}
                </FormItem>
              </Col>
            <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'个性设置'}>
                  {getFieldDecorator('isSelf')(
                    <Select placeholder = '请选择个性设置' style={selectWidth} size = 'default'>
                      <Option value="">全部</Option>
                      <Option value="1">启用</Option>                   
                      <Option value="0">未启用</Option>
                    </Select>
                  )}
                </FormItem>
              </Col>
              </Row> */}
              <Row gutter ={16} style = {{marginBottom: 16}}>
              <Col className="gutter-row" span={14}>
                <Button type="primary" onClick={this.props.setShelved} style ={{marginRight:'8px'}} size = 'default'>设置上架状态</Button>
                <FileOperation history={this.props.history} type="ImportZtSkuShelved"
                               buttonText="导入上下架" buttonProps={{type: 'primary', size: 'default'}}
                               fileType={["xlsx", "xls"]}/>
                <FileOperation type="template" style={{marginLeft: 8}}
                               buttonText="下载模板" buttonProps={{type: 'primary'}}
                               templateUrl="/file/exportZtSkuShelved"/>
                <FileOperation history={this.props.history} type="ExportZtSku" style={{marginLeft: 8}}
                               buttonText="导出商品" buttonProps={{type: 'primary', size: 'default'}} queryData={this.queryData()}/>
              </Col>       
              <Col className="gutter-row" style={{ textAlign: 'right'}} span={10}>
                <Button type="primary"  htmlType="submit" size = 'default'>查询</Button>
                <Button size = 'default' style={{ marginLeft: 8 }} onClick={this.handleReset}>清空</Button>
              </Col>
            </Row>
          </Form>
        </div>
      )
    }
  }
  const StoreItemListForm = Form.create()(StoreItemList)
  export default StoreItemListForm