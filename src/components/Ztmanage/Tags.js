import React, { Component } from 'react'
import { Collapse, Tag, Button, Popconfirm, Modal, Tooltip, Row, Col, Input,Card } from 'antd';
import {toJS} from 'mobx'
const Panel = Collapse.Panel;
const CheckableTag = Tag.CheckableTag;

// @inject('ztItemStore', 'classifyStore') @observer
class TagsPagiationn extends Component{
    constructor(props) {
        super(props)
        this.state = {
            isEditing:false,
            addTag:false,
            checkedList:[],
            containerHeight:0,
            showTagsList:[]
        }
    }
    componentDidMount() {
        this.getContainerHight()
        window.onresize = this.getContainerHight
    }
    getContainerHight = ()  =>{
        this.setState({
            containerHeight: document.body.offsetHeight - 134 + 'px'
        })
    }
    connfirmEdit = () =>{
        this.props.editProductionTags({
            labelList:this.state.checkedList,
            itemId:this.props.itemId,
        },()=> {
            this.cancelEdit()
        })
    }
    cancelEdit = () =>{
        const tagType = this.props.tagsList[0]
        this.setState({
            isEditing: false,
            showTagsList:tagType?tagType.ztLabelList:[]
        })
    }
    editTaglist = () => {
        const tagType = this.props.filterLabelList[0]
        this.setState({
            isEditing: true,
            checkedList:[...this.props.temporary],
            showTagsList:tagType?tagType.ztLabelList:[]
        })
    }
    /**弹窗内方法 */
    // handleOk = () =>{
    //     this.props.editProductionTags({
    //         labelList:this.state.checkedList,
    //         itemId:this.props.itemId,
    //     },()=> {
    //         this.setState({
    //             addTag: false
    //         })
    //     })
    // }
    // handleCancel = () => {
    //     this.setState({
    //         addTag: false
    //     })
    // }
    // openDialog = () => {
    //     this.setState({
    //         addTag: true,
    //         checkedList:[...this.props.temporary]
    //     })
    // }
    handleChange = (tag, checked) => {
        const { checkedList } = this.state;
        const nextSelectedTags = checked
          ? [...checkedList, tag]
          : checkedList.filter(t => t !== tag);
        this.setState({ checkedList: nextSelectedTags });
    }
    /**弹窗内方法 */
    render(){
        const {
            tagsList,
            filterTagList,
            filterLabelList,
            loading
        } = this.props;
        const {
            isEditing,
            addTag,
            checkedList,
            containerHeight,
            showTagsList
        } = this.state;
        const formItemLayoutInline2 = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 14 },
            },
        };
        return  (
            <section style = {{ height: containerHeight }}>
                {isEditing ?
                    <section className = "tagFlexContain" >
                        <Card.Grid className = 'tagTypeContain'>
                                {
                                    toJS(filterLabelList).map((tagType,i) => 
                                        <section key = {i} onClick = { e => this.setState({showTagsList:tagType.ztLabelList}) } className = 'tagType' >{tagType.labelTypeId+tagType.labelTypeName}</section>)
                                }
                            </Card.Grid>
                            <Card.Grid className = 'tagsContain' >
                                {
                                    toJS(showTagsList).map((tag,j) => 
                                        <Tooltip title={tag.labelNameEn} key = {j} >
                                            <CheckableTag checked = { checkedList.indexOf(tag.labelId) !==-1 } onChange = { checked => this.handleChange(tag.labelId, checked) } >{tag.labelName}</CheckableTag>
                                        </Tooltip>)
                                }
                        </Card.Grid>
                    </section>
                :
                    (tagsList.length>0?
                        <section className = "tagFlexContain" >
                            <Card.Grid className = 'tagTypeContain'>
                                {
                                    toJS(tagsList).map((tagType,i) => 
                                        <section key = {i} onClick = { e => this.setState({showTagsList:tagType.ztLabelList}) } className = 'tagType' >{tagType.labelTypeId+tagType.labelTypeName}</section>)
                                }
                            </Card.Grid>
                            <Card.Grid className = 'tagsContain' >
                                {
                                    toJS(showTagsList).map((tag,j) => 
                                        <Tooltip title={tag.labelNameEn} key = {j} >
                                            <Tag color = 'blue'>{tag.labelName}</Tag>
                                        </Tooltip>)
                                }
                            </Card.Grid>
                        </section>
                    :
                        <section className='noData'>
                            <span>
                                <i className='anticon anticon-frown-o'></i>
                                暂无数据
                            </span>
                        </section>)
                }
                {isEditing ?
                    <div style={{ textAlign:'right', marginTop:'16px', paddingRight:'15px' }}>
                        <Popconfirm title="确认取消修改？" okText="确认" cancelText="取消" onConfirm = { this.cancelEdit } >
                            <Button type="primary" size="default" style={{marginRight:'15px'}}>取消</Button>
                        </Popconfirm>
                        <Popconfirm title="确认保存修改？" okText="保存" cancelText="取消"  onConfirm = { this.connfirmEdit }>
                            <Button type="primary" size="default" >保存</Button>
                        </Popconfirm>
                    </div>
                : 
                    <div style={{ textAlign:'right', marginTop:'16px', paddingRight:'15px' }}>
                        <Button type="primary" onClick={ this.editTaglist } size="default">编辑标签</Button>
                        {/* <Button type="primary" onClick={ this.openDialog } size="default">添加标签</Button> */}
                    </div>
                }
                {
                //     isEditing ?
                //         (<section>
                //             <Row style = {{ marginBottom:'15px' }} >
                //                 <Col span={12}>
                //                     <Input placeholder="请输入标签名或类别名筛选" onChange = { e => filterTagList(e.target.value) } />
                //                 </Col>
                //             </Row>
                //             <Collapse bordered={false}>
                //                 {
                //                     toJS(filterLabelList).map((tagType,i) =>
                //                         <Panel header={tagType.labelTypeId+tagType.labelTypeName} key={'modal'+i}>
                //                             {
                //                                 toJS(tagType.ztLabelList).map((tag,j) => 
                //                                 <Tooltip title={tag.labelNameEn} key = {`modal${i}-${j}`} >
                //                                     <CheckableTag style = {{ backgroundColor:checkedList.indexOf(tag.labelId) !==-1?tagType.color:'' }} checked = { checkedList.indexOf(tag.labelId) !==-1 } onChange = { checked => this.handleChange(tag.labelId, checked) } >{tag.labelName}</CheckableTag>
                //                                 </Tooltip>)
                //                             }
                //                         </Panel>)
                //                 }
                //             </Collapse>
                //         </section>)
                //     :
                //     (tagsList.length>0?
                //         <Collapse bordered={false}>
                //             {
                //                 toJS(tagsList).map((tagType,i) =>
                //                     <Panel header={tagType.labelTypeId+tagType.labelTypeName} key={'main'+i}>
                //                         {
                //                             tagType.ztLabelList.length>0?toJS(tagType.ztLabelList).map((tag,j) => 
                //                                 <Tooltip title={tag.labelNameEn} key = {`main${i}-${j}`} >
                //                                     <Tag color = { tagType.color }>{tag.labelName}</Tag>
                //                                 </Tooltip>)
                //                             :null
                //                         }
                //                     </Panel>)
                //             }
                //         </Collapse>
                //     :
                //         <section className='noData'>
                //             <span>
                //                 <i className='anticon anticon-frown-o'></i>
                //                 暂无数据
                //             </span>
                //         </section>)
                // }
                // {
                //     isEditing ?
                //         <div style={{ textAlign:'right', marginTop:'30px' }}>
                //             <Popconfirm title="确认取消修改？" okText="确认" cancelText="取消" onConfirm = { this.cancelEdit } >
                //                 <Button type="primary" size="default" style={{marginRight:'15px'}}>取消</Button>
                //             </Popconfirm>
                //             <Popconfirm title="确认保存修改？" okText="保存" cancelText="取消"  onConfirm = { this.connfirmEdit }>
                //                 <Button type="primary" size="default" >保存</Button>
                //             </Popconfirm>
                //         </div>
                //     : 
                //         <div style={{ textAlign:'right', marginTop:'30px' }}>
                //             <Button type="primary" onClick={ this.editTaglist } size="default">编辑标签</Button>
                //             {/* <Button type="primary" onClick={ this.openDialog } size="default">添加标签</Button> */}
                //         </div>
                }
                {/* <Modal title="Basic Modal"
                       bodyStyle = {{ height:'calc( 100% - 101px )',overflow:'auto' }}
                       style = {{ top: '10%', padding:0 ,overflow:'hidden' }}
                       width  =  '80%'
                       height = '80%'
                       visible = { addTag }
                       onOk = { this.handleOk }
                       confirmLoading = { loading }
                       onCancel = { this.handleCancel }>
                    <Row style = {{ marginBottom:'15px' }} >
                        <Col span={12}>
                            <Input placeholder="请输入标签名或类别名筛选" onChange = { e => filterTagList(e.target.value) } />
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            {
                                filterLabelList.length>0?
                                    <Collapse bordered={false}>
                                        {
                                            toJS(filterLabelList).map((tagType,i) =>
                                                <Panel header={tagType.labelTypeId+tagType.labelTypeName} key={'modal'+i}>
                                                    {
                                                        toJS(tagType.ztLabelList).map((tag,j) => 
                                                        <Tooltip title={tag.labelNameEn} key = {`modal${i}-${j}`} >
                                                            <CheckableTag style = {{ backgroundColor:checkedList.indexOf(tag.labelId) !==-1?tagType.color:'' }} checked = { checkedList.indexOf(tag.labelId) !==-1 } onChange = { checked => this.handleChange(tag.labelId, checked) } >{tag.labelName}</CheckableTag>
                                                        </Tooltip>)
                                                    }
                                                </Panel>)
                                        }
                                    </Collapse>
                                :
                                    <section className='noData'>
                                        <span>
                                            <i className='anticon anticon-frown-o'></i>
                                            暂无数据
                                        </span>
                                    </section>
                            }
                        </Col>
                    </Row>
                </Modal> */}
            </section>
        )
        
    }
}
export default TagsPagiationn