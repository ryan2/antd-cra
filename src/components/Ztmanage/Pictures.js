import React, {
    Component
} from 'react'
import {
    Card,
    Spin,
    Carousel
} from 'antd';
import {
    toJS
} from 'mobx'
import '../ItemModify/itemModify.css'

class Pictures extends Component {
    constructor(props) {
        super(props)
        this.state = {
            containerHeight: 0
        }
    }
    componentDidMount() {
        const {
            getPictures,
            itemId
        } = this.props;
        this.getContainerHight()
        window.onresize = this.getContainerHight;
        getPictures(itemId)

    }
    componentWillUnmount(props) {
        window.onresize = null
    }
    getContainerHight = () => {
        this.setState({
            containerHeight: document.body.offsetHeight - 134
        })
    }

    render() {
        const {
            loading,
            getPictures,
            pictures,
            itemId
        } = this.props;
        const {
            containerHeight
        } = this.state
        return  (
            <Spin spinning={loading}>
                {/* <section style = {{ height:containerHeight}} className = 'cardContain' >
                    {
                        pictures.length>0?
                            toJS(pictures).map((img,i)=>
                                <Card.Grid key = {i} className = 'cards' >
                                    <img src = {img.imageKey} style = {{ width:'100%',height:'auto',display:'block',borderRadius:'10px' }} />
                                </Card.Grid>)
                        :
                            <section className='noData' style = {{flex:1}}>
                                <span>
                                    <i className='anticon anticon-frown-o'></i>
                                    暂无图片
                                </span>
                            </section>
                    }
                </section> */}
                {
                    pictures.length>0?
                        <Carousel effect="fade" style = {{ height:containerHeight*2/3 +'px',width:'60%'}} autoplay>
                            {
                                toJS(pictures).map((img,i)=>
                                    <section key = {i} >
                                        <img src = {img.imageKey} style = {{ width:'auto',height:'400px',display:'block',borderRadius:'10px',margin:'0 auto' }} />
                                    </section>)
                            }
                        </Carousel>
                    :
                        <section className='noData' style = {{flex:1}}>
                            <span>
                                <i className='anticon anticon-frown-o'></i>
                                暂无图片
                            </span>
                        </section>
                }
            </Spin>
        )
        
    }
}

export default Pictures