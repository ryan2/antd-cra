import React, {
    Component
} from 'react'
import {
    Card,
    Spin,
    Carousel
} from 'antd';
import {
    toJS
} from 'mobx'
import '../ItemModify/itemModify.css'

class Describe extends Component {
    constructor(props) {
        super(props)
        this.state = {
            containerHeight: 0
        }
    }
    componentDidMount() {
        const {
            getDescribes,
            itemId
        } = this.props;
        this.getContainerHight()
        window.onresize = this.getContainerHight;
        getDescribes(itemId)

    }
    componentWillUnmount(props) {
        window.onresize = null
    }
    getContainerHight = () => {
        this.setState({
            containerHeight: document.body.offsetHeight - 134
        })
    }
    getHtmlDescribe = des =>{
        const Utils={}
        Utils.isNullorEmpty=function(str){
            if((str==null||str==""||str==undefined||str=="undefined")&&(str!=0||str!="0"))
              return true;
            else
              return false;
          }
        String.prototype.unescapeHtml=function(){
            var s = "";
            if(Utils.isNullorEmpty(this)) return "";
            s = this.replace(/&amp;/g, "&");
            s = s.replace(/&lt;/g, "<");
            s = s.replace(/&gt;/g, ">");
            s = s.replace(/&nbsp;/g, " ");
            s = s.replace(/&#39;/g, "\'");
            s = s.replace(/&quot;/g, "\"");
            s = s.replace(/<br>/g, "\n");
            return s;
        }
        return des.content.unescapeHtml()
    }
    render() {
        const {
            loading,
            describes
        } = this.props;
        const {
            containerHeight
        } = this.state
        return  (
            <Spin spinning={loading}>
                <section style = {{ height:containerHeight}} className = 'cardContain' >
                    {
                        describes.length>0?
                            toJS(describes).map((des,i)=>
                                <Card.Grid key = {i} className = 'cards discriibe' >
                                    <div dangerouslySetInnerHTML= {{__html:this.getHtmlDescribe(des)}}></div>
                                </Card.Grid>)
                        :
                            <section className='noData' style = {{flex:1}}>
                                <span>
                                    <i className='anticon anticon-frown-o'></i>
                                    暂无文描信息
                                </span>
                            </section>
                    }
                </section>
            </Spin>
        )
        
    }
}

export default Describe