import React, { Component } from 'react'
import {Table, Icon,Tooltip, Row, Col, Modal } from 'antd'
import * as mobx from 'mobx'
import { observer,inject } from 'mobx-react'
import ShopItemLog from '../LogQuery/ShopItemLog/index'
import InventoryLog from '../LogQuery/InventoryLog/index'
import ShopPrice from '../LogQuery/ShopPrice/index'
 
@inject('recordStore','store')@observer
class RecordTable extends Component {
    state = {
        dataSource : [],
        pagination: {},
        page: undefined,
        pageSize: undefined,
        fileId: '',
        shopItemLogModal: false,
        inventoryLogModal: false,
        shopPriceModal:false,
        total: undefined 
    }
    
    componentWillMount() {
        // this.changeMsgData(1,10,'',this.props.store.baseUser.userName)
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.reload && nextProps.reload !== this.props.reload) {
            this.changeMsgData()
        }
    }

    changeMsgData = async (page, pageSize, type, operator, startDate, endDate) =>{
        let msgListData = []
        if(this.props.type === 'export'){
            await this.props.recordStore.getExportJobList(page, pageSize, type, operator, startDate, endDate)
            msgListData = mobx.toJS(this.props.recordStore.exportList).data
            this.setState({
                total: mobx.toJS(this.props.recordStore.exportList).total
            })      
        }else if(this.props.type === 'importRecord'){
            await this.props.recordStore.getImportJobList(page, pageSize, type, operator, startDate, endDate)
            msgListData = mobx.toJS(this.props.recordStore.importList).data
            this.setState({
                total: mobx.toJS(this.props.recordStore.importList).total
            })
        }else{
            await this.props.recordStore.getImportJobList(page, pageSize, type, operator, startDate, endDate)
            msgListData = mobx.toJS(this.props.recordStore.importList).data.filter( v => v.type !=="syncInventory" && v.type!=="syncGoods")
            this.setState({
                total: mobx.toJS(this.props.recordStore.importList).total
            })
        }
        this.init(msgListData)
        this.props.recordStore.closeLoading()
    }

    init = (msgListData) =>{
        const dataSource = this.dataSourceFun(msgListData)
        this.paginationFun(msgListData)
        this.setState({
          dataSource
        })
    }

    dataSourceFun = (msgListData) =>{
        if(msgListData.length !==0){
            const dataSource = msgListData.map( data =>{
                return {
                  key: data.id,
                  type: data.typeName,
                  fileName:data.fileName,
                  processMsg:data.processMsg,
                  operator: data.operator,
                  fileId: data.fileId,
                  operatorTime: data.requestTime,
                  status: (data.status ===100 && '操作成功') || (data.status === 0 &&'正在处理') || (data.status === 99 && '操作失败'),
                  url: data.url,
                  url1: data.url1,
                  url2: data.url2,
                }
              })
              this.paginationFun(msgListData)
              return dataSource
        }
    }

    paginationFun = (data) => {
        const total = this.state.total
        this.setState({
          pagination :{
            total,
            showTotal: () => `共${total}条数据`,
            pageSizeOptions: ['10', '20', '50', '100'],
            showSizeChanger: true,
          }
        })
    }

    handleChange = (pagination) => {
        if(this.props.formList !== undefined){
            const type = this.props.formList.type
            const operator = this.props.formList.operator !== undefined ?  this.props.formList.operator : this.props.store.baseUser.userName
            const startDate = this.props.formList.startDate
            const endDate = this.props.formList.endDate
            this.changeMsgData(pagination.current, pagination.pageSize, type, operator, startDate, endDate)
        }else{
            this.changeMsgData(pagination.current, pagination.pageSize)
        }
        this.setState({
          ...pagination,
          page: pagination.current,
          pageSize:  pagination.pageSize
        })
      }

    showShopItemLog = (fileId) =>{
        this.setState({
            fileId,
            shopItemLogModal : true,
        })
    }

    showInventoryLog = (fileId) =>{
        this.setState({
            fileId,
            inventoryLogModal : true,
        })
    }
    resetlog = (fileId) =>{
        this.props.recordStore.resetlogs(fileId)
    }
    showShopPrice = (fileId) =>{
        this.setState({
            fileId,
            shopPriceModal : true,
        })
    }
    getModalType = record =>{
        if(record.type === '库存同步'|| record.type === '导入门店商品') return <a onClick ={() =>{this.showInventoryLog(record.fileId)}}>库存任务</a>;
        else if(record.type==='导入商品锁价') return <a onClick = {() =>{this.showShopPrice(record.fileId)}}>锁价任务</a> ;
        else return <a onClick = {() =>{this.showShopItemLog(record.fileId)}}>任务</a> 
    }
    render () {
        const columns = [
            {
                title: '类型',
                dataIndex : 'type',
                key: 'type',
            },
            {
                title: '操作批',
                dataIndex : 'fileId',
                key: 'fileId',
            },
            {
                title: '操作人',
                dataIndex : 'operator',
                key: 'operator',
            },
            {
                title: '操作时间',
                dataIndex : 'operatorTime',
                key: 'operatorTime',
            },
            {
                title: '文件名',
                dataIndex : 'fileName',
                key: 'fileName',
                render: (text, record) => (
                    <Tooltip title={record.fileName}>
                      <span>{`${record.fileName==null?"":record.fileName.substring(0, 8)}...`}</span>
                    </Tooltip>
                )
            },
            {
                title: '状态',
                dataIndex : 'status',
                key: 'status',
            },
            {
                title: '处理结果',
                dataIndex : 'processMsg',
                key: 'processMsg',
            },
            {
                title: '操作',
                dataIndex : 'remark',
                key: 'remark',
                render: (text , record) => {
                    let content = ''
                    switch (record.status) {
                        case '操作成功' :
                            content = this.props.type === 'export' ? <a href = {record.url}>立即下载</a> : 
                            <span>
                                {record.type ==='商品稽核' || record.type ==='库存同步' ? '': 
                                    <span>
                                    <a href = {record.url1}>导入前</a>
                                    <span className="ant-divider" />
                                    <a href = {record.url2}>导入后</a>
                                    <span className="ant-divider" />
                                    </span>
                                }
                                {
                                    this.getModalType(record)
                                }
                                <span className="ant-divider" />
                                <a onClick ={() =>{this.resetlog(record.fileId)}}>重置错误</a>
                                
                                {/* {record.type === '导入门店商品' && <span><span className="ant-divider" /><a onClick ={() =>{this.showInventoryLog(record.fileId)}}>库存任务</a></span>} */}
                            </span>
                            break ;
                        case '正在处理' :
                            content = this.props.type === 'export' ? '正在导出,请稍后' : '正在导入,请稍后'
                            break;
                        case '操作失败' :
                            content = this.props.type === 'export' ?'导出失败' : '导入失败'
                            break;
                        default : 
                            content = ''
                            break;
                    }
                    return (
                        <span>
                            <p>{content}</p>
                        </span>
                    )
                }
            },            
        ]
        return( 
            <div>
            <Row gutter={16} style = {{marginTop : '16px'}}>
                <Col style={{ textAlign: 'right' }} span= {24}>
                <a  onClick = {() =>{this.changeMsgData(1,10,'',this.props.store.baseUser.userName)}}>
                <Icon type= 'reload' />
                刷新记录
                </a>
                </Col>
                <Col span={24}>
                <Table 
                columns = {columns}
                style = {{width : '100%' , }}
                dataSource = {this.state.dataSource}
                pagination = {this.state.pagination}
                onChange = {this.handleChange} 
                loading = {this.props.recordStore.loading}
                />
                </Col>
            </Row>
            <Modal
            title = '查看商品日志'
            visible = {this.state.shopItemLogModal}
            onCancel = {() => {this.setState({shopItemLogModal: false })}}
            footer = {null}
            width = {1200}
            maskClosable = {false}          
            >
              <ShopItemLog 
              batchId = {this.state.fileId}
              history = { this.props.history }
              />  
            </Modal>
            <Modal
            title = '查看商城价格日志'
            visible = {this.state.shopPriceModal}
            onCancel = {() => {this.setState({shopPriceModal: false })}}
            footer = {null}
            width = {1200}
            maskClosable = {false}          
            >
              <ShopPrice 
              batchId = {this.state.fileId}
              isModule
              history = { this.props.history }
              />  
            </Modal>
            <Modal
            title = '查看库存日志'
            visible = {this.state.inventoryLogModal}
            onCancel = {() => {this.setState({inventoryLogModal: false })}}
            footer = {null}
            width = {1200}
            maskClosable = {false}          
            >
              <InventoryLog 
              batchId = {this.state.fileId}
              />  
            </Modal>
            </div>
        )
    }
}

export default RecordTable