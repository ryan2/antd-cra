// import { Map } from 'react-amap';
import React, { Component } from 'react'

import { Map, Polygon, Polyline, PolyEditor, MouseTool } from 'react-amap';

class Fence extends React.Component{
  constructor() {
    super();
    const self = this
    this.state = {
      lineActive: true,
      polygonActive: true,
      path: []
    };
    this.editorEvents = {
      created: (ins) => {console.log(ins)},
      addnode: () => {console.log('polyeditor addnode')},
      adjust: () => {console.log('polyeditor adjust')},
      removenode: () => {console.log('polyeditor removenode')},
      end: () => {console.log('polyeditor end')},
    };
    this.linePath = [
      {longitude: 150, latitude: 20 },
      {longitude: 170, latitude: 20 },
      {longitude: 150, latitude: 30 },
    ];
    this.polygonPath = [
      {longitude: 120, latitude: 30 },
      {longitude: 130, latitude: 30 },
      {longitude: 120, latitude: 40 },
    ];
    this.polygonStyle = {
        fillColor: '#87CEEB'
    }
    this.mapCenter = {longitude: 145, latitude: 30 }
    this.toolEvents = {
        created: (tool) => {
          console.log(tool)
          self.tool = tool;
        },
        draw({obj}) {
          self.drawWhat(obj);
        }
      }
  }

  drawWhat = (obj) => {
    const data = obj.getPath()
    let path = []
    data.forEach(index => {
      path.push({
          longitude: index.lng,
          latitude: index.lat
      })
    })
    this.setState({
        path
    }       
    )
    this.tool.close()
  }

    drawPolygon() {
    if (this.tool) {
      this.tool.polygon();
      this.setState({
        what: '准备绘制多边形'
      });
    }
  }
  render(){
    const plugins = [
        'MapType',
        'Scale',
        'OverView',
        'ControlBar', // v1.1.0 新增
        {
          name: 'ToolBar',
          options: {
            visible: true,  // 不设置该属性默认就是 true
            onCreated(ins){
              console.log(ins);
            },
          },
        }
      ]
      console.log(this.state.path);
    return <div>
      <div style={{width: '100%', height: '600px'}}>
        <Map plugins={plugins}>
          <MouseTool events={this.toolEvents}/>        
          <Polygon path={this.state.path} style ={this.polygonStyle}>
            <PolyEditor active={this.state.polygonActive} events={this.editorEvents} />
          </Polygon>
        </Map>
      </div>
      <button onClick={()=>{this.drawPolygon()}}>绘制</button>
      <button onClick={()=>{this.setState({polygonActive: !this.state.polygonActive})
      }}>保存</button>            
    </div>
  }
}


// import { Map, MouseTool } from 'react-amap';

// const layerStyle = {
//   padding: '10px',
//   background: '#fff',
//   border: '1px solid #ddd',
//   borderRadius: '4px',
//   position: 'absolute',
//   top: '10px',
//   left: '10px'
// };

// class App extends React.Component{
//   constructor(){
//     super();
//     const self =this;
//     this.state = {
//       what: '点击下方按钮开始绘制'
//     };
//     this.toolEvents = {
//       created: (tool) => {
//         console.log(tool)
//         self.tool = tool;
//       },
//       draw({obj}) {
//         self.drawWhat(obj);
//       }
//     }
//     this.mapPlugins = ['ToolBar'];
//     this.mapCenter = {longitude: 120, latitude: 35};
//   }

//   drawWhat(obj) {
//     let text = '';
//     switch(obj.CLASS_NAME) {
//       case 'AMap.Marker':
//        text = `你绘制了一个标记，坐标位置是 {${obj.getPosition()}}`;
//        break;
//       case 'AMap.Polygon':
//         text = `你绘制了一个多边形，有${obj.getPath().length}个端点`;
//         break;
//       case 'AMap.Circle':
//         text = `你绘制了一个圆形，圆心位置为{${obj.getCenter()}}`;
//         break;
//       default:
//         text = '';
//     }
//     this.setState({
//       what: text
//     });
//   }
  
//   drawCircle(){
//     if(this.tool){
//       this.tool.circle();
//       this.setState({
//         what: '准备绘制圆形'
//       });
//     }
//   }
  
//   drawRectangle(){
//     if(this.tool){
//       this.tool.rectangle();
//       this.setState({
//         what: '准备绘制多边形（矩形）'
//       });
//     }
//   }
  
//   drawMarker(){
//     if (this.tool){
//       this.tool.marker();
//       this.setState({
//         what: '准备绘制坐标点'
//       });
//     }
//   }

//   drawPolygon() {
//     if (this.tool) {
//       this.tool.polygon();
//       this.setState({
//         what: '准备绘制多边形'
//       });
//     }
//   }
  
//   close(){
//     if (this.tool){
//       this.tool.close();
//     }
//     this.setState({
//       what: '关闭了鼠标工具'
//     });
//   }
  
//   render(){
//     return <div>
//       <div style={{width: '100%', height: 370}}>
//         <Map 
//           plugins={this.mapPlugins}
//           center={this.mapCenter}
//         >
//           <MouseTool events={this.toolEvents}/>
//           <div style={layerStyle}>{this.state.what}</div>
//         </Map>
//        </div>
//        <button onClick={()=>{this.drawMarker()}}>Draw Marker</button>
//        <button onClick={()=>{this.drawRectangle()}}>Draw Rectangle</button>
//        <button onClick={()=>{this.drawCircle()}}>Draw Circle</button>
//        <button onClick={()=>{this.drawPolygon()}}>Draw Polygon</button>
//        <button onClick={()=>{this.close()}}>Close</button>
//      </div>
//   }
// }


export default Fence