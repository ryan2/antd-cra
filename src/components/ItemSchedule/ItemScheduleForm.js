import React, {
  Component
} from 'react'
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  message,
  Select
} from 'antd'
import FileOperation from '../FileOperation/FileOperation'
const FormItem = Form.Item
const Option = Select.Option

class ItemScheduleForm extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  handleSubmit = (e) => {
    e.preventDefault()
    const {
      categoryId,
      defaultValues
    } = this.props;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        if (!values.itemCode && !values.goodsName && values.status === undefined && !values.barcode &&
          !values.retailFormatId && categoryId === '' && !defaultValues.categoryId) {
          message.warning('请至少输入一项查询条件')
        } else {
          if (values.status === '') {
            delete values.status
          }
          this.props.onSearch(values)
        }
      }
    })
  }

  handleReset = () => {
    const {
      categoryId,
      defaultValues
    } = this.props;
    this.props.form.validateFields((err, values) => {
      if (JSON.stringify(values) === "{}" && categoryId === '' && !defaultValues.categoryId && !values.retailFormatId &&
        !defaultValues.itemCode && !defaultValues.goodsName && values.status === undefined && !defaultValues.barcode) {
        message.warning('查询条件已经为空')
      } else {
        console.log('aaaaaa')
        this.props.form.resetFields()
        this.props.onReset()
      }
    })
  }

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
        md: { span: 8 },
        lg: { span: 7 },
        xl: { span: 5 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
        md: { span: 16 },
        lg: { span: 16 },
        xl: { span: 14 },
      },
    }
    const { defaultValues, categoryId, canEdit, retailFormatList } = this.props
    const { getFieldDecorator, getFieldsValue } = this.props.form
    return (
      <div className="searchForm">
        <Form onSubmit={this.handleSubmit}>
          <Row gutter={16}>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品编码'}>
                {getFieldDecorator('itemCode', {
                  initialValue: defaultValues.itemCode
                })(
                    <Input placeholder="请输入商品编码" size="default"/>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品名称'}>
                {getFieldDecorator('goodsName', {
                  initialValue: defaultValues.goodsName
                })(
                    <Input placeholder="请输入商品名称" size="default"/>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品条码'}>
                {getFieldDecorator('barcode', {
                  initialValue: defaultValues.barcode
                })(
                    <Input placeholder="请输入商品条码" size="default"/>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品状态'}>
                {getFieldDecorator('status',  {
                  initialValue: defaultValues.status
                })(
                    <Select placeholder="请输选择商品状态" allowClear={true} size="default">
                      <Option value="">全部</Option>
                      <Option value="0">未启用</Option>
                      <Option value="1">启用</Option>
                      <Option value="-1">禁用</Option>
                    </Select>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品业态'}>
                {getFieldDecorator('retailFormatId',  {
                  initialValue: defaultValues.retailFormatId
                })(
                    <Select placeholder="请输选择商品业态" allowClear={true} size="default">
                      {retailFormatList.map(item =>
                              <Option key={item.retailFormatId} value={item.retailFormatId+''}>
                                {item.retailFormatName}
                              </Option>
                      )}
                    </Select>
                )}
              </FormItem>
            </Col>
            {/*<Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品类别'}>
                {getFieldDecorator('categoryId')(
                    <Cascader placeholder="请输选择商品类别" size="default"
                              options={this.props.categoryList} />
                )}
              </FormItem>
            </Col>*/}
          </Row>
          <Row  gutter={16}>
            <Col span={12}>
              {canEdit ?
                  <div>
                    <FileOperation history={this.props.history} type="ImportImage" style={{marginRight: 8}}
                                   buttonText="批量上传图片" buttonProps={{type: 'primary'}}
                                   fileType={["zip"]}/>
                    <FileOperation history={this.props.history} type="ImportProduct" style={{marginRight: 20}}
                                   buttonText="批量修改" buttonProps={{type: 'primary'}}
                                   fileType={["xlsx", "xls"]}/>
                  </div> :null}
            </Col>
            <Col span={12} style={{ textAlign: 'right', marginBottom: 16}}>
              <Button type="primary" htmlType="submit" size="default">查询</Button>
              <FileOperation history={this.props.history} type="ExportProduct" style={{margin: '0 8px'}}
                             buttonText="导出" buttonProps={{type: 'primary'}}
                             queryData={Object.assign({categoryId: categoryId}, getFieldsValue())}/>

              {canEdit ?
                  <FileOperation type="template" style={{marginRight: 8}}
                                 buttonText="下载模板" buttonProps={{type: 'primary'}}
                                 templateUrl="/file/exportProductList"/> : null }
              <Button onClick={this.handleReset} size="default">清空</Button>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}
export default Form.create()(ItemScheduleForm)

