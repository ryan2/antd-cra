import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { Table, Tag } from 'antd'
import WarehouseForm from './warehouseForm'
import '../ItemSchedule/itemSchedule.css'

// Global Component
@inject('wareStore' ,'store')@observer
class WarehouseComponent extends Component {
  constructor(props){
    super(props)
    const { cacheData } = props.wareStore;
    this.state = {
      pagination: {
        defaultCurrent: props.wareStore.page || 1,
        defaultPageSize: props.wareStore.pageSize || 10,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '50', '100'],
        showTotal: () => `共${this.props.wareStore.total}条数据`,
      },
      searchData: props.wareStore.cacheData,
      widowWidth: document.body.offsetWidth,
      widowHeight: document.body.offsetHeight,
      categoryId: cacheData.categoryId || '',
      // menuOpenKeys: props.wareStore.menuOpenKeys.slice(),
      // menuSelectedKeys: cacheData.categoryId ? [cacheData.categoryId] : [],
    }
  }

  componentDidMount() {
    const { cacheData,  warehouseGoodsList } = this.props.wareStore;
    let _cacheData = { ...cacheData }
    delete _cacheData.categoryId
    window.addEventListener('resize', this.onWindowResize)
    // this.props.wareStore.getCityList()
    if(warehouseGoodsList.length === 0) {
      //getWarehouseGoodsList(cacheData)
    }
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.onWindowResize)
  }

  onWindowResize = () => {
    this.setState({
      widowWidth: document.body.offsetWidth,
      widowHeight: document.body.offsetHeight
    })
  }

  handlePageChange = (pagination) => {
    const { searchData } = this.state;
    this.state.pagination.current = pagination.current;
    this.props.wareStore.getWarehouseGoodsList({
      ...searchData,
      page: pagination.current,
      pageSize: pagination.pageSize,
    })
    this.setState({
      pagination: this.state.pagination
    })
  }

  handleSearchSubmit = (params) => {
    this.props.wareStore.getWarehouseGoodsList(params)
    this.state.pagination.current = 1;
    this.setState({
      searchData: params,
      pagination: this.state.pagination
    })
  }

  handleFormReset = () => {
    // this.props.wareStore.getCategoryListMenu({})
    this.state.pagination.current = 1;
    this.setState({
      searchData: {},
      categoryId: "",
      // menuOpenKeys: [],
      // menuSelectedKeys: [],
      pagination: this.state.pagination
    })
    // this.props.wareStore.saveCacheData({categoryId: '', menuOpenKeys: []})
  }

  render() {
    const tagStyle = { width: 54, textAlign: 'center'}
    const columns = [{
        title: '门店',
        dataIndex: 'shopId',
        key: 'shopId',
        width: 120,
        render:(text,record) =><span>{text}-{record.shopName}</span>
      // },{
      //   title: '门店名称',
      //   dataIndex: 'shopName',
      //   key: 'shopName',
      //   width: 80,
      }, {
        title: '商品编码',
        dataIndex: 'itemCode',
        key: 'itemCode',
        width: 80,
      }, {
        title: '商品名称',
        dataIndex: 'goodsName',
        key: 'goodsName',
        // width:220
      },{
        title: '是否仓库',
        dataIndex: 'isDs',
        key: 'isDs',
        width: 100,
        render: (text) => (
          <span>
           <Tag color={text==0?'red':'green'} style={tagStyle}>{text==0?'否':'是'}</Tag>
          </span>
        )
      }
    ]
    const { loading, warehouseGoodsList, total, cacheData, retailFormatList } = this.props.wareStore
    const { categoryId, widowHeight } = this.state
    const pagination = Object.assign({total: total}, this.state.pagination)
    return (
      <div >
        <WarehouseForm wrappedComponentRef={(inst) => this.formRef  = inst}
          history={this.props.history}
          categoryId={categoryId}
          retailFormatList={retailFormatList}
          defaultValues={cacheData}
          onSearch={this.handleSearchSubmit}
          onReset={this.handleFormReset}/>
        <Table style={{width: '100%', minHeight: 450}} 
          loading={loading}
          columns={columns}
          dataSource={warehouseGoodsList.slice()}
          pagination={pagination}
          onChange={(pagination) => this.handlePageChange(pagination)}/>
      </div>
    )
  }
}
export default WarehouseComponent
