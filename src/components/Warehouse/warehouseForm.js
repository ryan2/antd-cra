import React, { Component } from 'react'
import { Row, Col, Form, Input, Button, message, Checkbox , Select  } from 'antd'
import FileOperation from '../FileOperation/FileOperation'
import { observer, inject } from 'mobx-react'
import * as mobx from 'mobx'
const FormItem = Form.Item
const Option = Select.Option;
@inject('wareStore' ,'store')@observer
class WarehouseForm extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentDidMount() {
    const { getCityList , getShopListByCityList , cityList , cityshopList,shopList } = this.props.wareStore;
    this.props.wareStore.getCityList();
  }
  handleSubmit = (e) => {
    e.preventDefault()
    const {categoryId, defaultValues} = this.props;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        // if (!values.itemCode && !values.goodsName && values.status === undefined && !values.barcode
        //     && !values.retailFormatId && categoryId === '' && !defaultValues.categoryId ) {
        //   message.warning('请至少输入一项查询条件')
        // } else {
        //   if(values.status === '') {
        //     delete values.status
        //   }
          this.props.onSearch(values)
        // }
      }
    })
  }

  handleReset = () => {
    this.props.form.validateFields((err, values) => {
      if (JSON.stringify(values) === "{}" && this.props.wareStore.shopList.length === 0) {
        message.warning('查询条件已经为空')
      } else {
        this.props.form.resetFields()
        this.props.onReset()
      }
    })
  }
  cityChange =  (value) =>{
    this.props.wareStore.getShopListByCityList({
      city: value
    })
    if(value !== undefined){
      this.props.form.setFieldsValue({
         shopId:[]   
      })      
    }
 }
  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
        md: { span: 8 },
        lg: { span: 7 },
        xl: { span: 5 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
        md: { span: 16 },
        lg: { span: 16 },
        xl: { span: 14 },
      },
    }
    const { defaultValues, categoryId} = this.props
    const { getFieldDecorator, getFieldsValue } = this.props.form
    return (
      <div className="searchForm">
        <Form onSubmit={this.handleSubmit}>
          <Row gutter={16}>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'城市'}>
                {getFieldDecorator('city', {
                  rules: [{ required: false, message: 'Please input city!' }],
                })(
                  <Select onChange = {this.cityChange} size='default' mode="multiple" placeholder="请选择城市">
                    {mobx.toJS(this.props.wareStore.cityList).map(data =><Option key={data.key}>{data.value}</Option>)}
                  </Select>
                  )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'门店名称'}>
                {getFieldDecorator('shopId', {
                  rules: [{ required: false, message: 'Please input store!' }],
                })(
                  <Select size = 'default'  placeholder="请选择门店" mode="multiple">
                    {mobx.toJS(this.props.wareStore.shopList).map(data =><Option key={data.shopId}>{data.shopNameAlias}</Option>)}
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品编码'}>
                {getFieldDecorator('itemCode', {
                  initialValue: defaultValues.goodsName
                })(
                    <Input placeholder="请输入商品编码" size="default"/>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'是否仓库'}>
                {getFieldDecorator('isDs', {
                  initialValue: defaultValues.isDs
                })(
                  <Select>
                    <Option value="">全部</Option>
                    <Option value="0">卖场</Option>
                    <Option value="1">仓库</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row  gutter={16}>
            <Col span={12}>
              <FileOperation history={this.props.history} type="ImportWarehouse"
                buttonText="导入列表" buttonProps={{type: 'primary'}}
                fileType={["xlsx", "xls"]}/>
              <FileOperation history={this.props.history} type="ExportWarehouse" style={{margin: '0 8px'}}
                             buttonText="导出列表" buttonProps={{type: 'primary'}}
                             queryData={getFieldsValue()}/>
              <FileOperation type="template" buttonText="下载模板" buttonProps={{type: 'primary'}}
                              templateUrl="/file/exportWarehouse"/>
            </Col>
            <Col span={12} style={{ textAlign: 'right', marginBottom: 16}}>
              <Button type="primary" htmlType="submit" size="default">查询</Button>
              <Button onClick={this.handleReset} style={{marginLeft:'8px'}} size="default">清空</Button>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}
export default Form.create()(WarehouseForm)

