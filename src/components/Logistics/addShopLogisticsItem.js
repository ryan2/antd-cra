import {
  Select,
  Row,
  Col,
  Button,
  Input,
  Form,
  Modal,
  Spin,
  Popconfirm,
  TimePicker,
  Tooltip,
  Icon,   
  notification,
  message
} from 'antd';
import React, {
  Component
} from 'react'
import {
  observer,
  inject
} from 'mobx-react'
import * as mobx from 'mobx'
import moment from 'moment';
import '../ItemSchedule/itemSchedule.css'
const Option = Select.Option;
const FormItem = Form.Item;
let map = null;
let polygon = null;
let editPolygon = null;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 10
    },
    md: {
      span: 8
    },
    lg: {
      span: 7
    },
    xl: {
      span: 5
    },
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 14
    },
    md: {
      span: 16
    },
    lg: {
      span: 16
    },
    xl: {
      span: 14
    },
  }
}

@inject('logisticsStore', 'store') @observer
class AddShopLogisticsItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDetail: false, //默认false 为选择门店及类型页面
      isEditing:false,  //默认false 为新增门店物流
      showMap: false,
      visible: false,
      confirmLoading: false,
      radius:5,  //正多边形半径
      edge:16,   //正多边形数量
      changeMap:false
    };
  }
  componentDidMount() {
    const {
      getTypeList,
      getCityList,
    } = this.props.logisticsStore;
    getCityList();
    getTypeList();
    this.initPageState()
  }
  initPageState = () => {
    const {
      getShopLogisticsList,
      editShopLogisticsItem,
      editingShop
    } = this.props.logisticsStore;
    if (this.props.match.params.shopId) {
      const search = {
        shopId: [this.props.match.params.shopId],
        logisticsType: this.props.match.params.logisticsType
      };
      !editingShop.shopId && getShopLogisticsList(search, true);
      this.setState({
        isDetail: true,
        isEditing: true
      })
    } else editShopLogisticsItem({
      "shopId": "",
      "logisticsType": "",
      "firstWeight": '',
      "firstFee": '',
      "nextFee": '',
      "limitWeight": '',
      "freeShippingId": '',
      "logisticsTimeId": '',
      "sFee": '',
      "isToday": '',
      "usedTime": '',
      "choseDays": '',
      "gid": ''
    })
  }
  /**选择城市搜索门店列表 */
  cityChange = (value) => {
    this.props.logisticsStore.getShopListByCityList({
      city: value
    })
    if (value !== undefined) {
      this.props.form.setFieldsValue({
        shopId: []
      })
    }
  }
  changeShop = value => {
    const shop = JSON.parse(value);
    // console.log(shop, shop.shopId)
    this.props.logisticsStore.editShopLogisticsItem({
      latitude: shop.latitude,
      longitude: shop.longitude,
      shopId: shop.shopId
    })
  }
  changeType = value => {
    this.props.logisticsStore.editShopLogisticsItem({
      logisticsType: value
    })
  }
  /**点击下一步按钮操作验证门店及类型信息 */
  nextStep = e => {
    e.preventDefault();
    const {
      getShopLogisticsList,
      editingShop
    } = this.props.logisticsStore
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const shopObj = JSON.parse(values.shopId)
        values.shopId = [shopObj.shopId];
        values.city = [shopObj.city];
        getShopLogisticsList(values, true, true, () => {
          this.props.history.push(`/logistics/add/${editingShop.shopId}/${editingShop.logisticsType}`)
          let content = '';
          if (editingShop.flag) content = '门店物流类型已被禁用,保存后即可启用';
          else content = '修改请继续,否则请点击返回'
          Modal.warning({
            title: '门店物流类型已存在',
            content,
          });
        })
        this.setState({
          isDetail: true
        })
      }
    });
  }
  /**验证并保存门店信息 */
  saveInformation = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const {
          editShopLogisticsItem,
          addShopLogisticsItem,
          updataShopLogistics
        } = this.props.logisticsStore;
        values.flag = 0;
        if(this.state.edge<3) values.gids = null;
        editShopLogisticsItem(Object.assign(values,this.props.match.params))
          if (this.state.isEditing) updataShopLogistics()
          else addShopLogisticsItem(()=>{
            this.props.history.push('/logistics/shop')
          })
      }
    });
  }
  //取消则返回列表页
  cancleEdit = () => {
    this.props.history.push('/logistics/shop')
  }
  componentWillUnmount() {
    map = null;
    polygon = null;
    editPolygon = null;
  }
  /**打开地理围栏弹窗 */
  openDialog = isForce => {
    const isToday = this.props.form.getFieldValue('isToday').toString();
    if (isToday.length === 0) message.warning('请先选择是否当天配送')
    else
      this.setState({
        visible: true,
      }, () => {
        const {
          editingShop
        } = this.props.logisticsStore;
        const latitude = editingShop.latitude * 1 || 30.54;
        const longitude = editingShop.longitude * 1 || 114.34;
        if (map === null) this.creatMap(longitude, latitude)
        if (polygon === null) this.createPolygon(longitude, latitude, editingShop.gids, isForce)
        if (isToday === '0') notification.warning({
          message: '编辑提示',
          description: '当前选择为非当天配送，请确认所配置范围大小。若无需新增或编辑请点击取消退出范围选择的弹框。',
          duration: 0,
        });
      });
  }
  //创建化地图并定位 无经纬度则默认定位到湖北省政府附近
  creatMap = (longitude, latitude) => {
    map = new AMap.Map('amapContainer', {
      resizeEnable: true,
      center: [longitude, latitude],
      zoom: 12
    });
    // 定位坐标的标记
    let marker = new AMap.Marker({
      icon: "https://webapi.amap.com/theme/v1.3/markers/n/mark_b.png",
      position: [longitude, latitude]
    });
    marker.setMap(map)
    // 添加控件
    AMap.plugin(['AMap.ToolBar', 'AMap.Scale'], function () {
      map.addControl(new AMap.ToolBar());
      map.addControl(new AMap.Scale());
    })
    /***
     * 参考圆
     */
    // var circle = new AMap.Circle({
    //   center: new AMap.LngLat(longitude, latitude), // 圆心位置
    //   radius: 5000, //半径
    //   strokeColor: "#f00", //线颜色
    //   strokeOpacity: 0.4, //线透明度
    //   strokeWeight: 3, //线粗细度
    //   fillColor: "#f00", //填充颜色
    //   fillOpacity: 0.15 //填充透明度
    // });
    // circle.setMap(map);
  }
  // 根据定位划出围栏,新增围栏默认定位点为中心的一个矩形
  createPolygon = (longitude, latitude, gids, isForce) => {
    let defaultPoint = gids
    let path = [];
    const isToday = this.props.form.getFieldValue('isToday').toString();
    if (!defaultPoint || isForce) { //新建地理围栏或强制重绘
      defaultPoint = this.getDifflatitude(latitude, longitude)
    }
    if (isToday === '1' || isForce || gids)
      defaultPoint.split(';').forEach(p => {
        path.push([p.split(',')[1], p.split(',')[0]])
      })
    polygon = new AMap.Polygon({
      map,
      path,
      fillColor: '#4c82ff', // 多边形填充颜色
      fillOpacity: 0.35,
      strokeColor: '#4c82ff', // 线条颜色
    });
    editPolygon = new AMap.PolyEditor(map, polygon);
    editPolygon.open()
  }
  //获取多边形的顶点
  getDifflatitude = (lat, lng) =>{
    const r= 6371000.79;
    const {radius,edge} =this.state;
    const angle = 2 * Math.PI/edge;
    // const center = new AMap.LngLat(lng,lat)
    const pointArr=[];
    for(let i=0;i<edge;i++){
      const dx= radius*1000*Math.cos(i*angle)
      const dy= radius*1000*Math.sin(i*angle)
      const dlng = dx / (r*Math.cos(lat*Math.PI/180)*Math.PI/180);
      const dlat = dy/(r*Math.PI/180)
      const point = `${lat+dlat}, ${lng+dlng}`
      // const popinnt = new AMap.LngLat(lng+dlng, lat+dlat)
      // console.log(Math.round(center.distance(popinnt)))
      pointArr.push(point)
    }
    return pointArr.join(';')
  }
  /**关闭地理围栏弹窗 */
  closeDialog = () => {
    this.setState({
      visible: false,
    });
    polygon.setMap(null)
    polygon = null;
    editPolygon.close();
  }
  //重置地图围栏
  initMap = ()  =>{
    polygon.setMap(null)
    polygon = null;
    editPolygon.close();
    this.openDialog(true)
  }
  /**点击弹窗确认按钮 */
  handleOk = () => {
    this.setState({
      confirmLoading: true,
    });
    const {
      editShopLogisticsItem,
      editingShop
    } = this.props.logisticsStore;
    // editPolygon.close();
    const pointArray = new Array();
    polygon.getPath().forEach(p => {
      const obj = p.lat+','+p.lng
      pointArray.push(obj)
    });
    let assignData = {
        gids: pointArray.join(';'),
      }
    if(!editingShop.gid) {
      const newStr= new Date().getTime()+String.fromCharCode(91+Number.parseInt(Math.random()*26),65+Number.parseInt(Math.random()*26))+Number.parseInt(Math.random()*10)
      assignData.gid = newStr
    }
    editShopLogisticsItem(assignData)
    this.setState({
      visible: false,
      confirmLoading: false,
      changeMap:true
    })
  }
  // startTime = (index,timeString) => {
  //   this.props.logisticsStore.editInterval(timeString,'timeStart',index)
  // }
  // endTime = (index,timeString) => {
  //   this.props.logisticsStore.editInterval(timeString,'timeEnd',index)
  // }
  // changeIpt = (value,data,index,key) =>{
  //   console.log(value,data,index,key)
  // }
  render(){
    const {
      isDetail,
      visible,
      confirmLoading,
      isEditing,
      changeMap,
      edge
    } = this.state
    const {
      shopList,
      cityList,
      typeList,
      editingShop,
      loading,
      addInterval,
      intervalList,
      editInterval,
      editIntervalList,
      deleteIntervalList,
      auxiliary,
      isAdding
    } = this.props.logisticsStore;
    
    const { getFieldDecorator } = this.props.form
    return(
      <Form className='searchForm'>
        <header className='logisticsTitle'>
          <span>
            {
              isDetail ? '门店基础信息' : '门店及物流类型'
            }
          </span>
        </header>
        <Spin spinning={loading}>
          {
            isDetail ?
              <section>
                {
                  isEditing?(
                    <Row gutter={24} className="addContainer">
                      <Col className="gutter-row" span={10}>
                        <FormItem {...formItemLayout} label={'门店名称'}>
                          {getFieldDecorator('shopNameAlias', {
                            rules: [{ required: true, message: '请选择门店!' }],
                            initialValue: editingShop.shopNameAlias
                          })(
                            <Input disabled/>
                          )}
                        </FormItem>
                      </Col>
                      <Col className="gutter-row" span={10}>
                        <FormItem {...formItemLayout} label={'物流类型'}>
                            {getFieldDecorator('logisticsType', {
                              rules: [{ required: true, message: '请选择物流类型!' }],
                              initialValue: editingShop.logisticsType+'-'+editingShop.logisticsName
                            })(
                              <Input disabled/>
                            )}
                        </FormItem>
                      </Col>
                    </Row>
                  ):null
                }
                <Row gutter={24} className="addContainer">
                  <Col className="gutter-row" span={10}>
                    <FormItem {...formItemLayout} label={'首重重量(克)'}>
                      {getFieldDecorator('firstWeight', {
                        rules: [{ required: true,pattern:/^\d+(\.\d{0,2})?$/, message: '请填写两位小数以内首重重量!' }],
                        initialValue: editingShop.firstWeight
                      })(
                        <Input placeholder="请填写首重重量"/>
                      )}
                    </FormItem>
                  </Col>
                  <Col className="gutter-row" span={10}>
                    <FormItem {...formItemLayout} label={'首重运费'}>
                      {getFieldDecorator('firstFee', {
                        rules: [{ required: true,pattern:/^\d+(\.\d{0,2})?$/, message: '请填写两位小数以内首重运费!' }],
                        initialValue: editingShop.firstFee
                      })(
                        <Input placeholder="请填写首重运费"/>
                      )}
                    </FormItem>
                  </Col>
                  <Col className="gutter-row" span={10}>
                    <FormItem {...formItemLayout} label={'续重加价(元/千克)'}>
                      {getFieldDecorator('nextFee', {
                        rules: [{ required: true,pattern:/^\d+(\.\d{0,2})?$/, message: '请填写两位小数以内续重加价!' }],
                        initialValue: editingShop.nextFee
                      })(
                        <Input placeholder="请填写续重加价"/>
                      )}
                    </FormItem>
                  </Col>
                  <Col className="gutter-row" span={10}>
                    <FormItem {...formItemLayout} label={'限制重量(克)'}>
                      {getFieldDecorator('limitWeight', {
                        rules: [{ required: true,pattern:/^[0-9]+$/, message: '请填写正整数的限制重量!' }],
                        initialValue: editingShop.limitWeight
                      })(
                        <Input placeholder="请填写限制重量"/>
                      )}
                    </FormItem>
                  </Col>
                  <Col className="gutter-row" span={10}>
                    <FormItem {...formItemLayout} label={'服务费'}>
                      {getFieldDecorator('sFee', {
                        rules: [{ required: true,pattern:/^\d+(\.\d{0,2})?$/, message: '请填写两位小数服务费!' }],
                        initialValue: editingShop.sFee
                      })(
                        <Input placeholder="请填写服务费"/>
                      )}
                    </FormItem>
                  </Col>
                  <Col className="gutter-row" span={10}>
                    <FormItem {...formItemLayout} label={'是否可当天配送'}>
                      {getFieldDecorator('isToday', {
                        rules: [{ required: true, message: '请选择是否可当天配送!' }],
                        initialValue: editingShop.isToday.toString()
                      })(
                        <Select size = 'default'  placeholder="请选择是否可当天配送">
                          <Option key='0'>否</Option>
                          <Option key='1'>是</Option>
                        </Select>
                      )}
                    </FormItem>
                  </Col>
                  <Col className="gutter-row" span={10}>
                    <FormItem {...formItemLayout} label={'备货时长(小时)'}>
                      {getFieldDecorator('usedTime', {
                        rules: [{ required: true,pattern:/^[0-9]+$/, message: '请填写正整数备货时长!' }],
                        initialValue: editingShop.usedTime
                      })(
                        <Input placeholder="请填写备货时长"/>
                      )}
                    </FormItem>
                  </Col>
                  <Col className="gutter-row" span={10}>
                    <FormItem {...formItemLayout} label={'可选天数(天)'}>
                      {getFieldDecorator('choseDays', {
                        rules: [{ required: true,pattern:/^[0-9]+$/, message: '请填写正整数天数!' }],
                        initialValue: editingShop.choseDays
                      })(
                        <Input placeholder="请填写可选天数"/>
                      )}
                    </FormItem>
                  </Col>
                  <Col className="gutter-row" span={10}>
                    <FormItem {...formItemLayout} label={'地理围栏ID'}>
                      {getFieldDecorator('gid', {
                        rules: [{ required: false, message: '请填写地理围栏ID!' }],
                        initialValue: editingShop.gid
                      })(
                        <Input disabled />
                      )}
                    </FormItem>
                  </Col>
                  <Col span={24}>
                    <Button onClick={  e => this.openDialog(false) } style={{margin:'0 8px 20px'}} size="default">维护地理围栏</Button>
                    <Modal title="范围选择"
                      visible = { visible }
                      onOk = { this.handleOk }
                      confirmLoading = { confirmLoading }
                      onCancel = { this.closeDialog }
                      bodyStyle = {{ padding:0, height:'calc( 100% - 101px )' }}
                      style = {{ top: '2%', padding:0 }}
                      width  =  '80%'
                      height = '95%'
                      destroyOnClose = { true }
                      keyboard = { false }
                      maskClosable = { false }
                    >
                      <Row style = {{padding:'6px 20px'}} >
                        <Col span={8}>
                          <FormItem  {...formItemLayout} label={'围栏半径(公里)'} style = {{margin:0}} >
                            <Input placeholder='默认半径5公里' onChange = { e => this.setState({radius:e.target.value}) } size="small" />
                          </FormItem>
                        </Col>
                        <Col span={8}>
                          <FormItem  {...formItemLayout} label={'多边形边数'} style = {{margin:0}} >
                            <Input placeholder='默认正16边形' onChange = { e => this.setState({edge:e.target.value}) } size="small" />
                          </FormItem>
                        </Col>
                        <Col span={8} style={{ textAlign:'right'}}>
                          <Popconfirm 
                              title={edge>2?"确定重置地图?":"多边形边数少于3边将删除地理围栏,继续此操作?"} 
                              okText='确定'
                              cancelText='取消'
                              placement="bottom"
                              onConfirm={this.initMap}
                          >
                            <Button size="default">重置地图</Button>
                          </Popconfirm>
                        </Col>
                      </Row>
                      <section id='amapContainer' style={{height:'calc( 100% - 40px)'}}></section>
                    </Modal>
                  </Col>
                </Row>
                <header className='logisticsTitle'>
                  <span>
                    配送时段维护
                  </span>
                </header>
                  <Row gutter={24} className="addContainer" style = {{ marginBottom:'10px',textIndent:'7px' }} >
                    <Col className="gutter-row" span={4}>开始时间</Col>
                    <Col className="gutter-row" span={4}>结束时间</Col>
                    <Col className="gutter-row" span={4}>名称</Col>
                    <Col className="gutter-row" span={4}>加价</Col>
                    <Col className="gutter-row" span={5}>最大订单数</Col>
                    <Col className="gutter-row" span={3}>操作</Col>
                  </Row>
                {
                  mobx.toJS(intervalList).map((data,index) =>
                    <Row gutter={24} className="addContainer" key = { index+','+auxiliary } style = {{ marginBottom:'10px' }} >
                      <Col className="gutter-row" span={4}>
                        <TimePicker placeholder='开始时间' onChange={ (e,value) => editInterval(value,'timeStart',index) } defaultValue={moment(data.timeStart, 'HH:mm')} format = 'HH:mm'/>
                        {data.hasInterval?<Tooltip title='已存在相同时间段,请修改时间' ><Icon type='exclamation-circle' style={{marginLeft:'10px',color:'#4c82ff'}} /></Tooltip>:null}
                      </Col>
                      <Col className="gutter-row" span={4}>
                        <TimePicker placeholder='结束时间' onChange={ (e,value) => editInterval(value,'timeEnd',index) } defaultValue={moment(data.timeEnd, 'HH:mm')} format = 'HH:mm'/>
                        {data.nextDay?<span style={{marginLeft:'10px',color:'#f40'}} >次日</span>:null}
                      </Col>
                      <Col className="gutter-row" span={4}>
                        <Input placeholder="名称" defaultValue = { data.logisticsTimeName } onChange = { e => editInterval(e.target.value,'logisticsTimeName',index) } />
                      </Col>
                      <Col className="gutter-row" span={4}>
                        <Input placeholder="加价" defaultValue = { data.fee||'0' }  onChange = { e => editInterval(e.target.value,'fee',index) }/>
                      </Col>
                      <Col className="gutter-row" span={5}>
                        <Input placeholder="最大订单数" defaultValue = { data.maxOrders||'100' }  onChange = { e => editInterval(e.target.value,'maxOrders',index) }/>
                      </Col>
                      <Col className="gutter-row" span={3}>
                        {
                          data.logisticsTimeId!==undefined?(
                            data.isEditing?
                              <Tooltip title='点击保存此项修改' >
                                <Button type="primary" shape="circle" icon="check" size='small' onClick = { e => editIntervalList(index) } />
                              </Tooltip>
                            :
                              <Button disabled type="primary" shape="circle" icon="check" size='small' />)
                          :
                            <Popconfirm 
                                title="新增条目将重置列表,继续保存?" 
                                okText='确定'
                                cancelText='取消'
                                onConfirm = { e => editIntervalList(index) }
                            >
                              <Tooltip title='点击保存此项' >
                                <Button type="primary" shape="circle" icon="check" size='small' />
                              </Tooltip>
                            </Popconfirm>
                        }
                        <Popconfirm 
                              title="确定删除此时间段项?" 
                              okText='确定'
                              cancelText='取消'
                              onConfirm = { e => deleteIntervalList(index) }
                          >
                            <Tooltip title='点击删除此项' >
                              <Button shape="circle" icon="close" size='small' style = {{ marginLeft:'15px' }} /> 
                            </Tooltip>
                        </Popconfirm>
                      </Col>
                    </Row>)
                }
              </section>
              :
              <Row gutter={24} className="addContainer">
                <Col className="gutter-row" span={8}>
                  <FormItem {...formItemLayout} label={'城市'}>
                    {getFieldDecorator('city', {
                      rules: [{ required: true, message: '请选择城市!' }],
                      initialValue: editingShop.city
                    })(
                      <Select onChange = {this.cityChange} size='default' placeholder="请选择城市">
                          {mobx.toJS(cityList).map(data =><Option key={data.key}>{data.value}</Option>)}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col className="gutter-row" span={8}>
                  <FormItem {...formItemLayout} label={'门店名称'}>
                    {getFieldDecorator('shopId', {
                      rules: [{ required: true, message: '请选择门店!' }],
                      initialValue: editingShop.shopId
                    })(
                      <Select size = 'default'  placeholder="请选择门店" onChange = { value => this.changeShop(value) } >
                        {mobx.toJS(shopList).map(data =><Option key={ JSON.stringify(data)}>{data.shopNameAlias}</Option>)}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col className="gutter-row" span={8}>
                  <FormItem {...formItemLayout} label={'物流类型'}>
                      {getFieldDecorator('logisticsType', {
                        rules: [{ required: true, message: '请选择物流类型!' }],
                        initialValue: editingShop.logisticsType
                      })(
                        <Select size = 'default'  placeholder="请选择物流类型" onChange = { value => this.changeType(value) } >
                            {mobx.toJS(typeList).map(data =><Option key={data.logisticsType}>{data.logisticsName}</Option>)}
                        </Select>
                      )}
                  </FormItem>
                </Col>
              </Row>
          }
          <Row gutter={24}>
              <Col span={4} style={{ marginleft:'10px'}}>
                {
                  isDetail ? (
                    isAdding?
                      <Tooltip title='请先保存已增加条目' >
                        <Button disabled size="default">添加时段</Button>
                      </Tooltip>
                    :
                      <Button onClick={ addInterval } size="default">添加时段</Button>
                  )
                  : 
                    null
                }
              </Col>
              <Col span={20} style={{ marginBottom:'16px', textAlign:'right'}}>
                {
                  isDetail ? (
                    changeMap?<Popconfirm 
                          title="保存后地理围栏也将保存,确定继续?" 
                          okText='确定'
                          cancelText='取消'
                          onConfirm={this.saveInformation}
                      >
                        <Button type="primary" size="default">保存门店基本信息</Button>
                      </Popconfirm>
                    :
                    <Button type="primary" onClick={ this.saveInformation } size="default">保存门店基本信息</Button>)
                  : 
                    <Button type="primary" htmlType="submit" onClick={ this.nextStep } size="default">下一步</Button>
                }
                <Button onClick={ this.cancleEdit } style={{margin:'0 8px'}} size="default">返回</Button>
              </Col>
          </Row>
        </Spin>
      </Form>
    )
  }
}

export default Form.create()(AddShopLogisticsItem)