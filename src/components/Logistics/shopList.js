import React, {
    Component
} from 'react'
import {
    observer,
    inject
} from 'mobx-react'
import {
    Table,
    Tag,
    Spin,
    Card,
    Modal,
    Checkbox,
    Collapse
} from 'antd'
import {
    Link
} from 'react-router-dom'
import {toJS} from 'mobx'
import ShopForm from './shopListForm'
import '../ItemSchedule/itemSchedule.css'
import '../ItemModify/itemModify.css'
const Panel = Collapse.Panel;

@inject('logisticsStore', 'store') @observer
class ShopList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchData: props.logisticsStore.cacheData,
            pagination: {
                defaultCurrent: props.logisticsStore.page || 1,
                defaultPageSize: props.logisticsStore.pageSize || 10,
                showSizeChanger: true,
                pageSizeOptions: ['10', '20', '50', '100'],
                showTotal: () => `共${this.props.logisticsStore.total}条数据`,
            },
            containerHeight:0,
            windowX:0,
            city:{},
            modalVisible:false,
            indeterminate:false,
            checkAll:false,
            checkedList:[],
            changedCity:true //城市是否已变更
        }
    }
    componentWillMount() {
        const {
            getTypeList,
            getCityList,
            initObservableData,
            cityList,
            changeCacheData,
            cacheData
        } = this.props.logisticsStore;
        // getTypeList();
        initObservableData()
        this.getContainerHight()
        window.onresize = this.getContainerHight;
        if (this.state.searchData.city) {
            this.cityChange(Object.assign(cacheData,cityList.filter(p => p.value === this.state.searchData.city[0])[0]),true)
        }else getCityList();
    }
    componentWillUnmount(props) {
        window.onresize = null
    }
    getContainerHight = () => {
        this.setState({
            containerHeight: document.body.offsetHeight - 190,
            windowX:document.body.offsetWidth
        })
    }
    searchList = params => {
        const {
            searchData,
            pagination
        } = this.state;
        const search = Object.assign({flag:0,logisticsType:searchData.logisticsType,city:searchData.city},params)
        this.setState({
            searchData: search,
            pagination: Object.assign(pagination,{current:params.page||1})
        })
        this.props.logisticsStore.getShopLogisticsList(Object.assign(search,{page:pagination.page,pageSize:pagination.pageSize}))
        this.props.logisticsStore.changeCacheData(Object.assign(search,{page:params.page||1}))
    }
    handlePageChange = pagination => {
        const {
            searchData
        } = this.state;
        this.state.pagination.current = pagination.current;
        this.state.pagination.pageSize = pagination.pageSize;
        this.props.logisticsStore.getShopLogisticsList({
            ...searchData,
            page: pagination.current,
            pageSize: pagination.pageSize,
        })
        this.props.logisticsStore.changeCacheData({
            page: pagination.current,
            pageSize: pagination.pageSize
        })
        this.setState({
            pagination: this.state.pagination
        })
    }
    forbidden = item => {
        const {
            editShopLogisticsItem,
            updataShopLogistics,
            getShopLogisticsList
        } = this.props.logisticsStore;
        const {
            searchData,
            pagination
        } = this.state;
        editShopLogisticsItem(Object.assign(item, {
            flag: 1
        }))
        updataShopLogistics(()=>{
            getShopLogisticsList({
                ...searchData,
                page: pagination.current,
                pageSize: pagination.pageSize,
            })
        })
    }
    cityChange = (city,isRetain) => {
        const {
            getShopListByCityList,
            editShopLogisticsItem,
            cacheData
        } = this.props.logisticsStore;
        editShopLogisticsItem({
            city: city.value
        })
        if(!isRetain){
            getShopListByCityList({
                city: city.value
            })
            this.searchList({
                ...city,
                city: [city.value],
                page: city.page || 1
            })
        }
        this.setState({
            city,
            changedCity: !isRetain&&city.value !== undefined
        })
    }
    openModal = () => {
        const {
            noDistrict,
            districtList
        } = this.props.logisticsStore;
        const splitArr = noDistrict ? noDistrict.district.split(',') : []
        this.setState({
            modalVisible: false,
            indeterminate: splitArr.length > 0 && (splitArr.length < districtList.length),
            checkAll:splitArr.length === districtList.length,
            checkedList: splitArr,
            modalVisible: true
        })
    }
    checkAllChange = e => {
        const newList = []
        if (e.target.checked) {
            this.props.logisticsStore.districtList.forEach(p => newList.push(p.district))
        }
        this.setState({
            checkedList: e.target.checked ? newList : [],
            indeterminate: false,
            checkAll: e.target.checked,
        });
    }
    checkChange = (checked, district) => {
        const {
            districtList
        } = this.props.logisticsStore;
        const {
            checkedList
        } = this.state;
        const newList = []
        if (checked) newList.push(...checkedList, district);
        else newList.push(...checkedList.filter(p => p !== district));
        this.setState({
            checkedList: newList,
            indeterminate: !!newList.length && (newList.length < districtList.length),
            checkAll: newList.length === districtList.length,
        });
    }
    editNoDistrict = () =>{
        const {
            checkedList,
            city
        } = this.state;
        this.props.logisticsStore.editNoDistrict({
            cityCode:city.code,
            district:checkedList.join(',')
        })
        this.setState({modalVisible:false})
    }
    clearIpt = () =>{
        this.setState({
            searchData:{},
            city:{}
        },()=>{
            this.props.logisticsStore.changeCacheData({
                logisticsType:'',
                city:[],
                shopId:[],
                page:1,
                code:0,
                key:'',
                value:''
            })
            this.handlePageChange(1)
            this.props.logisticsStore.initObservableData()
        })
    }
    onExpands = (bol,obj)=>{
        if(bol) obj.rowSpan=obj.rowSpans;
        else obj.rowSpan=1
    }
    render(){
        const tableHeader = [{
            title: '门店',
            dataIndex: 'shopId',
            key: 'shopId',
            // fixed:'left',
            width:150,
            render:(text,record) =>{
                // const obj = {
                //     children:record.rowSpan?<span>{text}-{record.shopNameAlias}</span>:null,
                //     props: {
                //         rowSpan: record.rowSpan
                //     }
                // }
                return record.rowSpan?<span>{text}-{record.shopNameAlias}</span>:null
            }
        }, {
            title: '物流类型',
            dataIndex: 'logisticsType',
            key: 'logisticsType',
            // fixed:'left',
            width:150,
            render:(text,record) =><span>{text}-{record.logisticsName}</span>
        }, {
            title: '首重(克)',
            dataIndex: 'firstWeight',
            key: 'firstWeight',
            width:100,
        }, {
            title: '首重运费',
            dataIndex: 'firstFee',
            key: 'firstFee',
            width:100,
        }, {
            title: '续重加价(元/千克)',
            dataIndex: 'nextFee',
            key: 'nextFee',
            width:120,
        }, {
            title: '限制重量(克)',
            dataIndex: 'limitWeight',
            key: 'limitWeight',
            width:100,
        }, {
            title: '服务费',
            dataIndex: 'sFee',
            key: 'sFee',
            width:100,
        }, {
            title: '可当天配送',
            dataIndex: 'isToday',
            key: 'isToday',
            render:text=><span>{text?'是':'否'}</span>,
            width:100,
        }, {
            title: '备货时长(小时)',
            dataIndex: 'usedTime',
            key: 'usedTime',
            width:100,
        }, {
            title: '可选天数',
            dataIndex: 'choseDays',
            key: 'choseDays',
            width:100,
        }, {
            title: '操作',
            key: 'action',
            // fixed:'right',
            width:150,
            render: (text, record) => 
                <span>
                    <Link to={ `/logistics/add/${record.shopId}/${record.logisticsType}` }><Tag color = "blue" >编辑</Tag></Link>
                    <Tag color = 'red' onClick = { e => this.forbidden(record) } >禁用</Tag>
                </span>
        }]
        const {
            shopLogisticsList,
            loading,
            total,
            cityList,
            getNoDistrict,
            getDistrict,
            districtList
        } = this.props.logisticsStore;
        const {
            containerHeight,
            windowX,
            city,
            modalVisible,
            indeterminate,
            checkAll,
            checkedList,
            changedCity
        } = this.state
        const pagination = Object.assign({total: total}, this.state.pagination)
        const style = {
            backgroundColor:'#f0f0f0',
            color:'#fff'
        }
        return (
            <Spin spinning={loading}>
                <ShopForm 
                    defaultValues={this.state.searchData} 
                    onSearch={this.searchList}
                    city = { city }
                    getDistrict = { getDistrict }
                    showModal = { this.openModal }
                    getNoDistrict = { getNoDistrict }
                    changedCity = { changedCity }
                    cityChanged = { e => this.setState({changedCity:false}) }
                    clearIpt = { this.clearIpt }
                    history = {this.props.history}
                />
                <section style = {{display:'flex',height:containerHeight+'px',position:'relative'}}>
                    <Card.Grid className = 'tagTypeContain' >
                        {toJS(cityList).map((data,i) =>
                            <section className = 'tagType' key={i} onClick = {e=>this.cityChange(data)} style = {city.code==data.code?style:null}>
                                {data.value}
                            </section>)}
                    </Card.Grid>
                    <div style = {{flex:1}}>
                        <Table style={{width: windowX-418, minHeight: containerHeight,flex:1,maxWidth:'1300px'}} 
                            columns={tableHeader} 
                            dataSource={shopLogisticsList.slice()} 
                            pagination={pagination}
                            onExpand = {this.onExpands}
                            size="small"
                            scroll={{ x: 1300, y: containerHeight-130 }}
                            onChange={ pagination => this.handlePageChange(pagination)}/>
                    </div>
                </section>
                <Modal title="不可配送区域"
                    visible = { modalVisible }
                    onOk = { this.editNoDistrict }
                    confirmLoading = { loading }
                    width = '60%'
                    onCancel = { e => this.setState({modalVisible:false}) }>
                    <Checkbox
                        indeterminate={indeterminate}
                        onChange={this.checkAllChange}
                        checked={checkAll}
                        style = {{marginBottom:'10px',paddingLeft:'16px'}}
                    >全选</Checkbox>
                    <section style = {{display:'flex',justifyContent:'space-between',flexWrap:'wrap'}}>
                        {
                            toJS(districtList).map((district,i)=> <Checkbox style = {{width:'30%',marginBottom:'10px',paddingLeft:'16px'}} key = {i}  checked = { checkedList.indexOf(district.district)!==-1 } onChange={ e => this.checkChange(e.target.checked,district.district)}>{district.district}</Checkbox>)
                        }
                    </section>
                </Modal>
            </Spin>
        )
    }
}
export default ShopList;