import React, {
    Component
} from 'react'
import {
    observer,
    inject
} from 'mobx-react'
import {
    Table,
    Select,
    Input,
    Popconfirm,
    Spin,
    message
} from 'antd'
import TypeForm from './logisticsTypeForm'
import '../ItemSchedule/itemSchedule.css'
const Option = Select.Option;
class EditableCell extends Component {
    constructor(props) {
        super(props);
    }
    render(){
        const {record, column, onChange} = this.props;
        if(record.editable){
            if (column === 'logisticsName') {
                return <Input style={{ margin: '-5px 0' }} value={record[column]} onChange={e => onChange(e.target.value)} maxLength='10' />
            }else if(column === 'district'){
                return <Input style={{ margin: '-5px 0' }} value={record[column]} onChange={e => onChange(e.target.value)} maxLength='255' />
            }else if(column === 'isFresh'){
                return (
                    <Select style={{ margin:'-5px 0',width:'50px' }} defaultValue={ record[column].toString() } onChange={value => onChange(value)} >
                        <Option value="1">是</Option>
                        <Option value="0">否</Option>
                    </Select>
                )
            }else if(column === 'logisticsType'){
                if(record.newItem) return <Input style={{ margin: '-5px 0' }} value={record[column]} onChange={e => onChange(e.target.value)} maxLength='2' />
                else return <span>{record[column]}</span>
            }
        }else{
            if( column !== 'isFresh') return <span>{record[column]}</span>;
            else return <span>{record[column]?'是':'否'}</span>
        } 
    }
}
@inject('logisticsStore', 'store') @observer
class TypeComponent extends Component {
    componentWillMount() {
        const {
            getTypeList
        } = this.props.logisticsStore;
        getTypeList();
    }
    constructor(props) {
        super(props);
        this.state = {
            typeList: this.props.logisticsStore.typeList
        }
    }
    renderColumns(record, column) {
        return ( < EditableCell record = { record } column = { column } onChange = { value => this.handleChange(value, record, column) } />
        );
    }
    handleChange(value, record, column) {
        const newData = [...this.props.logisticsStore.typeList];
        const target = newData.find(item => record.key === item.key);
        if (target) {
            target[column] = value;
            this.props.logisticsStore.changeTypeList(newData);
        }
    }
    edit(key) {
        const newData = [...this.props.logisticsStore.typeList];
        const target = newData.find(item => key === item.key);
        if (target) {
            target.editable = true;
            this.props.logisticsStore.changeTypeList(newData)
        }
    }
    save(key) {
        const newData = [...this.props.logisticsStore.typeList];
        const target = newData.find(item => key === item.key);
        if (target) {
            target.editable = false;
            this.props.logisticsStore.editTypeItem(target, () => {
                this.props.logisticsStore.changeTypeList(newData)
            })
        }
    }
    cancel(key) {
        const newData = [...this.props.logisticsStore.typeList];
        const i = newData.findIndex(item => key === item.key);
        if (i !== -1) {
            const target = newData[i];
            if (target.newItem) newData.splice(i, 1);
            else target.editable = false;
            this.props.logisticsStore.changeTypeList(newData, true, !target.newItem)
        }
    }
    render() {
        const tableHeader = [{
            title: '物流类型编码',
            dataIndex: 'logisticsType',
            key: 'logisticsType',
            render: (text, record) => this.renderColumns( record, 'logisticsType')
        }, {
            title: '物流类型名称',
            dataIndex: 'logisticsName',
            key: 'logisticsName',
            render: (text, record) => this.renderColumns( record, 'logisticsName')
        }, {
            title: '是否生鲜',
            dataIndex: 'isFresh',
            key: 'isFresh',
            render: (text, record) => this.renderColumns( record, 'isFresh')
        }, {
            title: '备注',
            dataIndex: 'district',
            key: 'district',
            render: (text, record) => this.renderColumns( record, 'district')
        }, {
            title: '操作',
            key: 'action',
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        { 
                            editable ?
                                <span>
                                    <a onClick={() => this.save(record.key)}>保存</a>
                                    <Popconfirm 
                                        title="确定取消修改?" 
                                        okText='确定'
                                        cancelText='取消'
                                        onConfirm={() => this.cancel(record.key)}
                                    >
                                        <a>取消</a>
                                    </Popconfirm>
                                </span>
                            : <a onClick={() => this.edit(record.key)}>编辑</a>
                        }
                    </div>
                );
            }
        }]
        const {
            typeList,
            loading
        } = this.props.logisticsStore
        return (
            <Spin spinning={loading}>
                <TypeForm />
                <Table 
                    columns={tableHeader} 
                    dataSource={typeList.slice()}
                    pagination={false} 
                />
            </Spin>
        )
    }
}
export default TypeComponent;