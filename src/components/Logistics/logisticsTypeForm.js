import React, { Component } from 'react'
import { Row, Col, Button} from 'antd'
import { observer, inject } from 'mobx-react'

@inject('logisticsStore', 'store') @observer
class TypeForm extends Component {
    constructor(props){
        super(props)
        this.state={}
    }
    render(){
        const {
            getTypeList,
            addTypeItem,
            typeCouldChange
        } = this.props.logisticsStore;
        return(
            <section className='searchForm'>
                <Row gutter={16}>
                    <Col span={12} style={{ margin:'0 0 16px 10px'}}>
                        <Button type="primary" onClick={addTypeItem} disabled = { !typeCouldChange } size="default">新增</Button>
                        <Button onClick={getTypeList} style={{marginLeft:'8px'}} size="default">刷新</Button>
                    </Col>
                </Row>
            </section>
        )
    }
}
export default TypeForm;