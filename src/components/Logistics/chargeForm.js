import React, {
    Component
} from 'react'
import {
    Row,
    Col,
    Button
} from 'antd'
import {
    observer,
    inject
} from 'mobx-react'

@inject('logisticsStore', 'store') @observer
class ChargeForm extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    addNewItem = () =>{
        this.props.logisticsStore.addChargeItem()
        const tableBody = document.getElementsByClassName('ant-table-body')[0]
        setTimeout(()=>{
            tableBody.scrollTop = tableBody.scrollHeight;
        },100)
    }
    render() {
            const {
                getChargeList,
                addChargeItem,
                chargeCouldChange
            } = this.props.logisticsStore;
            return (
            <section className='searchForm'>
                <Row gutter={16}>
                    <Col span={12} style={{ margin:'0 0 16px 10px'}}>
                        <Button type="primary" onClick={this.addNewItem} disabled = { !chargeCouldChange } size="default">新增</Button>
                        <Button onClick={getChargeList} style={{marginLeft:'8px'}} size="default">刷新</Button>
                    </Col>
                </Row>
            </section>
        )
    }
}
export default ChargeForm;