import React, {
    Component
} from 'react'
import {
    observer,
    inject
} from 'mobx-react'
import {
    Select,
    Row,
    Col,
    Button,
    Form
} from 'antd'
import * as mobx from 'mobx'
import '../ItemSchedule/itemSchedule.css'
const Option = Select.Option;
const FormItem = Form.Item

@inject('logisticsStore', 'store') @observer
class ShopListForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cityCode : ''
        }
    }
    componentDidMount() {
        const {
            getTypeList
        } = this.props.logisticsStore;
        const {shopId} = this.props.defaultValues;
        getTypeList();
        if(shopId) this.props.form.setFieldsValue({shopId})
        
    }
    componentWillUpdate() {
        const {
            cityCode
        } = this.state
        const {
            changedCity,
            cityChanged
        } = this.props
        if (changedCity) {
            this.props.form.setFieldsValue({
                shopId: []
            })
            cityChanged()
        }
    }
    handleSubmit = (e) => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.props.onSearch(values)
            }
        })
    }
    addNewItem = () => {
        this.props.history.push('/logistics/add')
    }
    beforeShow = () =>{
        const {
            city,
            getDistrict,
            getNoDistrict,
            showModal
        } = this.props
        if(city.code!==this.state.cityCode){
            getDistrict(city,()=>{
                this.setState({cityCode:city.code})
                showModal()
            })
        }else showModal()
    }
    clearIpt = () =>{
        this.props.form.resetFields();
        // this.props.clearIpt()
    }
    render(){
        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 10 },
              md: { span: 8 },
              lg: { span: 7 },
              xl: { span: 5 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 14 },
              md: { span: 16 },
              lg: { span: 16 },
              xl: { span: 14 },
            },
          }
        const {
            shopList,
            cityList,
            typeList
        } = this.props.logisticsStore;
        const { defaultValues,city} = this.props
        const { getFieldDecorator } = this.props.form
        return (
            <Form className='searchForm' onSubmit={this.handleSubmit}>
                <Row gutter={24}>
                    <Col className="gutter-row" span={6}>
                        <FormItem {...formItemLayout} label={'门店名称'}>
                            {getFieldDecorator('shopId', {
                                initialValue: defaultValues.shopId
                            })(
                                <Select size = 'default'  placeholder="请选择门店" mode="multiple">
                                {/* showSearch filterOption={(inputValue, option) => option.props.children.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0} */}
                                    {mobx.toJS(shopList).map(data =><Option key={data.shopId}>{data.shopNameAlias}</Option>)}
                                </Select>
                            )}
                        </FormItem>
                    </Col>
                    <Col className="gutter-row" span={6}>
                        <FormItem {...formItemLayout} label={'物流类型'}>
                            {getFieldDecorator('logisticsType', {
                                initialValue: defaultValues.logisticsType
                            })(
                                <Select size = 'default'  placeholder="请选择物流类型">
                                    {mobx.toJS(typeList).map(data =><Option key={data.logisticsType}>{data.logisticsName}</Option>)}
                                </Select>
                            )}
                        </FormItem>
                    </Col>
                </Row>
                <Row gutter={24}>
                    <Col span={12}>
                        <Button type="primary" onClick={ this.beforeShow } disabled = {city.code?false:true} size="default">设置不可配送区域</Button>
                    </Col>
                    <Col span={12} style={{ marginBottom:'16px', textAlign:'right'}}>
                        <Button type="primary" htmlType="submit" size="default">查询</Button>
                        <Button type="primary" onClick={this.addNewItem} size="default" style={{margin:'0 8px'}}>新增</Button>
                        <Button size="default" onClick = { this.clearIpt } >清空</Button>
                    </Col>
                </Row>
            </Form>
        )
    }
}
export default Form.create()(ShopListForm);