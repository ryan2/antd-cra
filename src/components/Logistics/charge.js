import React, {
    Component
} from 'react'
import {
    observer,
    inject
} from 'mobx-react'
import {
    Table,
    Select,
    Input,
    Popconfirm,
    Spin,
    message
} from 'antd'
import ChargeForm from './chargeForm'
import * as mobx from 'mobx'
import '../ItemSchedule/itemSchedule.css'
const Option = Select.Option;
@inject('logisticsStore', 'store') @observer
class EditableCell extends Component {
    constructor(props) {
        super(props);
    }
    render(){
        const {record, column, onChange} = this.props;
        if(record.editable){
            if (column !== 'logisticsType' && column !== 'freeShippingId') {
                return <Input style={{ margin: '-5px 0' }} value={record[column]} onChange={e => onChange(e.target.value)}/>
            }else if(column === 'logisticsType'){
                return (
                    <Select style={{ margin:'-5px 0',width:'50px' }} defaultValue={ record[column].toString() } onChange={value => onChange(value)} style = {{ width:'100%' }} >
                        {mobx.toJS(this.props.logisticsStore.typeList).map(data =><Option key={data.logisticsType}>{data.logisticsType}-{data.logisticsName}</Option>)}
                    </Select>
                )
            }else return <span>{record[column]}</span>
        }else{
            // if( column !== 'logisticsType') return <span>{record[column]}</span>;
            // else return <span>{record[column]?'是':'否'}</span>
            if(column==='logisticsType') return <span>{record[column]}-{record['logisticsName']}</span>
            else return <span>{record[column]}</span>
        } 
    }
}
@inject('logisticsStore', 'store') @observer
class ChargeComponent extends Component {
    componentWillMount() {
        const {
            getChargeList,
            getTypeList
        } = this.props.logisticsStore;
        getChargeList();
        getTypeList();
        this.getContainerHight()
        window.onresize = this.getContainerHight;
    }
    constructor(props) {
        super(props);
        this.state = {
            containerHeight: 0
        }
        // this.cacheData = this.props.logisticsStore.chargeList.map(item => ({ ...item }));
    }
    componentWillUnmount(props) {
        window.onresize = null
    }
    getContainerHight = () => {
        this.setState({
            containerHeight: document.body.offsetHeight - 176
        })
    }
    renderColumns(record, column) {
        return ( < EditableCell record = {
                record
            }
            column = {
                column
            }
            onChange = {
                value => this.handleChange(value, record, column)
            }
            />
        );
    }
    handleChange(value, record, column) {
        const newData = JSON.parse(JSON.stringify(this.props.logisticsStore.chargeList))
        const target = newData.find(item => record.freeShippingId === item.freeShippingId);
        if (target) {
            target[column] = value;
            this.props.logisticsStore.changeChargeList(newData);
        }
    }
    edit(key) {
        const newData = JSON.parse(JSON.stringify(this.props.logisticsStore.chargeList))
        const target = newData.find(item => key === item.freeShippingId);
        if (target) {
            target.editable = true;
            this.props.logisticsStore.changeChargeList(newData)
        }
    }
    save(key) {
        const newData = JSON.parse(JSON.stringify(this.props.logisticsStore.chargeList))
        const target = newData.find(item => key === item.freeShippingId);
        if (target) {
            this.props.logisticsStore.editChargeItem(target, () => {
                target.editable = false;
                this.props.logisticsStore.getChargeList()
            })
        }
    }
    cancel(key) {
        const newData = JSON.parse(JSON.stringify(this.props.logisticsStore.chargeList))
        const i = newData.findIndex(item => key === item.freeShippingId);
        if (i !== -1) {
            const target = newData[i];
            if (target.newItem) newData.splice(i, 1);
            else target.editable = false;
            this.props.logisticsStore.changeChargeList(newData, true, !target.newItem)
        }
    }
    render() {
        const { deleteChargeItem } = this.props.logisticsStore;
        const tableHeader = [{
            title: '序号',
            dataIndex: 'freeShippingId',
            key: 'freeShippingId',
            render: (text, record) => this.renderColumns( record, 'freeShippingId')
        }, {
            title: '免邮名称',
            dataIndex: 'freeShippingName',
            key: 'freeShippingName',
            render: (text, record) => this.renderColumns( record, 'freeShippingName')
        }, {
            title: '物流类型',
            dataIndex: 'logisticsType',
            key: 'logisticsType',
            width: 150,
            render: (text, record) => this.renderColumns( record, 'logisticsType')
        }, {
            title: '免邮金额条件',
            dataIndex: 'amount',
            key: 'amount',
            render: (text, record) => this.renderColumns( record, 'amount')
        }, {
            title: '最大金额',
            dataIndex: 'amountMax',
            key: 'amountMax',
            render: (text, record) => this.renderColumns( record, 'amountMax')
        }, {
            title: '减免金额',
            dataIndex: 'derateAmount',
            key: 'derateAmount',
            render: (text, record) => this.renderColumns( record, 'derateAmount')
        }, {
            title: '免邮减免重量(克)',
            dataIndex: 'weight',
            key: 'weight',
            render: (text, record) => this.renderColumns( record, 'weight')
        }, {
            title: '操作',
            key: 'action',
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        { 
                            editable ?
                                <span>
                                    <a onClick={() => this.save(record.freeShippingId)}>保存</a>
                                    <Popconfirm 
                                        title="确定取消修改?" 
                                        okText='确定'
                                        cancelText='取消'
                                        onConfirm={() => this.cancel(record.freeShippingId)}
                                    >
                                        <a>取消</a>
                                    </Popconfirm>
                                </span>
                            : <span>
                                <a onClick={() => this.edit(record.freeShippingId)}>修改</a>
                                <Popconfirm 
                                        title="确定删除?" 
                                        okText='确定'
                                        cancelText='取消'
                                        onConfirm={() => deleteChargeItem(record.freeShippingId)}>
                                    <a>删除</a>
                                </Popconfirm>
                              </span>
                        }
                    </div>
                );
            }
        }]
        const {
            chargeList,
            loading
        } = this.props.logisticsStore
        return (
            <Spin spinning={loading}>
                <ChargeForm />
                <Table 
                    columns = { tableHeader }
                    dataSource = { chargeList.slice() }
                    pagination = { false }
                    scroll = {{ y: this.state.containerHeight }}
                    rowKey = 'freeShippingId'
                    className = 'onlyYscroll'
                />
            </Spin>
        )
    }
}
export default ChargeComponent;