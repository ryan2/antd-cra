import React, { Component } from 'react'
import { Form, Input, Select, Spin, Cascader, Row, Col, Card, Switch, message, InputNumber} from 'antd'
import './index.css'
const FormItem = Form.Item
const Option = Select.Option

const statusSelect = [
    {
      key: -1,
      options: [
          {
              value: '禁用',
              key : '禁用'
          },
          {
              value: '启用',
              key: '启用'
          }
      ]
    },
    {
      key: 0,
      options: [
        {
            value: '未启用',
            key : '未启用'
        },
        {
            value: '启用',
            key: '启用'
        }
      ]
    },
    {
      key: 1,
      options: [
        {
            value: '启用',
            key : '启用'
        },
        {
            value: '禁用',
            key: '禁用'
        }
      ]
    }
]


class ItemDetail extends Component {
    state = {
        unableStatus: true,
    }

    constructor(props) {
        super(props)
        this.categoryList =[]
    }


    componentWillMount = () => {
        const newCategoryList = this.handleCategoryList(this.props.categoryList, true)
        this.categoryList = newCategoryList
      }
      
    
    statusChange = (value) =>{
        this.props.statusChange(value)
    }
    
    handleCategoryList = (data, disable) =>{
        let newCategoryList = []
        data.forEach( item => {
            item.value = item.categoryId 
            item.label = item.categoryName
            if(disable) {
                item.disabled = item.flag === 0 ? true : false
            }
            if(item.level === 1){
                item.children = data.filter( v => v.parentCategoryId === item.categoryId)
                newCategoryList.push(item)
            }
        })
        return newCategoryList
    }

    changePersonnal = (checked) =>{  
        if(this.props.unableState === true){
            message.error('请先修改启用状态')
        }else{
            this.props.changePersonnal(checked)
            this.props.changeChecked(checked)
        }
        
    }

    render (){
        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 20 },
        }
        const { getFieldDecorator } = this.props.form
        const selectWidth = {
            width: '100%'
        }
        const {formSource} = this.props
        let shelved = ''
        switch(formSource.shelved){
            case 0:
                shelved = '下架'
                break ;
            case 1:
                shelved = '上架'
                break ;
            default:
                shelved = ''
        }
        let refresh = ''
        switch(formSource.lockInventoryRefresh){
            case 0:
                refresh = '手工触发'
                break
            case 1:
                refresh = '每天'
                break ;
            case 2:
                refresh = '每周'
                break ;
            case 3:
                refresh = '每月'
                break ;
            default:
                refresh = ''
        }
        let status = ''
        switch(formSource.status){
            case -1:
                status = '禁用'
                break ;
            case 0:
                status = '未启用'
                break ;
            case 1:
                status = '启用'
                break ;
            default:
                status = ''
        }
                                    
         const statusOptions = statusSelect.find( item =>  item.key === this.props.status).options.map (data => <Option key={data.key}>{data.value}</Option>)
         let personnalStatus = this.props.personnalStatus === false ? true : false
        return (
            <Form onSubmit={this.handleSubmit}>
                <Spin spinning={this.props.formLoading}>
                <Card title = {'是否个性化设置'} extra = {<Switch checkedChildren="是" unCheckedChildren="否" onChange ={this.changePersonnal} checked = {this.props.checkStatus}/>}>
                <Row gutter ={16}>
                    <Col span = {12}>
                    <FormItem {...formItemLayout} label={'商品名称'}>
                    {getFieldDecorator('goodsName', {
                        rules: [{ required: true, message: '必须填写商品名称',},{max: 20, message:'最大长度不超过20!'}],
                        initialValue: formSource.goodsName
                    })(
                        <Input  style={selectWidth}   disabled={this.props.unableState || personnalStatus}/>            
                        )}
                    </FormItem>
                    </Col>               
                    <Col span = {12}>                
                    <FormItem {...formItemLayout} label={'英文名称'}>
                    {getFieldDecorator('goodsEnName', {
                        rules: [{ required: false },{max: 30, message:'最大长度不超过30!'}],
                        initialValue: formSource.goodsEnName
                    })(
                        <Input  style={selectWidth} disabled={this.props.unableState || personnalStatus}/>            
                        )}
                    </FormItem>
                    </Col>
                </Row>
                <Row gutter ={16}>
                    <Col span = {12}>                               
                    <FormItem {...formItemLayout} label={'商品规格'}>
                    {getFieldDecorator('spec', {
                        rules: [{ required: false },{max: 6, message:'最大长度不超过6!'}],
                        initialValue: formSource.spec
                    })(
                        <Input style={selectWidth} disabled={this.props.unableState || personnalStatus} />                    
                        )}
                    </FormItem>
                    </Col>
                    <Col span = {12}>  
                    <FormItem {...formItemLayout} label={'销售单位'}>
                    {getFieldDecorator('unit', {
                        rules: [{ required: false },{max: 6, message:'最大长度不超过6!'}],
                        initialValue: formSource.unit
                    })(
                        <Input style={selectWidth}  disabled={this.props.unableState || personnalStatus}/>                    
                        )}
                    </FormItem>
                    </Col>
                </Row>
                <Row gutter ={16}>
                    <Col span = {12}>
                    <FormItem {...formItemLayout} label={'商品条码'}>
                    {getFieldDecorator('barcode', {
                        rules: [{ required: true, message: '必须填写商品条码', },{max: 16, message:'最大长度不超过16!'}],
                        initialValue: formSource.barcode
                    })(
                        <Input style={selectWidth} disabled={this.props.unableState || personnalStatus} />                    
                        )}
                    </FormItem>
                    </Col>
                    <Col span = {12}>
                    <FormItem {...formItemLayout} label={'重量(g)'}>
                    {getFieldDecorator('weight', {
                        rules: [{ required: false }, {pattern: /^\d+$/, message: '商品重量为正整数!'}],
                        initialValue: formSource.weight
                    })(
                        <InputNumber style={selectWidth} disabled={this.props.unableState || personnalStatus} max = {999999} min={0}/>                 
                        )}
                    </FormItem>
                    </Col>
                </Row>
                <Row gutter ={16}>                
                <Col span = {12}>                                                             
                    <FormItem {...formItemLayout} label={'商品分类'}>
                    {getFieldDecorator('categoryId', {
                        rules: [{ required: true ,message: '必须选择商品分类',}],
                        initialValue: formSource.categoryId
                    })(
                        <Cascader   options = {this.categoryList} disabled={this.props.unableState || personnalStatus}/>                    
                        )}
                    </FormItem>
                    </Col>
                </Row>
                </Card>                   
                <Row gutter ={16} style = {{marginTop:'20px'}}> 
                    <Col span = {12}>                               
                    <FormItem {...formItemLayout} label={'安全库存'}>
                    {getFieldDecorator('safeInventory', {
                        rules: [{ required: false },{pattern: /^\d+$/, message: '安全库存为正整数!'}],
                        initialValue: formSource.safeInventory                    
                    })(
                        <InputNumber style={selectWidth} disabled={this.props.unableState} max ={9999} min={0}/>                 
                        )}
                    </FormItem>
                    </Col>         
                    <Col span = {12}>                                               
                    <FormItem {...formItemLayout} label={'锁定库存'}>
                    {getFieldDecorator('lockInventory', {
                        rules: [{ required: false },{pattern: /^-?\d+$/, message: '锁定库存为整数!'}],
                        initialValue: formSource.lockInventory                                        
                    })(
                        <InputNumber style={selectWidth} disabled={this.props.unableState} max ={9999}/>                 
                        )}
                    </FormItem>
                    </Col>                
                </Row>  
                <Row gutter ={16}>                                 
                    <Col span = {12}>                                      
                    <FormItem {...formItemLayout} label={'启用状态'}>
                    {getFieldDecorator('status', {
                        rules: [{ required: false }],
                        initialValue: status                                                                                
                    })(
                        <Select  style={selectWidth} onChange = {this.statusChange}>
                        {statusOptions}
                        </Select>
                        )}
                    </FormItem>
                    </Col>
                    <Col span = {12}>                                                                                                                              
                    <FormItem {...formItemLayout} label={'锁定价格'}>
                    {getFieldDecorator('lockPrice', {
                        rules: [{ required: false },{pattern: /^-?\d+\.?\d{0,2}$/, message: '最大为两位小数!'}],
                        initialValue: formSource.lockPrice                                                            
                    })(
                        <InputNumber style={selectWidth} disabled={this.props.unableState} max={9999.99}/>                 
                        )}
                    </FormItem>
                    </Col>                              
                </Row>
                <Row gutter ={16}> 
                    <Col span = {12}>                                                                                              
                    <FormItem {...formItemLayout} label={'上架状态'}>
                    {getFieldDecorator('shelved', {
                        rules: [{ required: false }],
                        initialValue: shelved                                                                                
                    })(
                        <Select  style={selectWidth} disabled={this.props.unableState}>                   
                            <Option value="下架">下架</Option>
                            <Option value="上架">上架</Option>
                        </Select>
                        )}
                    </FormItem>
                    </Col>                                                
                    <Col span = {12}>                                                                                                          
                    <FormItem {...formItemLayout} label={'更新间隔'}>
                    {getFieldDecorator('lockInventoryRefresh', {
                        rules: [{ required: false}],
                        initialValue: refresh                                                                                
                    })(
                        <Select  style={selectWidth} disabled={this.props.unableState}>
                            <Option value="手工触发">手工触发</Option>
                            <Option value="每天">每天</Option>
                            <Option value="每周">每周</Option>
                            <Option value="每月">每月</Option>
                        </Select>
                        )}
                    </FormItem>
                    </Col>                                                
                </Row>
                <Row gutter ={16}>                
                <Col span = {12}>                                                             
                    <FormItem {...formItemLayout} label={'价格系数'}>
                    {getFieldDecorator('priceRate', {
                        rules: [{ required: false },{pattern: /^-?\d+\.?\d{0,2}$/, message: '最大为两位小数!'}],
                        initialValue: formSource.priceRate
                    })(
                        <InputNumber style={selectWidth} disabled={this.props.unableState}  max={9.99} min={0.01}/>                                    
                        )}
                    </FormItem>
                    </Col>
                </Row>             
                </Spin>
            </Form>
        )      
    }
}

const StoreItemDetail = Form.create({})(ItemDetail)
export default StoreItemDetail