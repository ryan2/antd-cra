import React from 'react';
import { Card, Spin } from 'antd';

import  './index.css';

const ChartCard = ({
  loading = false, contentHeight, title, action, total, footer, children, ...rest
}) => {
  const content = (
    <div className={'chartCard'}>
      <div className={'meta'}>
        <span className={'title'}>{title}</span>
        <span className={'action'}>{action}</span>
      </div>
      {
        // eslint-disable-next-line
        total && <p className={'total'} dangerouslySetInnerHTML={{ __html: total }} />
      }
      <div className={'content'} style={{ height: contentHeight || 'auto' }}>
        <div className={contentHeight && 'contentFixed'}>
        {children}
        </div>
      </div>
      {
        footer && (
          <div className={'footer'}>
            {footer}
          </div>
        )
      }
    </div>
  );

  return (
    <Card
      bodyStyle={{ padding: '20px 24px 8px 24px' }}
      {...rest}
    >
      {<Spin spinning={loading}>{content}</Spin>}
    </Card>
  );
};

export default ChartCard;
