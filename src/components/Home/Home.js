import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as mobx from 'mobx'
import { observer,inject } from 'mobx-react'
import {Row, Col} from 'antd'
import ChartCard from '../Charts/ChartCard';
import MiniBar from '../Charts/MiniBar'
import Pie from '../Charts/Pie'
import { Chart, Geom, Axis, Tooltip, Legend} from 'bizcharts'
import { DataSet } from '@antv/data-set';

// const data = [
//   { name:'百度外卖', '周一.': 3000, '周二.': 4000, '周三.' :2000, '周四.': 6000, '周五.': 5500, '周六.': 7000, '周日.':9000, },
//   { name:'美团', '周一.': 5000, '周二.': 3000, '周三.' :6000, '周四.': 2000, '周五.': 7500, '周六.': 6000, '周日.': 7200, },
//   { name:'饿了么', '周一.': 7000, '周二.': 6000, '周三.' :4000, '周四.': 5500, '周五.': 9000, '周六.': 6000, '周日.': 3000,}
// ];


@inject('store', 'homeStore') @observer
class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  static contextTypes = {
    store: PropTypes.object,
  }
  
  componentWillMount() {
    this.props.homeStore.getData()
  }
  
  render() {
    let title
    if (this.props.store.collapse) {
      title = <h2 style={{ marginBottom: 16 ,color: '#108ee9'}}>首页</h2>
    } else {
      title = ''
    }
    const channelList = mobx.toJS(this.props.homeStore.channelList)
    const totalList = mobx.toJS(this.props.homeStore.totalList)
    const ds = new DataSet();
    const dv = ds.createView().source(channelList);
    dv.transform({
      type: 'fold',
      fields: [ 'Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'], // 展开字段集
      key: '日期', // key字段
      value: '订单量', // value字段
    });
    // let user = this.props.store.baseUser
    // let userCom = ''
    // if (user) {
    //   userCom = `Your Account: ${user.username}`
    // }
//     const visitData =[
//    {x: "2017-11-01", y: 3000},
//    {x: "2017-11-02", y: 6000},
//    {x: "2017-11-03", y: 8000},
//    {x: "2017-11-04", y: 2000},
//    {x: "2017-11-05", y: 4000},
//    {x: "2017-11-06", y: 6000}, 
//    {x: "2017-11-07", y: 9000},   
//    {x: "2017-11-08", y: 1000},
//    {x: "2017-11-09", y: 2000},
//    {x: "2017-11-10", y: 5000},
//    {x: "2017-11-11", y: 7050},
//    {x: "2017-11-12", y: 8200},
//    {x: "2017-11-13", y: 7000},
//    {x: "2017-11-14", y: 8200},
//    {x: "2017-11-15", y: 7900},
// ]
    return (
      <div>
        {title}
        <Row gutter={16}>
          <Col className="gutter-row" span={12}>
          <ChartCard
            bordered={false}
            title="订单总量"
            total = {'5,5840'}
            footer={'日均订单  3,100'}
            contentHeight={80}
          >
            <MiniBar
              animate ={true}
              height={80}
              data={totalList}
            />
          </ChartCard>
        </Col>
        <Col className="gutter-row" span={12}>
          <ChartCard
            bordered={false}
            title="在售商品"
            total = {'8,5432'}
            footer={'上架商品占比    90%'}
            contentHeight={80}
          >
            <Pie
                    animate={true}
                    percent={90}
                    subTitle="上架商品"
                    total="90%"
                    height={150}
                    lineWidth={2}
                    margin = {[0,0,0,200]}
                  />
          </ChartCard>
        </Col>
        </Row>
        <Row gutter={16} style= {{marginTop: '50px'}}>
        <ChartCard
            bordered={false}
            title="订单数量"
            footer={'各平台订单量 '}
            contentHeight={400}
          >
              <Chart height={400} data={dv} forceFit>
              <Axis name="日期" />
              <Axis name="订单量" />
              <Legend />
              <Tooltip crosshairs={{type : "y"}}/>
              <Geom type='interval' position="日期*订单量" color={'name'} adjust={[{type: 'dodge',marginRatio: 1/32}]} />
            </Chart>
          </ChartCard>

        </Row>
      </div>
    )
  }
}

export default Home
