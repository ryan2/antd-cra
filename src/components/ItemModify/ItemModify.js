import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { Card, Spin, message, Modal } from 'antd';
import ItemModifyForm from './ItemModifyForm'
import './itemModify.css'

@inject('itemStore', 'classifyStore')@observer
class ItemModify extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount () {
    this.props.classifyStore.getCategoryList({})
    this.props.itemStore.getProductItem(this.props.match.params)
  }

  handleSubmit = (params) => {
    params.itemCode = this.props.match.params.itemCode
    params.retailFormatId = this.props.match.params.retailFormatId
    this.props.itemStore.updateProductItem(params).then(res=> {
      if( res.status === 200 ) {
        message.success('操作成功')
        this.props.history.push('/item/schedule')
      } else {
        Modal.error({
          title: '操作失败',
          content: res.err
        })
      }
    })
  }

  handleCancel = () => {
    this.props.history.push('/item/schedule')
  }

  render() {
    const { productItem, loading, submitLoading, handleCategoryData } = this.props.itemStore
    const {categoryList} = this.props.classifyStore
    return (
      <div>
        <Card title="商品主档信息" bordered={false} noHovering>
          <Spin spinning={loading}>
            <ItemModifyForm defaultData={productItem}
                            categoryList={handleCategoryData(categoryList, true)}
                            onSubmit={this.handleSubmit}
                            onCancel={this.handleCancel}
                            loading={submitLoading}/>
          </Spin>
        </Card>
      </div>
    )
  }
}

export default ItemModify
