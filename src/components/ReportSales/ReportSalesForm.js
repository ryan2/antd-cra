import React, { Component } from 'react'
import { Row, Col, Form, Input, Button, message, DatePicker, Select  } from 'antd'
import FileOperation from '../FileOperation/FileOperation'
const FormItem = Form.Item
const Option = Select.Option

class ReportSalesForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startDate_createTime: null,
      endDate_createTime: null,
      startDate_recordTime: null,
      endDate_recordTime: null,
      startDate_acctime: null,
      endDate_acctime: null,
      //endOpen: false,
    }
  }

  componentDidMount() {
    this.props.getCityList()
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        /*values.startDate = this.state.startDate
        values.endDate = this.state.endDate*/

        /*if(!values.startDate || !values.endDate) {
          message.warning('必须选择开始日期和结束日期')
        }*/
        if (!values.channelOrderId && !values.city && !values.shopId && !values.endDate_acctime
            && !values.endDate_createTime && !values.endDate_recordTime && !values.startDate_acctime
            && !values.startDate_createTime && !values.startDate_recordTime ) {
          message.warning('请至少输入一项查询条件')
        } else {
          for (let key in values) {
            if(values[key]) {
              values[key] = this.state[key] || values[key]
            }
          }
          values.city = values.city === '全部' ? '' : values.city
          values.shopId = (values.shopId === '全部' || values.shopId === '0') ? '' : values.shopId
          this.props.onSearch(values)
        }
      }
    })
  }

  handleReset = () => {
    this.props.form.validateFields((err, values) => {
      if (!values.channelOrderId && !values.city && !values.shopId && !values.endDate_acctime
          && !values.endDate_createTime && !values.endDate_recordTime && !values.startDate_acctime
          && !values.startDate_createTime && !values.startDate_recordTime ) {
        message.warning('查询条件已经为空')
      } else {
        this.props.form.resetFields()
        // this.props.onSearch({})
      }
    })
  }

  disabledStartDate = (startValue, endDate) => {
    const endValue = this.props.form.getFieldValue(endDate);
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }

  disabledEndDate = (endValue, startDate) => {
    const startValue = this.props.form.getFieldValue(startDate);
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.valueOf() <= startValue.valueOf();
  }

  onChange = (field, date, dateString) => {
    this.props.form.setFieldsValue({
      [field]: date,
    })
    this.setState({
      [field]: dateString,
    })
  }

  onStartChange = (date, dateString, startDate) => {
    this.onChange(startDate, date, dateString);
  }

  onEndChange = (date, dateString, endDate) => {
    this.onChange(endDate, date, dateString);
  }

  /*handleStartOpenChange = (open) => {
    if (!open) {
      this.setState({ endOpen: true });
    }
  }

  handleEndOpenChange = (open) => {
    this.setState({ endOpen: open });
  }*/

  handleCityChange = (value) => {
    this.props.getShopListByCity({city: value})
    if(value !== undefined){
      this.props.form.setFieldsValue({
        shopId: []
      })
    }
  }

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
        md: { span: 8 },
        lg: { span: 6 },
        xl: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
        md: { span: 16 },
        lg: { span: 16 },
        xl: { span: 9 },
      },
    }
    const formItemLayout2 = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 5 },
        md: { span: 4 },
        lg: { span: 3 },
        xl: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 19 },
        md: { span: 20 },
        lg: { span: 19 },
        xl: { span: 20 },
      },
    }
    const { cityList, cityshopList } = this.props
    const { getFieldDecorator, getFieldsValue } = this.props.form
    const dateRange = [
      {id: 1, label: '下单时间', start: 'startDate_createTime', end: 'endDate_createTime'},
      {id: 2, label: '收入时间', start: 'startDate_recordTime', end: 'endDate_recordTime'},
      {id: 3, label: '记账时间', start: 'startDate_acctime', end: 'endDate_acctime'},
    ]
    return (
      <div className="searchForm">
        <Form
          onSubmit={this.handleSubmit} className="ant-advanced-custom-form"
        >
          <Row>
            {dateRange.map(item =>
              <Col className="gutter-row" xs={12} xl={8} key={item.id}>
                <FormItem {...formItemLayout2} label={item.label} >
                  <Col span={11}>
                    <FormItem>
                      {getFieldDecorator(item.start)(
                          <DatePicker placeholder="请选择开始日期"
                                      style={{width: '100%'}} size="default"
                                      disabledDate={(startValue) => this.disabledStartDate(startValue, item.end)}
                                      onChange={(date, dateString) => this.onStartChange(date, dateString, item.start)}
                              />)}
                    </FormItem>
                  </Col>
                  <Col span={2} style={{textAlign: 'center', fontSize: '16px'}}>-</Col>
                  <Col span={11}>
                    <FormItem>
                      {getFieldDecorator(item.end)(
                          <DatePicker placeholder="请选择结束日期"
                                      style={{width: '100%'}} size="default"
                                      disabledDate={(endValue) => this.disabledEndDate(endValue, item.start)}
                                      onChange={(date, dateString) => this.onEndChange(date, dateString, item.end)}
                              />)}
                    </FormItem>
                  </Col>
                </FormItem>
              </Col>
            )}
            <Col className="gutter-row" xs={12} xl={8}>
              <FormItem {...formItemLayout2} label='城市门店' >
                <Col span={11}>
                  <FormItem>
                    {getFieldDecorator('city')(
                        <Select onChange = {this.handleCityChange}
                                style={{width: '100%'}} size = 'default'
                                placeholder="请选择城市">
                          {cityList.map(data =><Option key={data.key}>{data.value}</Option>)}
                        </Select>
                    )}
                  </FormItem>
                </Col>
                <Col span={2} style={{textAlign: 'center', fontSize: '16px'}}>-</Col>
                <Col span={11}>
                  <FormItem>
                    {getFieldDecorator('shopId')(
                        <Select  style={{width: '100%'}} size = 'default' mode="tags"
                                 placeholder="请选择平台门店">
                          {cityshopList.map(data =><Option key={data.shopId}>{data.shopNameAlias}</Option>)}
                        </Select>
                    )}
                  </FormItem>
                </Col>
              </FormItem>
            </Col>
            <Col className="gutter-row" xs={12} xl={8}>
              <FormItem {...formItemLayout2} label='订单号' >
                <Col span={11}>
                  <FormItem>
                    {getFieldDecorator('channelOrderId')(
                        <Input placeholder="请输入订单号" size="default"/>
                    )}
                  </FormItem>
                </Col>
              </FormItem>
            </Col>
            {/*<Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label='城市'>
                {getFieldDecorator('city')(
                    <Select onChange = {this.handleCityChange} style={{width: '100%'}} size = 'default'
                            placeholder="请选择城市">
                      {cityList.map(data =><Option key={data}>{data}</Option>)}
                    </Select>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label='平台门店'>
                {getFieldDecorator('shopId')(
                    <Select  style={{width: '100%'}} size = 'default'
                             placeholder="请选择平台门店">
                      {cityshopList.map(data =><Option key={data.shopId}>{data.shopNameAlias}</Option>)}
                    </Select>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" xs={6} xl={8}>
              <FormItem {...formItemLayout} label={'订单号'}>
                {getFieldDecorator('channelOrderId')(
                    <Input placeholder="请输入订单号" size="default"/>
                )}
              </FormItem>
            </Col>*/}
            <Col className="gutter-row" style={{ textAlign: 'right', marginBottom: 24 }}
                 xs={12} lg={11} xl={8}>
              <Button type="primary" htmlType="submit">查询</Button>
              <FileOperation history={this.props.history} type="ExportSales" style={{margin: '0 8px'}}
                             buttonText="导出" buttonProps={{type: 'primary'}}
                             queryData={getFieldsValue()}/>
              <Button onClick={this.handleReset}>
                清空
            </Button>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}
export default Form.create()(ReportSalesForm)

