import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { observer, inject } from 'mobx-react'
import { Table } from 'antd'
import ReportSalesForm from './ReportSalesForm'
/*import FileOperation from '../FileOperation/FileOperation'*/
import '../ItemSchedule/itemSchedule.css'

// Global Component
@inject('salesStore', 'store')@observer
class ReportSales extends Component {
  state = {
    pagination: {
      showSizeChanger: true,
      pageSizeOptions: ['10', '20', '50', '100'],
      showTotal: () => `共${this.props.salesStore.total}条数据`,
    },
    searchData: {}
  }

  componentWillMount() {
  }

  componentDidMount() {
    //this.props.salesStore.getSalesList()
  }

  componentWillReceiveProps(nextProps) {
  }

  handlePageChange = (pagination) => {
    this.props.salesStore.getSalesList(Object.assign({
      page: pagination.current,
      pageSize: pagination.pageSize
    }, this.state.searchData))
  }

  handleSearchSubmit = (params) => {
    this.props.salesStore.getSalesList(params)
    this.setState({
      searchData: params
    })
  }

  render() {
    const columns = [
      {
        title: '城市',
        dataIndex: 'city',
        key: 'city',
        width: 80,
      }, {
        title: '渠道',
        dataIndex: 'channelName',
        key: 'channelName',
        width: 80,
      }, {
        title: '物流状态',
        dataIndex: 'logisticsState',
        key: 'logisticsState',
        width: 80,
      }, {
        title: '订单状态',
        dataIndex: 'orderStatus',
        key: 'orderStatus',
        width: 80,
      }, {
        title: '收银状态',
        dataIndex: 'silverStatus',
        key: 'silverStatus',
        width: 80,
      }, {
        title: '日期',
        dataIndex: 'createTime',
        key: 'createTime',
        width: 150,
      }, {
        title: '订单号',
        dataIndex: 'channelOrderId',
        key: 'channelOrderId',
        width: 150,
      }, {
        title: '门店',
        dataIndex: 'channelShopId',
        key: 'channelShopId',
        width: 100,
      }, {
        title: '订单金额',
        dataIndex: 'totalSaleValue',
        key: 'totalSaleValue',
        width: 80,
      }, {
        title: '商品金额',
        dataIndex: 'salevalue',
        key: 'salevalue',
        width: 80,
      }, {
        title: '包装袋',
        dataIndex: 'logisticsSfee',
        key: 'logisticsSfee',
        width: 80,
      }, {
        title: '商家折扣',
        dataIndex: 'totalDiscValue',
        key: 'totaldiscValue',
        width: 80,
      }, {
        title: '平台折扣',
        dataIndex: 'expressReceivables',
        key: 'expressReceivables',
        width: 80,
      }, {
        title: '整单折扣',
        dataIndex: 'pickupReceivables',
        key: 'pickupReceivables',
        width: 80,
      }, {
        title: '佣金',
        dataIndex: 'freightDiscount',
        key: 'freightDiscount',
        width: 80,
      }, {
        title: '应收款',
        dataIndex: 'point',
        key: 'point',
        width: 80,
      }, {
        title: '配送费',
        dataIndex: 'transportCost',
        key: 'transportCost',
        width: 80,
      }, {
        title: '顾客付款金额',
        dataIndex: 'payValue',
        key: 'payValue',
        width: 100,
      }, {
        title: '原始订单号',
        dataIndex: 'originalOrderId',
        key: 'originalOrderId',
        width: 100,
      }, {
        title: '收入时间',
        dataIndex: 'recordTime',
        key: 'recordTime',
        width: 150,
      }, {
        title: '验货时间',
        dataIndex: 'inspectionTime',
        key: 'inspectionTime',
        width: 150,
      }, {
        title: '记账时间',
        dataIndex: 'acctime',
        key: 'acctime',
        width: 150,
      }
    ]
    const { loading, salesList, total, cityList, cityshopList, getCityList, getShopListByCity} = this.props.salesStore;
    const pagination = Object.assign({total: total}, this.state.pagination);
    return (
        <div>
          {this.props.store.collapse ?
              <h2 style={{marginBottom: 16}}>
                <Link to="/item/schedule">销售报表</Link>
              </h2> : null
          }
          <ReportSalesForm history={this.props.history}
                           cityList={cityList}
                           cityshopList={cityshopList}
                           getCityList={getCityList}
                           getShopListByCity={getShopListByCity}
                           onSearch={this.handleSearchSubmit}/>
          <div style={{width: '100%'}}>
            <Table style={{width: '100%'}} scroll={{x: 2200}}
                   loading={loading}
                   columns={columns}
                   dataSource={salesList.slice()}
                   pagination={pagination}
                   onChange={(pagination) => this.handlePageChange(pagination)}/>
          </div>
        </div>
    )
  }
}

export default ReportSales
