import React, { Component } from 'react'
import { observer } from 'mobx-react'
import '../../css/ItemBind.css'
import { Row, Col, Form, Select, Input, Button, Tree } from 'antd'
const Option = Select.Option
const FormItem = Form.Item
const TreeNode = Tree.TreeNode

const gData = [
  {
    key: '1',
    label: '奶制品',
    children: [
      {
        key: '1-1',
        label: '酸奶',
        children: [
          {
            key: '1-1-1',
            label: '酸奶A'
          }
        ]
      },
      {
        key: '1-2',
        label: '含乳饮料',
        children: [
          {
            key: '1-2-1',
            label: '含乳饮料A'
          }
        ]
      },
      {
        key: '1-3',
        label: '常温奶',
        children: [
          {
            key: '1-3-1',
            label: '常温奶A'
          }
        ]
      }
    ]
  },
  {
    key: '2',
    label: '食品粮油',
    children: [
      {
        key: '2-1',
        label: '米面',
        children: [
          {
            key: '2-1-1',
            label: '大米'
          },
          {
            key: '2-1-2',
            label: '面粉'
          },
          {
            key: '2-1-3',
            label: '方便面'
          },
          {
            key: '2-1-4',
            label: '面条'
          }
        ]
      }
    ]
  }
];

class LeftComponent extends React.Component {
  state = {
    gData
  }
  render() {
    const loop = data => data.map((item) => {
      if (item.children && item.children.length) {
        return <TreeNode key={item.key} title={item.label}>{loop(item.children)}</TreeNode>;
      }
      return <TreeNode key={item.key} title={item.label} />;
    });
    return (
      <div>
        <Tree
          className="draggable-tree"
        >
          {loop(this.state.gData)}
        </Tree>
      </div>
    );
  }
}


@observer
// Query List Form Component
class ItemBindList extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentDidMount() {
  }
  handleSubmit = (e) => {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values)
      }
    })
  }
  render() {
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
    }
    const { getFieldDecorator } = this.props.form
    const selectWidth = {
      width: '100%'
    }
    return (
      <div>
        <Form
          onSubmit={this.handleSubmit} className="ant-advanced-custom-form"
        >
          <Row gutter={16}>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'部门'}>
                {getFieldDecorator('input-dept')(
                  <Input placeholder="dept" style={selectWidth} />
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'单品编码'}>
                {getFieldDecorator('input-itemcode')(
                  <Input placeholder="item code" style={selectWidth} />
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品名称'}>
                {getFieldDecorator('input-itemname')(
                  <Input placeholder="item name" style={selectWidth} />
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品条码'}>
                {getFieldDecorator('input-barcode')(
                  <Input placeholder="bar code" style={selectWidth} />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'状态'}>
                {getFieldDecorator('select-status')(
                  <Select placeholder="status">
                    <Option value="">请选择</Option>
                    <Option value="1">已分类</Option>
                    <Option value="0">未分类</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" style={{ textAlign: 'right' }} span={18}>
              <Button type="primary" size="large" htmlType="submit">查询</Button>
              <Button type="primary" size="large" style={{ marginLeft: 8 }}>批量上传</Button>
              <Button size="large" style={{ marginLeft: 8 }} onClick={this.handleReset}>
                清空
              </Button>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}
const ItemBindListForm = Form.create()(ItemBindList)

@observer
class RightComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <div>
        <ItemBindListForm />
      </div>
    )
  }
}

@observer
class ItemBind extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <div>
        <Row gutter={8}>
          <Col span={4}>
            <div className="itemBind-gutter-row" >
              <LeftComponent />
            </div>
          </Col>
          <Col span={20}>
            <div className="itemBind-gutter-row" >
              <RightComponent />
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

export default ItemBind
