import React, {
    Component
} from 'react'
import RecordTable from '../RecordTable/index'
import {
    observer,
    inject
} from 'mobx-react'
import ImportRecordListForm from './ImportRecordList'

@inject('store') @observer
class importRecord extends Component {
    state = {
        page: undefined,
        pageSize: undefined,
        formList: {}
    }

    handleSubmit = (page, pageSize, type, operator, startDate, endDate) => {
        this.setState({
            page: this.refs.RecordTable.wrappedInstance.state.page,
            pageSize: this.refs.RecordTable.wrappedInstance.state.pageSize
        })
        this.refs.RecordTable.wrappedInstance.changeMsgData(this.refs.RecordTable.wrappedInstance.state.page, this.refs.RecordTable.wrappedInstance.state.pageSize, type, operator, startDate, endDate)
    }

    getFormList = formList => {
        this.setState({
            formList
        })
    }
    render () {
        let title
        if (this.props.store.collapse) {
          title = <h2 style={{ marginBottom: 16 ,color: '#108ee9'}}>导入记录</h2>
        } else {
          title = ''
        }
        return (
            <div>
            {title}
            <ImportRecordListForm 
                handleSubmit = {this.handleSubmit}
                page = {this.state.page}
                pageSize = {this.state.pageSize}
                getFormList = {this.getFormList}
            />
            <RecordTable
                ref = 'RecordTable'
                type = 'importRecord'
                formList = {this.state.formList}
                history = { this.props.history }
            />
            </div>
        )
    }
}

export default importRecord