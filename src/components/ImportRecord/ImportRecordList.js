import React, {
  Component
} from 'react'
import {
  Row,
  Col,
  Form,
  Select,
  Input,
  Button,
  DatePicker,
  message
} from 'antd'
import {
  inject,
  observer
} from 'mobx-react';
// import { observer,inject } from 'mobx-react'
const Option = Select.Option
const FormItem = Form.Item


// @inject('shopStore')@observer
@inject('store') @observer
class ImportRecordList extends Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.formList = {}
  }

  state = {
    startDate: '',
    endDate: '',
  }

  componentDidMount() {
    this.props.form.setFieldsValue({
      operator: this.props.store.baseUser.userName
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (this.state.endDate < this.state.startDate) {
        message.error('时间区间错误')
      } else if (err) {
        message.error(err)
      } else {
        const page = this.props.page
        const pageSize = this.props.pageSize
        const type = values.type
        const operator = values.operator
        const startDate = this.state.startDate
        const endDate = this.state.endDate
        const formList = {
          type: type,
          operator: operator,
          startDate: startDate,
          endDate: endDate,
        }
        this.formList = formList
        if (startDate === undefined && endDate !== undefined) {
          message.error('请选择开始日期')
        } else if (endDate === undefined && startDate !== undefined) {
          message.error('请选择结束日期')
        } else {
          this.props.getFormList(formList)
          this.props.handleSubmit(page, pageSize, type, operator, startDate, endDate)
        }
      }
    })
  }

  handleReset = () => {
    this.formList = {
      taskType: undefined,
      operator: '',
      shopId: undefined,
      channelId: undefined,
      startDate: undefined,
      endDate: undefined
    }
    this.props.getFormList(this.formList)
    this.props.form.resetFields()
    // this.props.handleSubmit()
  }

  render() {
    const formItemLayout = {
      labelCol: {      
        xs: { span: 24 },
  
        sm: { span: 10 },
  
        md: { span: 8 },
  
        lg: { span: 7 },
  
        xl: { span: 5 },
  
      },
      wrapperCol: {         
        xs: { span: 24 },
  
        sm: { span: 14 },
  
        md: { span: 16 },
  
        lg: { span: 16 },
  
        xl: { span: 14 },
      }
    }
    const {
      getFieldDecorator
    } = this.props.form
    const selectWidth = {
      width: '100%'
    }
    const dateFormat = 'YYYY-MM-DD'
    return (
      <div>
        <Form
          onSubmit={this.handleSubmit} className="ant-advanced-custom-form"
        >
          <Row gutter={16}>
            <Col className="gutter-row" span={6} >
              <FormItem {...formItemLayout} label={'操作类型'} >
                {getFieldDecorator('type')(
                  <Select placeholder="全部" style={selectWidth} size = 'default'>
                    <Option value="">全部</Option>
                    <Option value="ImportProduct">主档商品</Option>
                    <Option value="ImportGoods">门店商品</Option>
                    <Option value="ImportImage">商品图片</Option>    
                    <Option value="syncGoods">商品稽核</Option>
                    <Option value="syncInventory">库存同步</Option> 
                    <Option value="ImportO2oGoodsFilter">主档商品筛选</Option>                                                                                                                                                                                                                                                                                                                                                                                     
                  </Select>
                  )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'操作人'}>
                {getFieldDecorator('operator')(
                  <Input placeholder="请输入操作人" style={selectWidth} size = 'default'/>
                  )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'开始日期'}>
                {getFieldDecorator('startDate')(
                  <DatePicker placeholder="请选择开始日期" format={dateFormat} style={selectWidth} onChange = {(date,dateString) =>{this.setState({startDate : dateString})}} size = 'default'/>
                  )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'结束日期'}>
              {getFieldDecorator('endDate')(
              <DatePicker  placeholder="请选择结束日期" format={dateFormat} style={selectWidth} onChange = {(date,dateString) =>{this.setState({endDate : dateString})}} size = 'default'/>
                  )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col className="gutter-row" style={{ textAlign: 'right' }} span={24}>             
              <Button type="primary" size="default" htmlType="submit" >查询</Button> 
              <Button size="default" style={{ marginLeft: 8 }} onClick={this.handleReset}>清空</Button> 
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}
const ImportRecordListForm = Form.create()(ImportRecordList)
export default ImportRecordListForm