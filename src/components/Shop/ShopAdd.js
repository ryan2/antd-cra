import React, { Component } from 'react'
import { Form, Input, Select, message} from 'antd'
import { observer,inject} from 'mobx-react'
import * as mobx from 'mobx'
const FormItem = Form.Item
const Option = Select.Option

@inject('shopStore')@observer
class ShopAdd extends Component {

    state = {
        channelList: [],
        shopList: {},
        changeShopList: [],
        value : '',
        channelShopNameValue:'',
        channelValue: '',
        channelShopValue: '',
    }
    
    componentWillMount = ()  =>{
    }

    channelChange = (value) =>{
        this.setState({
            changeShopList : mobx.toJS(this.props.shopStore.shopList)[value].shopList,
            value : mobx.toJS(this.props.shopStore.shopList)[value].shopList[0].shopNameAlias,
            channelValue: value
        })
        this.props.shopChange(mobx.toJS(this.props.shopStore.shopList)[value].shopList[0].shopId)        
        this.props.channelChange(value)
    }

    shopChange = (value) =>{
        const chooseList = this.state.changeShopList.find( v => v.shopNameAlias === value)
        const chooseValue = chooseList.shopId
        this.props.shopChange(chooseValue)
        this.setState({
            value:chooseList.shopNameAlias
        })
    }

    changeValue = () =>{
        this.setState({
            value:'',
            channelShopNameValue:'',
            channelValue: '',
            channelShopValue: '',
        })
        
    }

    channelShopChange = (e) =>{
        e.preventDefault()
        if(e.target.value.length > 30){
            message.error('外卖店编码长度不超过30!')
        }else{
            this.props.channelShopChange(e.target.value)
            this.setState({
                channelShopValue: e.target.value
            })   
        } 
    }

    channelShopNameChange = (e) => {
        e.preventDefault()
        if(e.target.value.length > 20){
            message.error('店铺名称长度不超过20!')
        }else{
            this.props.channelShopNameChange(e.target.value)
            this.setState({
                channelShopNameValue: e.target.value
            })
        }
    }

    checkChannelShopId = async () => {
        let channelId = this.state.channelValue
        const channelShopId = this.state.channelShopValue
        await this.props.shopStore.checkShopId(channelId, channelShopId)        
        let reg = /^[0-9]*$/
        if(reg.test(channelShopId) === false){
            message.error('外卖店编码只能为数字! 请修改!')
            this.props.checkErr(false)            
        }else if(channelId === ''){
            message.error('渠道不能为空')
            this.props.checkErr(false)                        
        }else if(this.props.shopStore.checkChannelShopId === false){           
            message.error('外卖店编码已存在! 请修改!')
            this.props.checkErr(false)                                    
        }else{
            this.props.checkErr(true)
        }
    }

    render() {
        const channelList = mobx.toJS(this.props.shopStore.channelList)
        const channelOptions =channelList.map(data =><Option key={data.channelId}>{data.channelName}</Option>)
        const shopOptions =this.state.changeShopList.map(data =><Option key={data.shopNameAlias}>{data.shopNameAlias}</Option>)
        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 20 },
          }
        const selectWidth = {
            width: '100%'
        }
        return(
            <Form>
                <FormItem {...formItemLayout} label = {'渠道'}>
                        <Select onChange = {this.channelChange} value = {this.state.channelValue}>
                            {channelOptions}
                        </Select>
                </FormItem>
                <FormItem {...formItemLayout} label = {'家乐福店'}> 
                        <Select onChange = {this.shopChange}  value ={this.state.value} showSearch>
                             {shopOptions}
                        </Select> 
                </FormItem>
                <FormItem {...formItemLayout} label = {'店铺名称'}>
                        <Input  style={selectWidth} onChange = {this.channelShopNameChange} value = {this.state.channelShopNameValue}/>                                          
                </FormItem>
                <FormItem {...formItemLayout} label = {'外卖店编码'}>
                        <Input  style={selectWidth} onChange = {this.channelShopChange} value = {this.state.channelShopValue} onBlur ={this.checkChannelShopId}/>                        
                </FormItem>
            </Form>
        )
    }
}


export default ShopAdd