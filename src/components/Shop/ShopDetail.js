import React, { Component } from 'react'
import { Col, Form, Input, Button, Spin, Switch} from 'antd'
const FormItem = Form.Item
const { TextArea } = Input

class ShopDetail extends Component {
    constructor(props) {
      super(props)
      this.state = {}
    }

    state = {
      loading: true,
      formLoading: true,
      preOrder: undefined
    }
  
    handleSubmit = (e) =>{
      e.preventDefault()
      this.props.form.validateFieldsAndScroll((err, values) => {
        if(!err){
        let channelId = this.props.channelId
        const channelShopId = this.props.channelShopId
        const shopId = this.props.shopId
        const address = values.address
        const serviceTel = values.serviceTel
        const contactTel = values.storeTel
        const notice = values.notice
        const preOrder = this.state.preOrder
        this.props.handleOkStore(channelId, channelShopId, shopId, address, serviceTel, contactTel, notice, preOrder)
        this.handleReset()
        this.props.closeModalStore()
        const formLoading = this.state.formLoading
        this.props.changeFormLoading(formLoading)
        }
      })

    }

    handleReset = () => {
      this.props.form.resetFields()
    }

    handleCancel = () =>{
      this.props.closeModalStore()
      const formLoading = true
      this.props.changeFormLoading(formLoading)
    }

    preOrderChange = (checked)  =>{
      let preOrder = checked === true ? '1' : '0'
      this.setState({
        preOrder
      })
      this.props.preOrderChange(checked)
    }
  
    render() {
      const formItemLayout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 20 },
      }
      const { getFieldDecorator } = this.props.form
      const selectWidth = {
        width: '100%'
      }
      return (
        <Form onSubmit={this.handleSubmit}>
          <Spin spinning={this.props.formLoading}>
          <FormItem {...formItemLayout} label={'店名'}>
            {getFieldDecorator('storeName', {
              initialValue:this.props.formSource.channelShopName
            })(
              <Input  style={selectWidth} disabled={true} />            
              )}
          </FormItem>
          <FormItem {...formItemLayout} label={'地址'}>
            {getFieldDecorator('address', {
              initialValue:this.props.formSource.address,
              rules:[{max: 80, message:'地址长度不超过80!'}]  
            })(
              <Input  style={selectWidth} />
              )}
          </FormItem>
          <FormItem {...formItemLayout} label={'服务号码'}>
            {getFieldDecorator('serviceTel', {
              rules: [ {pattern: /^[1][3,4,5,8][0-9]{9}$/, message: 'Please check your tel!'}],
              initialValue:this.props.formSource.serviceTel
            })(
              <Input type="tel"  style={selectWidth} />
              )}
          </FormItem>
          <FormItem {...formItemLayout} label={'门店号码'}>
            {getFieldDecorator('storeTel', {
              rules: [{pattern: /^[1][3,4,5,8][0-9]{9}$/, message: 'Please check your tel!'}],
              initialValue:this.props.formSource.contactTel
            })(
              <Input type="tel"  style={selectWidth} />
              )}
          </FormItem>
          <FormItem {...formItemLayout} label={'公告'}>
            {getFieldDecorator('notice', {
              initialValue:this.props.formSource.notice,
              rules:[{max: 200, message:'公告长度不超过200!'}]             
            })(
              <TextArea   style={selectWidth} />
              )}
          </FormItem>
          <FormItem {...formItemLayout} label={'预订单'}>
            {getFieldDecorator('preOrder', {            
            })(
              <Switch checkedChildren="开" unCheckedChildren="关" checked = {this.props.formSource.preOrder} onChange ={this.preOrderChange}/>
              )}
          </FormItem>
          <Col className="gutter-row" style={{ textAlign: 'right' }} >
            {<Button size="large"  onClick={this.handleCancel}>取消</Button>}
            <Button type="primary" style={{ marginLeft: 12 }} size="large" htmlType="submit">保存</Button>
          </Col>
          </Spin>
        </Form>
      )
    }
  
  }
  const ShopDetailForm = Form.create({})(ShopDetail)
  export default ShopDetailForm