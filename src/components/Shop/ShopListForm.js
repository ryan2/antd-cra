import React, { Component } from 'react'
import { Row, Col, Form, Select, Input, Button} from 'antd'
import { observer,inject } from 'mobx-react'
import * as mobx from 'mobx'
const Option = Select.Option
const FormItem = Form.Item

@inject('shopStore', 'store')@observer
class ShopList extends Component {
    constructor(props) {
      super(props)
      this.state = {}
    }

    state = {
      click: false
    }

    componentDidMount() {
      this.props.shopStore.getCityList()
      this.props.store.getChannelList()      
    }

    componentWillUnmount = () => {
      this.props.shopStore.clearShopList()
    }

    handleSubmit = (e) => {
      const page = this.props.pagination.page
      const pageSize = this.props.pagination.pageSize
      e.preventDefault()
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
          const isOpen = values.status || 0
          const channelId = values.channel || 0
          const regionId = values.area || '' 
          const city = values.city === '全部' ? '' : values.city
          const channelShopName = values.store || '' 
          const shopId = (values.shopId === '全部' || values.shopId === '0') ? '' : values.shopId           
          this.props.changeMsgData(page, pageSize, isOpen, channelId, regionId, city, shopId)
        }
      })
    }

    cityChange =  (value) =>{
       this.props.shopStore.getShopListByCity(value) 
          if(value !== undefined){
            this.props.form.setFieldsValue({
               shopId:[]   
            })      
          }
    }

    handleReset = () => {
      this.props.form.resetFields()
      // this.props.changeMsgData()
    }

    exportShopList = (e) =>{
      e.preventDefault()
      this.props.form.validateFieldsAndScroll( async (err, values) => {
        if (!err) {
          const isOpen = values.status || 0
          const channelId = values.channel || 0
          const regionId = values.area || '' 
          const city = values.city || ''
          const channelShopName = values.store || ''                     
          await this.props.shopStore.exportChannelShopList(isOpen, channelId, regionId, city, channelShopName)
        }
      })
    }

    render() {
      const channelList = mobx.toJS(this.props.store.channelList)
      const channelOptions = channelList.map( item => <Option key = {item.channelId}>{item.channelName}</Option>)
      const formItemLayout = {
        labelCol: {      
          xs: { span: 24 },
   
          sm: { span: 10 },
   
         md: { span: 8 },
   
          lg: { span: 7 },
   
          xl: { span: 5 },
   
        },
        wrapperCol: {         
          xs: { span: 24 },
   
          sm: { span: 14 },
   
          md: { span: 16 },
   
          lg: { span: 16 },
   
          xl: { span: 14 },
        },
      }
      const { getFieldDecorator } = this.props.form
      const selectWidth = {
        width: '100%'
      }
      return (
        <div>
          <Form
            onSubmit={this.handleSubmit} className="ant-advanced-custom-form"
          >
            <Row gutter={16}>
              <Col className="gutter-row" span={6} >
                <FormItem {...formItemLayout} label={'门店状态'} >
                  {getFieldDecorator('status', {
                    rules: [{ required: false, message: 'Please select store status!' }],
                  })(
                    <Select placeholder="全部" style={selectWidth} size = 'default'>
                      <Option value="">全部</Option>                   
                      <Option value="0">暂停营业</Option>
                      <Option value="1">正常营业</Option>
                      <Option value="-1">禁用</Option>                      
                    </Select>
                    )}
                </FormItem>
              </Col>
              <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label='渠道'>
                  {getFieldDecorator('channel', {
                    rules: [{ required: false, message: 'Please select channel!' }],
                  })(
                    <Select placeholder="全部" style={selectWidth} size = 'default'>
                      <Option value="">全部</Option>                                       
                      {channelOptions}
                    </Select>
                    )}
                </FormItem>
              </Col>
              <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'城市'}>
                  {getFieldDecorator('city', {
                    rules: [{ required: false, message: 'Please input city!' }],
                  })(
                    <Select onChange = {this.cityChange} style={selectWidth} size = 'default'  placeholder="请选择城市">
                      {mobx.toJS(this.props.shopStore.cityList).map(data =>(<Option key={data.key}>{data.value}</Option>))}
                    </Select>
                    )}
                </FormItem>
              </Col>
              <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'家乐福门店'}>
                  {getFieldDecorator('shopId', {
                    rules: [{ required: false, message: 'Please input store!' }],
                  })(
                    <Select  style={selectWidth} size = 'default'  placeholder="请选择门店" mode="multiple">
                      {mobx.toJS(this.props.shopStore.cityshopList).map(data =><Option key={data.shopId}>{data.shopNameAlias}</Option>)}
                    </Select>
                    )}
                </FormItem>
              </Col>
            </Row>
            <Row gutter={16} style = {{marginBottom: 16}}>
              <Col className="gutter-row" span={16}>
                {this.props.canEdit ?
                <div>
                  <Button type="primary" onClick={this.props.setStatus} style ={{marginRight:'8px'}}>设置营业状态</Button>
                  <Button type="primary" onClick={this.props.setHours} style ={{marginRight:'8px'}}>设置营业时间</Button>
                  <Button type="primary" onClick={this.props.setNotice} style ={{marginRight:'8px'}}>设置店铺公告</Button>
                  <Button type="primary" onClick={this.props.setAdd} style ={{marginRight:'8px'}}>新增店铺信息</Button>
                </div> : null
                }
              </Col>
              <Col className="gutter-row" style={{ textAlign: 'right' }} span={8}>             
                <Button type="primary" size="large" htmlType="submit" size = 'default'>查询</Button> 
                <Button size="large" style={{ marginLeft: 8 }} onClick={this.handleReset} size = 'default'>清空</Button> 
              </Col>
            </Row>
          </Form>
        </div>
      )
    }
  }
  const ShopListForm = Form.create()(ShopList)
  export default ShopListForm