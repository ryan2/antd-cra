import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { observer,inject } from 'mobx-react'
import moment from 'moment'
import * as mobx from 'mobx'
import  ShopListForm  from "./ShopListForm"
import ShopDetailForm from './ShopDetail'
import ShopAdd from './ShopAdd'
import ShopLogTable  from '../LogQuery/ShopLog/shopLogTable'
import { Form, Input, Button, Table, Modal, Radio, message, Calendar, DatePicker, TimePicker, Row, Col, Icon} from 'antd'
const FormItem = Form.Item
const RadioGroup = Radio.Group
const { RangePicker } = DatePicker
const { TextArea } = Input


function range(start, end) {
  const result = [];
  for (let i = start; i < end; i++) {
    result.push(i);
  }
  return result;
}

function getAllDate(startDate, endDate){
  var bd = new Date(startDate),be = new Date(endDate);  
  var bd_time = bd.getTime(), be_time = be.getTime(),time_diff = be_time - bd_time;  
  var d_arr = [];  
  for(var i=0; i<= time_diff; i+=86400000){  
          var ds = new Date(bd_time+i);  
          d_arr.push(moment(ds).format('YYYY-MM-DD'))  
  }  
  return d_arr
}


//将时间转换为moment
function getMomentByTime(time){
  const nowDate = moment().format('YYYY-MM-DD')
  const date = `${nowDate} ${time}`
  return moment(date)
}

@inject('shopStore')@inject('store')@observer
class ShopListComponent extends Component {

  constructor(props){
    super(props)
    this.page = 0
    this.pageSize = 10
    this.pagination = {}
  }

  state = {
    visibleStatus: false,
    visibleHours: false,
    visibleNotice: false,
    visibleStore: false,
    visibleAdd : false,
    statusValue: 1,
    selectedKeys: '',
    selectedRows:[],
    rangeDate: [],
    rangeTime: [],
    timeStart: null,
    timeEnd: null,
    dataSource: [],
    loading: false,
    pagination: {},
    formSource: {},
    formLoading: true,
    channelId: '',
    channelShopId: null,
    shopId: null,
    noticeData: '',
    timeResult : [],
    channelIdValue: '',
    shopIdValue : '',
    channelShopIdValue: '',
    channelShopNameValue: '',
    channelList: [],
    shopList: [],
    logChannelId : undefined,
    logShopId: undefined,
    logChannelShopId: undefined,
    visibleLog: false,
    err : true,
    visibleTime : false,
    timeTwo: false,
    timeThree: false,
    timeStartOne: '',
    timeStartTwo: '',
    timeStartThree: '',   
    timeEndOne: '',
    timeEndTwo: '',
    timeEndThree: '',
    openTimeOne:'',
    openTimeTwo:'',
    openTimeThree:'',
    timeStartOneValue: null,
    timeStartTwoValue: null,
    timeStartThreeValue: null,
    timeEndOneValue: null,
    timeEndTwoValue: null,
    timeEndThreeValue: null,
    rangeDateValue: [],
    chooseOpenTime: undefined,
    selectDateStatus : false,
    disabledTimeOne: false,
    disabledTimeTwo: false, 
    disabledTimeThree: false,     
  }

  componentWillMount() {
    this.changeMsgData()
  }
  
  componentWillUnmount(){ 
    this.setState = (state,callback)=>{
      return;
    };  
}


  showModalStatus = () => {
    this.setState({
      visibleStatus: true,
    })
  }

  showModalHours = () => {
    let rangeDateValue = []
    rangeDateValue.push(moment(moment().format('YYYY-MM-DD')))
    this.setState({
      visibleHours: true,
      rangeDateValue
    })
  }

  showModalNotice = () => {
    this.setState({
      visibleNotice: true,
    })
  }

  handleOkStatus = async () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    })
    const isOpen = this.state.statusValue
    const selectRow = []
    this.state.selectedRows.forEach(data => {
      selectRow.push({
        channelId : data.channelId,
        channelShopId: data.channelShopId,
        shopId: data.shopId,
        isOpen: isOpen
      })
    })
    const result =  await this.changeShopisOpen(selectRow)
    if(result.data.status === 200){
      this.setState({
        visibleStatus: false,
        confirmLoading: false,
      })
      message.success('保存成功！')
    }else{
      message.error('保存失败')
    }
    this.refs.ShopListForm.validateFieldsAndScroll( (err, values) =>{
      const isOpen = values.status
      const city = values.city 
      const shopId = (values.shopId === '全部' || values.shopId === '0') ? '' : values.shopId
      const channelId = values.channel
      this.changeMsgData(1, 10, isOpen, channelId, '', city,shopId)
    }) 
  }

  changeShopisOpen = async (data) => {
    const result = await this.props.shopStore.changeShopisOpen(data)
    return result
  }

  handleOkNotice = async() => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    })
    const notice = this.state.noticeData
    const selectRow = []
    this.state.selectedRows.forEach(data => {
      selectRow.push({
        channelId : data.channelId,
        channelShopId: data.channelShopId,
        shopId: data.shopId,
        notice: notice
      })
    })
    const result =  await this.changeShopisOpen(selectRow)
    if(result.data.status === 200){
      this.setState({
        visibleNotice: false,
        confirmLoading: false,
        noticeData: ''
      })
      message.success('保存成功！')
    }else{
      message.error('保存失败')
    }
    this.refs.ShopListForm.validateFieldsAndScroll( (err, values) =>{
      const isOpen = values.status
      const city = values.city 
      const shopId = (values.shopId === '全部' || values.shopId === '0') ? '' : values.shopId
      const channelId = values.channel
      this.changeMsgData(1, 10, isOpen, channelId, '', city,shopId)
    }) 
  }


  handleCancelStatus = () => {
    this.setState({
      visibleStatus: false,
    })
    message.warning('取消保存！')
  }

  handleCancelNotice = () => {
    this.setState({
      visibleNotice: false,
    })
    message.warning('取消保存！')
  }

  setStatus = () => {
    if (this.state.selectedRows.length !== 0) {
      this.showModalStatus()
    } else {
      Modal.warning({
        title: '设置营业状态',
        content: '请至少选择一家门店！',
      })
    }
  }

  setHours = async () => {
    if (this.state.selectedRows.length !== 0) {
      this.showModalHours()
      const channelId = this.state.selectedRows[0].channelId
      const channelShopId = this.state.selectedRows[0].channelShopId
      const result = await this.props.shopStore.getShopTime(channelId, channelShopId )
      const timeResult = result.data.data
      this.setState({
        timeResult
      })
    } else {
      Modal.warning({
        title: '设置营业时间',
        content: '请至少选择一家门店！',
      })
    }

  }

  setNotice = () => {
    if (this.state.selectedRows.length !== 0) {
      this.showModalNotice()
      this.setState({
        noticeData:''
      })
    } else {
      Modal.warning({
        title: '设置店铺公告',
        content: '请至少选择一家门店！',
      })
    }
  }

  setAdd =  async() =>{
    this.setState({
      visibleAdd:true
    })
   await this.props.shopStore.getChannelShopsList()
   this.setState({
    channelList: mobx.toJS(this.props.shopStore.channelList),
    shopList: mobx.toJS(this.props.shopStore.shopList)
   })
  }
  
  onChangeStatus = (e) => {
    this.setState({
      statusValue: e.target.value,
    })
  }
  

  dateCellRender = (value) => {
    const openTimeList= []
    const openTimeList1 = []
    this.state.timeResult.forEach( data => {
      let date = data.startDate
      let endDate = data.endDate
      let openTime = data.openTime
      let dataString = value.format('YYYY-MM-DD')
        let list =getAllDate(date, endDate)
        list.forEach( item =>{
          if(item === dataString){
            openTimeList.push({
              openTime,
              dataString
            })  
          }
        }) 
    })
    return (
      <div style={ {textAlign: 'left' , marginTop:'5px'}}>
        {openTimeList.map( data => {
          if(data.openTime.split(',').length ===1){
            return (
              <div key ={data.dataString}>{data.openTime}</div>
            )
          }else{
            return (
              data.openTime.split(',').map(item => <div >{item}</div> )
            )
          }
        }
        )}
      </div>
    )
  }

  onChangeRange = (date, dateString) => {
    this.setState({ rangeDate: dateString, rangeDateValue: date })
  }


  onChangeTimeStart = (time, timeString) => {
    this.setState({timeStartOne: timeString, timeStartOneValue: time})
  }

  onChangeTimeEnd = (time, timeString) => {
    this.setState({timeEndOne: timeString, timeEndOneValue: time})
    const openTimeOne = `${this.state.timeStartOne}-${timeString}`
    this.setState({
      openTimeOne
    })
  }

  onChangeTimeStartTwo  = (time, timeString) =>{
    this.setState({timeStartTwo: timeString, timeStartTwoValue: time})
  }

  onChangeTimeEndTwo = (time, timeString) =>{
    this.setState({timeEndTwo: timeString, timeEndTwoValue: time})
    const openTimeTwo = `${this.state.timeStartTwo}-${timeString}`
    this.setState({
      openTimeTwo
    })
  }

  onChangeTimeStartThree  = (time, timeString) =>{
    this.setState({timeStartThree: timeString, timeStartThreeValue: time})
  }

  onChangeTimeEndThree = (time, timeString) =>{
    this.setState({timeEndThree: timeString, timeEndThreeValue: time})
    const openTimeThree = `${this.state.timeStartThree}-${timeString}`
    this.setState({
      openTimeThree
    })
  }

  checkDisabledHours = (time) =>{
    const hours = range(0,time.split(':')[0])
    return hours
  }

  checkDisabledMinutes  = (h , time) =>{
    if(h < time.split(':')[0]){
      return range(0, 60)
    }else if(h == time.split(':')[0]){
      return range(0,time.split(':')[1]*1+1)
    }
    return []
  }

  cancelTime = () =>{
    this.setState({
      visibleTime: false,
      timeStartOneValue: null,
      timeStartTwoValue: null,
      timeStartThreeValue: null,
      timeEndOneValue: null,
      timeEndTwoValue: null,
      timeEndThreeValue: null,
      chooseOpenTime: undefined,
      timeTwo: false,
      timeThree: false,
      disabledTimeOne: false,
      disabledTimeTwo: false,
      disabledTimeThree: false,
      timeStartOne: '',
      timeStartTwo: '',
      timeStartThree: '',   
      timeEndOne: '',
      timeEndTwo: '',
      timeEndThree: '',
    })
  }

  clickTime = () =>{
    if(this.state.rangeDateValue.length < 2){
      message.warning('请选择营业日期!')
    }else{
      this.setState({visibleTime: true})
    }
    let chooseOpenTimeList =[]
    if(this.state.chooseOpenTime !== undefined){
      chooseOpenTimeList = this.state.chooseOpenTime.split(',')
    }else{
      chooseOpenTimeList = []
    }
    this.setState({
      openTime: chooseOpenTimeList
    })
    if(this.state.selectDateStatus && chooseOpenTimeList.length !== 0){
      if(chooseOpenTimeList.length >0){
        this.setState({
          timeStartOneValue: getMomentByTime(chooseOpenTimeList[0].split('-')[0]),
          timeEndOneValue: getMomentByTime(chooseOpenTimeList[0].split('-')[1]),
          timeStartOne: chooseOpenTimeList[0].split('-')[0],
          timeEndOne: chooseOpenTimeList[0].split('-')[1],
          openTimeOne: `${chooseOpenTimeList[0].split('-')[0]}-${chooseOpenTimeList[0].split('-')[1]}`
        })
        if(chooseOpenTimeList.length >1){
          this.setState({
            timeTwo:true,
            timeStartTwoValue: getMomentByTime(chooseOpenTimeList[1].split('-')[0]),
            timeEndTwoValue: getMomentByTime(chooseOpenTimeList[1].split('-')[1]),
            timeStartTwo: chooseOpenTimeList[1].split('-')[0],
            timeEndTwo: chooseOpenTimeList[1].split('-')[1],
            openTimeTwo: `${chooseOpenTimeList[1].split('-')[0]}-${chooseOpenTimeList[1].split('-')[1]}`            
          })
        }
        if(chooseOpenTimeList.length >2){
          this.setState({
            timeThree: true,
            timeStartThreeValue: getMomentByTime(chooseOpenTimeList[2].split('-')[0]),
            timeEndThreeValue: getMomentByTime(chooseOpenTimeList[2].split('-')[1]),
            timeStartThree: chooseOpenTimeList[2].split('-')[0],
            timeEndThree: chooseOpenTimeList[2].split('-')[1],
            openTimeThree: `${chooseOpenTimeList[2].split('-')[0]}-${chooseOpenTimeList[2].split('-')[1]}`                        
          })
        }
      }
    }
  }

  setTimeOk = async() =>{
    let len = this.state.openTimeOne === ''
    if (len) {
      message.warning('请选择营业时间段!')
    }else{
      const startDate = this.state.rangeDate[0]
      const endDate = this.state.rangeDate[1]
      let openTime = []
      if(this.state.openTimeOne!=='' && this.state.openTimeTwo!=='' && this.state.openTimeThree!==''){
        openTime =[this.state.openTimeOne,this.state.openTimeTwo, this.state.openTimeThree]
      }else if(this.state.openTimeOne!=='' && this.state.openTimeTwo!==''){
        openTime =[this.state.openTimeOne, this.state.openTimeTwo]
      }else if(this.state.openTimeOne!==''){
        openTime = [this.state.openTimeOne]
      }
      openTime = openTime.join(',')
      const selectRow = []
      this.state.selectedRows.forEach(data => {
        selectRow.push({
          channelId : data.channelId,
          channelShopId: data.channelShopId,
          shopId: data.shopId,
          startDate: startDate,
          endDate: endDate,
          openTime: openTime
        })
      })
      await this.props.shopStore.updateChannelShopTime(selectRow)
      if(this.props.shopStore.checkTimestatus){
        message.success('操作成功')
        this.setState({
          visibleHours: false,
          visibleTime: false,
          rangeDateValue: null,
          timeStartOneValue: null,
          timeStartTwoValue: null,
          timeStartThreeValue: null,
          timeEndOneValue: null,
          timeEndTwoValue: null,
          timeEndThreeValue: null,
          chooseOpenTime: undefined,
          timeTwo: false,
          timeThree: false,
          disabledTimeOne: false,
          disabledTimeTwo: false,
          disabledTimeThree: false,
          timeStartOne: '',
          timeStartTwo: '',
          timeStartThree: '',   
          timeEndOne: '',
          timeEndTwo: '',
          timeEndThree: '',
        })
      }else{
        message.error('操作失败')
      }
    }
  }

  delTimeTwo = () =>{
    if(this.state.timeTwo && this.state.timeThree){
      message.warning('请先删除时间段3!')
    }else{
      let openTime = this.state.openTime
      openTime.pop()
      this.setState({
        timeTwo: false,
        openTime: openTime,
        timeStartTwoValue: null,
        timeEndTwoValue: null,
        disabledTimeOne: false
      })
    }
  }

  delTimeThree = () =>{
      let openTime = this.state.openTime
      openTime.pop()
      this.setState({
        timeThree: false,
        openTime: openTime,
        timeStartThreeValue: null,
        timeEndThreeValue: null,
        disabledTimeTwo: false
      })
  }

  handleCancelDate = () => {
    this.setState({
      visibleHours: false
    })
  }

  handleOkStore = async(channelId, channelShopId, shopId, address, serviceTel, contactTel, notice, preOrder) => {
    const result = await this.props.shopStore.changeShopMsg(channelId, channelShopId, shopId, address, serviceTel, contactTel, notice, preOrder)
    if(result.data.data ==='修改成功'){
      message.success('保存成功')
      this.refs.ShopListForm.validateFieldsAndScroll( (err, values) =>{
        const isOpen = values.status
        const city = values.city 
        const shopId = (values.shopId === '全部' || values.shopId === '0') ? '' : values.shopId
        const channelId = values.channel
        this.changeMsgData(1, 10, isOpen, channelId, '', city,shopId)
      })
    }else{
      message.error('保存失败')
    }
  }

  handleCancelStore = () => {
    this.refs.ShopDetailForm.resetFields()
    this.setState({
      visibleStore: false,
      formSource:{},
      formLoading: true,
    })
    message.warning('取消保存！')
  }

  handleCancelAdd = () =>{
    this.setState({
      visibleAdd:false,
    })
    this.refs.ShopAddForm.wrappedInstance.changeValue()
    
  }

  addOk = async() => {
    const {channelIdValue, shopIdValue, channelShopIdValue, channelShopNameValue} = this.state    
    if(channelIdValue === '' || shopIdValue === '' || channelShopIdValue === '' || channelShopNameValue === ''){
      message.error('输入的值不能为空!')
    }else if(this.props.shopStore.checkChannelShopId === false){
      message.error('门店ID已存在! 请修改!')
    }else if (this.state.err === false){
      message.error('输入错误')
    }else{
      const result = await this.props.shopStore.insertChannelShop(channelIdValue, shopIdValue, channelShopIdValue, channelShopNameValue)
      if(result.data.status === 200){
        message.success('新增成功')
        this.setState({visibleAdd:false})
        this.refs.ShopListForm.validateFieldsAndScroll( (err, values) =>{
          const isOpen = values.status
          const city = values.city 
          const shopId = (values.shopId === '全部' || values.shopId === '0') ? '' : values.shopId
          const channelId = values.channel
          this.changeMsgData(1, 10, isOpen, channelId, '', city,shopId)
        }) 
        this.refs.ShopAddForm.wrappedInstance.changeValue()
    }else{
      message.error(result.data.err)
    }
  }}

  checkErr = (err) =>{
    this.setState({
      err
    })
  }

  showModalStore = async (channelId, channelShopId) => {
    this.setState({
      visibleStore: true,
      channelId,
      channelShopId
    })
    const result = await this.formSourceFun(channelId, channelShopId)
    this.setState({
      formSource:{
        channelId: result.data.channelId,
        channelShopId: result.data.channelShopId,
        channelShopName: result.data.channelShopName,
        address: result.data.address,
        contactTel: result.data.contactTel,
        serviceTel: result.data.serviceTel,
        notice: result.data.notice,
        preOrder:result.data.preOrder === 0 ? false : true
      },
      formLoading: false,
      shopId:result.data.shopId
    })
  }
  
  changeFormLoading = (formLoading) => {
    this.setState({
      formLoading
    })
  }
  closeModalStore = () => {
    this.setState({
      visibleStore: false,
    })
  }

  formSourceFun = async (channelId, channelShopId) =>{
    const result = await this.props.shopStore.getShopMsg(channelId, channelShopId)
    return result
  }

  changeMsgData = async (page, pageSize, isOpen, channelId, regionId, city, shopId) => {
    const msgListData = await this.props.shopStore.getMsgListData(page, pageSize, isOpen, channelId, regionId, city, shopId )
    this.init(msgListData)
    this.props.shopStore.closeLoading()
  }

  init = (msgListData) => {
    const dataSource = this.dataSourceFun(msgListData)
    this.setState({dataSource})
  }

  dataSourceFun = (msgListData) =>{
    if(msgListData.length !==0){
      const dataSource = msgListData.data.map((data) => {
        return {
          key: data.key,
          storeID: data.channelShopId,
          storeName: data.channelShopName,
          channel: data.channelName,
          channelId: data.channelId,
          area: data.regionName,
          city: data.city,
          status: data.isOpen ===0 ? '暂停营业':'正常营业',
          shopId: data.shopId,
          hours: data.openTime,
          preOrder: data.preOrder === 0 ? '关' : '开',
          shopNameAlias: data.shopNameAlias
        }
      })
      this.paginationFun(msgListData)
      return dataSource
    }
  }
  handleTableChange = (pagination) => {
    this.refs.ShopListForm.validateFieldsAndScroll((err , values) => {
      let isOpen = values.status
      let channelId = values.channel
      const regionId = values.area
      const city = values.city
      const shopId = (values.shopId === '全部' || values.shopId === '0') ? '' : values.shopId
    this.changeMsgData(pagination.current, pagination.pageSize, isOpen, channelId, regionId, city, shopId)
    })
    this.setState({
      ...pagination
    })
    this.handleRowSelectChange([],[])
  }
  paginationFun = (data) => {
    const total = data.total
    this.setState({
      pagination :{
        total,
        showTotal: () => `共${total}条数据`,
        pageSizeOptions: ['10', '20', '50', '100'],
        showSizeChanger: true,
      }
    })
  }
  
  changeNotice = (e) => {
    e.preventDefault();
    this.setState({
      noticeData:e.target.value
    })
  }

  preOrderChange = (data) =>{
    this.setState({
      formSource:{
        ...this.state.formSource,
        preOrder: data
      }
    })
  }

  handleRowSelectChange = (selectedRowKeys, selectedRows) => {
    let selectedRow = []
    selectedRows.forEach(data => {    
      selectedRow.push({
        channelShopId: data.storeID,
        channelId: data.channelId,
        shopId : data.shopId
      })
    })   
    this.setState({ selectedKeys:selectedRowKeys, selectedRows:selectedRow})
  }

  showLog = (channelId , shopId, channelShopId) =>{
    this.setState({
      logChannelId: channelId,
      logShopId : shopId,
      logChannelShopId: channelShopId,
      visibleLog: true
    })
    
  }

  handleCancelLog = () =>{
    this.setState({
      logChannelId: undefined,
      logShopId : undefined,
      logChannelShopId: undefined,
      visibleLog: false
    })
  }

  disabledDate(current) {
    return current && current < moment().startOf('day')
  }

  addTime = () =>{
    const {timeTwo, timeThree} = this.state
    if(timeTwo === false && timeThree === false){
      if(this.state.timeStartOne !=='' && this.state.timeEndOne !== ''){
        this.setState({timeTwo: true, disabledTimeOne: true})
      }else{
        message.warning('请输入时间段1!')
      }
    }
    if(timeTwo && timeThree === false){
      if(this.state.timeStartTwo !=='' && this.state.timeEndTwo !== ''){
        this.setState({timeThree: true, disabledTimeTwo: true,  disabledTimeOne: true})
      }else{
        message.warning('请输入时间段2!')
      }
    }
    if(timeTwo && timeThree){
      message.warning('最多添加三个时间段')
    }
  }

  selectDateCell = (date) =>{ 
    this.setState({
      rangeDateValue: [date, date],
      rangeDate: [date.format('YYYY-MM-DD'), date.format('YYYY-MM-DD')],
      selectDateStatus : true
    })
    if(this.state.timeResult.find(v =>v.startDate == date.format('YYYY-MM-DD'))!==undefined){
      const chooseOpenTime = this.state.timeResult.find(v =>v.startDate == date.format('YYYY-MM-DD')).openTime
      this.setState({
        chooseOpenTime
      })
    }else{
      this.setState({chooseOpenTime: undefined})
    }
  }

  render() {
    const canEdit = this.props.store.hasUserAuth('edit');
    const columns = [
      {
        title: '平台门店',
        dataIndex: 'storeName',
        key: 'storeName',
      }, {
        title: '渠道',
        dataIndex: 'channel',
        key: 'channel',
      }, 
      {
        title: '平台门店ID',
        dataIndex: 'storeID',
        key: 'storeID'
      },
      {
        title: '家乐福门店ID',
        dataIndex: 'shopId',
        key: 'shopId',
      },
      {
        title: '家乐福门店',
        dataIndex: 'shopNameAlias',
        key: 'shopNameAlias',
      }, {
        title: '预订单',
        dataIndex: 'preOrder',
        key: 'preOrder',
      }, {
        title: '城市',
        dataIndex: 'city',
        key: 'city',
      }, {
        title: '营业状态',
        dataIndex: 'status',
        key: 'status',
      }, {
        title: '营业时间',
        dataIndex: 'hours',
        key: 'hours',
      }, {
        title: '操作',
        key: 'action',
        render: (text, record) => {
          return(
          <span>
            {canEdit?
            <span>
              <a onClick={() =>{this.showModalStore(record.channelId,record.storeID)}}>修改信息</a>
              <span className="ant-divider" />
              <Link to={`/store/item/${record.shopId}/${record.channelId}`}>管理商品</Link>
              <span className="ant-divider" />
            </span> : null
            }
            <a onClick={() =>{this.showLog(record.channelId,record.shopId , record.storeID)}}>查看日志</a>                        
          </span>
          )
        },
      }
    ]

    const rowSelection = {
      selectedRowKeys : this.state.selectedKeys,
      onChange: this.handleRowSelectChange,
      getCheckboxProps: record => ({
        disabled: record.name === 'Disabled User',    // Column configuration not to be checked
      }),
    }

    const radioStyle = {
      display: 'block',
      height: '30px',
      lineHeight: '30px',
    }
    let title
    if (this.props.store.collapse) {
      title = <h2 style={{ marginBottom: 16 ,color: '#108ee9'}}>店铺管理</h2>
    } else {
      title = ''
    }

    const { visibleStatus, visibleHours, visibleNotice, visibleStore, confirmLoading } = this.state
    return (            
      <div>
        {title}
        <ShopListForm
        ref = "ShopListForm"
        changeMsgData={this.changeMsgData}
        pagination={this.state.pagination}
        setStatus = {this.setStatus}
        setHours = {this.setHours}
        setNotice = {this.setNotice}
        setAdd = {this.setAdd}
        canEdit = {canEdit}
        />
        <div>
          <Table rowSelection={canEdit ? rowSelection : null}
            columns={columns} 
            dataSource={this.state.dataSource}
            loading={this.props.shopStore.loading}
            pagination={this.state.pagination}
            onChange={this.handleTableChange}          
          />
        </div>
        <Modal title="设置营业状态"
          visible={visibleStatus}
          onOk={this.handleOkStatus}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancelStatus}
          width={300}
          maskClosable = {false}
        >
          <RadioGroup onChange={this.onChangeStatus} value={this.state.statusValue}>
            <Radio style={radioStyle} value={0}>暂停营业</Radio>
            <Radio style={radioStyle} value={1}>正常营业</Radio> 
          </RadioGroup>
        </Modal>
        <Modal title="设置营业时间"
          visible={visibleHours}
          confirmLoading={confirmLoading}
          width={750}
          onCancel = {() => {this.setState({visibleHours: false, rangeDateValue: []})}}
          footer = {null}
          maskClosable = {false} 
        >
          <Form layout="inline">
            <FormItem>
              <RangePicker onChange={this.onChangeRange} value ={this.state.rangeDateValue} disabledDate = {this.disabledDate} size = 'default'/>
            </FormItem>
            <FormItem>
            <a onClick = {this.clickTime}>设置营业时间段</a>
            </FormItem>
            <Modal
              title = '设置营业时间段'
              visible = {this.state.visibleTime}
              onCancel = {this.cancelTime}
              onOk = {this.setTimeOk}
              maskClosable = {false}              
            >
              <Row gutter ={16}>
                <Col className="gutter-row" span={24}>
                时间段1: &nbsp; &nbsp;<TimePicker placeholder={'开始时间'} value ={this.state.timeStartOneValue} 
                onChange={(this.onChangeTimeStart)}
                disabled ={this.state.disabledTimeOne}                
                format={'HH:mm'} size = 'default'/>
                &nbsp; - &nbsp;
                <TimePicker placeholder={'结束时间'} value ={this.state.timeEndOneValue}
                onChange={this.onChangeTimeEnd}
                disabled ={this.state.disabledTimeOne}                 
                disabledHours ={() => { return this.checkDisabledHours(this.state.timeStartOne)}} 
                disabledMinutes={(h) => {return this.checkDisabledMinutes(h, this.state.timeStartOne)}} format={'HH:mm'} size = 'default'/>
                &nbsp; &nbsp;
                <a onClick = {this.addTime}>新增</a>
                </Col>
              </Row>
              {this.state.timeTwo &&
                <Row gutter ={16} style = {{marginTop: 16}}>
                  <Col className="gutter-row" span={24}>
                  时间段2: &nbsp; &nbsp;<TimePicker placeholder={'开始时间'}   format={'HH:mm'} size = 'default' 
                  onChange={this.onChangeTimeStartTwo}
                  disabled ={this.state.disabledTimeTwo}
                  value ={this.state.timeStartTwoValue}
                  disabledHours ={() => {return this.checkDisabledHours(this.state.timeEndOne)}} 
                  disabledMinutes={(h) => {return this.checkDisabledMinutes(h, this.state.timeEndOne)}}/>
                  &nbsp; - &nbsp;
                  <TimePicker placeholder={'结束时间'}  format={'HH:mm'} size = 'default' 
                  onChange={this.onChangeTimeEndTwo} 
                  value ={this.state.timeEndTwoValue}
                  disabled ={this.state.disabledTimeTwo}                  
                  disabledHours ={() =>{return this.checkDisabledHours(this.state.timeStartTwo)}} 
                  disabledMinutes={(h) => {return this.checkDisabledMinutes(h, this.state.timeStartTwo)}}/>
                  &nbsp; &nbsp;
                  <Icon type="close" style={{ fontSize: 14}} onClick = {this.delTimeTwo} />
                  </Col>
                </Row>              
              }
              {this.state.timeThree &&
                <Row gutter ={16} style = {{marginTop: 16}}>
                  <Col className="gutter-row" span={24}>
                  时间段3: &nbsp; &nbsp;<TimePicker placeholder={'开始时间'} format={'HH:mm'} size = 'default' 
                  onChange={this.onChangeTimeStartThree}
                  disabled ={this.state.disabledTimeThree}
                  value ={this.state.timeStartThreeValue} 
                  disabledHours ={() => {return this.checkDisabledHours(this.state.timeEndTwo)}} 
                  disabledMinutes={(h) => {return this.checkDisabledMinutes(h, this.state.timeEndTwo)}}/>
                  &nbsp; - &nbsp;
                  <TimePicker placeholder={'结束时间'} format={'HH:mm'} size = 'default' 
                   onChange={this.onChangeTimeEndThree}
                   disabled ={this.state.disabledTimeThree}
                   value ={this.state.timeEndThreeValue}
                   disabledHours ={() => {return this.checkDisabledHours(this.state.timeStartThree)}} 
                   disabledMinutes={(h) => {return this.checkDisabledMinutes(h, this.state.timeStartThree)}}/>
                  &nbsp; &nbsp;
                  <Icon type="close" style={{ fontSize: 14,  }} onClick = {this.delTimeThree}/>
                  </Col>
                </Row>              
              }
            </Modal> 
          </Form>
          <Calendar dateCellRender={this.dateCellRender} onSelect = {this.selectDateCell}/>
        </Modal>
        <Modal title="设置店铺公告"
          visible={visibleNotice}
          onOk={this.handleOkNotice}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancelNotice}
          okText="保存"
          maskClosable = {false}          
        >
          <TextArea rows={4} onChange ={this.changeNotice} value={this.state.noticeData}/>
        </Modal>
        <Modal
          title = '新增店铺信息'
          visible = {this.state.visibleAdd}
          onCancel = {this.handleCancelAdd}
          onOk = {this.addOk}
          maskClosable = {false}          
        >
          <ShopAdd
            ref = 'ShopAddForm'
            channelChange = {(value) => {this.setState({channelIdValue: value})}}
            shopChange = {(value) => {this.setState({shopIdValue: value})}}
            channelShopChange = {(value) => {this.setState({channelShopIdValue: value})}}
            channelShopNameChange = {(value) => {this.setState({channelShopNameValue: value})}}
            channelList= { this.state.channelList}
            shopList = {this.state.shopList}
            checkErr = {this.checkErr}
          />
        </Modal>
        <Modal 
          title="修改门店信息"
          visible={visibleStore}
          confirmLoading={confirmLoading}
          onCancel={() => {this.handleCancelStore() }}
          footer={null}
          maskClosable = {false}          
        >
          <ShopDetailForm 
            ref = "ShopDetailForm"
            formSource = {this.state.formSource}
            preOrderChange = {this.preOrderChange}
            formLoading = {this.state.formLoading}
            channelId = {this.state.channelId}
            channelShopId = {this.state.channelShopId}
            shopId = {this.state.shopId}
            handleOkStore = {this.handleOkStore}
            closeModalStore= {this.closeModalStore}
            changeFormLoading= {this.changeFormLoading}
          />
        </Modal>
        <Modal 
          title="查看操作日志"
          visible={this.state.visibleLog}
          onCancel={() => {this.handleCancelLog() }}
          footer={null}
          width ={1000}
          maskClosable = {false}          
        >
        <ShopLogTable 
          channelId = {this.state.logChannelId}
          shopId = {this.state.logShopId}
          channelShopId = {this.state.logChannelShopId}
        />
        </Modal>
      </div>
    )
  }
}

export default ShopListComponent