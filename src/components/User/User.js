import React, { Component } from 'react'
import { connect } from 'react-redux'
import { selectAction } from '../redux/actions'
import { Form, Input} from 'antd'
const FormItem = Form.Item

class User extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentDidMount(){
    const { dispatch } = this.props
    dispatch(selectAction('init.'))
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.selectReducer !== this.props.selectReducer) {
      const { dispatch, selectReducer } = nextProps
      dispatch(selectAction(selectReducer))
    }
  }
  handleChange = (e) => {
    let val = e.target.value
    this.props.dispatch(selectAction(val))
  }
  render() {
    const { selectReducer } = this.props
    return (
      <div>
          <h2>Welcome, User!  {selectReducer}</h2>
          <hr/>
          <Form layout="inline">
            <FormItem>
                测试Redux数据流：
            </FormItem>
            <FormItem>
                <Input placeholder="输入内容" onChange={this.handleChange}/>
            </FormItem>
          </Form>
      </div>
    )
  }
}
const mapStateToProps = state => {
  const { selectReducer } = state
  return {
    selectReducer
  }
}

export default connect(mapStateToProps)(User)
