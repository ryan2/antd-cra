import React, { Component } from 'react'
import { Row, Col, Form, Icon, Input, Button, Checkbox, Card, Alert, message } from 'antd'
import axios from 'axios'
import { apiUrl } from '../../api/constant'
import Base64 from './base64.js'
const FormItem = Form.Item

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  handlePushUrl = (e) => {
    this.props.history.push(e)
  }
  render() {
    return (
      <div>
        <Row>
          <Col style={{ marginTop: 80 }}>
            <Card style={{ width: 500, margin: 'auto' }}>
              <div style={{ textAlign: 'center', marginTop: 10 }}>
                <div><img src={require('../../images/logo.png')} alt="Logo"></img></div>
                {/* <div style={{marginTop: 40}}><label style={{fontSize: '7em', fontFamily: 'fantasy', color: '#108ee9'}}>Rider</label></div> */}
                <div><label style={{ fontSize: 18, fontWeight: 400 }}>家乐福O2O业务管理系统</label></div>
              </div>
              <WrappedNormalLoginForm handlePush={this.handlePushUrl} />
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

class NormalLoginForm extends React.Component {
  state = {
    loginError: '',
    showLoginError: false,
    apiLogin: `${apiUrl}/auth`,
    apiGetSessionId: `${apiUrl}/sessionId`,
    apiGetVerigyCode: `${apiUrl}/verifyCode`,
    imgSrcParams: new Date().getTime(),
    sessionId: null,
    loading: false
  }

  componentWillMount () {
    this.handleGetSessionId()
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.setState({
      loginError: '',
      showLoginError: false
    })
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({
          loading: true
        })
        const url = this.state.apiLogin
        //base64加密登录密码
        values.password = Base64.encode(values.password);
        const [username, password, verifyCode] = [values.username, values.password, values.verifyCode]
        const param = { username, password, verifyCode, sessionId: this.state.sessionId }
        axios.post(url, param)
          .then(res => {
            if (res.data.data!==null) {
            const token = res.data.data.token              
              let a = { "AccessToken": token, "UserId": username }
              window.localStorage.setItem("riderMain", JSON.stringify(a))
              message.success('登录成功！')
              this.props.handlePush('/index')
            } else {
              const err = res.data.err
              if(err === '登陆账号或密码不正确.'){
                this.handleRefreshVerigyCode()
              }
              this.setState({
                loginError: err,
                showLoginError: true,
                loading: false
              })
            }
          })
          .catch(res => {
            this.setState({
              loginError: res,
              showLoginError: true,
              loading: false
            })
            console.log('error:', res)
          })
      }
    })
  }

  handleCloseError = () => {
    this.setState({
      loginError: '',
      showLoginError: false
    })
  }

  //获取验证码的sessionId
  handleGetSessionId = () => {
    axios.get(this.state.apiGetSessionId)
        .then(res => {
          if(res.status === 200 && res.data && res.data.data.sessionId ) {
            this.setState({
              sessionId: res.data.data.sessionId
            })
          } else {
            message.error('获取验证码失败')
          }
        })
  }

  handleRefreshVerigyCode = () => {
    this.setState({
      imgSrcParams: new Date().getTime()
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    let alert = ''
    if (this.state.showLoginError) {
      alert = <Alert message={this.state.loginError} type="error" showIcon closable onClose={this.handleCloseError}/>
    }
    return (
      <Form onSubmit={this.handleSubmit} style={{ margin: 'auto', width: '60%', marginTop: 60, maxWidth: 300 }}>
        <FormItem>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: '请输入用户名!' }],
          })(
            <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Username" />
            )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: '请输入密码!' }],
          })(
            <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Password" />
            )}
        </FormItem>
        <Row>
          <Col span={12}>
            <FormItem>
              {getFieldDecorator('verifyCode', {
                rules: [{ required: true, message: '请输入验证码!' }],
              })(
                  <Input placeholder="验证码" />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <div style={{height: 32, lineHeight: '32px', textAlign: 'right'}}>
              {this.state.sessionId ?
              <img src={`${this.state.apiGetVerigyCode}?data=${encodeURIComponent(JSON.stringify({sessionId: this.state.sessionId}))}&&dataParams=${this.state.imgSrcParams}`}
                   onClick={this.handleRefreshVerigyCode}/> : null }
            </div>
          </Col>
        </Row>
        {/* <FormItem
        >
          <Row gutter={8}>
            <Col span={14}>
              {getFieldDecorator('captcha', {
                rules: [{ required: true, message: 'Please input the captcha you got!' }],
              })(
                <Input prefix={<Icon type="key" style={{ fontSize: 13 }} />} size="large" placeholder="Captcha" />
                )}
            </Col>
            <Col span={10}>
              <Button size="large">Get captcha</Button>
            </Col>
          </Row>
        </FormItem> */}
        {alert}
        <FormItem>
          {/* {getFieldDecorator('remember', {
            valuePropName: 'checked',
            initialValue: true,
          })(
            <Checkbox>Remember me</Checkbox>
            )} */}
          {/* <a style={{ float: 'right' }} href="">Forgot password</a> */}
          <Button type="primary" htmlType="submit" loading={this.state.loading} style={{ width: '100%' }}>
            Log in
          </Button>
          {/* Or <a href="">register now!</a> */}
        </FormItem>
      </Form>
    )
  }
}

const WrappedNormalLoginForm = Form.create()(NormalLoginForm)

export default Login
