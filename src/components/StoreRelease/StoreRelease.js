import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { observer,inject } from 'mobx-react'
import { Form, Upload, Modal, Input, Icon, Row, Col, Button, Card, Table, Select, Radio } from 'antd';
const RadioGroup = Radio.Group;
const Option = Select.Option
const FormItem = Form.Item;
const Search = Input.Search;

@inject('store')@observer
class StoreRelease extends Component {
  constructor(props) {
    super(props)
    this.state = {
      saleValue: 1,
      previewVisible: false,
      previewImage: '',  
      fileList: [{
        uid: -1,
        name: 'xxx.png',
        status: 'done',
        url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
      }],
    }
  }

  componentDidMount() {
  }


  componentWillReceiveProps(nextProps) {
  }

  handleCancel = () => this.setState({ previewVisible: false })
  
  handlePreview = (file) => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true,
    });
  }

  handleChange = ({ fileList }) => this.setState({ fileList })


  onChangeSaleTime = (e) => {
    console.log('radio checked', e.target.value);
    this.setState({
      saleValue: e.target.value,
    });
  }

  static contextTypes = {
    store: PropTypes.object,
  }

  render() {

    let title
    if (this.props.store.collapse) {
      title = <h2 style={{ marginBottom: 16 }}><Link to="/store/item">商品发布</Link></h2>
    } else {
      title = ''
    }

    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };
    const formItemLayoutInline = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };

    const { previewVisible, previewImage, fileList } = this.state;
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">Upload</div>
      </div>
    );

    const columns = [{
      title: '*价格',
      dataIndex: 'price',
      key: 'price',
    }, {
      title: '*库存',
      dataIndex: 'stock',
      key: 'stock',
    }, {
      title: '*包装盒价格',
      dataIndex: 'boxPrice',
      key: 'boxPrice',
    }, {
      title: '*包装盒数量',
      dataIndex: 'boxCount',
      key: 'boxCount',
    }, {
      title: 'UPC/EAN/条形码',
      dataIndex: 'barCode',
      key: 'barCode',
    }, {
      title: 'SKU码/货号',
      dataIndex: 'skuCode',
      key: 'skuCode',
    }, {
      title: '重量（克/g）',
      dataIndex: 'weight',
      key: 'weight',
    }]
    
    const data = []

    return (
      <div>
        {title}
        <Card title="基本信息" bordered={true} style={{ width: '80%' }}>
          <Form onSubmit={this.handleSubmit}>
            <FormItem
              {...formItemLayout}
              label="类目"
              hasFeedback
            >
              {getFieldDecorator('type', {
                rules: [{
                  required: true, message: 'Please input your E-mail!',
                }],
              })(
                <Search
                  placeholder="select type"
                  style={{ width: 200 }}
                  disabled
                  onSearch={value => console.log(value)}
                />
                )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="商品编码"
              hasFeedback
            >
              {getFieldDecorator('itemCode', {
                rules: [{
                  required: true, message: 'Please input item code!',
                }],
              })(
                <Input placeholder="input item code" />
                )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="商品名称"
              hasFeedback
            >
              {getFieldDecorator('itemName', {
                rules: [{
                  required: true, message: 'Please your item name!',
                }],
              })(
                <Input placeholder="item name" onBlur={this.handleConfirmBlur} />
                )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="英文名称"
              hasFeedback
            >
              {getFieldDecorator('englishName', {
                rules: [{ required: true, message: 'Please input your english name!', whitespace: true }],
              })(
                <Input placeholder="english name" />
                )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="销售单位"
            >
              <Row gutter={8}>
                <Col span={5}>
                  {getFieldDecorator('saleUnit', {
                    rules: [{ required: true, message: 'Please input the sale unit!' }],
                  })(
                    <Input placeholder="sale unit" size="large" />
                    )}
                </Col>
                <Col span={9}>
                  <FormItem
                    label="商品规格"
                    {...formItemLayoutInline}
                  >
                    {getFieldDecorator('itemSpec', {
                      rules: [{ required: true, message: 'Please input item spec!' }],
                    })(
                      <Input placeholder="item spec" />
                      )}
                  </FormItem>
                </Col>
                <Col span={9}>
                  <FormItem
                    label="商品重量"
                    {...formItemLayoutInline}
                  >
                    {getFieldDecorator('weight', {
                      rules: [{ required: true, message: 'Please input item weight!' }],
                    })(
                      <Input placeholder="item weight" />
                      )}
                  </FormItem>
                </Col>
              </Row>
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="长度(cm)"
            >
              <Row gutter={8}>
                <Col span={5}>
                  {getFieldDecorator('length', {
                    rules: [{ required: true, message: 'Please input the length!' }],
                  })(
                    <Input placeholder="length" size="large" />
                    )}
                </Col>
                <Col span={9}>
                  <FormItem
                    label="宽度(cm)"
                    {...formItemLayoutInline}
                  >
                    {getFieldDecorator('width', {
                      rules: [{ required: true, message: 'Please input width!' }],
                    })(
                      <Input placeholder="width" />
                      )}
                  </FormItem>
                </Col>
                <Col span={9}>
                  <FormItem
                    label="高度(cm)"
                    {...formItemLayoutInline}
                  >
                    {getFieldDecorator('height', {
                      rules: [{ required: true, message: 'Please input height!' }],
                    })(
                      <Input placeholder="height" />
                      )}
                  </FormItem>
                </Col>
              </Row>
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="商品图片"
              hasFeedback
            >
              {getFieldDecorator('image', {
                rules: [{ required: true, message: 'Please upload your image!', whitespace: true }],
              })(
                <div>
                  <Upload
                    action="//jsonplaceholder.typicode.com/posts/"
                    listType="picture-card"
                    fileList={fileList}
                    onPreview={this.handlePreview}
                    onChange={this.handleChange}
                  >
                    {fileList.length >= 3 ? null : uploadButton}
                  </Upload>
                  <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                    <img alt="example" style={{ width: '100%' }} src={previewImage} />
                  </Modal>
                  <p>提示：请上传jpg/png格式文件大小不超过5M，建议分辨率不小于800*800</p>
                </div>
                )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="商品描述"
              hasFeedback
            >
              {getFieldDecorator('itemDesc', {
                rules: [{ required: true, message: 'Please input your description!', whitespace: true }],
              })(
                <Input.TextArea placeholder="item description" />
                )}
            </FormItem>
          </Form>
        </Card>
        <br />
        <Card title="价格库存" bordered={true} style={{ width: '80%' }}>
          <Table columns={columns} dataSource={data} />
        </Card>
        <br />
        <Card title="属性" bordered={true} style={{ width: '80%' }}>
          <FormItem
              {...formItemLayout}
              label="商品属性"
              hasFeedback
          >
            {getFieldDecorator('itemProperty', {
              rules: [{ required: true, message: 'Please input your item property!', whitespace: true }],
            })(
              <Select placeholder="item property" style={{width: '100%'}}>
                <Option value="1">常温</Option>
                <Option value="2">冷藏</Option>
                <Option value="3">冷冻</Option>
                <Option value="4">增加自定义</Option>
              </Select>
              )}
          </FormItem>
        </Card>
        <br />
        <Card title="售卖时间" bordered={true} style={{ width: '80%' }}>
          <RadioGroup onChange={this.onChangeSaleTime} value={this.state.saleValue}>
            <Radio value={1}>时间不限</Radio>
            <Radio value={2}>自定义时间</Radio>
          </RadioGroup>
        </Card>
        <br />
        <Col className="gutter-row" style={{textAlign: 'right', width: '80%'}}>
          <Button type="primary" size="large" style={{marginRight: 5}}>保存并返回</Button>
          <Button type="primary" size="large" style={{marginRight: 5}}>保存并继续新建</Button>
          <Button size="large">取消</Button>
        </Col>
      </div>
    )
  }
}

const StoreReleaseForm = Form.create()(StoreRelease)

export default StoreReleaseForm
