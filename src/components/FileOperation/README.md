
批量导入/导出
====
该组件用于文件导入、导出

#### 用法
  ```js

  render() {
  	return (
  	<FileOperation type="ImportProduct"
  	            style={{marginRight: 8}}
                buttonText="批量修改"
                fileType={["xlsx", "xls"]}/>
  	);
  }

  ```
#### Props ( 属性 )
  - `history`: PropTypes.obj.isRequired    history
  - `style`: PropTypes.obj             样式
  - `buttonText`: PropTypes.string,    按钮文字
  - `buttonProps`: PropTypes.obj,      按钮属性
  - `fileType`: PropTypes.array,       上传文件类型限制
  - `queryData`: PropTypes.obj,        导出的查询参数
  - `type`: PropTypes.string.isRequired      类型（ ImportProduct
                                              ImportGoods
                                              ImportImage
                                              ExportProduct
                                              ExportGoods ）

