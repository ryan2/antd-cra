import React, { Component } from 'react';
import { Modal, Form, Checkbox, Spin, Button, Col, Row } from 'antd';
const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;

class UserModuleList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      moduleCheck: false,
      signList: []
    }
  }

  componentWillReceiveProps(nextProps) {
  }

  handleModuleCheck = (e) => {
    const {callback, module, signOptions, signList} = this.props;
    let _signList = signList
    if(e.target.checked) {
      _signList = signOptions
    }
    callback({moduleId: module.moduleId, sign: _signList}, e.target.checked)
  }

  handleSignChange = (e) => {
    const {callback, module, moduleCheck} = this.props;
    let _moduleCheck = moduleCheck;
    if(e.length > 0) {
      _moduleCheck = true
    }
    callback({moduleId: module.moduleId, sign: e}, _moduleCheck)
  }

  render () {
    const {module, signOptions, signList, moduleCheck} = this.props;
    const signOptionsLabel = {
      1: '查询',
      2: '编辑',
      4: '删除',
      8: '审核',
      16: '终审',
      32: '打印',
    }
    //查询功能为必须模块 设置为不可操作
    let _signOptions = signOptions.map(item => {
      let option = {
        value: item,
        label: signOptionsLabel[item]
      }
      if(item === 1) {
        option.disabled = true
      }
      return option
    })
    return (
      <div style={{ marginBottom: 10 }}>
        <Row style={{ borderBottom: '1px solid #E9E9E9'}}>
          <Col xs={24} sm={6}>
            <Checkbox
                onChange={this.handleModuleCheck}
                checked={moduleCheck}
                >
              {module.moduleName}
            </Checkbox>
          </Col>
          <Col xs={24} sm={18}>
            <CheckboxGroup
                options={_signOptions}
                value={signList}
                onChange={this.handleSignChange} />
          </Col>
        </Row>
      </div>
    )
  }
}


class UserAuthModule extends Component {
  constructor (props) {
    super (props);
    this.state  = {
      confirmDirty: false,
      autoExpandParent: true,
      checkedKeys: [],
      selectedKeys: [],
    }
  }

  //取消
  handleCancel = () => {
    const { onCancel, form } = this.props;
    onCancel();
    form.resetFields();
  }

  //提交修改、创建
  handleSubmit = () => {
    const { onCreate, form, roleInfo } = this.props;
    const { validateFields } = form;
    validateFields((err, values) => {
      if ( !err) {
        values.roleId = roleInfo.roleId;
        onCreate(values, 'module').then(() => {
          form.resetFields();
        });
      }
    });
  }

  handleModuleFind = (moduleList, moduleId, needSign) => {
    let module = moduleList.find(_item => _item.moduleId === moduleId)
    if(module) {
      return needSign ? module.sign : true
    }
    return needSign ? [] : false
  }

  handleSetValue = (moduleSign, status) => {
    const {defaultValues, onSelect, form} = this.props;
    let _moduleValue = defaultValues;
    let index, flag;
    _moduleValue.forEach((v, i) => {
      if(v.moduleId === moduleSign.moduleId) {
        index = i;
        flag = true;
      }
    });
    if(flag && status) {
      _moduleValue[index] = moduleSign
    } else if (flag && !status) {
      _moduleValue.splice(index, 1)
    } else if ( !flag && status) {
      _moduleValue.push(moduleSign)
    }
    onSelect(_moduleValue)
    form.setFieldsValue({roleModuleList: _moduleValue})
  }

  handleModuleValidate = (rule, value, callback) => {
    if (!value || ( value && value.length === 0)) {
      callback('请至少为当前角色设置一个模块')
    }
    callback()
  }

  render () {
    const { visible, defaultValues, form, allModule, roleInfo, loading, submitLoading } = this.props;
    const { getFieldDecorator } = form;
    const formTailLayout = {
      labelCol: { span: 3 },
      wrapperCol: { span: 20 },
    };
    return (
        <Modal
          title="设置模块"
          width={800}
          maskClosable={false}
          visible={visible}
          onCancel={this.handleCancel}
          onOk={this.handleSubmit}
          footer={[
            <Button key='back' size="large" onClick={this.handleCancel}>取消</Button>,
            <Button key='submit' type="primary" size="large" loading={submitLoading} onClick={this.handleSubmit}>
              保存
            </Button>,
          ]}
            >
          <Form style={{margin: 20}}>
            <FormItem label="当前角色" {...formTailLayout}>
              <span style={{paddingLeft: 6}}>{roleInfo.roleName}</span>
            </FormItem>
            <FormItem label="设置模块" {...formTailLayout}>
              {getFieldDecorator('roleModuleList', {
                initialValue: defaultValues,
                rules: [{
                  validator: this.handleModuleValidate
                }],
              })(
                <Spin spinning={loading}>
                  <div style={{overflowY: 'scroll', height: 350}}>
                    {allModule.map(item => (
                        <UserModuleList
                            key={item.moduleId}
                            module={item}
                            signOptions={item.sign}
                            callback={this.handleSetValue}
                            moduleCheck={this.handleModuleFind(defaultValues, item.moduleId)}
                            signList={this.handleModuleFind(defaultValues, item.moduleId, true)}/>
                    ))}
                  </div>
                </Spin>
              )}
            </FormItem>
          </Form>
        </Modal>
    )
  }
}

export default Form.create()(UserAuthModule);