import React, { Component } from 'react';
import { Modal, Form, Input, Radio, Button } from 'antd';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;

class UserAuthEdit extends Component {
  constructor (props) {
    super (props);
    this.state  = {
      confirmDirty: false,
      autoExpandParent: true,
      checkedKeys: [],
      selectedKeys: [],
    }
  }

  //取消
  handleCancel = () => {
    const { onCancel, form } = this.props;
    /*this.setState({
     checkedKeys: []
     })*/
    onCancel();
    form.resetFields();
  }

  //提交修改、创建
  handleSubmit = () => {
    const { onCreate, defaultValues, form } = this.props;
    const { validateFields } = form;
    validateFields((err, values) => {
      if ( !err) {
        if(defaultValues.roleId) {
          values.roleId = defaultValues.roleId;
        }
        //console.log(values);
        onCreate(values, 'role').then(() => {
          form.resetFields();
        });
      }
    });
  }

  handleCheckRoleName = (rule, value, callback) => {
    const {defaultValues} = this.props;
    if(defaultValues && defaultValues.roleName && defaultValues.roleName === value) {
        callback();
    } else {
      this.props.checkRoleName({roleName: value}).then(res => {
        if(res.status !== 200){
            callback(res.err);
        } else {
            callback();
        }
      })
    }

  }

  render () {
    const { visible, defaultValues, form, submitLoading } = this.props;
    const isEdit = defaultValues.roleId ? true : false;
    const { getFieldDecorator } = form;
    const formTailLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 15 },
    };
    return (
        <Modal
            title={isEdit ? '编辑角色' : '新增角色'}
            maskClosable={false}
            visible={visible}
            onCancel={this.handleCancel}
            onOk={this.handleSubmit}
            footer={[
            <Button key='back' size="large" onClick={this.handleCancel}>取消</Button>,
            <Button key='submit' type="primary" size="large" loading={submitLoading} onClick={this.handleSubmit}>
              保存
            </Button>,
          ]}
            >
          <Form style={{margin: 20}}>
            {isEdit ?
                <FormItem label="当前角色" {...formTailLayout}>
                  <span style={{paddingLeft: 6}}>{defaultValues.roleName}</span>
                </FormItem> : null
            }
            <FormItem label={isEdit ? "修改名称" : "角色名称"} {...formTailLayout}>
              {getFieldDecorator('roleName', {
                initialValue: defaultValues && defaultValues.roleName || '',
                rules: [{
                    required: true, message: '角色名称不能为空'
                }, {
                    validator: this.handleCheckRoleName
                }, {
                    max: 15,
                    message: '最大长度不超过15'
                }],
              })(
                  <Input placeholder="请输入角色名称"/>
              )}
            </FormItem>
            <FormItem label={isEdit ? "修改状态" : "状态"} {...formTailLayout}>
              {getFieldDecorator('enable', {
                initialValue: defaultValues.enable !== undefined ? defaultValues.enable + '' : '1',
              })(
                  <RadioGroup>
                    <Radio value="1">启用</Radio>
                    <Radio value="0">禁用</Radio>
                  </RadioGroup>
              )}
            </FormItem>
          </Form>
        </Modal>
    )
  }
}

export default Form.create()(UserAuthEdit);