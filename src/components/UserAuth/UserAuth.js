import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import * as mobx from 'mobx'
import UserAuthEdit from './UserAuthEdit'
import UserAuthModule from './UserAuthModule'
import {Button, Table, Modal, message} from 'antd'

@inject('userStore', 'store')@observer
class UserList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      roleVisible: false,
      currentRole: {},
      moduleVisible: false,
    }
  }

  componentDidMount () {
    const { getRoleList, moduleList, getModuleList } = this.props.userStore;
    getRoleList();
    if(moduleList.length === 0) {
      getModuleList();
    }
  }

  //新增角色弹窗
  handleAddRole = () => {
    this.setState({
      roleVisible: true,
      currentRole: {}
    })
  }

  //编辑角色弹窗
  handleUpdataRole = (role) => {
    this.setState({
      roleVisible: true,
      currentRole: role
    })
  }

  //取消新增、编辑角色及模块
  handleCancel = (type) => {
    this.setState({
      [type]: false
    })
  }

  //新增、编辑角色确认
  /*handleCreateConfirm = (params, type) => {
    const _this = this;
    Modal.confirm({
      title: `是否确认${params.roleId ? "修改" : "新增" }当前角色${type==='module'? "关联模块" : ""}？`,
      onOk() {
        return _this.handleCreate(params, type)
      },
      onCancel() {},
    });
  }*/

  //新增、编辑角色
  handleCreate = (params, type) => {
    const { addRole, updateRole, getRoleList, addRoleModule} = this.props.userStore;
    console.log(this.props.userStore);
    const _this = this;
    let action;
    if (type === 'role') {
      if(this.state.currentRole.roleId) {
        action = updateRole;
      } else {
        action = addRole;
      }
    } else if (type === 'module') {
      action = addRoleModule;
    }
    return action(params).then( res=> {
      if(res.status === 200) {
        _this.handleCancel (`${type}Visible`);
        message.success('操作成功');
        getRoleList();
      } else {
        Modal.error({
          title: '操作失败',
          content: res.err
        })
      }
    })
  }

  //设置角色模块弹窗
  handleSetRoleModule = (role) => {
    this.props.userStore.getRoleModule({roleId: role.roleId});
    this.setState({
      moduleVisible: true,
      currentRole: role
    })
  }

  render() {
    const {roleList, roleLoading, moduleList, roleModuleList, userModuleLoading, submitLoading,
        checkRoleName, selectRoleModule } = this.props.userStore;
    const canEdit = this.props.store.hasUserAuth('edit');
    const columns = [{
      title: '角色名称',
      dataIndex: 'roleName',
      key: 'roleName',
      width: 300
    }, {
      title: '状态',
      dataIndex: 'enable',
      key: 'enable',
      width: 200,
      render: (text) => <span>{text === 0 ? '已禁用' : '已启用'}</span>
    }];
    if(canEdit) {
      columns.push({
        title: '操作',
        key: 'action',
        render: (text, record) =>
            <div>
              <a style={{marginRight: 10}} onClick={() => this.handleUpdataRole(record)}>编辑</a>
              <a onClick={() => this.handleSetRoleModule(record)}>设置模块</a>
            </div>
      })
    }

    return (
      <div>
        {this.props.store.collapse ?
            <h2 style={{marginBottom: 16}}>
              <Link to="/user/auth">角色管理</Link>
            </h2> : null
        }
        {canEdit ?
            <Button type='primary'
                    style={{marginBottom: 10}} onClick={this.handleAddRole}>新增角色</Button> : null
        }
        <Table columns={columns}
               loading={roleLoading} dataSource={roleList.slice()} />
        <UserAuthEdit visible={this.state.roleVisible}
                      submitLoading={submitLoading}
                      defaultValues={this.state.currentRole}
                      checkRoleName={checkRoleName}
                      onCancel={() => this.handleCancel('roleVisible')}
                      onCreate={this.handleCreate}/>
        <UserAuthModule visible={this.state.moduleVisible}
                        loading={userModuleLoading}
                        submitLoading={submitLoading}
                        onCancel={() => this.handleCancel('moduleVisible')}
                        allModule={moduleList}
                        defaultValues={mobx.toJS(roleModuleList)}
                        onSelect={selectRoleModule}
                        roleInfo={this.state.currentRole}
                        onCreate={this.handleCreate}/>
      </div>
    )
  }
}

export default UserList
