import React, { Component } from 'react'
import ItemReportList from './itemReportList'
import { Table, Modal} from 'antd'
import * as mobx from 'mobx'
import { observer, inject } from 'mobx-react'
import ItemReportAdd from './itemReportAdd'
import ItemReportDetail from './itemReportDetail'
import ItemReportChange from './itemReportChange'

@inject('store', 'reportStore')@observer
class ItemReport extends Component {

    state = {
        reportAddModal: false,
        reportDetailModal: false,
        changeReportDetailModal: false,
        pagination: {
            showSizeChanger: true,
            pageSizeOptions: ['10', '20', '50', '100'],
            showTotal: () => `共${this.props.reportStore.total}条数据`,
          },
        searchData: {},
        noteValue: '',
        dataSource:[],
        reportCheck: false,
        page: undefined,
        pageSize: undefined
    }

    componentDidMount = () => {
        this.props.reportStore.getReportList({flag:[0,10]})
        this.setState({
            searchData: {
                flag: [0,10]
            }
        })
      }

    changeReportAdd = () =>{
        this.setState({
            reportAddModal : true
        })
    }

    handleSearchSubmit = (params) => {
        this.props.reportStore.getReportList(params)
        this.setState({
          searchData: params
        })
    }

    handlePageChange = (pagination) => {
        this.props.reportStore.getReportList(Object.assign({
          page: pagination.current,
          pageSize: pagination.pageSize
        }, this.state.searchData))
        this.setState({
            page : pagination.current,
            pageSize: pagination.pageSize
        })
    }

    changeReportModal = (modal) => {
        this.setState({
            reportAddModal: modal
        })
    }

    //查看
    checkItemDetail = (sheetId, type) => {
        this.props.reportStore.getItemDetail({
            sheetId,
            page: 1,
            pageSize: 10
        })
        //this.props.reportStore.getSheetId(sheetId)
        this.setState({
            reportDetailModal: true,
            reportCheck: type==='check' ? true : false
        })
    }

    //编辑
    changeItemDetail = async(sheetId) =>{
        this.setState({
            changeReportDetailModal: true,
        })
        await this.props.reportStore.getItemDetail({
            sheetId,
            page: 1,
            pageSize: 100
        })
        const noteValue = this.props.reportStore.sheetHead.note
        const dataSource = mobx.toJS(this.props.reportStore.sheetDetail)        
        //this.props.reportStore.getSheetId(sheetId)
        this.setState({
            noteValue,
            dataSource
        })
    }

    closeChangeItemDetail = () => {
        this.setState({
            changeReportDetailModal: false,            
        })
    }

    handleItemReportDetailModalClose = () => {
        const {initSheetId, getReportList} = this.props.reportStore;
        this.setState({reportDetailModal: false});
        initSheetId();
        if(this.state.reportCheck) {
            this.props.reportStore.getReportList(Object.assign({
                page: this.state.page,
                pageSize: this.state.pageSize
            },this.state.searchData))
        }
    }

    closeAddModal = () => {
        this.setState({reportAddModal: false})
        this.refs.itemReportAdd.wrappedInstance.clearDataSource()
    }

    render() {
        let title
        if (this.props.store.collapse) {
          title = <h2 style={{ marginBottom: 16 ,color: '#108ee9'}}>商品提报</h2>
        } else {
          title = ''
        }
        const canEdit = this.props.store.hasUserAuth('edit');
        const canCheck = this.props.store.hasUserAuth('check') 
        const columns = [
            {
                title: '单据编码',
                dataIndex: 'sheetId',
                key: 'sheetId',
            },
            {
                title: '状态',
                dataIndex: 'flag',
                key: 'flag',
            },
            {
                title: '创建人',
                dataIndex: 'creator',
                key: 'creator',
            },
            {
                title: '创建时间',
                dataIndex: 'createTime',
                key: 'createTime',
            },
            {
                title: '操作',
                dataIndex: 'operation',
                render: (text, record) => {
                  return ( 
                      <div>   
                      <a onClick ={() => {this.checkItemDetail(record.sheetId)}}>查看</a>
                      {canEdit && 
                        <span>
                        <span className="ant-divider" />
                        <a onClick ={() => {this.changeItemDetail(record.sheetId)}}>编辑</a>
                        </span>
                      }
                      {canCheck && 
                        <span>
                        <span className="ant-divider" />
                        <a onClick ={() => {this.checkItemDetail(record.sheetId, 'check')}}>审核</a>
                        </span>
                      }
                      </div>
                  );
                },
              }
        ]
        const { loading, reportList, total, initSheetId} = this.props.reportStore
        const pagination = Object.assign({total: total}, this.state.pagination);
        return (
            <div>
                {title}
                <ItemReportList
                    changeReportAdd = {this.changeReportAdd}
                    onSearch = {this.handleSearchSubmit}
                />
                <div style={{width: '100%'}}>
                <Table 
                    style={{width: '100%'}} 
                    columns={columns}
                    dataSource={mobx.toJS(reportList)}
                    pagination = {pagination}
                    loading={loading}
                    onChange={(pagination) => this.handlePageChange(pagination)}
                />
                </div>
                <Modal 
                    title="商品提报单"
                    visible={this.state.reportAddModal}
                    footer = {null}
                    onCancel={this.closeAddModal}
                    width={1400}
                    maskClosable = {false}
                >
                    <ItemReportAdd 
                        changeReportModal = {this.changeReportModal}
                        ref = 'itemReportAdd'
                        page = {this.state.page}
                        pageSize = {this.state.pageSize}
                        searchData = {this.state.searchData}
                    />
                </Modal>
                <Modal 
                    title={`${this.state.reportCheck ? '审核':'查看'}单据明细`}
                    visible={this.state.reportDetailModal}
                    footer = {null}
                    onCancel={this.handleItemReportDetailModalClose}
                    width={1400}
                    maskClosable = {false}
                    type={this.state.reportCheck}
                >
                <ItemReportDetail reportCheck={this.state.reportCheck}/>
                </Modal>
                <Modal 
                    title="编辑单据明细"
                    visible={this.state.changeReportDetailModal}
                    footer = {null}
                    onCancel={() =>{this.setState({changeReportDetailModal: false});this.props.reportStore.getReportList(Object.assign({
                        page: this.state.page,
                        pageSize: this.state.pageSize
                    },this.state.searchData))}}
                    width={1400}
                    maskClosable = {false}
                >
                <ItemReportChange 
                 noteValue = {this.state.noteValue}
                 dataSource = {this.state.dataSource}
                 closeChangeItemDetail= {this.closeChangeItemDetail}
                />
                </Modal>                  
            </div>
        )
    }
}

export default ItemReport