import React, { Component } from 'react'
import { Table, Input, Icon, Button, message, Select, Col, Card, Form, Row } from 'antd';
import * as mobx from 'mobx'
import { observer, inject } from 'mobx-react'
import './index.css'
import AddTable from './addTable'
const Option = Select.Option;
const FormItem = Form.Item

@inject('reportStore', 'store')@observer
class itemReportAdd extends Component {

      state = {
        note: ''
      }

      handleCancel = () => {
        this.setState({
          note:''
        })
        this.refs.addTable.wrappedInstance.clearDataSource()
        this.props.changeReportModal(false)
      }

      closeModal = () => {
        this.props.changeReportModal(false)        
      }

      clearDataSource = () => {
        this.refs.addTable.wrappedInstance.clearDataSource()        
        this.setState({
          note: ''
        })
      }

      handleSubmit = async() => {
        const {note} = this.state
        const dataSource = this.refs.addTable.wrappedInstance.getDataSource()
        let flag = dataSource.find(v => v.itemCode === '' || v.goodsName === '' || v.shopId === '' || v.price === '' || v.shopId === undefined)
        if(flag === undefined){
          const params = {
            sheetHead: {
              note: note
            },
            sheetDetail: dataSource
          }
          await this.props.reportStore.addItemReport(params)
          if(this.props.reportStore.addStatus){
            this.props.changeReportModal(false)
            this.props.reportStore.getReportList(this.props.searchData)
            this.clearDataSource()
          }
        }else{
          message.error('请输入正确提报单')
        }
      }

      changeNote = (e) => {
        e.preventDefault();
        let value = e.target.value
        this.setState({
          note: value
        })
      }

      render() {
        const formItemLayout = {
          labelCol: { span: 3 },
          wrapperCol: { span: 21},
        }
        const selectWidth = {
          width: '100%'
        }
        return (
          <div>
            <Card>
            <Row gutter={16}>
            <Col className="gutter-row" span={12} >
              <FormItem {...formItemLayout} label = {'创建人'}>
                  <p>{this.props.store.baseUser.userName}</p>                                         
              </FormItem>
            </Col>
            <Col className="gutter-row" span={12} >
              <FormItem {...formItemLayout} label = {'备注'}>
              <Input  style={selectWidth} onChange= {this.changeNote} value = {this.state.note} size = 'default'/>                                          
              </FormItem>
            </Col>
            </Row>         
            </Card>
            <Card style ={{marginTop:'20px'}}>
              <AddTable 
                ref = 'addTable'
                exportStatus = {true}
                closeModal = {this.closeModal}
                page = {this.props.page}
                pageSize = {this.props.pageSize}
                searchData = {this.props.searchData}
              />
              <Col className="gutter-row" style={{ textAlign: 'right', marginTop:'20px'}} >
              <Button size="large"  onClick={this.handleCancel}>取消</Button>
              <Button type="primary" style={{ marginLeft: 12 }} size="large" onClick = {this.handleSubmit} loading ={this.props.reportStore.addLoading}>保存</Button>
              </Col>
          </Card>
          </div>
        );
      }
    }

export default itemReportAdd
