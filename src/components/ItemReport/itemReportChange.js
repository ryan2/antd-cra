import React, { Component } from 'react'
import { Table, Input, Icon, Button, message, Select, Col, Card, Form, Row, Modal ,Upload, Tag, InputNumber} from 'antd';
import * as mobx from 'mobx'
import { observer, inject } from 'mobx-react'
import './index.css'
import AddTable from './addTable'
import { apiUrl } from '../../api/constant';
const Option = Select.Option;
const FormItem = Form.Item


//商品编码组件
class EditableCell extends Component {
    
    state = {
      value: this.props.value,
    }

    handleChange = (e) => {
      const value = e.target.value;
        this.setState({
            value
        })
    }
    
    check = () => {
      if(this.state.value.length < 8 || this.state.value.length > 10){
        message.error('商品编码不能低于八字符且不能超过十字符!')
      }else{
        this.props.onChange(this.state.value);
      }
    }
    
    
    render() {
      const { value} = this.state;
      return (
        <div className="editable-cell">
              <div className="editable-cell-input-wrapper">
                <Input
                  value={value}
                  onChange={this.handleChange}
                  onPressEnter={this.check}
                  onBlur = {this.check}
                />
              </div>
        </div>
      );
    }
  }

  //商品名称组件
  class GoodsNameCell extends Component {
    
    state = {
      value: this.props.value,
    }

    componentWillReceiveProps = (nextProps) => {
      this.setState({
          value: nextProps.value
      })
    }
    
    
    handleChange = (e) => {
      const value = e.target.value;
      if(value ===''){
            message.error('商品名称不能为空')
        }else if(value.length > 20){
            message.error('商品名称过长')
        }else{
            this.setState({ value });
        }
    }
    
    check = () => {
      if (this.props.onChange) {
        this.props.onChange(this.state.value);
      }
    }
    
    render() {
      const { value} = this.state;
      return (
        <div className="editable-cell">
              <div className="editable-cell-input-wrapper">
                <Input
                  value={ value }
                  onChange={this.handleChange}
                  onPressEnter={this.check}
                  onBlur = {this.check}
                  disabled = {this.props.isHave === 1 ? true : false}
                />
              </div>
        </div>
      );
    }
  }

    //门店名称组件
    @inject('store','reportStore')@observer
    class ShopIdCell extends Component {
      
      state = {
        value: this.props.value,
      }   
      
      handleChange = (value) => {
        this.setState({value})
      }
      
      check = () => {
        this.setState({ editable: false });
        this.props.onChange(this.state.value)
      }
      
      render() {
        let value = isNaN(this.state.value*1) === true ? this.state.value : this.props.reportStore.shopList.find(v => v.shopId === this.state.value).shopNameAlias
        const shopList = mobx.toJS(this.props.reportStore.shopList).map(item => <Option key={item.shopId}>{`${item.shopNameAlias}`}</Option>)
        return (
          <div className="editable-cell">
                <div className="editable-cell-input-wrapper">
                   <Select
                    style={{ width: '100%' }}
                    onChange = {this.handleChange}
                    value ={ value}
                    onBlur = {this.check}
                  >
                    {shopList}
                  </Select>
                </div>
          </div>
        );
      }
    }

    //商品价格组件
  class GoodsPriceCell extends Component {
    
    state = {
      value: this.props.value,
    }

    componentWillReceiveProps = (nextProps) => {
      this.setState({
          value: nextProps.value
      })
    }
    
    handleChange = (value) => {
      this.setState({value})
    }
    
    check = () => {
      if (this.props.onChange) {
        this.props.onChange(this.state.value);
      }
    }
    
    render() {
      const { value, editable } = this.state;
      return (
        <div className="editable-cell">
              <div className="editable-cell-input-wrapper">
                <InputNumber
                  width = '100%'
                  max = {999999999999} 
                  min={0}
                  value={ value }
                  onChange={this.handleChange}
                  onPressEnter={this.check}
                  onBlur = {this.check}
                  placeholder = '请填写商品价格'
                />
              </div>
        </div>
      );
    }
  }

@inject('reportStore','store')@observer
class ItemReportChange extends Component {
    constructor (props) {
        super(props);
        this.columns = [{
            title: '商品编码',
            dataIndex: 'itemCode',
            width: '10%',
            render: (text, record) => {
                const {flag} = this.props.reportStore.sheetHead
                if(flag === '新建'){
                    return (
                        <EditableCell
                        value={text}
                        onChange={this.onCellChange(record.rowNo, 'itemCode')}
                      />
                    )
                }else{
                    return  text
                }

            }
          },{
            title: '商品条码',
            dataIndex: 'barcode',
            key: 'barcode',
            width: '10%',
          }, {
            title: '商品名称',
            dataIndex: 'goodsName',
            width: '30%',
            render: (text, record) => {
                const {flag} = this.props.reportStore.sheetHead
                if(flag === '新建'){
                    return (
                        <GoodsNameCell
                          value ={text}
                          onChange={this.goodsNameChange(record.rowNo, 'goodsName')}
                          goodsName = {this.state.goodsNameValue}
                          isHave = {record.isHave}
                        />
                        )
                }else{
                    return text
                }              
            }
          },{
            title: '商品价格',
            dataIndex: 'price',
            key: 'price',
            width: '15%',
            render: (text, record) => {
              const {flag} = this.props.reportStore.sheetHead
              if(flag === '新建'){
                return (
                  <GoodsPriceCell
                    value ={text}
                    onChange={this.goodsNameChange(record.key, 'price')}
                    existStatus = {record.existStatus}
                  />
                  )
              }else{
                return text
              }           
             
          } 
          }, {
            title: '商品图片',
            dataIndex: 'imageUrl',
            key: 'imageUrl',
            width: '10%',
            render: (text, record) => {
                return (
                  <div>
                    {record.imageUrl === '' ? '暂无图片' : 
                    <img src = {record.imageUrl} width ={50} height ={50} onClick={() => this.handleGoodsImg(record.imageUrl)}/>}
                  </div>
                )
            } 
          }, {
            title: '',
            dataIndex: 'isHave',
            width: '5%',            
            render: (text, record) => {
             return (
              <span>
              {text === 1 ? <Tag color="green">{'已有'}</Tag> : null}
              {text === 0 ? <Tag color="red">{'新品'}</Tag> : null}
              </span>
             )
            } 
          }, {
            title: '门店名称',
            dataIndex: 'shopNameAlias',
            width: '20%',
            render: (text, record) => {
                const {flag} = this.props.reportStore.sheetHead
                if(flag === '新建'){
                    return (
                        <ShopIdCell
                          key = {record.rowNo}
                          value={text}
                          onChange={this.shopIdChange(record.rowNo, 'shopId')}
                        />)              
                }else{
                    return text
                }                
              }
          },]
    }


    componentDidMount = () => {
        this.setState({
            noteValue : this.props.noteValue
        })
        this.props.reportStore.getShopById(this.props.store.baseUser.userName)
    }

    componentWillReceiveProps = (nextProps) => {
      this.setState({
          noteValue: nextProps.noteValue,
          pagination: {
            ...this.state.pagination,
            current: 1
            },
          dataSource: nextProps.dataSource,
          count: nextProps.dataSource.length
      })
    }  

    state = {
        noteValue: '',
        pagination: {
            current: 1,
            showTotal: () => `共${this.props.reportStore.detailTotal}条数据`,
            pageSize: 100
          },
        dataSource: [],
        count:0,
        addDetailModal: false,
        changeListStatus: true,
        selectedRows: [],
        selectedRowKeys: [],
        goodImgShow: false,
        goodImgSrc: '',
    }

    changeNoteValue = (e) =>{
        e.preventDefault();
        this.setState({
            noteValue: e.target.value
        })
    }

    handleGoodsImg = (imgUrl) => {
      let _imgUrl = ''
      if(imgUrl) {
        _imgUrl = imgUrl
      }
      this.setState({
        goodImgShow: !this.state.goodImgShow,
        goodImgSrc: _imgUrl
      })
    }

    handlePageChange = async (pagination) => {
      if(this.state.changeListStatus === false){
        message.error('已修改提报单,请先保存!')
      }else{
        await  this.props.reportStore.getItemDetail(Object.assign({
          page: pagination.current,
          pageSize: pagination.pageSize
        }, {sheetId:this.props.reportStore.sheetId}))
        this.setState({
            pagination: {
                ...this.state.pagination,
                current: pagination.current
            },
            dataSource: mobx.toJS(this.props.reportStore.sheetDetail),
            count: mobx.toJS(this.props.reportStore.sheetDetail).length
        })
      }
    }

    onCellChange =  (key, dataIndex) => {         
        return async(value) => {
          const dataSource = [...this.state.dataSource];
          const target = dataSource.find(item => item.rowNo === key);
          if (target) {
            target[dataIndex] = value
            target.animation = 2
            this.setState({ dataSource, changeListStatus: false});
          }
          await this.props.reportStore.getNameById(value)
          if(this.props.reportStore.goodsName !== ''){
            target.isHave = 1
          }else{
            target.isHave = 0
          }
          target.goodsName = this.props.reportStore.goodsName
          target.price = this.props.reportStore.price
          target.imageUrl = this.props.reportStore.imageUrl
          target.barcode = this.props.reportStore.barcode
          this.setState({
            dataSource,
          })
        };
      }

      goodsNameChange =  (key, dataIndex) => {         
        return (value) => {
          const dataSource = [...this.state.dataSource];
          const target = dataSource.find(item => item.rowNo === key);
          if (target) {
            target[dataIndex] = value;
            target.animation = 2            
            this.setState({ dataSource, changeListStatus: false});
          }
        };
      }

      shopIdChange = (key, dataIndex) => {
        return (value) => { 
            if(isNaN(value*1)){
              value = this.props.reportStore.shopList.find(v => v.shopNameAlias === value).shopId
            }    
            const dataSource = this.state.dataSource
            const target = dataSource.find(item => item.rowNo === key);
            const shopNameAlias = mobx.toJS(this.props.reportStore.shopList).find(v => v.shopId === value).shopNameAlias
            if(target) {
                target[dataIndex] = value
                target.shopNameAlias = shopNameAlias
                target.animation = 2            
                this.setState({ dataSource, changeListStatus: false});
            }
        }
      }

      handleAdd = () =>{
          this.setState({
              addDetailModal: true
          })
      }

      //删除
      onDelete = async() => {
        const {selectedRows} = this.state
        const {sheetId, changeItemDetail} = this.props.reportStore        
        if (selectedRows.length === 0) {
          Modal.warning({
            title: '删除明细',
            content: '请至少选择一条明细！',
          })
        } else {
          selectedRows.forEach(item => {
            item.animation = 3
            item.status = item.status ==='通过' ? 1 : 0
          })
          await changeItemDetail({
            sheetHead: {
                sheetId: sheetId
            },
            sheetDetail: selectedRows
        })
          if(this.props.reportStore.changeDetailStatus){
            await this.props.reportStore.getItemDetail({
                sheetId,
                page: 1,
                pageSize: 100
            })
            this.setState({
                dataSource: mobx.toJS(this.props.reportStore.sheetDetail)
            })
            this.handleRowSelectChange([],[])
            this.setState({
              selectedRows: []
            })
          }
        }
      }

      //新增
      addDetail = async() =>{
        const {sheetId, changeItemDetail} = this.props.reportStore
        const dataSource = this.refs.addTable.wrappedInstance.getDataSource()
        let flag = true 
        dataSource.forEach ( item => {
          if(item.itemCode !== '' && item.goodsName !== '' && item.shopId !== ''){
            flag = true
          }else{
            flag = false
          }
        })
        if(!flag){
          message.error('请输入正确提报单')
        }else{
          dataSource.forEach( item => {
            item.animation = 1
            item.sheetId = sheetId
        })
        await changeItemDetail({
            sheetHead: {
                sheetId: sheetId
            },
            sheetDetail: dataSource
        })
        if(this.props.reportStore.changeDetailStatus){
            this.setState({addDetailModal: false})
            this.refs.addTable.wrappedInstance.clearDataSource()
            await this.props.reportStore.getItemDetail({
                sheetId,
                page: 1,
                pageSize: 100
            })
            this.setState({
                dataSource: mobx.toJS(this.props.reportStore.sheetDetail)
            })
        }    
        }
         
      }

      //保存按钮
      saveReport = async() => {
          const { dataSource} = this.state
          const changeList = dataSource.filter(v => v.animation === 2)
          const dataList = changeList
          const status =  dataList.status ==='通过' ? 1 : 0
          let flag = true      
          dataList.sheetId = this.props.reportStore.sheetId
          dataList.forEach(item => {
              item.status = item.status ==='通过' ? 1 : 0
              if(item.itemCode === '' || item.goodsName === '' || item.shopId === ''){
                  flag = false
              }     
          })
          if(!flag){
              message.error('请输入所需字段')
          }else{
            await this.props.reportStore.changeItemDetail({
                sheetHead:{
                    sheetId: this.props.reportStore.sheetId,
                    note: this.state.noteValue
                },
                sheetDetail: dataList
            })
            if(this.props.reportStore.changeDetailStatus){
                this.props.reportStore.getItemDetail({
                    sheetId: this.props.reportStore.sheetId,
                    page: 1,
                    pageSize: 100
                })
                this.setState({
                    changeListStatus: true
                })
            }
          } 
      }

      //提交按钮
      submitReport = async() => {
        const {dataSource, noteValue} = this.state 
        const {sheetId} = this.props.reportStore                 
        const changeList = dataSource.filter(v => v.animation )      
          if(this.state.changeListStatus === false){
            message.error('已修改提报单,请先保存!')
          }else{
              await this.props.reportStore.saveItemReport({
                sheetId: this.props.reportStore.sheetId,
                flag: 10,
                note: noteValue
              })
              if(this.props.reportStore.saveStatus){
                  this.props.reportStore.initSheetId()                            
                  this.props.reportStore.getItemDetail({
                      sheetId: sheetId,
                      page: 1,
                      pageSize: 100
                  })
              }
          }
      }

      //撤回
      recallReport = async() =>{
        const {sheetId} = this.props.reportStore
        await this.props.reportStore.saveItemReport({
            sheetId: this.props.reportStore.sheetId,
            flag: 0,
            note: this.state.noteValue
          })
          if(this.props.reportStore.saveStatus){  
            this.props.reportStore.initSheetId()                            
            await this.props.reportStore.getItemDetail({
                sheetId: sheetId,
                page: 1,
                pageSize: 100
            })
          }
      }

    handleRowSelectChange = (selectedRowKeys, selectedRows) => {
        this.setState({
          selectedRowKeys,
          selectedRows
        })
      }

    exportDetail = () =>{
        this.props.reportStore.exportReportDetail({sheetId:this.props.reportStore.sheetId})
    }


    render() {
        const formItemLayout = {
            labelCol: { span: 3 },
            wrapperCol: { span: 21},
          }
        const selectWidth = {
            width: '100%'
          }
        const rowSelection = {
            selectedRowKeys: this.state.selectedRowKeys,
            onChange: this.handleRowSelectChange,
          }
        const {sheetId, note, flag, creator, createTime} = this.props.reportStore.sheetHead
        const {stepsStatus, cardLoading, sheetDetail, detailTotal, getItemDetail} = this.props.reportStore
        const pagination = Object.assign({total: detailTotal}, this.state.pagination);  
        let that = this
        const props = {
          name: 'file',
          action: `${apiUrl}/o2oGoodsFilter/importO2oGoodsFilterDetail?data=${encodeURIComponent(JSON.stringify({
            sheetId: this.props.reportStore.sheetId,
            userName: this.props.store.baseUser.userName 
          }))}`,
          showUploadList: false,
          onChange: async(info) => {
            if(info.file.status === 'done'){
              if(info.file.response.status === 200){
                message.success('导入成功')
                await getItemDetail({
                  sheetId: sheetId,
                  page: 1,
                  pageSize: 100
              })
              this.setState({
                dataSource: mobx.toJS(that.props.reportStore.sheetDetail)
              })
              }else{
                message.error(info.file.response.err)
              }
            }
            // if (info.file.status !== 'uploading') {
            //   console.log(info.file, info.fileList);
            // }
            // if (info.file.status === 'done') {
            //   message.success(`${info.file.name} file uploaded successfully`);
            // } else if (info.file.status === 'error') {
            //   message.error(`${info.file.name} file upload failed.`);
            // }
          },
        };  
        return (
            <div>
                <Card loading = {cardLoading} style ={{height : 165}}>
                <Row gutter={16}>
                    <Col className="gutter-row" span={12} >
                    <FormItem {...formItemLayout} label = {'单据编码'}>
                        <p>{sheetId}</p>                                         
                    </FormItem>
                    </Col>
                    <Col className="gutter-row" span={12} >
                    <FormItem {...formItemLayout} label = {'单据状态'}>
                        <p>{flag}</p>                                      
                    </FormItem>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col className="gutter-row" span={12} >
                    <FormItem {...formItemLayout} label = {'创建人'}>
                        <p>{creator}</p>                                         
                    </FormItem>
                    </Col>
                    <Col className="gutter-row" span={12} >
                    <FormItem {...formItemLayout} label = {'创建时间'}>
                        <p>{createTime}</p>                                      
                    </FormItem> 
                    </Col>  
                </Row> 
                <Row gutter={16}>
                    <Col className="gutter-row" span={12} >
                    <FormItem {...formItemLayout} label = {'备注'}>
                    {this.props.reportStore.sheetHead.flag === '新建' ?
                        <Input value = {this.state.noteValue} onChange = {this.changeNoteValue} size= 'default'/> 
                        : <p>{this.state.noteValue}</p>                 
                    }
                    </FormItem>
                    </Col>
                </Row> 
                </Card>
                <Card style = {{marginTop: '20px'}}>
                {
                    this.props.reportStore.sheetHead.flag === '新建' &&
                    <div>
                    <Button type="primary" className="editable-add-btn" onClick={this.handleAdd}>新增</Button>
                    <Button type="primary" className="editable-add-btn" onClick={this.onDelete} style = {{marginLeft: '10px'}}>删除</Button>
                 
                    {/* <Button type="primary" onClick ={this.exportDetail} style = {{marginLeft: '10px'}}>导出</Button> */}
                    {/* <Button type="primary" onClick ={() => {window.location.href = `${apiUrl}/file/exportSheetDetail`}} style = {{marginLeft: '10px'}}>导出模板</Button> */}
                    <Button type="primary" className="editable-add-btn" onClick={this.submitReport} style = {{marginLeft:'10px',float: 'right'}}>提交</Button>                     
                    <Button type="primary" className="editable-add-btn" onClick={this.saveReport} style = {{marginLeft:'10px',float: 'right'}}>保存</Button>
                    {/* <Upload style = {{marginLeft: '10px'}} {...props}>
                      <Button  type="primary" className="editable-add-btn" >导入</Button>
                    </Upload> */}
                    </div>
                }
                {
                    this.props.reportStore.sheetHead.flag === '提报' &&
                    <Button type="primary" className="editable-add-btn" onClick={this.recallReport} style = {{marginLeft:'10px'}}>撤回</Button>                  
                }                
                <Table bordered 
                    dataSource={this.state.dataSource} 
                    columns={this.columns}
                    loading = {cardLoading}
                    pagination = {pagination}
                    rowSelection = {this.props.reportStore.sheetHead.flag === '新建' ?rowSelection : null}
                    onChange={(pagination) => this.handlePageChange(pagination)}
                />
                </Card>
                <Modal
                    title="新增明细"
                    visible={this.state.addDetailModal}
                    onCancel={() =>{this.setState({addDetailModal: false});this.refs.addTable.wrappedInstance.clearDataSource()}}
                    onOk = {this.addDetail}
                    footer={[
                        <Button key="取消" onClick={() =>{this.setState({addDetailModal: false});this.refs.addTable.wrappedInstance.clearDataSource()}}>取消</Button>,
                        <Button key="保存" type="primary" loading={this.props.reportStore.changeDetailLoading} onClick={this.addDetail}>
                          保存
                        </Button>, 
                      ]}            
                    width={1400}
                    maskClosable = {false}
                >
                    <AddTable 
                        ref = 'addTable'
                    />
                </Modal>
                <Modal visible={this.state.goodImgShow}
                  footer={null}
                  onCancel={this.handleGoodsImg}>
                    <img style={{width: '100%'}}
                    src={this.state.goodImgSrc}/>
                </Modal>
            </div>
        )
    }
}

export default ItemReportChange