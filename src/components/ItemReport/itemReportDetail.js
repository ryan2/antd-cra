import React, { Component } from 'react'
import { Table, Input, Icon, Button, message, Select, Col, Card, Form, Row, Steps, Modal } from 'antd';
import * as mobx from 'mobx'
import { observer, inject } from 'mobx-react'
import './index.css'
const Option = Select.Option;
const FormItem = Form.Item
const Step = Steps.Step
const { TextArea } = Input

@inject('reportStore')@observer
class ItemReportDetail extends Component {

    state = {
        pagination: {
            showSizeChanger: true,
            current: 1,
            pageSizeOptions: ['10', '20', '50', '100'],
            showTotal: () => `共${this.props.reportStore.detailTotal}条数据`,
          },
        selectedRowKeys: [],
        refuseModalVisible: false,
        showTextArea: false,
        goodImgShow: false,
        goodImgSrc: '',
    }

    componentWillReceiveProps = () =>{
        let pagination = this.state.pagination
        this.setState({
            pagination: {
                ...pagination,
                current: 1
            }
        })
    }
    

    handlePageChange = (pagination) => {
        let _pagination = this.state.pagination
        this.props.reportStore.getItemDetail(Object.assign({
          page: pagination.current,
          pageSize: pagination.pageSize
        }, {sheetId:this.props.reportStore.sheetId}))
        this.setState({
            pagination: {
                ..._pagination,
                current: pagination.current,
            }
        })
    }

    //勾选单据
    handleSelectChange = (selectedRowKeys, selectedRows) => {
        this.setState({
            selectedRowKeys,
        });
    }

    //点击拒审status=0、通过status=1
    handleChangeCheck = (status) => {
        const {getStatusMsg, statusMsg} = this.props.reportStore;
        if(this.state.selectedRowKeys.length === 0) {
            Modal.warning({
                title: '请至少选择一条数据！',
            });
        } else {
            if(status === 0) {
                this.setState({
                    refuseModalVisible: true
                })
                if(statusMsg.length === 0) {
                    getStatusMsg();
                }
            }
            if(status === 1) {
                this.handleRefuseConfirm(1)
            }
        }
    }

    //选择拒审原因
    handleReasonChange = (e) => {
        this.setState({showTextArea: e === '-1' ? true : false})
    }

    //取消拒审
    handleRefuseCancel = () => {
        const form = this.form;
        form.resetFields();
        this.setState({
            refuseModalVisible: false
        })
    }


    //确认拒审、通过
    handleRefuse = () => {
        const form = this.form;
        form.validateFields((err, values) => {
            if (!err) {
                let reason = values.reason;
                if(values.reason === '-1') {
                    reason =  values.elseReason;
                }
                this.handleRefuseConfirm(0, reason);
            }
        });
    }

    //确认拒审、通过
    handleRefuseConfirm = (status, refuseReason) => {
        const {sheetId, refuseCheck, getItemDetail} = this.props.reportStore;
        const _this = this;
        Modal.confirm({
            title: `是否${status === 0 ?  '拒审' : '通过'}所选商品?`,
            //content: '商品拒审后不可撤销！',
            onOk() {
                let data = _this.state.selectedRowKeys.map(item => {
                    let _data = {sheetId: sheetId, rowNo: item, status: status, animation: 4}
                    if(status === 0) {
                        _data.statusMsg = refuseReason
                    }
                    return _data
                })
                return refuseCheck({sheetHead: {sheetId: sheetId}, sheetDetail: data}).then(res=> {
                    if(res.data && res.data.status === 200) {
                        message.success('操作成功');
                        _this.setState({refuseModalVisible: false, selectedRowKeys: []})
                        getItemDetail({
                            sheetId,
                            page: _this.state.pagination.current,
                            pageSize: _this.state.pagination.pageSize || 10
                        })
                    } else {
                        message.error(res.data.err);
                    }
                }).catch(err => {
                    message.error(err);
                })
            },
            onCancel() {},
        });
    }

    //审核通过，撤销单据
    handlePassCheck = (flag) => {
        const {sheetId, passCheck, initSheetId, getItemDetail} = this.props.reportStore;
        const _sheetId = sheetId,  _this = this;
        Modal.confirm({
            title: `确认是否${flag ===100 ? '通过审核' : '撤销'}当前单据?`,
            content: flag ===100 ? '审核通过后当前单据所有状态为通过的商品，将全部通过审核。' : '撤销后此单据状态变为“新建”。',
            onOk() {
                return passCheck({sheetId: sheetId, flag: flag}).then(res=> {
                    if(res.data && res.data.status === 200) {
                        message.success('操作成功');
                        initSheetId();
                        getItemDetail({
                            sheetId: _sheetId,
                            page: _this.state.pagination.current,
                            pageSize: _this.state.pagination.pageSize || 10
                        })
                    } else {
                        message.error(res.data.err);
                    }
                }).catch(err => {
                    message.error(err);
                })
            },
            onCancel() {},
        });
    }

    refuseFormRef = (form) => {
        this.form = form;
    }

    handleGoodsImg = (imgUrl) => {
        let _imgUrl = ''
        if(imgUrl) {
          _imgUrl = imgUrl
        }
        this.setState({
          goodImgShow: !this.state.goodImgShow,
          goodImgSrc: _imgUrl
        })
      }

    render() {
        const formItemLayout = {
            labelCol: { span: 3 },
            wrapperCol: { span: 21},
          }
        const selectWidth = {
            width: '100%'
          }
        const {sheetId, note, flag, creator, createTime} = this.props.reportStore.sheetHead
        const {stepsStatus, cardLoading, sheetDetail, detailTotal, sheetHeadLoading, statusMsg} = this.props.reportStore
        const {reportCheck} = this.props
        const pagination = Object.assign({total: detailTotal}, this.state.pagination);
        const columns = [{
            title: '商品编码',
            dataIndex: 'itemCode',
            key: 'itemCode',
            width: reportCheck ? '10%' : '10%',
          },{
            title: '商品条码',
            dataIndex: 'barcode',
            key: 'barcode',
            width: reportCheck ? '15%' : '15%',
          }, {
            title: '商品名称',
            dataIndex: 'goodsName',
            key: 'goodsName',
            width: reportCheck ? '20%' : '30%',
          }, {
            title: '商品价格',
            dataIndex: 'price',
            key: 'price',
            width: reportCheck ? '10%' : '10%',
          }, {
            title: '商品图片',
            dataIndex: 'imageUrl',
            key: 'imageUrl',
            width: reportCheck ? '10%' : '10%',
            render: (text, record) => {
                return (
                  <div>
                    {record.imageUrl === '' ? '暂无图片' : 
                    <img src = {record.imageUrl} width ={50} height ={50} onClick={() => this.handleGoodsImg(record.imageUrl)}/>}
                  </div>
                )
            } 
          },{
            title: '门店名称',
            dataIndex: 'shopNameAlias',
            key: 'shopNameAlias',
            width: reportCheck ? '20%' : '25%',
          }]
        if(reportCheck) {
            columns.push({
                title: '状态',
                dataIndex: 'status',
                key: 'status',
                width: '5%',
                render: (text) =>
                    <span className={text ==='通过' ? 'itemStatusPassed' : 'itemStatusRefused'}>{text}</span>
            })
            columns.push({
                title: '拒审原因',
                dataIndex: 'statusMsg',
                key: 'statusMsg',
                width: '10%',
                render: (text) =>
                    <span className={text ==='通过' ? 'itemStatusPassed' : 'itemStatusRefused'}>{text}</span>
            })
        }
        const rowSelection = {
            selectedRowKeys: this.state.selectedRowKeys,
            onChange: this.handleSelectChange,
            /*getCheckboxProps: record => ({
                disabled: record.status === '拒绝', // 拒审的商品禁止选中
            }),*/
        };
        return (
            <div>
                <Card loading = {sheetHeadLoading} style ={{height : 165}}>
                <Row gutter={16}>
                    <Col className="gutter-row" span={12} >  
                    <FormItem {...formItemLayout} label = {'单据编码'}>
                        <p>{sheetId}</p>                                         
                    </FormItem>
                    </Col>
                    <Col className="gutter-row" span={12} >
                    <FormItem {...formItemLayout} label = {'单据状态'}>
                        <p>{ this.props.reportStore.sheetHead.flag}</p>                                      
                    </FormItem>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col className="gutter-row" span={12} >
                    <FormItem {...formItemLayout} label = {'创建人'}>
                        <p>{creator}</p>                                         
                    </FormItem>
                    </Col>
                    <Col className="gutter-row" span={12} >
                    <FormItem {...formItemLayout} label = {'创建时间'}>
                        <p>{createTime}</p>                                      
                    </FormItem>
                    </Col>  
                </Row> 
                <Row gutter={16}>
                    <Col className="gutter-row" span={12} >
                    <FormItem {...formItemLayout} label = {'备注'}>
                        <p>{note}</p>                                          
                    </FormItem>
                    </Col>
                </Row> 
                </Card>
                {reportCheck ? null :
                <Card   style ={{height : 80, marginTop : 20}}>
                    <Steps current={stepsStatus} progressDot >
                        <Step title= "新建" />
                        <Step title= "提报" />
                        <Step title= "生效" />
                    </Steps>
                </Card>}
                <Card   style ={{marginTop : 20}}>
                    {flag && flag !== '新建' && reportCheck ?
                        <Row style ={{marginBottom : 10}}>
                            <Col span={16}>
                                {flag === '提报' ?
                                <div>
                                    <Button type="primary" style ={{marginRight : 10}}
                                            onClick={() => this.handleChangeCheck(0)}>拒绝</Button>
                                    <Button type="primary" style ={{marginRight : 10}}
                                            onClick={() => this.handleChangeCheck(1)}>通过</Button>
                                </div>  : null}
                            </Col>
                            <Col span={8} style={{textAlign: 'right'}}>
                                {flag === '提报' ?
                                    <div>
                                        <Button type="primary" style ={{marginRight : 10}}
                                                onClick={() => this.handlePassCheck(0)}>撤回</Button>
                                        <Button type="primary" onClick={() => this.handlePassCheck(100)}>审核通过</Button>
                                    </div> : null
                                }
                            </Col>
                        </Row> :null}
                <Table bordered
                       dataSource={mobx.toJS(sheetDetail)}
                       columns={columns}
                       loading = {cardLoading}
                       pagination = {pagination}
                       onChange={(pagination) => this.handlePageChange(pagination)}
                       rowSelection={(reportCheck && flag === '提报')? rowSelection : null}
                />
                <Modal visible={this.state.goodImgShow}
                  footer={null}
                  onCancel={this.handleGoodsImg}>
                    <img style={{width: '100%'}}
                    src={this.state.goodImgSrc}/>
                </Modal>
                </Card>
                <RefuseForm ref={this.refuseFormRef}
                            visible={this.state.refuseModalVisible}
                            statusMsg={statusMsg}
                            onCreate={this.handleRefuse}
                            onCancel={this.handleRefuseCancel}
                            onReasonChange={this.handleReasonChange}
                            showTextArea={this.state.showTextArea}/>
            </div>
        )
    }
}

const RefuseForm = Form.create()(
    (props) => {
        const { visible, onCancel, onCreate, form, onReasonChange, showTextArea, statusMsg } = props;
        const { getFieldDecorator } = form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        return (
            <Modal
                visible={visible}
                maskClosable = {false}
                title="拒审单据"
                onCancel={onCancel}
                onOk={onCreate}
                >
                <Form>
                    <FormItem label="原因" {...formItemLayout}>
                        {getFieldDecorator('reason', {
                            rules: [{ required: true, message: '必须选择拒审原因' }],
                        })(
                            <Select placeholder="请选择拒审原因" onChange={onReasonChange}>
                                {statusMsg.map(item =>
                                    <Option key={item.msg}>{item.msg}</Option>)}
                                <Option key="-1">其他</Option>
                            </Select>
                        )}
                    </FormItem>
                    {showTextArea ?
                    <FormItem label="其它" {...formItemLayout}>
                        {getFieldDecorator('elseReason', {
                            rules: [
                                { required: true, message: '必须填写拒审原因' },
                                { max: 30, message: '内容不超过30个字'}],
                        })(
                            <TextArea placeholder="请填写拒审原因"
                                      autosize={{ minRows: 3, maxRows: 6 }}/>
                        )}
                    </FormItem> : null }
                </Form>
            </Modal>
        );
    }
);

export default ItemReportDetail