import React, { Component } from 'react'
import { Table, Input, Icon, Button, message, Select, Col, Card, Form, Row, Modal, Upload, Tag, InputNumber } from 'antd';
import * as mobx from 'mobx'
import { apiUrl } from '../../api/constant';
import { observer, inject } from 'mobx-react'
import './index.css'
const Option = Select.Option;
const FormItem = Form.Item

//商品编码组件
class EditableCell extends Component {
    
    state = {
      value: this.props.value,
    }

    componentWillReceiveProps = (nextProps) => {
      this.setState({
        value: nextProps.value
    })
    }
    

    handleChange = (e) => {
      const value = e.target.value;
        this.setState({
            value
        })
      }
    
    check = () => {
      if(this.state.value.length < 8 || this.state.value.length > 10){
        message.error('商品编码不能低于八字符且不能超过十字符!')
      }else{
        this.props.onChange(this.state.value);
      }

    }
    
    render() {
      const { value} = this.state;
      return (
        <div className="editable-cell">
              <div className="editable-cell-input-wrapper">
                <Input
                  value={value}
                  onChange={this.handleChange}
                  onPressEnter={this.check}                  
                  onBlur = {this.check}
                  placeholder = '请填写商品编码'                  
                />
              </div>
        </div>
      );
    }
  }

  //商品名称组件
  class GoodsNameCell extends Component {
    
    state = {
      value: this.props.value,
    }

    componentWillReceiveProps = (nextProps) => {
      this.setState({
          value: nextProps.value
      })
    }
    
    
    handleChange = (e) => {
      const value = e.target.value;
      if(value.length < 0){
            message.error('商品名称不能为空')
        }else if(value.length > 30){
            message.error('商品名称过长')
        }else{
            this.setState({ value });
        }
    }
    
    check = () => {
      if (this.props.onChange) {
        this.props.onChange(this.state.value);
      }
    }
    
    render() {
      const { value, editable } = this.state;
      return (
        <div className="editable-cell">
              <div className="editable-cell-input-wrapper">
                <Input
                  value={ value }
                  onChange={this.handleChange}
                  onPressEnter={this.check}
                  onBlur = {this.check}
                  disabled = {this.props.existStatus}
                  placeholder = '请填写商品名称'
                />
              </div>
        </div>
      );
    }
  }

  //商品价格组件
  class GoodsPriceCell extends Component {
    
    state = {
      value: this.props.value,
    }

    componentWillReceiveProps = (nextProps) => {
      this.setState({
          value: nextProps.value
      })
    }
    
    handleChange = (value) => {
      this.setState({value})
    }
    
    check = () => {
      if (this.props.onChange) {
        this.props.onChange(this.state.value);
      }
    }
    
    render() {
      const { value, editable } = this.state;
      return (
        <div className="editable-cell">
              <div className="editable-cell-input-wrapper">
                <InputNumber
                  width = '100%'
                  max = {999999999999} 
                  min={0}
                  value={ value }
                  onChange={this.handleChange}
                  onPressEnter={this.check}
                  onBlur = {this.check}
                  placeholder = '请填写商品价格'
                />
              </div>
        </div>
      );
    }
  }

  //门店名称组件
  @inject('store','reportStore')@observer
  class ShopIdCell extends Component {
    
    state = {
      value: [],
    }

    componentWillReceiveProps = (nextProps) => {
      if(nextProps.value !== '') {
        this.setState({
          value: [nextProps.value]
        })
      }else{
        this.setState({
          value: []
        })
      }
    }
    
    
    handleChange = (value) => {
      let flag = value.find(v => v === '-1')
      if(flag !== undefined){
        value = ['-1']
      }
      this.setState({value})      
    }
    
    check = () => {
      if (this.props.onChange) {
        let target = ''
        let targetShopId = ''
        let flag = true 
        this.state.value.forEach( item => {
          if(isNaN(item*1)){
            flag = false
          }
        })
        if(!flag){        
          this.state.value.forEach(item => {
            if(isNaN(item*1)){
              target =  item
              targetShopId =  this.props.reportStore.shopList.find(v => v.shopNameAlias === item).shopId
            }
          })
          this.state.value.splice(this.state.value.indexOf(target),1,targetShopId)
          this.props.onChange(this.state.value);
        }else{        
          this.props.onChange(this.state.value);          
        }
      }
    }


        
    render() {
      const { value} = this.state;
      const list =  mobx.toJS(this.props.reportStore.shopList)
      list.unshift({
        shopId: -1,
        shopNameAlias: '全部'
      })
      const shopList =list.map(item => <Option key={item.shopId}>{`${item.shopNameAlias}`}</Option>)
      return (
        <div className="editable-cell">
              <div className="editable-cell-input-wrapper">
                 <Select
                  mode="multiple"
                  style={{ width: '100%' }}
                  onChange = {this.handleChange}
                  value ={ value}
                  onBlur = {this.check}
                  placeholder = '请选择门店'                  
                > 
                  {shopList}
                </Select>
              </div>
        </div>
      );
    }
  } 

  @inject('reportStore', 'store')@observer
  class AddTable extends Component {
    constructor(props) {
        super(props);
        this.columns = [{
          title: '商品编码',
          dataIndex: 'itemCode',
          width: '15%',
          render: (text, record) => (
            <EditableCell
              value={text}
              onChange={this.onCellChange(record.key, 'itemCode')}
            />
          ),
        }, {
          title: '商品名称',
          dataIndex: 'goodsName',
          width: '20%',
          render: (text, record) => {
            return (
            <GoodsNameCell
              value ={text}
              onChange={this.goodsNameChange(record.key, 'goodsName')}
              goodsName = {this.state.goodsNameValue}
              existStatus = {record.existStatus}
            />
            )
        } 
        }, {
          title: '商品价格',
          dataIndex: 'price',
          width: '15%',
          render: (text, record) => {
            return (
            <GoodsPriceCell
              value ={text}
              onChange={this.goodsNameChange(record.key, 'price')}
              existStatus = {record.existStatus}
            />
            )
        } 
        },{
          title: '商品图片',
          dataIndex: 'imageUrl',
          width: '7%',
          render: (text, record) => {
            return (
              <div>
                {record.imageUrl === '' ? '暂无图片' : 
                <img src = {record.imageUrl} width ={50} height ={50} onClick={() => this.handleGoodsImg(record.imageUrl)}/>}
              </div>
            )
        } 
        },
        {
          title: '商品条码',
          dataIndex: 'barcode',
          width: '10%',
          render: (text, record) => {
            return (
              <div>
                {record.barcode === '' ? '暂无条码' : <p>{record.barcode}</p>}
              </div>
            )
        } 
        },
        {
          title: '',
          dataIndex: 'existStatus',
          width: '3%',
          render: (text, record) => {
           return (
            <span>
            {text === true ? <Tag color="green">{'已有'}</Tag> : null}
            {text === false ? <Tag color="red">{'新品'}</Tag> : null}
            </span>
           )
          } 
        }, {
          title: '门店名称',
          dataIndex: 'shopId',
          width: '25%',
          render: (text, record) => {
            if(text === undefined || text === ''){
              text = ''
            }else{
              text = this.props.reportStore.shopList.find(v => v.shopId === text).shopNameAlias

            }
            return (
            <ShopIdCell
              key = {record.key}
              value={text}
              onChange={this.shopIdChange(record.key, 'shopId', record.itemCode, record.goodsName, record.existStatus, record.price, record.imageUrl, record.barcode)}
            />)          
          }
        },{
          title: '操作',
          dataIndex: 'operation',
          width: '5%',          
          render: (text, record) => (
            <a onClick = {() => {this.deleteRow(record.key)}}>删除</a>
          )
        }];
    
        this.state = {
          dataSource: [
            {
              key: 0,
              itemCode: '',
              goodsName: '',
              shopId: '',
              barcode:'',
              imageUrl: '',
              price: '',
              existStatus: false
            }
          ],
          count: 1,
          itemCodeValue: '',
          goodsNameValue: '',
          shopIdValue: [],
          note: '',
          selectedRowKeys: [],
          goodImgShow: false,
          goodImgSrc: '',
        };
      }

      componentDidMount = () => {
        this.props.reportStore.getShopById(this.props.store.baseUser.userName)
      }

      handleGoodsImg = (imgUrl) => {
        let _imgUrl = ''
        if(imgUrl) {
          _imgUrl = imgUrl
        }
        this.setState({
          goodImgShow: !this.state.goodImgShow,
          goodImgSrc: _imgUrl
        })
      }
      
      onCellChange =  (key, dataIndex) => {         
        return async(value) => {
          const dataSource = [...this.state.dataSource];
          const target = dataSource.find(item => item.key === key);
          if (target) {
            target[dataIndex] = value;
            this.setState({ dataSource, itemCodeValue: value});
          }
          await this.props.reportStore.getNameById(value)
          target.goodsName = this.props.reportStore.goodsName
          target.barcode = this.props.reportStore.barcode
          target.imageUrl = this.props.reportStore.imageUrl
          if(this.props.reportStore.goodsName !== ''){
            target.existStatus = true
          }else{
            target.existStatus = false
          }
          this.setState({
            dataSource,
            goodsNameValue: this.props.reportStore.goodsName
          })
        };
      }

      goodsNameChange =  (key, dataIndex) => {         
        return (value) => {
          const dataSource = [...this.state.dataSource];
          const target = dataSource.find(item => item.key === key);
          if (target) {
            target[dataIndex] = value;
            this.setState({ dataSource, goodsNameValue: value});
          }
        };
      }

      shopIdChange = (key, dataIndex, itemCode, goodsName, existStatus, price, imageUrl, barcode) => {
        return (value) => {
          if(value.find(v => v=== '-1')!== undefined){
              let target = value.find(v => v==='-1')
              let index = value.indexOf(target)
              value.splice(index, 1)
              mobx.toJS(this.props.reportStore.shopList).forEach(  item => {
                value.push(item.shopId)
              })
          }   
          if(value.length >1){
            const dataSource = this.state.dataSource
            const target = dataSource.find(item => item.key === key);
            let index = dataSource.indexOf(target)
            if(index > -1){
              dataSource.splice(index, 1)
            }
            for(let i in value){
              dataSource.push({
                  key: this.state.count*1 + i*1,
                  itemCode: itemCode,
                  goodsName: goodsName,
                  shopId: value[i],
                  existStatus,
                  price,
                  imageUrl,
                  barcode
              })
            }
            this.setState({dataSource, count: dataSource[dataSource.length-1].key*1+1})
          }else{
            const dataSource = [...this.state.dataSource]
            const target = dataSource.find(item => item.key === key)
            target[dataIndex] = value[0]
            this.setState({dataSource})
          }
        }
      }

      onDelete = (key) => {
        const {selectedRowKeys} = this.state
        const dataSource = [...this.state.dataSource];
        if (selectedRowKeys.length === 0) {
          Modal.warning({
            title: '删除明细',
            content: '请至少选择一条明细！',
          })
        }else{
          selectedRowKeys.forEach(item => {
            const target = dataSource.find(v => v.key === item)
            if(dataSource.indexOf(target)> -1){
              dataSource.splice(dataSource.indexOf(target), 1)
            }
          })
          this.setState({dataSource})  
        }
      }

      handleAdd = () => {
        const { count, dataSource } = this.state;
        let flag = true 
        dataSource.forEach ( item => {
          if(item.itemCode !== '' && item.goodsName !== '' && item.shopId !== '' && item.price !==''){
            flag = true
          }else{
            flag = false
          }
        })
        if(!flag){
          message.error('请输入所需字段!')
        }else{
          const newData = {
            key: count,
            itemCode: '',
            goodsName: '',
            shopId: '',
            barcode: '',
            imageUrl: '',
            price: '',
            existStatus: false
          };
          this.setState({
            dataSource: [...dataSource, newData],
            count: count + 1,
          });
        }
      }

      clearDataSource = () => {
          this.setState({
              dataSource: [
                {
                  key: 0,
                  itemCode: '',
                  goodsName: '',
                  shopId: '',
                  barcode: '',
                  imageUrl: '',
                  price: '',
                  existStatus: false
                }
              ]
          })
      }

      //删除行
      deleteRow = (key) => {
        const dataSource = [...this.state.dataSource];
        this.setState({ dataSource: dataSource.filter(item => item.key !== key) });
      }

      getDataSource = () =>{
          return this.state.dataSource
      }

      handleRowSelectChange = (selectedRowKeys, selectedRows) => {
        this.setState({
          selectedRowKeys
        })
      }

      render() {
        const rowSelection = {
          onChange: this.handleRowSelectChange,
        }
        const props = {
          name: 'file',
          action: `${apiUrl}/o2oGoodsFilter/importO2oGoodsFilterDetail?data=${encodeURIComponent(JSON.stringify({
            sheetId: '',
            userName: this.props.store.baseUser.userName
          }))}`,
          showUploadList: false,
          onChange: async(info) => {
            if(info.file.status === 'done'){
              if(info.file.response.status === 200){
                message.success('导入成功')
                this.props.closeModal()
                await this.props.reportStore.getReportList(Object.assign({
                  page: this.props.page,
                  pageSize: this.props.pageSize
                },this.props.searchData))
              }else{
                message.error(info.file.response.err)
              }
            }
          },
        }; 
        return (
            <div>
                <Button type="primary" className="editable-add-btn" onClick={this.handleAdd}>新增</Button>
                <Button type="primary" className="editable-add-btn" onClick={this.onDelete} style ={{marginLeft: 10}}>删除</Button>
                {
                  this.props.exportStatus && 
                  <span>
                  {/* <Button type="primary" onClick ={() => {window.location.href = `${apiUrl}/file/exportSheetDetail`}} style = {{marginLeft: '10px'}}>导出模板</Button> 
                  <Upload style = {{marginLeft: '10px'}} {...props}>
                        <Button  type="primary" className="editable-add-btn" >导入</Button>
                  </Upload>   */}
                  </span>
                }
                <Table bordered
                 dataSource={this.state.dataSource} 
                 columns={this.columns} 
                 rowSelection= {rowSelection}
                 />
                <Modal visible={this.state.goodImgShow}
                  footer={null}
                  onCancel={this.handleGoodsImg}>
                    <img style={{width: '100%'}}
                    src={this.state.goodImgSrc}/>
                </Modal>
            </div>
        )
      }
  }
  
  export default AddTable