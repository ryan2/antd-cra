import React, { Component } from 'react'
import { Row, Col, Form, Input, Button, message, DatePicker, Select } from 'antd'
import { observer, inject } from 'mobx-react'
import moment from 'moment'
const FormItem = Form.Item
const Option = Select.Option

class itemReportList extends Component {

    componentDidMount() {
        this.props.form.setFieldsValue({
            flag: ['0','10']
        })
    }

    state = {
        startDate: null,
        endDate: null,
        status : null,
    }

    disabledDate = (current) =>{
        return current && current < moment(this.state.startDate)
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, values) => {
            if(!err){
                values.startDate = values.startDate !== undefined ? moment(values.startDate).format('YYYY-MM-DD'): values.startDate
                values.endDate = values.endDate !== undefined ?  moment(values.endDate).format('YYYY-MM-DD'): values.endDate
                this.props.onSearch(values)
            }
        })
    }

    handleReset = () => {
        this.props.form.resetFields()
        // this.props.onSearch()
    }

    render() {
        const formItemLayout = {
            labelCol: {      
              xs: { span: 24 },
       
              sm: { span: 10 },
       
              md: { span: 8 },
       
              lg: { span: 7 },
       
              xl: { span: 5 },
       
            },
            wrapperCol: {         
              xs: { span: 24 },
       
              sm: { span: 14 },
       
              md: { span: 16 },
       
              lg: { span: 16 },
       
              xl: { span: 14 },
            },
          }
          const { getFieldDecorator } = this.props.form
          const selectWidth = {
            width: '100%'
          }
          const dateFormat = 'YYYY-MM-DD'
        return (
            <div>
                <Form
                    onSubmit={this.handleSubmit} className="ant-advanced-custom-form"
                >
                    <Row gutter={16}>
                    <Col className="gutter-row" span={6} >
                        <FormItem {...formItemLayout} label={'单据状态'} >
                        {getFieldDecorator('flag')(
                            <Select placeholder="全部" style={selectWidth} size = 'default' mode = 'multiple'>                                                                                                                                                                                                                                                                                                                                                                                   
                            <Option value="0">新建</Option>                                                                                                                                                                                                                                                                                                                                                                                    
                            <Option value="10">提报</Option>                                                                                                                                                                                                                                                                                                                                                                                    
                            <Option value="100">生效</Option>                                                                                                                                                                                                                                                                                                                                                                                    
                            </Select>
                            )}
                        </FormItem>
                    </Col>
                    <Col className="gutter-row" span={6}>
                    <FormItem {...formItemLayout} label={'开始日期'}>
                      {getFieldDecorator('startDate')(
                       <DatePicker placeholder="请选择开始日期" format={dateFormat} style={selectWidth} onChange = {(date,dateString) =>{this.setState({startDate : dateString})}} size = 'default'/>
                        )}
                    </FormItem>
                    </Col>
                    <Col className="gutter-row" span={6}>
                        <FormItem {...formItemLayout} label={'结束日期'}>
                        {getFieldDecorator('endDate')(
                        <DatePicker  placeholder="请选择结束日期" format={dateFormat} style={selectWidth} onChange = {(date,dateString) =>{this.setState({endDate : dateString})}} size = 'default' disabledDate={this.disabledDate}/>
                            )}
                        </FormItem>
                    </Col>
                    <Col className="gutter-row" style={{ textAlign: 'right' }} span={6}>             
                            <Button type="primary" size="default" htmlType="submit" >查询</Button> 
                            <Button size="default" style={{ marginLeft: 8 }} onClick={this.handleReset}>清空</Button> 
                    </Col>
                    </Row>
                    <Row gutter={16} style = {{marginBottom: 16}}>
                    <Col className="gutter-row" span={16}>              
                        <Button type="primary" onClick = {this.props.changeReportAdd} style ={{marginRight:'8px'}}>新建单据</Button>
                    </Col>
                    </Row>
                </Form>               
            </div>
        )
    }
}

export default Form.create()(itemReportList)
