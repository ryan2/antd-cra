import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { observer, inject } from 'mobx-react'
import { Table, Tooltip, Menu, Tag, Modal, Button,Timeline, Icon} from 'antd'
import LockPriceForm from './LockPriceForm'
import './lockPrice.css'

// Global Component
@inject('lockPriceStore', 'store')@observer
class LockPrice extends Component {
  constructor(props){
    super(props)
    const { cacheData } = props.lockPriceStore;
    this.state = {
      pagination: {
        defaultCurrent: props.lockPriceStore.page || 1,
        defaultPageSize: props.lockPriceStore.pageSize || 10,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '50', '100'],
        showTotal: () => `共${this.props.lockPriceStore.total}条数据`,
      },
      searchData: props.lockPriceStore.cacheData,
      widowWidth: document.body.offsetWidth,
      widowHeight: document.body.offsetHeight,
      visible:false,
    }
  }

  componentWillMount() {
  }

  componentDidMount() {
    const { getO2oGoodsLockPriceList, cacheData, lockPriceList,} = this.props.lockPriceStore;
    let _cacheData = { ...cacheData }
    window.addEventListener('resize', this.onWindowResize)
    if(lockPriceList.length === 0) {
      //getO2oGoodsLockPriceList(cacheData)
    }
  }

  componentWillReceiveProps(nextProps) {
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onWindowResize)
  }

  onWindowResize = () => {
    this.setState({
      widowWidth: document.body.offsetWidth,
      widowHeight: document.body.offsetHeight
    })
  }

  handlePageChange = (pagination) => {
    const { searchData } = this.state;
    this.state.pagination.current = pagination.current;
    this.props.lockPriceStore.getO2oGoodsLockPriceList({
      ...searchData,
      page: pagination.current,
      pageSize: pagination.pageSize,
    })
    this.setState({
      pagination: this.state.pagination
    })
  }

  handleSearchSubmit = (params) => {
    this.props.lockPriceStore.getO2oGoodsLockPriceList({...params})
    this.state.pagination.current = 1;
    this.setState({
      searchData: params,
      pagination: this.state.pagination
    })
  }

  handleFormReset = () => {
    this.props.lockPriceStore.getO2oGoodsLockPriceList({})
    this.state.pagination.current = 1;
    this.setState({
      searchData: {},
      pagination: this.state.pagination
    })
    this.props.lockPriceStore.saveCacheData()
  }

  showDetailModal=(itemCode,shopId,channelId) => {
    let params ={}
    params.itemCode=itemCode
    params.shopId=shopId
    params.channelId=channelId
    this.setState({visible:true})
    this.props.lockPriceStore.getO2oGoodsLockPriceByItemCode(params)
  }

  handleOk = () => {
    this.setState({
      visible: false
    })
  }

  handleCancel = () => {
    this.setState({
      visible: false
    })
  }


  render() {
    const tagStyle = { width: 54, textAlign: 'center'}
    const canEdit = this.props.store.hasUserAuth('edit');
    const columns = [
      {
        title: '商品编码',
        dataIndex: 'itemCode',
        key: 'itemCode',
        width: 80,
      }, 
      {
        title: '商品名称',
        dataIndex: 'goodsName',
        key: 'goodsName',
        width: 200,
      }, 
      {
        title: '家乐福门店',
        dataIndex: 'shopId',
        key: 'shopId',
        width: 150,
        render: (text, record) => <span>{`[${record.shopId}]${record.shopName}`}</span>
      }, 
      {
        title: '渠道',
        dataIndex: 'channelId',
        key: 'channelId',
        width: 120,
        render: (text, record) => <span>{`${record.channelName}`}</span>
      }, 
      {
        title: '锁定价格',
        dataIndex: 'lockPrice',
        key: 'lockPrice',
        width: 80,
        render:(text, record) =><span>{Number(text).toFixed(2)}</span>
      }, 
      {
        title: '生效档期',
        dataIndex: 'startTime',
        key: 'startTime',
        width: 180,
        render: (text, record) => <span>{`${record.startTime}--${record.endTime}`}</span>
      }, 
      {
        title: '创建时间',
        dataIndex: 'createTime',
        key: 'createTime',
        width: 180,
      },
      {
        title: '操作',
        key: 'action',
        fixed: this.state.widowWidth > 1360 ? false : 'right',
        width: 80,
        // fixed:'right',
        render: (text, record) => (
          <span>
              <a onClick={() =>{this.showDetailModal(record.itemCode,record.shopId,record.channelId)}}>查看记录</a>
          </span>
          
        ),
      }
    ]
    const { loading, lockPriceList, total, cacheData,lockPrices} = this.props.lockPriceStore
    const { widowHeight, widowWidth } = this.state
    const pagination = Object.assign({total: total}, this.state.pagination)
    return (
      <div >
        {this.props.store.collapse ?
            <h2 style={{marginBottom: 16, flex: '0 0 auto'}}>
              <Link to="/lockPrice">商品锁价管理</Link>
            </h2> : null
        }
        <LockPriceForm wrappedComponentRef={(inst) => this.formRef  = inst}
                          history={this.props.history}
                          defaultValues={cacheData}
                          onSearch={this.handleSearchSubmit}
                          onReset={this.handleFormReset}
                          canEdit={canEdit}/>
        <div style={{display: 'flex', position: 'relative'}}>
          <div ref='tableRapper' className="tableRapper">
            <Table style={{ height: widowHeight - 335,width:widowWidth - 198}} scroll={{ y: widowHeight - 335}}
                   loading={loading}
                   columns={columns}
                   dataSource={lockPriceList.slice()}
                   pagination={pagination}
                   onChange={(pagination) => this.handlePageChange(pagination)}/>
          </div>
        </div>
        <Modal
          title="锁价调整记录"
          visible={this.state.visible}
          // onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={null}
        >
         <Timeline>
           {
             lockPrices.map(item =>
             <Timeline.Item key={item.key} dot={item.flag===1 ?<Icon type="clock-circle-o" style={{ fontSize: 16,color :'red'}} />:null}>
              <span> <Icon type="calendar" style={{ fontSize: 16, marginRight:8}}/>{item.startTime}--{item.endTime}</span>
              <span> <Icon type="pay-circle-o" style={{ fontSize: 16, color: 'red',marginRight:4, marginLeft:20}}/> {Number(item.lockPrice).toFixed(2)}</span>
             </Timeline.Item>
             )
           }
           
        </Timeline>
        </Modal>
      </div>
    )
  }
}
export default LockPrice
