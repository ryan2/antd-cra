import React, { Component } from 'react'
import { Row, Col, Form, Input, Button, message, Select } from 'antd'
import { observer, inject } from 'mobx-react'
import * as mobx from 'mobx'
import FileOperation from '../FileOperation/FileOperation'
// import {apiUrl} from '../../api/constant'
const FormItem = Form.Item
const Option = Select.Option
@inject('lockPriceStore' ,'store')@observer
class LockPriceForm extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentWillMount(){
      this.props.lockPriceStore.getChannelShopsList();
      this.props.lockPriceStore.getCityList();
  }
  handleSubmit = (e) => {
    e.preventDefault()
    // const {defaultValues} = this.props;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
          if(values.status === '') {
            delete values.status
          }
          this.props.onSearch(values)
      }
    })
  }
  cityChange =  (value) =>{
    this.props.lockPriceStore.getShopListByCityList({
      city: value
    })
    if(value !== undefined){
      this.props.form.setFieldsValue({
         shopId:[]   
      })      
    }
 }
  handleReset = () => {
    const {defaultValues} = this.props;
    this.props.lockPriceStore.clearShopList()
    this.props.form.validateFields((err, values) => {
      if (JSON.stringify(values) === "{}" && !defaultValues.itemCode && !defaultValues.goodsName && !defaultValues.channelId) {
        message.warning('查询条件已经为空')
      } else {
        this.props.form.resetFields()
        // this.props.onReset()
      }
    })
  }

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
        md: { span: 8 },
        lg: { span: 7 },
        xl: { span: 5 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
        md: { span: 16 },
        lg: { span: 16 },
        xl: { span: 14 },
      },
    }
    const { defaultValues, categoryId, canEdit} = this.props
    const { getFieldDecorator, getFieldsValue } = this.props.form
    return (
      <div className="searchForm">
        <Form onSubmit={this.handleSubmit}>
          <Row gutter={16}>
          <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'城市'}>
                {getFieldDecorator('city', {
                  rules: [{ required: false, message: 'Please input city!' }],
                })(
                  <Select onChange = {this.cityChange} size='default' mode="multiple" placeholder="请选择城市">
                    {mobx.toJS(this.props.lockPriceStore.cityList).map(data =><Option key={data.key}>{data.value}</Option>)}
                  </Select>
                  )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'门店名称'}>
                {getFieldDecorator('shopId', {
                  rules: [{ required: false, message: 'Please input store!' }],
                })(
                  <Select size = 'default'  placeholder="请选择门店" mode="multiple">
                    {mobx.toJS(this.props.lockPriceStore.shopList).map(data =><Option key={data.shopId}>{data.shopNameAlias}</Option>)}
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品编码'}>
                {getFieldDecorator('itemCode', {
                  initialValue: defaultValues.itemCode
                })(
                    <Input placeholder="请输入商品编码" size="default"/>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品名称'}>
                {getFieldDecorator('goodsName', {
                  initialValue: defaultValues.goodsName
                })(
                    <Input placeholder="请输入商品名称" size="default"/>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'渠道'}>
                {getFieldDecorator('channelId', {
                  initialValue: defaultValues.channelId
                })(
                  <Select placeholder = '请选择商品渠道' size = 'default'>
                      <Option value="">全部</Option> 
                      {mobx.toJS(this.props.lockPriceStore.channelList).map(data =><Option key={data.channelId}>{data.channelName}</Option>)}
                    </Select>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row  gutter={16}>
            <Col span={12} style={{ marginBottom: 16}}>
              <FileOperation history={this.props.history} type="ImportLockPrice" style={{marginLeft: 8}}
                                                 buttonText="导入" buttonProps={{type: 'primary', size: 'default'}}
                                                 fileType={["xlsx", "xls"]}/>  
              <FileOperation type="template" style={{marginLeft: 8}}
                                                buttonText="下载模板" buttonProps={{type: 'primary'}}
                                                templateUrl="/file/exportLockPrice"/>
            </Col>
            <Col span={12} style={{ textAlign: 'right', marginBottom: 16}}>
              <Button type="primary" htmlType="submit" size="default">查询</Button>
              <Button onClick={this.handleReset} size="default" style={{ marginLeft: 8 }}>清空</Button>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}
export default Form.create()(LockPriceForm)

