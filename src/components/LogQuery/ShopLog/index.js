import React, {
    Component
} from 'react'
import ShopLogListForm from './shopLogList'
import {
    observer,
    inject
} from 'mobx-react'
import ShopLogTable from './shopLogTable'

@inject('store') @observer
class ShopLog extends Component {
    state = {
        page: undefined,
        pageSize: undefined,
        formList: {}
    }

    componentWillMount() {
        this.props.store.getChannelList()
    }
    
    handleSubmit = (page, pageSize, taskType, operator, shopId, channelId, channelShopId, startDate, endDate, processStatus) => {
        this.setState({
            page: this.refs.ShopLogTable.wrappedInstance.state.page,
            pageSize: this.refs.ShopLogTable.wrappedInstance.state.pageSize
        })
        this.refs.ShopLogTable.wrappedInstance.changeMsgData(page, pageSize, taskType, operator, shopId, channelId, channelShopId, startDate, endDate, processStatus)
    }

    getFormList = formList => {
        this.setState({
            formList
        })
    }

    onReset = (page, pageSize) => {
        this.refs.ShopLogTable.wrappedInstance.onReset(page, pageSize)
    }

    render() {
        let title
        if (this.props.store.collapse) {
            title = <h2 style={{ marginBottom: 16 ,color: '#108ee9'}}>门店日志</h2>
        } else {
            title = ''
        }
        return (
            <div>
                {title}
                <ShopLogListForm 
                    handleSubmit = {this.handleSubmit}
                    page = {this.state.page}
                    pageSize = {this.state.pageSize}
                    getFormList = {this.getFormList}
                    onReset = {this.onReset}
                    channelList = {this.props.store.channelList}
                    history = {this.props.history}
                />
                <ShopLogTable
                    ref = 'ShopLogTable'
                    channelList = {this.props.store.channelList}
                    formList = {this.state.formList}
                />
            </div>)
    }
}

export default ShopLog