import React, {
  Component
} from 'react'
import {
  Row,
  Col,
  Form,
  Select,
  Input,
  Button,
  DatePicker,
  message
} from 'antd'
import {toJS} from 'mobx'
import FileOperation from '../../FileOperation/FileOperation'
import moment from 'moment'
const Option = Select.Option
const FormItem = Form.Item


class ShopLogList extends Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.formList = {}
  }

  state = {
    startDate: '',
    endDate: '',
  }

  componentDidMount() {
    this.props.form.setFieldsValue({
      startDate: moment(moment().format('YYYY-MM-DD')),
      endDate: moment(moment().format('YYYY-MM-DD')),
    })
    this.setState({
      startDate: moment().format('YYYY-MM-DD'),
      endDate: moment().format('YYYY-MM-DD')
    })
  }

  disabledDate = (current) => {
    return current && current < moment(this.state.startDate)
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.onReset(1, 10)
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        message.error(err)
      } else {
        const page = this.props.page
        const pageSize = this.props.pageSize
        const taskType = values.taskType
        const operator = values.operator
        const shopId = values.shopId
        const channelId = values.channelId
        const startDate = this.state.startDate
        const endDate = this.state.endDate
        const processStatus = values.processStatus
        const formList = {
          taskType: taskType,
          operator: operator,
          shopId: shopId,
          channelId: channelId,
          startDate: startDate,
          endDate: endDate,
          processStatus: processStatus
        }
        this.formList = formList
        if (startDate === undefined && endDate !== undefined) {
          message.error('请选择开始日期')
        } else if (endDate === undefined && startDate !== undefined) {
          message.error('请选择结束日期')
        } else {
          this.props.getFormList(formList)
          this.props.handleSubmit(1, 10, taskType, operator, shopId, channelId, '', startDate, endDate, processStatus)
        }
      }
    })
  }

  handleReset = () => {
    this.formList = {
      taskType: undefined,
      operator: undefined,
      shopId: undefined,
      channelId: undefined,
      startDate: undefined,
      endDate: undefined
    }
    this.setState({
      startDate: undefined,
      endDate: undefined
    })
    this.props.getFormList(this.formList)
    this.props.form.resetFields()
    // this.props.handleSubmit()
    this.props.onReset(1, 10)
  }
  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
  
        sm: { span: 10 },
  
        md: { span: 8 },
  
        lg: { span: 7 },
  
        xl: { span: 5 },
  
      },
      wrapperCol: {         
        xs: { span: 24 },
  
        sm: { span: 14 },
  
        md: { span: 16 },
  
        lg: { span: 16 },
  
        xl: { span: 14 },
      },
    }
    const {
      getFieldDecorator,
      getFieldsValue
    } = this.props.form
    const selectWidth = {
      width: '100%'
    }
    const dateFormat = 'YYYY-MM-DD'
    return (
      <div>
        <Form
          onSubmit={this.handleSubmit} className="ant-advanced-custom-form"
        >
          <Row gutter={16}>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'门店编码'}>
                {getFieldDecorator('shopId')(
                  <Input placeholder="请输入门店编码" style={selectWidth} size = 'default'/>
                  )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'操作人'}>
                {getFieldDecorator('operator')(
                  <Input placeholder="请输入操作人" style={selectWidth} size = 'default'/>
                  )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6} >            
              <FormItem {...formItemLayout} label='渠道'>
                    {getFieldDecorator('channelId')(
                      <Select placeholder="全部" style={selectWidth} size = 'default'>                                       
                        <Option value="">全部</Option> 
                        {
                          toJS(this.props.channelList).map( item => <Option key = {item.channelId}>{item.channelName}</Option>)
                        }
                      </Select>
                      )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6} >            
              <FormItem {...formItemLayout} label='状态'>
                    {getFieldDecorator('processStatus')(
                      <Select placeholder="全部" style={selectWidth} size = 'default'>                                       
                        <Option value="">全部</Option> 
                        <Option value="0">待处理</Option>
                        <Option value="100">完成</Option>
                        <Option value="99">异常</Option>
                      </Select>
                      )}
              </FormItem>
            </Col>        
          </Row>
          <Row gutter={16} style = {{marginBottom :'16px'}}>
          <Col className="gutter-row" span={6} >
              <FormItem {...formItemLayout} label={'任务类型'} >
                {getFieldDecorator('taskType')(
                  <Select placeholder="全部" style={selectWidth} size = 'default'>
                    <Option value="">全部</Option>
                    <Option value="1">营业状态修改</Option>
                    <Option value="2">营业时间修改</Option>
                    <Option value="3">店铺公告修改</Option>
                    <Option value="4">店铺信息修改</Option>                                                                                                                                          
                  </Select>
                  )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'开始日期'}>
                {getFieldDecorator('startDate')(
                  <DatePicker placeholder="请选择开始日期" format={dateFormat} style={selectWidth} onChange = {(date,dateString) =>{this.setState({startDate : dateString})}} size = 'default'/>
                  )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'结束日期'}>
              {getFieldDecorator('endDate')(
              <DatePicker  placeholder="请选择结束日期" format={dateFormat} style={selectWidth} onChange = {(date,dateString) =>{this.setState({endDate : dateString})}} size = 'default' disabledDate= {this.disabledDate}/>
                  )}
              </FormItem>
            </Col>
            <Col className="gutter-row" style={{ textAlign: 'right' }} span={6}>             
              <Button type="primary" size="default" htmlType="submit" >查询</Button>
              <FileOperation history={this.props.history} type="ExportChannelShopLog"
                              style={{margin: '0 8px'}}
                              buttonText="导出" buttonProps={{type: 'primary', size:'default'}}
                              queryData={getFieldsValue()}/>
              <Button size="default" onClick={this.handleReset}>清空</Button>
            </Col>
          </Row>
        </Form>
      </div>)
  }
}
const ShopLogListForm = Form.create()(ShopLogList)
export default ShopLogListForm