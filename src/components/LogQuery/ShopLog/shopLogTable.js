import React, {
    Component
} from 'react'
import {
    Table,
    Tooltip
} from 'antd'
import * as mobx from 'mobx'
import {
    observer,
    inject
} from 'mobx-react'

@inject('logStore') @observer
class ShopLogTable extends Component {
    state = {
        dataSource: [],
        pagination: {},
        page: undefined,
        pageSize: undefined,
        channelId: undefined,
        shopId: undefined,
        channelShopId: undefined,
    }

    componentDidMount() {
        if (this.props.channelId !== undefined && this.props.shopId !== undefined && this.props.channelShopId !== undefined) {
            this.changeMsgData(this.state.page, this.state.pageSize, '', '', this.props.shopId, this.props.channelId, this.props.channelShopId)
            this.setState({
                channelId: this.props.channelId,
                shopId: this.props.shopId,
                channelShopId: this.props.channelShopId
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        const {
            channelId,
            shopId,
            channelShopId
        } = this.state;
        if (nextProps.channelId !== undefined && nextProps.shopId !== undefined && nextProps.channelShopId !== undefined && (nextProps.channelId !== channelId || nextProps.shopId !== shopId || nextProps.channelShopId !== channelShopId)) {
            this.changeMsgData(this.state.page, this.state.pageSize, '', '', nextProps.shopId, nextProps.channelId, nextProps.channelShopId);
            this.setState({
                channelId: nextProps.channelId,
                shopId: nextProps.shopId,
                channelShopId: nextProps.channelShopId
            })
        }
    }

    changeMsgData = async (page, pageSize, taskType, operator, shopId, channelId, channelShopId, startDate, endDate, processStatus) => {
        let msgListData = []
        if (this.props.channelId !== undefined && this.props.shopId !== undefined && this.props.channelShopId !== undefined) {
            await this.props.logStore.getShopLogList(page, pageSize, taskType, operator, this.props.shopId, this.props.channelId, this.props.channelShopId, startDate, endDate, processStatus)
            msgListData = mobx.toJS(this.props.logStore.shopLogList)
        } else {
            await this.props.logStore.getShopLogList(page, pageSize, taskType, operator, shopId, channelId, channelShopId, startDate, endDate, processStatus)
            msgListData = mobx.toJS(this.props.logStore.shopLogList)
        }
        this.init(msgListData)
    }

    init = (msgListData) => {
        const dataSource = this.dataSourceFun(msgListData)
        this.setState({
            dataSource
        })
    }

    onReset = (page, pageSize) => {
        this.setState({
            pagination: {
                current: page,
                pageSize: pageSize
            }
        })
    }

    getChanne = id => {
        const channe = this.props.channelList.find(p => p.channelId === id);
        if (channe) return channe.channelName
        else return '无渠道'
    }
    
    dataSourceFun = (msgListData) => {
        if (msgListData.length !== 0) {
            const dataSource = msgListData.data.map(data => {
                return {
                    key: data.key,
                    channelId: this.getChanne(data.channelId),
                    operator: data.operator,
                    shopId: data.shopId,
                    channelShopId: data.channelShopId,
                    taskType: data.taskType,
                    createTime: data.createTime,
                    isOpen: data.isOpen,
                    notice: data.notice,
                    openTime: data.openTime,
                    processStatus: data.processStatus,
                    processMsg: data.processMsg,
                    processTime: data.processTime
                }
            })
            this.paginationFun(msgListData)
            return dataSource
        }
    }

    paginationFun = (data) => {
        const total = data.total
        this.setState({
            pagination: {
                total,
                showTotal: () => `共${total}条数据`,
                pageSizeOptions: ['10', '20', '50', '100'],
                showSizeChanger: true,
            }
        })
    }

    handleChange = (pagination) => {
        if (this.props.channelId === undefined && this.props.shopId === undefined && this.props.channelShopId === undefined) {
            const {
                channelId,
                endDate,
                operator,
                shopId,
                startDate,
                taskType,
                processStatus
            } = this.props.formList
            this.changeMsgData(pagination.current, pagination.pageSize, taskType, operator, shopId, channelId, '', startDate, endDate, processStatus)
        } else {
            this.changeMsgData(pagination.current, pagination.pageSize, '', '', this.props.shopId, this.props.channelId, this.props.channelShopId)
        }
        this.setState({
            ...pagination,
            page: pagination.current,
            pageSize: pagination.pageSize
        })
    }

    render() {
        const columns = [{
                title: '任务类型',
                dataIndex: 'taskType',
                key: 'taskType',
            },
            {
                title: '渠道',
                dataIndex: 'channelId',
                key: 'channelId',
            },
            {
                title: '操作人',
                dataIndex: 'operator',
                key: 'operator',
            },
            {
                title: '门店编码',
                dataIndex: 'shopId',
                key: 'shopId',
            },
            {
                title: '渠道店铺编码',
                dataIndex: 'channelShopId',
                key: 'channelShopId',
            },
            {
                title: '任务创建时间',
                dataIndex: 'createTime',
                key: 'createTime',
            },
            {
                title: '任务执行时间',
                dataIndex: 'processTime',
                key: 'processTime',
            },
            {
                title: '状态',
                dataIndex: 'processStatus',
                key: 'processStatus',
            },
            {
                title: '错误信息',
                dataIndex: 'processMsg',
                key: 'processMsg',
                render: (text, record) => {
                        if (record.processMsg === 'success') {
                            return ('')
                        } else {
                            return (
                                record.processMsg &&
                                    <Tooltip title={record.processMsg}>
                                        <span>{`${record.processMsg.substring(0, 8)}...`}</span>
                                    </Tooltip>)
                        }
                    }
            },
            {
                title: '修改结果',
                    dataIndex: 'isOpen' || 'notice' || 'openTime',
                    key: 'isOpen' || 'notice' || 'openTime',
                    render: (text, record) => {
                        const isOpen = (record.isOpen === 1 && '开店') || (record.isOpen === 0 && '关店')
                        text = (record.isOpen === undefined || record.isOpen === '' && record.notice === undefined || record.notice === '' ? record.openTime : undefined) ||
                            (record.notice === undefined || record.notice === '' && record.openTime === undefined || record.openTime === '' ? isOpen : undefined) ||
                            (record.isOpen === undefined || record.isOpen === '' && record.openTime === undefined || record.openTime === '' ? record.notice : undefined)
                        if (record.taskType === "店铺信息修改") {
                            text = undefined
                        }
                        return (
                            text
                        )
                    }
            }]
        return(
            <Table 
                style = {{marginTop:'16px' , width: '100%'}}
                columns = {columns}
                style = {{width : '100%'}}
                dataSource = {this.state.dataSource}
                pagination = {this.state.pagination}
                onChange = {this.handleChange} 
                loading = {this.props.logStore.loading}
            />
        )
    }
}

export default ShopLogTable