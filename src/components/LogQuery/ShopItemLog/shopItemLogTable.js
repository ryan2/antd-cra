import React, {
    Component
} from 'react'
import {
    Table,
    Tooltip,
    Tag
} from 'antd'
import * as mobx from 'mobx'
import {
    observer,
    inject
} from 'mobx-react'

@inject('logStore') @observer
class ShopItemLogTable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataSource: [],
            pagination: {},
            page: undefined,
            pageSize: undefined,
            channelId: undefined,
            shopId: undefined,
            itemId: undefined,
        }
    }
    componentDidMount() {
        if (this.props.channelId !== undefined && this.props.shopId !== undefined && this.props.itemId !== undefined) {
            this.changeMsgData(this.state.page, this.state.pageSize, '', '', this.props.shopId, this.props.channelId, this.props.itemId)
            this.setState({
                channelId: this.props.channelId,
                shopId: this.props.shopId,
                itemId: this.props.itemId
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        const {
            channelId,
            shopId,
            itemId
        } = this.state;
        if (nextProps.channelId !== undefined && nextProps.shopId !== undefined && nextProps.itemId !== undefined && (nextProps.channelId !== channelId || nextProps.shopId !== shopId || nextProps.itemId !== itemId)) {
            this.changeMsgData(this.state.page, this.state.pageSize, '', '', nextProps.shopId, nextProps.channelId, nextProps.itemId);
            this.setState({
                channelId: nextProps.channelId,
                shopId: nextProps.shopId,
                itemId: nextProps.itemId
            })
        }
    }

    onReset = (page, pageSize) => {
        this.setState({
            pagination: {
                current: page,
                pageSize: pageSize
            }
        })
    }

    changeMsgData = async (page, pageSize, taskType, operator, shopId, channelId, itemCode, startDate, endDate, processStatus, batchId) => {
        let msgListData = []
        if (this.props.channelId !== undefined && this.props.shopId !== undefined && this.props.itemId !== undefined) {
            await this.props.logStore.getGoodsLogList(page, pageSize, taskType, operator, this.props.shopId, this.props.channelId, this.props.itemId, startDate, endDate, processStatus, batchId)
            msgListData = mobx.toJS(this.props.logStore.shopItemLogList)
        } else {
            await this.props.logStore.getGoodsLogList(page, pageSize, taskType, operator, shopId, channelId, itemCode, startDate, endDate, processStatus, batchId)
            msgListData = mobx.toJS(this.props.logStore.shopItemLogList)
        }
        this.init(msgListData)
    }

    init = (msgListData) => {
        const dataSource = this.dataSourceFun(msgListData)
        this.setState({
            dataSource
        })
    }

    getChanne = id => {
        const channe = this.props.channelList.find(p => p.channelId === id);
        if (channe) return channe.channelName
        else return '无渠道'
    }
    
    dataSourceFun = (msgListData) => {
        if (msgListData.length !== 0) {
            const dataSource = msgListData.data.map(data => {
                return {
                    key: data.key,
                    channelId: this.getChanne(data.channelId),
                    operator: data.operator,
                    shopId: data.shopId,
                    channelShopId: data.channelShopId,
                    taskType: data.taskType,
                    createTime: data.createTime,
                    price: data.price,
                    batchId: data.batchId,
                    shelved: data.shelved,
                    openTime: data.openTime,
                    processStatus: data.processStatus,
                    itemCode: data.itemCode,
                    processMsg: data.processMsg,
                    logId: data.logId,
                    processTime: data.processTime
                }
            })
            this.paginationFun(msgListData)
            return dataSource
        }
    }

    paginationFun = (data) => {
        const total = data.total
        this.setState({
            pagination: {
                total,
                showTotal: () => `共${total}条数据`,
                pageSizeOptions: ['10', '20', '50', '100'],
                showSizeChanger: true,
            }
        })
    }

    handleChange = (pagination) => {
        if (this.props.channelId === undefined && this.props.shopId === undefined && this.props.itemCode === undefined) {
            const {
                channelId,
                endDate,
                operator,
                shopId,
                startDate,
                taskType,
                itemCode,
                processStatus,
                batchId
            } = this.props.formList
            this.changeMsgData(pagination.current, pagination.pageSize, taskType, operator, shopId, channelId, itemCode, startDate, endDate, processStatus, batchId)
        } else {
            this.changeMsgData(pagination.current, pagination.pageSize, '', '', this.props.shopId, this.props.channelId, this.props.itemCode, '', '', '', this.props.batchId)
        }
        this.setState({
            ...pagination,
            page: pagination.current,
            pageSize: pagination.pageSize
        })
    }

    resetting = logId =>{
        this.props.logStore.resetting(logId,res=>{
            this.props.handleSubmit()
        })
    }

    render (){
        const columns =[
            {
                title: '任务类型',
                dataIndex : 'taskType',
                key: 'taskType',
            },
            {
                title: '渠道',
                dataIndex : 'channelId',
                key: 'channelId',
            },
            {
                title: '操作批',
                dataIndex : 'batchId',
                key: 'batchId',
                render:text=>text&&<Tooltip title={text}>
                                <span>{`${text.substring(0, 8)}...`}</span>
                            </Tooltip>
            },
            {
                title: '操作人',
                dataIndex : 'operator',
                key: 'operator',
            },
            {
                title: '门店编码',
                dataIndex : 'shopId',
                key: 'shopId',
            },
            {
                title: '商品编码',
                dataIndex : 'itemCode',
                key: 'itemCode',
            },
            {
                title: '任务创建时间',
                dataIndex : 'createTime',
                key: 'createTime',
            },
            {
                title: '任务执行时间',
                dataIndex : 'processTime',
                key: 'processTime',
            },
            {
                title: '状态',
                dataIndex : 'processStatus',
                key: 'processStatus',
                render: (text, record) => {
                    if(record.processMsg === 'success'){
                        return <span>{text}</span>
                    }else{
                        return (
                            <Tooltip title={record.processMsg}>
                              <span>{text}</span>
                            </Tooltip>
                        )
                    }
                }
            },
            // {
            //     title: '错误信息',
            //     dataIndex : 'processMsg',
            //     key: 'processMsg',
            //     render: (text, record) => {
            //         if(record.processMsg === 'success'){
            //             return ('')
            //         }else{
            //             return (
            //                 record.processMsg &&
            //                 <Tooltip title={record.processMsg}>
            //                   <span>{`${record.processMsg.substring(0, 8)}...`}</span>
            //                 </Tooltip>
            //             )
            //         }
            //     }
            // },
            {
                title: '修改结果',
                dataIndex : 'price' || 'shelved',
                key: 'price' || 'shelved',
                render:(text, record) =>{
                    const shelved = (record.shelved === 1 && '上架') || (record.shelved === 0 && '下架') 
                    if(record.taskType === '价格'){
                        text = record.price
                    }else if(record.taskType === '上下架'){
                        text = shelved
                    }else{
                        text = ''
                    }
                    return (
                        text
                    )
                }
            },
            {
                title:'操作',
                key:'action',
                render:(text,record)=> record.processStatus==='异常'?<Tag color = 'blue' onClick = { e => this.resetting(record.logId) } >重置</Tag>:null
            }
        ]
        return(
            <Table 
                style = {{marginTop:'16px' , width: '100%'}}
                columns = {columns}
                style = {{width : '100%' }}
                dataSource = {this.state.dataSource}
                pagination = {this.state.pagination}
                onChange = {this.handleChange} 
                loading = {this.props.logStore.loading}
            />
        )
    }
}

export default ShopItemLogTable