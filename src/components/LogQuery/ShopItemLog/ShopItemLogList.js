import React, {
  Component
} from 'react'
import {
  Row,
  Col,
  Form,
  Select,
  Input,
  Button,
  DatePicker,
  message
} from 'antd'
import {toJS} from 'mobx'
import FileOperation from '../../FileOperation/FileOperation'
import moment from 'moment'
import {
  observer,
  inject
} from 'mobx-react'
const Option = Select.Option
const FormItem = Form.Item

@inject('recordStore') @observer
class ShopItemLogList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      batchId:''
    }
    this.formList = {}
  }

  state = {
    startDate: '',
    endDate: '',
    startDateValue: undefined
  }

  componentDidMount() {
    if (this.props.batchId !== undefined) {
      this.props.form.setFieldsValue({
        batchId: this.props.batchId
      })
      this.setState({
        batchId: this.props.batchId
      })
    } else {
      this.props.form.setFieldsValue({
        startDate: moment(moment().format('YYYY-MM-DD')),
        endDate: moment(moment().format('YYYY-MM-DD')),
      })
      this.setState({
        startDate: moment().format('YYYY-MM-DD'),
        endDate: moment().format('YYYY-MM-DD')
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      batchId,
      form
    } = nextProps
    if (batchId !== undefined && batchId !== this.state.batchId) {
      form.resetFields()
      form.setFieldsValue({
        batchId: batchId
      })
      this.setState({
        batchId,
        startDate:'',
        endDate:''
      })
    }
  }


  handleSubmit = (e) => {
    e.preventDefault();
    this.props.onReset(1, 10)
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        message.error(err)
      } else {
        const page = this.props.page
        const pageSize = this.props.pageSize
        const taskType = values.taskType
        const operator = values.operator
        const shopId = values.shopId
        const channelId = values.channelId
        const startDate = this.state.startDate
        const endDate = this.state.endDate
        const itemCode = values.itemCode
        const processStatus = values.processStatus
        const batchId = values.batchId
        const formList = {
          taskType: taskType,
          operator: operator,
          shopId: shopId,
          channelId: channelId,
          startDate: startDate,
          endDate: endDate,
          itemCode: itemCode,
          processStatus: processStatus,
          batchId: batchId
        }
        this.formList = formList
        if (startDate === undefined && endDate !== undefined) {
          message.error('请选择开始日期')
        } else if (endDate === undefined && startDate !== undefined) {
          message.error('请选择结束日期')
        } else {
          this.props.getFormList(formList)
          this.props.handleSubmit(1, 10, taskType, operator, shopId, channelId, itemCode, startDate, endDate, processStatus, batchId)
        }
      }
    })
  }

  disabledDate = (current) => {
    return current && current < moment(this.state.startDate)
  }

  resetlog = () => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        message.error(err)
      } else {
      
        const batchId = values.batchId
        console.log("batchId:"+batchId);
       
        if (batchId === undefined || batchId == '') {
          message.error('需要有操作批')
        } else {
          this.props.recordStore.resetlogs(batchId)
        }
      }
    })
  }

  handleReset = () => {
    this.formList = {
      taskType: undefined,
      operator: undefined,
      shopId: undefined,
      channelId: undefined,
      startDate: undefined,
      endDate: undefined
    }
    this.setState({
      startDate: undefined,
      endDate: undefined
    })
    this.props.getFormList(this.formList)
    this.props.form.resetFields()
    // this.props.handleSubmit()
    this.props.onReset(1, 10)
  }

    render() {
      const formItemLayout = {
        labelCol: {      
          xs: { span: 24 },
   
          sm: { span: 10 },
   
          md: { span: 8 },
   
          lg: { span: 7 },
   
          xl: { span: 5 },
   
        },
        wrapperCol: {         
          xs: { span: 24 },
   
          sm: { span: 14 },
   
          md: { span: 16 },
   
          lg: { span: 16 },
   
          xl: { span: 14 },
        },
      }
      const { getFieldDecorator, getFieldsValue } = this.props.form
      const selectWidth = {
        width: '100%'
      }
      const dateFormat = 'YYYY-MM-DD'
      return (
        <div>
          <Form
            onSubmit={this.handleSubmit} className="ant-advanced-custom-form"
          >
            <Row gutter={16}>
              <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'门店编码'}>
                  {getFieldDecorator('shopId')(
                    <Input placeholder="请输入门店编码" style={selectWidth} size = 'default'/>
                    )}
                </FormItem>
              </Col>
              <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'商品编码'}>
                  {getFieldDecorator('itemCode')(
                    <Input placeholder="请输入商品编码" style={selectWidth} size = 'default'/>
                    )}
                </FormItem>
              </Col>
              <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'操作人'}>
                  {getFieldDecorator('operator')(
                    <Input placeholder="请输入操作人" style={selectWidth} size = 'default'/>
                    )}
                </FormItem>
              </Col>
              <Col className="gutter-row" span={6} >            
                <FormItem {...formItemLayout} label='状态'>
                      {getFieldDecorator('processStatus')(
                        <Select placeholder="全部" style={selectWidth} size = 'default'>                                       
                          <Option value="">全部</Option> 
                          <Option value="0">待处理</Option>
                          <Option value="100">完成</Option>
                          <Option value="99">异常</Option>
                        </Select>
                        )}
                </FormItem>
              </Col> 
            </Row>         
            <Row gutter={16}>
            <Col className="gutter-row" span={6} >
                <FormItem {...formItemLayout} label={'任务类型'} >
                  {getFieldDecorator('taskType')(
                    <Select placeholder="全部" style={selectWidth} size = 'default'>
                      <Option value="">全部</Option>
                      <Option value="1">发布</Option>
                      <Option value="2">修改</Option>
                      <Option value="3">禁用</Option>
                      <Option value="4">价格</Option>                                                                                                                                          
                      <Option value="5">上下架</Option>
                    </Select>
                    )}
                </FormItem>
              </Col>
            <Col className="gutter-row" span={6} >            
                <FormItem {...formItemLayout} label='渠道'>
                      {getFieldDecorator('channelId')(
                        <Select placeholder="全部" style={selectWidth} size = 'default'>                                       
                          <Option value="">全部</Option> 
                          {
                            toJS(this.props.channelList).map( item => <Option key = {item.channelId}>{item.channelName}</Option>)
                          }
                        </Select>
                        )}
                </FormItem>
              </Col>
              {
                this.props.batchId === undefined ?
                <div style = {{paddingLeft: '0px', paddingRight: '0px'}}>
                  <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'开始日期'}>
                  {getFieldDecorator('startDate', {
                  })(
                   <DatePicker placeholder="请选择开始日期" format={dateFormat} style={selectWidth} onChange = {(date,dateString) =>{this.setState({startDate : dateString, startDateValue: date})}} size = 'default'/>
                    )}
                </FormItem>
              </Col>
              <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'结束日期'}>
                {getFieldDecorator('endDate')(
                <DatePicker  placeholder="请选择结束日期" format={dateFormat} style={selectWidth} onChange = {(date,dateString) =>{this.setState({endDate : dateString})}} size = 'default' disabledDate= {this.disabledDate}/>
                    )}
                </FormItem>
              </Col>
              </div> :
              <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'任务批'}>
              {getFieldDecorator('batchId')(
                  <Input placeholder="请输入任务批" style={selectWidth} size = 'default'/>
                  )}
              </FormItem>
              </Col>
              }           
              </Row> 
              <Row gutter={16} style = {{marginBottom : '16px'}}>
              { this.props.batchId === undefined ? 
                <Col className="gutter-row" span={6}>
                <FormItem {...formItemLayout} label={'任务批'}>
                {getFieldDecorator('batchId')(
                    <Input placeholder="请输入任务批" style={selectWidth} size = 'default'/>
                    )}
                </FormItem>
                
                </Col>              
              :
              ''
              }
              <Col className="gutter-row"  span={3}>             
                <Button size="default" onClick={this.resetlog}>重置错误</Button>
              </Col>

              <Col className="gutter-row" style={{ textAlign: 'right' }} span={ this.props.batchId === undefined ? 15: 21}>             
                <Button type="primary" size="default" htmlType="submit" >查询</Button>
                <FileOperation history={this.props.history} type="ExportGoodsLog"
                               style={{margin: '0 8px'}}
                               buttonText="导出" buttonProps={{type: 'primary', size:'default'}}
                               queryData={getFieldsValue()}/>
                <Button size="default" onClick={this.handleReset}>清空</Button>
              </Col>
              </Row>             
          </Form>
        </div>
      )
    }
  }
  const ShopItemLogListForm = Form.create()(ShopItemLogList)
  export default ShopItemLogListForm