import React, {
    Component
} from 'react'
import ShopItemLogListForm from './ShopItemLogList'
import {
    observer,
    inject
} from 'mobx-react'
import ShopItemLogTable from './shopItemLogTable'

@inject('store') @observer
class ShopItemLog extends Component {
    state = {
        page: undefined,
        pageSize: undefined,
        formList: {},
        batchId: undefined
    }

    componentWillMount() {
        this.props.store.getChannelList()
    }
    
    componentDidMount() {
        if (this.props.batchId !== undefined) {
            this.refs.ShopItemLogTable.wrappedInstance.changeMsgData(1, 10, '', '', '', '', '', '', '', '', this.props.batchId)
            this.setState({
                formList: {
                    ...this.state.formList,
                    batchId: this.props.batchId
                },
                batchId: this.props.batchId
            })
        }
    }

    componentWillReceiveProps = (nextProps) => {
        const {
            batchId
        } = nextProps
        if (batchId !== this.props.batchId) {
            this.refs.ShopItemLogTable.wrappedInstance.changeMsgData(1, 10, '', '', '', '', '', '', '', '', batchId)
            this.onReset(1, 10)
            this.setState({
                formList: {
                    ...this.state.formList,
                    batchId
                },
                batchId
            })
        }
    }

    handleSubmit = (page, pageSize, taskType, operator, shopId, channelId, itemCode, startDate, endDate, processStatus, batchId) => {
        this.setState({
            page: this.refs.ShopItemLogTable.wrappedInstance.state.page,
            pageSize: this.refs.ShopItemLogTable.wrappedInstance.state.pageSize
        })
        this.refs.ShopItemLogTable.wrappedInstance.changeMsgData(page, pageSize, taskType, operator, shopId, channelId, itemCode, startDate, endDate, processStatus, batchId)
    }

    onReset = (page, pageSize) => {
        this.refs.ShopItemLogTable.wrappedInstance.onReset(page, pageSize)
    }

    getFormList = formList => {
        this.setState({
            formList
        })
    }
    render() {
        let title
        if (this.props.store.collapse) {
            title = <h2 style={{ marginBottom: 16 ,color: '#108ee9'}}>商品日志</h2>
        } else {
            title = ''
        }
        const {
            page,
            pageSize,
            formList,
            batchId
        } = this.state
        return (
            <div>
                {title}
                <ShopItemLogListForm 
                    handleSubmit = {this.handleSubmit}
                    page = { page }
                    pageSize = { pageSize }
                    getFormList = {this.getFormList}
                    onReset = {this.onReset}
                    history = {this.props.history}
                    channelList = {this.props.store.channelList}
                    batchId = {batchId} />
                <ShopItemLogTable
                    ref = 'ShopItemLogTable'
                    channelList = {this.props.store.channelList}
                    handleSubmit = {this.handleSubmit}
                    formList = { formList } />
            </div>)
    }
}

export default  ShopItemLog