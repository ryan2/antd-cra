import React, {
    Component
} from 'react'
import CategoryLogListForm from './categoryLogList'
import {
    observer,
    inject
} from 'mobx-react'
import CategoryLogTable from './categoryLogTable'

@inject('store') @observer
class CategoryLog extends Component {
    state = {
        page: undefined,
        pageSize: undefined,
        formList: {}
    }

    componentWillMount() {
        this.props.store.getChannelList()
    }
    
    handleSubmit = (page, pageSize, taskType, operator, shopId, channelId, startDate, endDate, processStatus) => {
        this.setState({
            page: this.refs.CategoryLogTable.wrappedInstance.state.page,
            pageSize: this.refs.CategoryLogTable.wrappedInstance.state.pageSize
        })
        this.refs.CategoryLogTable.wrappedInstance.changeMsgData(page, pageSize, taskType, operator, shopId, channelId, startDate, endDate, processStatus)
    }

    getFormList = formList => {
        this.setState({
            formList
        })
    }

    onReset = (page, pageSize) => {
        this.refs.CategoryLogTable.wrappedInstance.onReset(page, pageSize)
    }
    render() {
        let title
        if (this.props.store.collapse) {
            title = <h2 style={{ marginBottom: 16 ,color: '#108ee9'}}>类别日志</h2>
        } else {
            title = ''
        }
        return (
            <div>
                {title}
                <CategoryLogListForm 
                    handleSubmit = {this.handleSubmit}
                    page = {this.state.page}
                    pageSize = {this.state.pageSize}
                    getFormList = {this.getFormList}
                    onReset = {this.onReset}
                    history = {this.props.history}
                    channelList = {this.props.store.channelList}
                />
                <CategoryLogTable
                    ref = 'CategoryLogTable'
                    formList = {this.state.formList}
                    channelList = {this.props.store.channelList}
                />
            </div>)
    }
}

export default CategoryLog