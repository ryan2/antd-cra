import React, {
    Component
} from 'react'
import {
    Table,
    Tooltip
} from 'antd'
import * as mobx from 'mobx'
import {
    observer,
    inject
} from 'mobx-react'

@inject('logStore') @observer
class CategoryLogTable extends Component {
    state = {
        dataSource: [],
        pagination: {},
        page: undefined,
        pageSize: undefined,
    }

    onReset = (page, pageSize) => {
        this.setState({
            pagination: {
                current: page,
                pageSize: pageSize
            }
        })
    }
    changeMsgData = async (page, pageSize, taskType, operator, shopId, channelId, startDate, endDate, processStatus) => {
        await this.props.logStore.getCategoryLogList(page, pageSize, taskType, operator, shopId, channelId, startDate, endDate, processStatus)
        let msgListData = mobx.toJS(this.props.logStore.categoryLogList)
        this.init(msgListData)
    }

    init = (msgListData) => {
        const dataSource = this.dataSourceFun(msgListData)
        this.setState({
            dataSource
        })
    }

    getChanne = id => {
        const channe = this.props.channelList.find(p => p.channelId === id);
        if (channe) return channe.channelName
        else return '无渠道'
    }
    
    dataSourceFun = (msgListData) => {
        if (msgListData.length !== 0) {
            const dataSource = msgListData.data.map(data => {
                return {
                    key: data.key,
                    channelId: this.getChanne(data.channelId),
                    operator: data.operator,
                    shopId: data.shopId,
                    channelShopId: data.channelShopId,
                    taskType: data.taskType,
                    createTime: data.createTime,
                    isOpen: data.isOpen,
                    notice: data.notice,
                    openTime: data.openTime,
                    processStatus: data.processStatus,
                    categoryName: data.categoryName,
                    processMsg: data.processMsg,
                    processTime: data.processTime
                }
            })
            this.paginationFun(msgListData)
            return dataSource
        }
    }

    paginationFun = (data) => {
        const total = data.total
        this.setState({
            pagination: {
                total,
                showTotal: () => `共${total}条数据`,
                pageSizeOptions: ['10', '20', '50', '100'],
                showSizeChanger: true,
            }
        })
    }

    handleChange = (pagination) => {
        const {
            channelId,
            endDate,
            operator,
            shopId,
            startDate,
            taskType,
            processStatus
        } = this.props.formList
        this.changeMsgData(pagination.current, pagination.pageSize, taskType, operator, shopId, channelId, startDate, endDate, processStatus)
        this.setState({
            ...pagination,
            page: pagination.current,
            pageSize: pagination.pageSize
        })
    }

    render() {
        const columns =[
            {
                title: '任务类型',
                dataIndex : 'taskType',
                key: 'taskType',
            },
            {
                title: '渠道',
                dataIndex : 'channelId',
                key: 'channelId',
            },
            {
                title: '操作人',
                dataIndex : 'operator',
                key: 'operator',
            },
            {
                title: '门店编码',
                dataIndex : 'shopId',
                key: 'shopId',
            },
            {
                title: '渠道店铺编码',
                dataIndex : 'channelShopId',
                key: 'channelShopId',
            },
            {
                title: '任务创建时间',
                dataIndex : 'createTime',
                key: 'createTime',
            },
            {
                title: '任务执行时间',
                dataIndex : 'processTime',
                key: 'processTime',
            },
            {
                title: '状态',
                dataIndex : 'processStatus',
                key: 'processStatus',
            },
            {
                title: '错误信息',
                dataIndex : 'processMsg',
                key: 'processMsg',
                render: (text, record) => {
                    if(record.processMsg === 'success'){
                        return ('')
                    }else{
                        return (
                            record.processMsg &&
                            <Tooltip title={record.processMsg}>
                              <span>{`${record.processMsg.substring(0, 8)}...`}</span>
                            </Tooltip>
                        )
                    }
                }
            },
            {
                title: '修改结果',
                dataIndex :  'categoryName',
                key: 'categoryName',
            },
        ]
        return(
            <Table 
                style = {{marginTop:'16px' , width: '100%'}}
                columns = {columns}
                style = {{width : '100%' , }}
                dataSource = {this.state.dataSource}
                pagination = {this.state.pagination}
                onChange = {this.handleChange} 
                loading = {this.props.logStore.loading}
            />
        )
    }
}

export default CategoryLogTable