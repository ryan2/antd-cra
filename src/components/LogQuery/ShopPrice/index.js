import React, {
    Component
} from 'react'
import ShopPriceList from './ShopPriceList'
import {
    observer,
    inject
} from 'mobx-react'
import ShopLogTable from './ShopPriceTable'

@inject('store') @observer
class ShopLog extends Component {
    state = {
        page: undefined,
        pageSize: undefined,
        formList: {},
        batchId: undefined
    }
    componentDidMount() {
        if (this.props.batchId !== undefined) {
            this.refs.ShopLogTable.wrappedInstance.changeMsgData(1, 10, '', '', '', '', '', this.props.batchId)
            this.setState({
                formList: {
                    ...this.state.formList,
                    batchId: this.props.batchId
                },
                batchId: this.props.batchId
            })
        }
    }

    componentWillReceiveProps = (nextProps) => {
        const {
            batchId
        } = nextProps
        if (batchId !== this.props.batchId) {
            this.refs.ShopLogTable.wrappedInstance.changeMsgData(1, 10, '', '', '', '', '', batchId)
            this.onReset(1, 10)
            this.setState({
                formList: {
                    ...this.state.formList,
                    batchId: batchId
                },
                batchId
            })
        }
    }
    handleSubmit = (page, pageSize, shopId, itemCode, startDate, endDate, processStatus, batchId, operator) => {
        this.setState({
            page: this.refs.ShopLogTable.wrappedInstance.state.page,
            pageSize: this.refs.ShopLogTable.wrappedInstance.state.pageSize
        })
        this.refs.ShopLogTable.wrappedInstance.changeMsgData(page, pageSize, shopId, itemCode, startDate, endDate, processStatus, batchId, operator)
    }

    getFormList = formList => {
        this.setState({
            formList
        })
    }

    onReset = (page, pageSize) => {
        this.refs.ShopLogTable.wrappedInstance.onReset(page, pageSize)
    }

    render() {
        const {
            page,
            pageSize,
            formList,
            batchId
        } = this.state
        return (
            <div>
                <ShopPriceList 
                    handleSubmit = {this.handleSubmit}
                    page = {page}
                    pageSize = {pageSize}
                    getFormList = {this.getFormList}
                    onReset = {this.onReset}
                    history = {this.props.history}
                    batchId = {batchId}
                />
                <ShopLogTable
                    ref = 'ShopLogTable'
                    formList = {formList}
                />
            </div>)
    }
}

export default ShopLog