import React, {
    Component
} from 'react'
import {
    Table,
    Tooltip
} from 'antd'
import * as mobx from 'mobx'
import {
    observer,
    inject
} from 'mobx-react'

@inject('logStore') @observer
class ShopPriceTable extends Component {
    state = {
        dataSource: [],
        pagination: {},
        page: undefined,
        pageSize: undefined,
        shopId: undefined,
        itemCode: undefined,
    }

    componentDidMount() {
        if (this.props.shopId !== undefined && this.props.itemCode !== undefined) {
            this.changeMsgData(this.state.page, this.state.pageSize, this.props.shopId, this.props.itemCode)
            this.setState({
                shopId: this.props.shopId,
                itemCode: this.props.itemCode
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        const {
            shopId,
            itemCode
        } = this.state;
        if (nextProps.shopId !== undefined && nextProps.itemCode !== undefined && (nextProps.shopId !== shopId || nextProps.itemCode !== itemCode)) {
            this.changeMsgData(this.state.page, this.state.pageSize, nextProps.shopId, nextProps.itemCode);
            this.setState({
                shopId: nextProps.shopId,
                itemCode: nextProps.itemCode
            })
        }
    }

    changeMsgData = async (page, pageSize, shopId, itemCode, startDate, endDate, processStatus, batchId, operator) => {
        let msgListData = []
        if (this.props.shopId !== undefined && this.props.itemCode !== undefined) {
            await this.props.logStore.getshopPriceList(page, pageSize, this.props.shopId, this.props.itemCode, startDate, endDate, processStatus, batchId, operator)
            msgListData = mobx.toJS(this.props.logStore.shopPriceList)
        } else {
            await this.props.logStore.getshopPriceList(page, pageSize, shopId, itemCode, startDate, endDate, processStatus, batchId, operator)
            msgListData = mobx.toJS(this.props.logStore.shopPriceList)
        }
        this.init(msgListData)
    }

    init = (msgListData) => {
        const dataSource = this.dataSourceFun(msgListData)
        this.setState({
            dataSource
        })
    }

    onReset = (page, pageSize) => {
        this.setState({
            pagination: {
                current: page,
                pageSize: pageSize
            }
        })
    }

    dataSourceFun = (msgListData) => {
        if (msgListData.length !== 0) {
            const dataSource = msgListData.data.map((data, index) => {
                return {
                    key: index,
                    operator: data.operator,
                    batchId: data.batchId,
                    shopId: data.shopId,
                    itemCode: data.itemCode,
                    goodsName: data.goodsName,
                    psp: data.psp,
                    nsp: data.nsp,
                    createDate: data.createDate,
                    processStatus: (data.processStatus === 0 && '待处理') || (data.processStatus === 100 && '完成') || (data.processStatus === 99 && '异常'),
                    processMsg: data.processMsg,
                    processTime: data.processTime
                }
            })
            this.paginationFun(msgListData)
            return dataSource
        }
    }

    paginationFun = (data) => {
        const total = data.total
        this.setState({
            pagination: {
                total,
                showTotal: () => `共${total}条数据`,
                pageSizeOptions: ['10', '20', '50', '100'],
                showSizeChanger: true,
            }
        })
    }

    handleChange = (pagination) => {
        if (this.props.shopId === undefined && this.props.itemCode === undefined) {
            const {
                endDate,
                shopId,
                itemCode,
                startDate,
                processStatus,
                batchId,
                operator
            } = this.props.formList
            this.changeMsgData(pagination.current, pagination.pageSize, shopId, itemCode, startDate, endDate, processStatus, batchId, operator)
        } else {
            this.changeMsgData(pagination.current, pagination.pageSize, this.props.shopId, this.props.itemCode)
        }
        this.setState({
            ...pagination,
            page: pagination.current,
            pageSize: pagination.pageSize
        })
    }

    render() {
        const columns = [{
                title: '门店编码',
                dataIndex: 'shopId',
                key: 'shopId',
            },
            {
                title: '商品编码',
                dataIndex: 'itemCode',
                key: 'itemCode',
            },
            {
                title: '商品名称',
                dataIndex: 'goodsName',
                key: 'goodsName',
            },
            {
                title: '价格',
                dataIndex: 'nsp',
                key: 'nsp',
            },
            {
                title: '创建时间',
                dataIndex: 'createDate',
                key: 'createDate',
            },
            {
                title: '操作人',
                dataIndex: 'operator',
                key: 'operator',
            },
            {
                title: '操作批',
                dataIndex: 'batchId',
                key: 'batchId',
            },
            {
                title: '处理时间',
                dataIndex: 'processTime',
                key: 'processTime',
            },
            {
                title: '处理状态',
                dataIndex: 'processStatus',
                key: 'processStatus',
            },
            {
                title: '消息',
                dataIndex: 'processMsg',
                key: 'processMsg',
                render: (text, record) => {
                    if (record.processMsg === 'success') {
                        return ('')
                    } else {
                        return (record.processMsg &&
                            <Tooltip title={record.processMsg}>
                                <span>{`${record.processMsg.substring(0, 8)}...`}</span>
                            </Tooltip>)
                    }
                }
            }]
        return(
            <Table 
                style = {{marginTop:'16px' , width: '100%'}}
                columns = {columns}
                style = {{width : '100%'}}
                dataSource = {this.state.dataSource}
                pagination = {this.state.pagination}
                onChange = {this.handleChange} 
                loading = {this.props.logStore.loading}
            />
        )
    }
}

export default ShopPriceTable