import React, {
    Component
} from 'react'
import {
    Table,
    Tooltip
} from 'antd'
import * as mobx from 'mobx'
import {
    observer,
    inject
} from 'mobx-react'

@inject('logStore') @observer
class InventoryLogTable extends Component {
    state = {
        dataSource: [],
        pagination: {},
        page: undefined,
        pageSize: undefined,
        channelId: undefined,
        shopId: undefined,
        itemCode: undefined,
    }
    componentDidMount() {
        if (this.props.channelId !== undefined && this.props.shopId !== undefined && this.props.itemCode !== undefined) {
            this.changeMsgData(this.state.page, this.state.pageSize, '', '', this.props.shopId, this.props.channelId, this.props.itemCode)
            this.setState({
                channelId: this.props.channelId,
                shopId: this.props.shopId,
                itemCode: this.props.itemCode
            })
        }
    }
    componentWillReceiveProps(nextProps) {
        const {
            channelId,
            shopId,
            itemCode
        } = this.state;
        if (nextProps.channelId !== undefined && nextProps.shopId !== undefined && nextProps.itemCode !== undefined && (nextProps.channelId !== channelId || nextProps.shopId !== shopId || nextProps.itemCode !== itemCode)) {
            this.changeMsgData(this.state.page, this.state.pageSize, '', nextProps.itemCode, nextProps.shopId, nextProps.channelId);
            this.setState({
                channelId: nextProps.channelId,
                shopId: nextProps.shopId,
                itemCode: nextProps.itemCode
            })
        }
    }

    onReset = (page, pageSize) => {
        this.setState({
            pagination: {
                current: page,
                pageSize: pageSize
            }
        })
    }
    changeMsgData = async (page, pageSize, channelShopId, itemCode, shopId, channelId, startDate, endDate, processStatus, batchId) => {
        let msgListData = []
        if (this.props.channelId !== undefined && this.props.shopId !== undefined && this.props.itemCode !== undefined) {
            await this.props.logStore.getInventoryLogList(page, pageSize, channelShopId, this.props.itemCode, this.props.shopId, this.props.channelId, startDate, endDate, processStatus, batchId)
            msgListData = mobx.toJS(this.props.logStore.inventoryLogList)
        } else {
            await this.props.logStore.getInventoryLogList(page, pageSize, channelShopId, itemCode, shopId, channelId, startDate, endDate, processStatus, batchId)
            msgListData = mobx.toJS(this.props.logStore.inventoryLogList)
        }
        this.init(msgListData)
    }

    init = (msgListData) => {
        const dataSource = this.dataSourceFun(msgListData)
        this.setState({
            dataSource
        })
    }

    getChanne = id => {
        const channe = this.props.channelList.find(p => p.channelId === id);
        if (channe) return channe.channelName
        else return '无渠道'
    }
    
    dataSourceFun = (msgListData) => {
        if (msgListData.length !== 0) {
            const dataSource = msgListData.data.map(data => {
                return {
                    key: data.key,
                    channelId: this.getChanne(data.channelId),
                    operator: data.operator,
                    shopId: data.shopId,
                    channelShopId: data.channelShopId,
                    createTime: data.createTime,
                    isOpen: data.isOpen,
                    notice: data.notice,
                    batchId: data.batchId,
                    openTime: data.openTime,
                    processStatus: data.processStatus,
                    categoryName: data.categoryName,
                    saleQty: data.saleQty,
                    actualStock: data.actualStock,
                    p4Stock: data.p4Stock,
                    itemId: data.itemId,
                    itemCode: data.itemCode,
                    processMsg: data.processMsg,
                    processTime: data.processTime
                }
            })
            this.paginationFun(msgListData)
            return dataSource
        }
    }

    paginationFun = (data) => {
        const total = data.total
        this.setState({
            pagination: {
                total,
                showTotal: () => `共${total}条数据`,
                pageSizeOptions: ['10', '20', '50', '100'],
                showSizeChanger: true,
            }
        })
    }

    handleChange = (pagination) => {
        if (this.props.channelId === undefined && this.props.shopId === undefined && this.props.itemCode === undefined) {
            const {
                channelId,
                endDate,
                operator,
                shopId,
                startDate,
                taskType,
                itemCode,
                processStatus,
                batchId
            } = this.props.formList
            this.changeMsgData(pagination.current, pagination.pageSize, '', itemCode, shopId, channelId, startDate, endDate, processStatus, batchId)
        } else {
            this.changeMsgData(pagination.current, pagination.pageSize, '', '', this.props.shopId, this.props.channelId, this.props.itemCode, '', '', '', this.props.batchId)
        }
        this.setState({
            ...pagination,
            page: pagination.current,
            pageSize: pagination.pageSize
        })
    }

    render() {
        const columns = [
        {
            title: '操作批',
            dataIndex: 'batchId',
            key: 'batchId',
        },
        {
            title: '操作人',
            dataIndex: 'operator',
            key: 'operator',
        },
        {
            title: '门店编码',
            dataIndex: 'shopId',
            key: 'shopId',
        },
        {
            title: '商品编码',
            dataIndex: 'itemCode',
            key: 'itemCode',
        },
        {
            title: '渠道店铺编码',
            dataIndex: 'channelShopId',
            key: 'channelShopId',
        },
        {
            title: '当天外卖销售量',
            dataIndex: 'saleQty',
            key: 'saleQty',
        },
        {
            title: '可卖数',
            dataIndex: 'actualStock',
            key: 'actualStock',
        },
        {
            title: 'P4可卖数',
            dataIndex: 'p4Stock',
            key: 'p4Stock',
        },
        {
            title: '任务创建时间',
            dataIndex: 'createTime',
            key: 'createTime',
        },
        {
            title: '任务执行时间',
            dataIndex: 'processTime',
            key: 'processTime',
        },
        {
            title: '状态',
            dataIndex: 'processStatus',
            key: 'processStatus',
        },
        {
            title: '错误信息',
            dataIndex: 'processMsg',
            key: 'processMsg',
            render: (text, record) => {
                if(record.processMsg === 'success'){
                    return ('')
                }else{
                    return (
                        record.processMsg &&
                        <Tooltip title={record.processMsg}>
                            <span>{`${record.processMsg.substring(0, 8)}...`}</span>
                        </Tooltip>
                    )
                }
            }
        }]
        return(
            <Table 
                style = {{marginTop:'16px' , width: '100%'}}
                columns = {columns}
                style = {{width : '100%' , }}
                dataSource = {this.state.dataSource}
                pagination = {this.state.pagination}
                onChange = {this.handleChange} 
                loading = {this.props.logStore.loading}
            />
        )
    }
}

export default InventoryLogTable