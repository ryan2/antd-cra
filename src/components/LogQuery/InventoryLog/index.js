import React, {
    Component
} from 'react'
import InventoryLogListForm from './inventoryLogList'
import {
    observer,
    inject
} from 'mobx-react'
import InventoryLogTable from './inventoryLogTable'

@inject('store') @observer
class InventoryLog extends Component {
    state = {
        page: undefined,
        pageSize: undefined,
        formList: {},
        pagination: {},
        batchId: undefined
    }

    componentWillMount() {
        this.props.store.getChannelList()
    }
    
    componentDidMount() {
        if (this.props.batchId !== undefined) {
            this.refs.InventoryLogTable.wrappedInstance.changeMsgData(1, 10, '', '', '', '', '', '', '', this.props.batchId)
            this.setState({
                formList: {
                    ...this.state.formList,
                    batchId: this.props.batchId
                },
                batchId: this.props.batchId
            })
        }
    }

    componentWillReceiveProps = (nextProps) => {
        const {
            batchId
        } = nextProps
        if (batchId !== this.props.batchId) {
            this.refs.InventoryLogTable.wrappedInstance.changeMsgData(1, 10, '', '', '', '', '', '', '', batchId)
            this.onReset(1, 10)
            this.setState({
                formList: {
                    ...this.state.formList,
                    batchId: batchId
                },
                batchId
            })
        }
    }

    handleSubmit = (page, pageSize, channelShopId, itemCode, shopId, channelId, startDate, endDate, processStatus, batchId) => {
        this.setState({
            page: this.refs.InventoryLogTable.wrappedInstance.state.page,
            pageSize: this.refs.InventoryLogTable.wrappedInstance.state.pageSize
        })
        this.refs.InventoryLogTable.wrappedInstance.changeMsgData(page, pageSize, channelShopId, itemCode, shopId, channelId, startDate, endDate, processStatus, batchId)
    }

    getFormList = formList => {
        this.setState({
            formList
        })
    }

    onReset = (page, pageSize) => {
        this.refs.InventoryLogTable.wrappedInstance.onReset(page, pageSize)
    }

    render() {
        const {
            page,
            pageSize,
            formList,
            batchId,
            pagination
        } = this.state
        let title
        if (this.props.store.collapse) {
            title = <h2 style={{ marginBottom: 16 ,color: '#108ee9'}}>库存日志</h2>
        } else {
            title = ''
        }
        return (
            <div>
                {title}
                <InventoryLogListForm 
                    handleSubmit = {this.handleSubmit}
                    pagination = {pagination}
                    page = {page}
                    pageSize = {pageSize}
                    getFormList = {this.getFormList}
                    onReset = {this.onReset}
                    history = {this.props.history}
                    batchId = {batchId}
                    channelList = {this.props.store.channelList}
                />
                <InventoryLogTable
                    ref = 'InventoryLogTable'
                    formList = {formList}
                    batchId = {batchId}      
                    channelList = {this.props.store.channelList}          
                />
            </div>)
    }
}

export default InventoryLog