import React, {
  Component
} from 'react'
import {
  Row,
  Col,
  Form,
  Select,
  Input,
  Button,
  DatePicker,
  message
} from 'antd'
import {toJS} from 'mobx'
import FileOperation from '../../FileOperation/FileOperation'
import moment from 'moment'
const Option = Select.Option
const FormItem = Form.Item


class InventoryLogList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      batchId:''
    }
    this.formList = {}
  }

  state = {
    startDate: '',
    endDate: '',
  }

  componentDidMount() {
    if (this.props.batchId !== undefined) {
      this.props.form.setFieldsValue({
        batchId: this.props.batchId
      })
      this.setState({
        batchId: this.props.batchId
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      batchId,
      form
    } = nextProps
    if (batchId !== undefined && batchId !== this.state.batchId) {
      form.resetFields()
      form.setFieldsValue({
        batchId
      })
      this.setState({
        batchId,
        startDate:'',
        endDate:''
      })
    }
  }

  disabledDate = (current) => {
    return current && current < moment(this.state.startDate)
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.onReset(1, 10)
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        message.error(err)
      } else {
        const channelShopId = values.channelShopId
        const itemCode = values.itemCode
        const shopId = values.shopId
        const channelId = values.channelId
        const startDate = this.state.startDate
        const endDate = this.state.endDate
        const processStatus = values.processStatus
        const batchId = values.batchId
        const formList = {
          channelShopId: channelShopId,
          itemCode: itemCode,
          shopId: shopId,
          channelId: channelId,
          startDate: startDate,
          endDate: endDate,
          processStatus: processStatus,
          batchId: batchId
        }
        this.formList = formList
        if (startDate === undefined && endDate !== undefined) {
          message.error('请选择开始日期')
        } else if (endDate === undefined && startDate !== undefined) {
          message.error('请选择结束日期')
        } else {
          this.props.getFormList(formList)
          this.props.handleSubmit(1, 10, channelShopId, itemCode, shopId, channelId, startDate, endDate, processStatus, batchId)
        }
      }
    })
  }

  handleReset = () => {
    this.formList = {
      channelShopId: undefined,
      itemCode: undefined,
      shopId: undefined,
      channelId: undefined,
      startDate: undefined,
      endDate: undefined
    }
    this.setState({
      startDate: undefined,
      endDate: undefined
    })
    this.props.getFormList(this.formList)
    this.props.form.resetFields()
    // this.props.handleSubmit()
    this.props.onReset(1, 10)
  }

  render() {
    const formItemLayout = {
      labelCol: {      
        xs: { span: 24 },
  
        sm: { span: 10 },
  
        md: { span: 8 },
  
        lg: { span: 7 },
  
        xl: { span: 5 },
  
      },
      wrapperCol: {         
        xs: { span: 24 },
  
        sm: { span: 14 },
  
        md: { span: 16 },
  
        lg: { span: 16 },
  
        xl: { span: 14 },
      },
    }
    const {
      getFieldDecorator,
      getFieldsValue
    } = this.props.form
    const selectWidth = {
      width: '100%'
    }
    const dateFormat = 'YYYY-MM-DD'
    return (
      <div>
        <Form
          onSubmit={this.handleSubmit} className="ant-advanced-custom-form"
        >
          <Row gutter={16}>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'门店编码'}>
                {getFieldDecorator('shopId')(
                  <Input placeholder="请输入门店编码" style={selectWidth} size = 'default'/>
                  )}                </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'任务批'}>
              {getFieldDecorator('batchId')(
                  <Input placeholder="请输入任务批" style={selectWidth} size = 'default'/>
                  )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6} >            
              <FormItem {...formItemLayout} label='渠道'>
                    {getFieldDecorator('channelId')(
                      <Select placeholder="全部" style={selectWidth} size = 'default'>                                       
                        <Option value="">全部</Option> 
                        {
                          toJS(this.props.channelList).map( item => <Option key = {item.channelId}>{item.channelName}</Option>)
                        }
                      </Select>
                      )}
              </FormItem>
            </Col> 
            <Col className="gutter-row" span={6} >            
              <FormItem {...formItemLayout} label='状态'>
                    {getFieldDecorator('processStatus')(
                      <Select placeholder="全部" style={selectWidth} size = 'default'>                                       
                        <Option value="">全部</Option> 
                        <Option value="0">待处理</Option>
                        <Option value="100">完成</Option>
                        <Option value="99">异常</Option>
                      </Select>
                      )}
              </FormItem>
            </Col> 
          
          </Row>
          <Row gutter={16} style = {{marginBottom :'16px'}}>
          <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品编码'}>
                {getFieldDecorator('itemCode')(
                  <Input placeholder="请输入商品编码" style={selectWidth} size = 'default'/>
                  )}
              </FormItem>
            </Col>
            {this.props.batchId === undefined &&
              <span style = {{paddingLeft: '0px', paddingRight: '0px'}}>
                          <Col className="gutter-row" span={6}>
                          <FormItem {...formItemLayout} label={'开始日期'}>
                            {getFieldDecorator('startDate')(
                              <DatePicker placeholder="请选择开始日期" format={dateFormat} style={selectWidth} onChange = {(date,dateString) =>{this.setState({startDate : dateString})}} size = 'default'/>
                              )}
                          </FormItem>
                        </Col>
                        <Col className="gutter-row" span={6}>
                          <FormItem {...formItemLayout} label={'结束日期'}>
                          {getFieldDecorator('endDate')(
                          <DatePicker  placeholder="请选择结束日期" format={dateFormat} style={selectWidth} onChange = {(date,dateString) =>{this.setState({endDate : dateString})}} size = 'default' disabledDate= {this.disabledDate}/>
                              )}
                          </FormItem>
                        </Col>
            </span>
            }
            <Col className="gutter-row" style={{ textAlign: 'right' }} span={ this.props.batchId === undefined ? 6:18}>             
              <Button type="primary" size="default" htmlType="submit" >查询</Button>
              <FileOperation history={this.props.history} type="ExportO2oInventoryLog"
                              style={{margin: '0 8px'}}
                              buttonText="导出" buttonProps={{type: 'primary', size:'default'}}
                              queryData={getFieldsValue()}/>
              <Button size="default" onClick={this.handleReset}>清空</Button>
            </Col>
          </Row>
        </Form>
      </div>)
  }
}
const InventoryLogListForm = Form.create()(InventoryLogList)
export default InventoryLogListForm