import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { observer, inject } from 'mobx-react'
import { Table, Tag, Icon, Menu, message, Modal } from 'antd'
import ItemFilterForm from './ItemFilterForm'
import ItemFilterAdd from './ItemFilterAdd'
import '../ItemSchedule/itemSchedule.css'

@inject('itemFilterStore', 'store')@observer
class ItemScheduleComponent extends Component {
  constructor(props){
    super(props)
    this.state = {
      pagination: {
        defaultCurrent: 1,
        defaultPageSize: 10,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '50', '100'],
        showTotal: () => `共${this.props.itemFilterStore.total}条数据`,
      },
      //searchData: props.itemFilterStore.cacheData,
      searchData: {},
      visible: false,
      widowWidth: document.body.offsetWidth,
      widowHeight: document.body.offsetHeight,
      goodImgShow: false,
      goodImgSrc: '',
    }
  }

  componentWillMount() {
  }

  componentDidMount() {
    const { getFilterList, getCityList} = this.props.itemFilterStore;
    window.addEventListener('resize', this.onWindowResize)
    //getFilterList({})
    getCityList({})
  }

  componentWillReceiveProps(nextProps) {
  }

  componentWillUnmount() {
    this.props.itemFilterStore.initData()
    window.removeEventListener('resize', this.onWindowResize)
  }

  onWindowResize = () => {
    this.setState({
      widowWidth: document.body.offsetWidth,
      widowHeight: document.body.offsetHeight
    })
  }

  handlePageChange = (pagination) => {
    const { searchData } = this.state;
    this.state.pagination.current = pagination.current;
    this.state.pagination.pageSize = pagination.pageSize;
    this.props.itemFilterStore.getFilterList({
      ...searchData,
      page: pagination.current,
      pageSize: pagination.pageSize,
    })
    this.setState({
      pagination: this.state.pagination
    })
  }

  handleSearchSubmit = (params) => {
    this.props.itemFilterStore.getFilterList(params)
    this.state.pagination.current = 1;
    this.setState({
      searchData: params,
      pagination: this.state.pagination
    })
  }

  handleFormReset = () => {
    this.props.itemFilterStore.getFilterList({})
    this.props.itemFilterStore.initShopListByCity('cityshopListSearch')
    this.state.pagination.current = 1;
    this.setState({
      searchData: {},
      pagination: this.state.pagination
    })
    //this.props.itemFilterStore.saveCacheData({})
  }

  handleAddItemFilter = () => {
    this.setState({
      visible: true
    })
  }

  handleCancelAddItemFilter = () => {
    this.props.itemFilterStore.initShopListByCity('cityshopListAdd')
    this.setState({
      visible: false
    })
  }

  handleCreateItemFilter = (params) => {
    return this.props.itemFilterStore.addItemFilter(params).then((res) => {
      if(res && res.status === 200) {
        const {searchData, pagination} = this.state;
        message.success('操作成功')
        this.handleCancelAddItemFilter();
        this.props.itemFilterStore.getFilterList({
          ...searchData,
          page: pagination.current,
          pageSize: pagination.pageSize
        })
        return true;
      } else if(res && res.status !== 200){
        message.error(res.err || '操作失败')
        return false
      }
    }).catch(e => {
      return false
    })
  }

  //查看、关闭 商品图片
  handleGoodsImg = (imgUrl) => {
    let _imgUrl = ''
    if(imgUrl) {
      _imgUrl = imgUrl
    }
    this.setState({
      goodImgShow: !this.state.goodImgShow,
      goodImgSrc: _imgUrl
    })
  }

  render() {
    const canEdit = this.props.store.hasUserAuth('edit');
    const tagStyle = { width: 54, textAlign: 'center'}
    const columns = [
      {
        title: '城市',
        dataIndex: 'city',
        key: 'city',
        width: 100,
      }, {
        title: '门店编码-名称',
        dataIndex: 'shopId',
        key: 'shopId',
        width: 120,
      }, {
        title: '商品编码',
        dataIndex: 'itemCode',
        key: 'itemCode',
        width: 100,
      }, {
        title: 'o2o商品名称',
        dataIndex: 'goodsName',
        key: 'goodsName',
        width: 200,
      }, {
        title: '商品图片',
        dataIndex: 'goodsImg',
        key: 'goodsImg',
        width: 100,
        render: (text, record) => (
          <div>
            {record.imageKey1 !== '' ?
                <img style={{width: 50, height: 50}}
                     src={record.imageUrl}
                     onClick={() => this.handleGoodsImg(record.imageUrl)}/>: '暂无图片'}
          </div>
        )
      }, {
        title: 'p4商品名称',
        dataIndex: 'Name',
        key: 'Name',
        width: 200,
      }, {
        title: 'p4商品状态',
        dataIndex: 'isMatch',
        key: 'isMatch',
        width: 100,
        render: (text, record) => (
          <span>
            {text === 0 ? <Tag color="red" style={tagStyle}>未匹配</Tag> : null}
            {text === 1 ? <Tag color="green" style={tagStyle}>匹配</Tag> : null}
          </span>
        )
      }, {
        title: '创建时间',
        dataIndex: 'createTime',
        key: 'createTime',
        width: 150,
      }
    ]
    const { loading, filterList, total, cityList, cityshopListSearch, cityshopListAdd, getShopListByCity } = this.props.itemFilterStore;
    const { widowHeight, goodImgShow, goodImgSrc } = this.state;
    const pagination = Object.assign({total: total}, this.state.pagination);
    return (
      <div>
        {this.props.store.collapse ?
          <h2 style={{marginBottom: 16, flex: '0 0 auto'}}>
            <Link to="/item/schedule">商品主档筛选</Link>
          </h2> : null
        }
        <ItemFilterForm history={this.props.history}
                        cityList={cityList}
                        cityshopList={cityshopListSearch}
                        getShopListByCity={getShopListByCity}
                        onAdd={this.handleAddItemFilter}
                        onSearch={this.handleSearchSubmit}
                        onReset={this.handleFormReset}
                        canEdit={canEdit}/>
        <div ref='tableRapper' className="tableRapper" style={{marginLeft: 0}}>
          <Table style={{width: '100%', minHeight: 450}} scroll={{x:1070, y: widowHeight - 290}}
                 loading={loading}
                 columns={columns}
                 dataSource={filterList.slice()}
                 className = 'onlyYscroll'
                 pagination={pagination}
                 onChange={(pagination) => this.handlePageChange(pagination)}/>
        </div>
        <ItemFilterAdd visible={this.state.visible}
                       cityList={cityList}
                       cityshopList={cityshopListAdd}
                       getShopListByCity={getShopListByCity}
                       onCancel={this.handleCancelAddItemFilter}
                       onCreate={this.handleCreateItemFilter}/>
        <Modal visible={goodImgShow}
               footer={null}
               onCancel={this.handleGoodsImg}>
          <img style={{width: '100%'}}
               src={goodImgSrc}/>
        </Modal>
      </div>
    )
  }
}
export default ItemScheduleComponent
