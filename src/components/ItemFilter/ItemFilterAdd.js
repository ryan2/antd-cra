import React, { Component } from 'react';
import { Modal, Form, Button, Select, Input } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

class ItemFilterAdd extends Component {
  constructor (props) {
    super (props);
    this.state  = {
    }
  }

  handleCityChange = (value) => {
    this.props.getShopListByCity({city: value}, 'cityshopListAdd')
    if(value !== undefined){
      this.props.form.setFieldsValue({
        shopId: undefined
      })
    }
  }

  //取消
  handleCancel = () => {
    const { onCancel, form } = this.props;
    onCancel();
    form.resetFields();
  }

  //提交修改、创建
  handleSubmit = () => {
    const { onCreate, form } = this.props;
    const { validateFields } = form;
    validateFields((err, values) => {
      if ( !err) {
        Modal.confirm({
          title: `确认新增商品？`,
          onOk() {
            return onCreate(values).then((res) => {
              if (res) {
                form.resetFields();
              }
            })
          },
          onCancel() {},
        });
      }
    });
  }

  render () {
    const { visible, form, cityList, cityshopList } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 18 },
    };
    return (
        <Modal
          title="新增商品"
          maskClosable={false}
          visible={visible}
          onCancel={this.handleCancel}
          onOk={this.handleSubmit}
            >
          <Form style={{margin: 20}}>
            <FormItem {...formItemLayout} label={'城市'}>
              {getFieldDecorator('city',{
                rules: [{
                  required: true, message: '必须选择城市',
                }]
              })(
                  <Select onChange = {this.handleCityChange}
                          style={{width: '100%'}} size = 'default'
                          placeholder="请选择城市">
                    {cityList.map(data =><Option key={data.key}>{data.value}</Option>)}
                  </Select>
              )}
            </FormItem>
            <FormItem {...formItemLayout} label={'家乐福门店'}>
              {getFieldDecorator('shopId'/*,{
                rules: [{
                  required: getFieldValue('city') ? false : true, message: '必须选择家乐福门店',
                }]
              }*/)(
                  <Select  style={{width: '100%'}} size = 'default' mode="multiple"
                           placeholder="请选择平台门店">
                    {cityshopList.map(data =><Option key={data.shopId}>{data.shopNameAlias}</Option>)}
                  </Select>
              )}
            </FormItem>
            <FormItem {...formItemLayout} label={'商品编码'}>
              {getFieldDecorator('itemCode',{
                rules: [{
                  required: true, message: '必须输入商品编码',
                }, {
                  max: 10, message: '最大长度不超过10'
                }]
              })(
                  <Input placeholder="请输入商品编码" size="default"/>
              )}
            </FormItem>
          </Form>
        </Modal>
    )
  }
}

export default Form.create()(ItemFilterAdd);