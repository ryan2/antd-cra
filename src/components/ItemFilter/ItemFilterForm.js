import React, { Component } from 'react'
import { Row, Col, Form, Input, Button, message, Select } from 'antd'
import FileOperation from '../FileOperation/FileOperation'
const FormItem = Form.Item
const Option = Select.Option

class ItemScheduleForm extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        if (!values.itemCode && !values.city && !values.shopId && values.status === undefined
            && values.isMatch === undefined ) {
          message.warning('请至少输入一项查询条件')
        } else {
          if(values.city === '全部') {
            values.city = ''
          }
          /*if(values.status === '') {
            delete values.status
          }
          if(values.isMatch === '') {
            delete values.isMatch
          }*/
          this.props.onSearch(values)
        }
      }
    })
  }

  handleReset = () => {
    this.props.form.validateFields((err, values) => {
      if (!values.itemCode && !values.city && !values.shopId && values.status === undefined
          && values.isMatch === undefined) {
        message.warning('查询条件已经为空')
      } else {
        this.props.form.resetFields()
        // this.props.onReset()
      }
    })
  }

  handleCityChange = (value) => {
    this.props.getShopListByCity({city: value}, 'cityshopListSearch')
    if(value !== undefined){
      this.props.form.setFieldsValue({
        shopId: []
      })
    }
  }

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
        md: { span: 9 },
        lg: { span: 8 },
        xl: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
        md: { span: 15 },
        lg: { span: 16 },
        xl: { span: 14 },
      },
    }
    const formItemLayout2 = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
        md: { span: 5 },
        lg: { span: 4 },
        xl: { span: 3 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 15 },
        md: { span: 10 },
        lg: { span: 8 },
        xl: { span: 8 },
      },
    }
    const { cityList, cityshopList, onAdd } = this.props
    const { getFieldDecorator, getFieldsValue } = this.props.form
    return (
      <div className="searchForm">
        <Form onSubmit={this.handleSubmit}>
          <Row gutter={16}>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'商品编码'}>
                {getFieldDecorator('itemCode')(
                    <Input placeholder="请输入商品条码" size="default"/>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'城市'}>
                {getFieldDecorator('city')(
                    <Select onChange = {this.handleCityChange}
                            style={{width: '100%'}} size = 'default'
                            placeholder="请选择城市">
                      {cityList.map(data =><Option key={data.key}>{data.value}</Option>)}
                    </Select>
                )}
              </FormItem>
            </Col>
            <Col className="gutter-row" span={6}>
              <FormItem {...formItemLayout} label={'家乐福门店'}>
                {getFieldDecorator('shopId')(
                    <Select  style={{width: '100%'}} size = 'default' mode="tags"
                             placeholder="请选择平台门店">
                      {cityshopList.map(data =><Option key={data.shopId}>{data.shopNameAlias}</Option>)}
                    </Select>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              {this.props.canEdit ?
              <div>
                <Button type='primary' onClick={onAdd}>新增</Button>
                <FileOperation history={this.props.history} type="ImportO2oGoodsFilter" style={{margin: '0 8px'}}
                               buttonText="批量导入" buttonProps={{type: 'primary'}}
                               fileType={["xlsx", "xls"]}/>
              </div> : null}
            </Col>
            <Col span={12} style={{ textAlign: 'right', marginBottom: 16}}>
              <Button type="primary" htmlType="submit" size="default">查询</Button>
              <FileOperation history={this.props.history} type="ExportO2oGoodsFilter" style={{margin: '0 8px'}}
                             buttonText="导出" buttonProps={{type: 'primary'}}
                             queryData={getFieldsValue()}/>
              <Button onClick={this.handleReset} size="default">
                清空
              </Button>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}
export default Form.create()(ItemScheduleForm)

