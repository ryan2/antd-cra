import React, { Component } from 'react'
import {Row, Col, Button} from 'antd'

class Order extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: 'abc'
    }
  }
  //JSX
  //render jsx => html
  handleClick = () => {
    this.setState({
      user: 'cde'
    })
  }
  componentWillMount = () => {
    //alert(1)
    // 页面渲染前
  }
  componentDidMount = () => {
    
    //alert(2)
    //渲染好了
    //ajax
    //  this.setState({
    //   user: 'cde'
    // })
  }
  
  
  render() {
    return (
      <div>
        <Row gutter={16}>
          <Col className="gutter-row" span={24}>
            <h1>{this.state.user}</h1>
            <Button onClick={this.handleClick}>test</Button>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Order
