import {observable , action, runInAction} from 'mobx'
import axios from 'axios'
import { message } from 'antd'
import {apiUrl} from '../api/constant'
import {fetchData} from '../api/datasource'

const urlConfig = {
    getCategoryList: '/category/getCategoryList',
    insertCategory: '/category/insertCategory',
    updateCategory: '/category/updateCategory',
    updateCategorySort: '/category/updateCategoryList',
    getRetailFormatList: '/retailFormat/getRetailFormatListByUser'         
}

class classifyStore {
    @observable loading = false
    @observable categoryList = []
    @observable buttonLoading = false
    @observable retailFormatList = []

    @action('获取类别清单') getCategoryList = async (params) => {
        this.loading = true
        const userName = JSON.parse(window.localStorage.getItem("riderMain")).UserId
        const data = encodeURIComponent(JSON.stringify(Object.assign({
            userName: userName
        },params)))
        const result = await fetchData(urlConfig.getCategoryList, 'get', data)
        if (result.status !== 200) {
            message.error('数据加载失败')
            this.categoryList = []
        }
        runInAction(() => {
            this.loading = false
            if (result.data.data) {
                this.categoryList = result.data.data
            } else {
                this.categoryList = []
            }
        })
    }

    @action('增加类别') insertCategory = async (params) => {
        this.buttonLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        const result = await fetchData(urlConfig.insertCategory, 'post', data)
        if (result.status !== 200) {
            message.error('操作失败')
        }
        runInAction(() => {
            this.buttonLoading = false

        })
        return result.data
    }

    @action('修改类别') updateCategory = async (params) => {
        this.buttonLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        const result = await fetchData(urlConfig.updateCategory, 'post', data)
        if (result.status !== 200) {
            message.error('操作失败')
        }
        runInAction(() => {
            this.buttonLoading = false
        })
        return result.data
    }

    @action('修改类别排序') updateCategorySort = async (params) => {
        this.buttonLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        const result = await fetchData(urlConfig.updateCategorySort, 'post', data)
        if (result.status !== 200) {
            message.error('操作失败')
        }
        runInAction(() => {
            this.buttonLoading = false
        })
        return result.data
    }

    @action('获取业态列表') getRetailFormatList = async() =>{
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await fetchData(urlConfig.getRetailFormatList, 'get', data)
            runInAction(() => {
                if(result.data.data){
                    this.retailFormatList = result.data.data
                }else{
                    message.error(result.data.err)
                    this.retailFormatList = []
                }
            })
        } catch (error) {
            console.log(error);
            message.error('获取业态列表失败')
            runInAction(() => {
                this.retailFormatList = []
            })
        }
    }
}

export default new classifyStore()