import {observable , action, runInAction} from 'mobx'
import axios from 'axios'
import { message } from 'antd'
import {apiUrl} from '../api/constant'
import {fetchData} from '../api/datasource'

const urlConfig = {
    getUserList: '/user/getUserList',
    getShopList: '/shop/getShopList',
    getUserShop: '/user/getUserShops',
    saveUserShop: '/user/saveUserShops',
    getUserRoles: '/user/getUserRoles',
    saveUserRoles: '/user/saveUserRoles',
    insertUser: '/user/insertUser',
    updateUser: '/user/updateUser',
    checkUser: '/user/checkUser',
    getRetailFormatList: '/retailFormat/getRetailFormatList',
    getRetailFormatListByUserId: '/retailFormat/getRetailFormatListByUserId',
    addUserRetailFormat: '/user/addUserRetailFormat',
    deleteUserRetailFormat: '/user/deleteUserRetailFormat',

    getRoleList: '/role/getRoleList',
    addRole: '/role/addRole',
    updateRole: '/role/updateRole',
    getRoleModule: '/role/getRoleModule',
    addRoleModule: '/role/addRoleModule',
    checkRoleName: '/role/checkRoleName',

    getModuleList: '/role/getModuleList',
    saveModuleMessage: '/role/saveModuleMessage',
    rtbImageUpload: '/image/rtbImageUpload',
}

class userStore {
    //用户
    @observable userLoading = false
    @observable userSearchLoading = false
    @observable shopLoading = false
    @observable userRoleLoading = false
    @observable submitLoading = false
    @observable userList = []
    @observable userSearch = []
    @observable shopList = []
    @observable shopListTotal = 0
    @observable userShop = []
    @observable userShopTotal = 0
    @observable currentShopUser = []
    @observable userRole = []
    @observable retailFormatList = []
    @observable userRetailFormat = []
    @observable userRetailFormatLoading = false
    @observable shopListFlag = false
    @observable userShopFlag = false

    @action('初始化数据') initUserInfo = async () => {
        this.shopList = []
        this.userShop = []
    }

    @action('获取用户列表') getUserList = async () => {
        this.userLoading = true
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await fetchData(urlConfig.getUserList, 'get', data )
            runInAction(() => {
                this.userLoading = false
                if (result.data.data) {
                    result.data.data.forEach(item => {
                        item.key = item.userId
                    })
                    this.userList = result.data.data
                } else {
                    this.userList = []
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.userLoading = false
                this.userList = []
            })
        }
    }

    @action('筛选用户') getUserSearch = async (params) => {
        this.userSearchLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.getUserList, 'get', data )
            runInAction(() => {
                this.userSearchLoading = false
                if (result.data.data) {
                    result.data.data.forEach(item => {
                        item.key = item.userId
                    })
                    this.userSearch = result.data.data
                } else {
                    this.userSearch = []
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.userSearchLoading = false
                this.userSearch = []
            })
        }
    }

    @action('初始化门店数据') initShopData = () => {
        this.shopList = []
        this.shopListTotal = 0
        this.userShop = []
        this.userShopTotal = 0
        this.userShopFlag = !this.userShopFlag
        this.shopListFlag = !this.shopListFlag
    }

    @action('获取门店列表') getShopList = async (params) => {
        this.shopLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.getShopList, 'get', data )
            let shopList = null
            runInAction(() => {
                this.shopListFlag = !this.shopListFlag
                this.shopLoading = false
                if (result.data.data) {
                    result.data.data.forEach((item, index) => {
                        item.key = index
                    })
                    shopList = result.data.data
                    this.shopList = result.data.data
                    this.shopListTotal = result.data.total
                } else {
                    this.shopList = []
                    this.shopListTotal = 0
                }
            })
            return shopList
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.shopListFlag = !this.shopListFlag
                this.shopLoading = false
                this.shopList = []
                this.shopListTotal = 0
            })
        }

    }

    @action('获取用户门店范围') getUserShop = async (params) => {
        this.shopLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.getUserShop, 'get', data )
            runInAction(() => {
                this.userShopFlag = !this.userShopFlag
                this.shopLoading = false
                this.currentShopUser = params.userId
                if (result.data.data) {
                    this.userShop = result.data.data
                    this.userShopTotal = result.data.total
                } else {
                    this.userShop = []
                    this.userShopTotal = 0
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.userShopFlag = !this.userShopFlag
                this.shopLoading = false
                this.userShop = []
                this.currentShopUser = params.userId
                this.userShopTotal = 0
            })
        }
    }

    @action('保存用户门店信息') saveUserShop = async (params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.saveUserShop, 'post', data )
            if (result.status !== 200) {
                message.error('操作失败')
            }
            return result.data
        } catch (e) {
            message.error('操作失败')
            return false
        }
    }

    @action('获取业态模板') getRetailFormatList = async () => {
        //this.shopLoading = true
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await fetchData(urlConfig.getRetailFormatList, 'get', data )
            runInAction(() => {
                //this.shopLoading = false
                if (result.data.data) {
                    this.retailFormatList = result.data.data
                } else {
                    this.retailFormatList = []
                }
            })
        } catch (e) {
            message.error('获取业态模板失败')
            runInAction(() => {
                //this.shopLoading = false
                this.retailFormatList = []
            })
        }
    }

    @action('根据用户Id获取业态模板') getRetailFormatListByUserId = async (params) => {
        this.userRetailFormatLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.getRetailFormatListByUserId, 'get', data )
            let userRetailFormat = []
            runInAction(() => {
                this.userRetailFormatLoading = false
                if (result.data.data) {
                    userRetailFormat = result.data.data.map(item => item.retailFormatId)
                    this.userRetailFormat = userRetailFormat
                } else {
                    this.userRetailFormat = []
                }
            })
            return userRetailFormat
        } catch (e) {
            message.error('获取业态模板失败')
            runInAction(() => {
                this.userRetailFormatLoading = false
                this.userRetailFormat = []
            })
        }
    }

    @action('新增用户业态') addUserRetailFormat = async (params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.addUserRetailFormat, 'post', data )
            if(result.status === 200) {
                message.success('操作成功')
                let userRetailFormat = result.data.data.map(item => item.retailFormatId)
                runInAction(() => {
                    this.userRetailFormat = userRetailFormat
                })
                return userRetailFormat
            }
        } catch (e) {
            message.error('操作失败')
        }
    }

    @action('删除用户业态') deleteUserRetailFormat = async (params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.deleteUserRetailFormat, 'post', data )
            if(result.status === 200) {
                message.success('操作成功')
                let userRetailFormat = result.data.data.map(item => item.retailFormatId)
                runInAction(() => {
                    this.userRetailFormat = userRetailFormat
                })
                return userRetailFormat
            }
        } catch (e) {
            message.error('操作失败')
        }
    }

    @action('获取用户角色') getUserRoles = async (params) => {
        this.userRole = []
        this.userRoleLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.getUserRoles, 'get', data )
            runInAction(() => {
                this.userRoleLoading = false
                if (result.data.data) {
                    this.userRole = result.data.data
                } else {
                    this.userRole = []
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.userRoleLoading = false
                this.userRole = []
            })
        }
    }

    @action('保存用户角色') saveUserRoles = async (params) => {
        this.submitLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.saveUserRoles, 'post', data )
            runInAction(() => {
                this.submitLoading = false
            })
            return result.data
        } catch (e) {
            message.error('操作失败')
            runInAction(() => {
                this.submitLoading = false
            })
            return false
        }

    }

    @action('创建用户') insertUser = async (params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.insertUser, 'post', data )
            return result.data
        } catch (e) {
            message.error('操作失败')
            return false
        }
    }

    @action('修改用户') updateUser = async (params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.updateUser, 'post', data )
            return result.data
        } catch (e) {
            message.error('操作失败')
        }
    }

    @action('验证用户名是否重复') checkUser = async (params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.checkUser, 'get', data )
            return result.data
        } catch (e) {
            message.error('用户验证失败')
            return false
        }
    }

    /*****************************角色***********************************************************/
    @observable roleLoading = false
    @observable userModuleLoading = false
    @observable roleList = []
    @observable roleModuleList = []

    @action('获取角色列表') getRoleList = async () => {
        this.roleLoading = true
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await fetchData(urlConfig.getRoleList, 'get', data )
            runInAction(() => {
                this.roleLoading = false
                if (result.data.data) {
                    result.data.data.forEach(item => {
                        item.key = item.roleId
                    })
                    this.roleList = result.data.data
                } else {
                    this.roleList = []
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.roleLoading = false
                this.roleList = []
            })
        }
    }

    @action('新增角色') addRole = async (params) => {
        this.submitLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.addRole, 'post', data )
            runInAction(() => {
                this.submitLoading = false
            })
            return result.data
        } catch (e) {
            message.error('操作失败')
            runInAction(() => {
                this.submitLoading = false
            })
            return false
        }
    }

    @action('修改角色') updateRole = async (params) => {
        this.submitLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.updateRole, 'post', data )
            runInAction(() => {
                this.submitLoading = false
            })
            return result.data
        } catch (e) {
            message.error('操作失败')
            runInAction(() => {
                this.submitLoading = false
            })
            return false
        }
    }

    @action('获取角色模块') getRoleModule = async (params) => {
        this.userModuleLoading = true;
        this.roleModuleList = [];
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.getRoleModule, 'get', data )
            runInAction(() => {
                this.userModuleLoading = false
                if (result.data.data) {
                    this.roleModuleList = result.data.data.roleModuleList
                } else {
                    this.roleModuleList = []
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.userModuleLoading = false;
                this.roleModuleList = []
            })
        }
    }

    @action('选择角色模块') selectRoleModule = async (params) => {
        this.roleModuleList = params
    }

    @action('新增、修改角色模块') addRoleModule = async (params) => {
        this.submitLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.addRoleModule, 'post', data )
            runInAction(() => {
                this.submitLoading = false
            })
            return result.data
        } catch (e) {
            message.error('操作失败')
            runInAction(() => {
                this.submitLoading = false
            })
            return false
        }
    }

    @action('验证角色名是否重复') checkRoleName = async (params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.checkRoleName, 'get', data )
            return result.data
        } catch (e) {
            message.error('角色名验证失败')
            return false
        }
    }

    /*****************************模块***********************************************************/
    @observable moduleListLoading = false
    @observable moduleMessageLoading = false
    @observable moduleMessage = ''
    @observable moduleList = []

    @action('获取模块列表') getModuleList = async () => {
        this.moduleListLoading = true
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await fetchData(urlConfig.getModuleList, 'get', data )
            runInAction(() => {
                this.moduleListLoading = false
                if (result.data.data && result.data.data.moduleList) {
                    result.data.data.moduleList.forEach(item => {
                        item.key = item.moduleId
                    })
                    this.moduleList = result.data.data.moduleList
                } else {
                    this.moduleList = []
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.moduleListLoading = false
                this.moduleList = []
            })
        }
    }

    @action('获取模块信息') getModuleMessage = async (params) => {
        this.moduleMessageLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.getModuleList, 'get', data )
            runInAction(() => {
                this.moduleMessageLoading = false
                if (result.data.data.moduleList && result.data.data.moduleList[0]) {
                    this.moduleMessage = result.data.data.moduleList[0].messages || ''
                } else {
                    this.moduleMessage = ''
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.moduleMessageLoading = false
                this.moduleMessage = ''
            })
        }
    }

    @action('清除模块信息') clearModuleMessage = () => {
        this.moduleMessage = ''
    }

    @action('保存模块信息') saveModuleMessage = async (params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.saveModuleMessage, 'post', data )
            return result.data
        } catch (e) {
            message.error('操作失败')
            return false
        }
    }

    @action('模块信息图片上传') rtbImageUpload = async (params) => {
        try {
            const formData = new FormData();
            formData.append('file', params);
            const result = await axios({
                method:'post',
                url: `${apiUrl}${urlConfig.rtbImageUpload}`,
                data: formData
            })
            return result.data
        } catch (e) {
            message.error('操作失败')
            return false
        }
    }
}

export default new userStore()