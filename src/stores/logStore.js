import {observable , action, runInAction} from 'mobx'
import axios from 'axios'
import { message } from 'antd'
import {apiUrl} from '../api/constant'
import {fetchData} from '../api/datasource'

class logStore {
    @observable shopLogList
    @observable loading
    @observable shopItemLogList
    @observable categoryLogList
    @observable inventoryLogList
    @observable shopPriceList
    

    constructor() {
        this.shopLogList = []
        this.loading = false
        this.shopItemLogList = []
        this.categoryLogList = []
        this.inventoryLogList = []
        this.shopPriceList = []
    }

    //门店日志
    @action.bound getShopLogList = async (page, pageSize, taskType, operator, shopId, channelId, channelShopId, startDate, endDate, processStatus) =>{
        this.loading = true 
        const data = encodeURIComponent(JSON.stringify({
            page : page || 1,
            pageSize: pageSize || 10,
            taskType : taskType || '',
            operator: operator || '',
            shopId: shopId || '',
            channelId: channelId || '',
            startDate: startDate || '',
            endDate: endDate || '',
            channelShopId: channelShopId || '',
            processStatus: processStatus
        }))
        const result = await axios({
            method: 'get',
            url: `${apiUrl}/log/getChannelShopLog?data=${data}`,
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        runInAction (() =>{
            if(result.data.data){
                result.data.data.forEach((item,index) => {
                    item.key = index
                })
                this.shopLogList = result.data
                this.loading = false
            }else{
                message.error(result.data.err)
                this.loading = false
            }
        })
    }

    //商品日志
    @action.bound getGoodsLogList = async (page, pageSize, taskType, operator, shopId, channelId, itemCode, startDate, endDate, processStatus, batchId ) =>{
        this.loading = true 
        const data = encodeURIComponent(JSON.stringify({
            page : page || 1,
            pageSize: pageSize || 10,
            taskType : taskType || '',
            operator: operator || '',
            shopId: shopId || '',
            channelId: channelId || '',
            startDate: startDate || '',
            endDate: endDate || '',
            itemCode: itemCode || '',
            processStatus: processStatus,
            batchId: batchId
        }))
        const result = await axios({
            method: 'get',
            url: `${apiUrl}/log/getO2oGoodsLog?data=${data}`,
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        runInAction (() =>{
            if(result.data.data){
                result.data.data.forEach((item,index) => {
                    item.key = index
                })
                this.shopItemLogList = result.data
                this.loading = false
            }else{
                message.error(result.data.err)
                this.loading = false
            }
        })
    }

    //类别日志
    @action.bound getCategoryLogList = async (page, pageSize, taskType, operator, shopId, channelId, startDate, endDate, processStatus) =>{
        this.loading = true 
        const data = encodeURIComponent(JSON.stringify({
            page : page || 1,
            pageSize: pageSize || 10,
            taskType : taskType || '',
            operator: operator || '',
            shopId: shopId || '',
            channelId: channelId || '',
            startDate: startDate || '',
            endDate: endDate || '',
            processStatus: processStatus
        }))
        const result = await axios({
            method: 'get',
            url: `${apiUrl}/log/getChannelCategoryLog?data=${data}`,
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        runInAction (() =>{
            if(result.data.data){
                result.data.data.forEach((item,index) => {
                    item.key = index
                })
                this.categoryLogList = result.data
                this.loading = false
            }else{
                message.error(result.data.err)
                this.loading = false
            }
        })
    }

    //库存日志
    @action.bound getInventoryLogList = async (page, pageSize,channelShopId, itemCode, shopId, channelId, startDate, endDate, processStatus, batchId) =>{
        this.loading = true 
        const data = encodeURIComponent(JSON.stringify({
            page : page || 1,
            pageSize: pageSize || 10,
            channelShopId : channelShopId || '',
            itemCode: itemCode || '',
            shopId: shopId || '',
            channelId: channelId || '',
            startDate: startDate || '',
            endDate: endDate || '',
            processStatus: processStatus || '',
            batchId: batchId
        }))
        const result = await axios({
            method: 'get',
            url: `${apiUrl}/log/getO2oInventoryLog?data=${data}`,
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        runInAction (() =>{
            if(result.data.data){
                result.data.data.forEach((item,index) => {
                    item.key = index
                })
                this.inventoryLogList = result.data
                this.loading = false
            }else{
                message.error(result.data.err)
                this.loading = false
            }
        })
    }
    //商品价格
    @action.bound getshopPriceList = async (page, pageSize, shopId, itemCode, startDate, endDate, processStatus, batchId, operator ) =>{
        this.loading = true 
        const data = encodeURIComponent(JSON.stringify({
            page : page || 1,
            pageSize: pageSize || 10,
            shopId: shopId || '',
            startDate: startDate || '',
            endDate: endDate || '',
            itemCode: itemCode || '',
            processStatus: processStatus,
            batchId: batchId,
            operator
        }))
        const result = await axios({
            method: 'get',
            url: `${apiUrl}/log/getP4PriceLog?data=${data}`,
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        runInAction (() =>{
            if(result.data.data){
                result.data.data.forEach((item,index) => {
                    item.key = index
                })
                this.shopPriceList = result.data
                this.loading = false
            }else{
                message.error(result.data.err)
                this.loading = false
            }
        })
    }

    @action('将异常订单重置到待处理') resetting = (logId,callbacks) =>{
        this.loading = true 
        fetchData('/log','post',encodeURIComponent(JSON.stringify({logId})))
        .then(res=>{
            runInAction(()=>{
                this.loading = false 
            })
            if(res.data.status===200) callbacks&&callbacks(res.data);
            else message.error(res.data.err)
        })
        .catch(err=>{
            runInAction(()=>{
                this.loading = false 
            })
            message.error(err)
        })
    }
}

export default new logStore()