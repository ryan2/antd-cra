import {observable , action, runInAction} from 'mobx'
import axios from 'axios'
import { message } from 'antd'
import {apiUrl} from '../api/constant'
import {fetchData} from '../api/datasource'

const urlConfig = {
    getSalesList: '/salesReport/getSalesReport',
    getCityList: '/shop/getCityList',
    getShopListByCity: '/shop/getShopListByCity',
}

class salesStore {
    @observable loading = false
    @observable total = 0
    @observable pageSize = 10
    @observable salesList = []
    @observable cityList = []
    @observable cityshopList = []

    @action('获取销售列表') getSalesList = async (params) => {
        if(!params) {
            this.salesList = []
            this.total = 0
            return false
        }
        this.loading = true
        if (params && params.pageSize) {
            this.pageSize = params.pageSize
        }
        const data = encodeURIComponent(JSON.stringify(Object.assign({
            page: 1,
            pageSize: this.pageSize
        }, params)))
        try {
            const result = await fetchData(urlConfig.getSalesList, 'get', data )
            runInAction(() => {
                this.loading = false
                if (result.data.data) {
                    result.data.data.forEach((item,index) => {
                        item.key = index
                        /*item.itemTypeName = itemTypeConfig.find(v => v.key === item.itemType).name
                         item.statusName = statusConfig.find(v => v.key === item.status).name*/
                    })
                    this.salesList = result.data.data
                    if(result.data.total !== -1) {
                        this.total = result.data.total
                    }
                } else {
                    this.salesList = []
                    this.total = 0
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.loading = false
                this.productList = []
            })
        }
    }

    @action('获取城市列表') getCityList = async () =>{
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await fetchData(urlConfig.getCityList, 'get', data )
            runInAction( () =>{
                if(result.data.data){
                    this.cityList = result.data.data
                }else{
                    this.cityList = []
                }
            })
        } catch (error) {
            console.log(error);
            message.error('数据加载失败')
            runInAction( () =>{
                this.cityList = []
            })
        }
    }

    @action('获取城市门店列表') getShopListByCity = async (params) =>{
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.getShopListByCity, 'get', data )
            runInAction( () =>{
                if(result.data.data){
                    this.cityshopList = result.data.data
                }else{
                    this.cityshopList = []
                }
            })
        } catch (error) {
            message.error('数据加载失败')
            runInAction( () =>{
                this.cityshopList = []
            })
        }
    }
}

export default new salesStore()