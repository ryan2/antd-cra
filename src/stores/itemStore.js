import {
    observable,
    action,
    runInAction
} from 'mobx'
import {
    message
} from 'antd'
import {
    fetchData
} from '../api/datasource'

const urlConfig = {
    getProductList: '/product/getProductList',
    exportProductList: '/product/export',
    getProductItem: '/product/getProduct',
    updateProductItem: '/product/updateProducts',
    imageUpload: '/image/upload',
    getCategoryList: '/category/getCategoryList',
    // getCategoryList: '/category/getCategoryListProduct',
    getRetailFormatList: '/retailFormat/getRetailFormatListByUser',
    getWarehouseGoodsList: '/warehouse/getWarehouseGoodsList'
}

const itemTypeConfig = [{
    key: 1,
    name: '常温'
}, {
    key: 2,
    name: '冷藏'
}, {
    key: 3,
    name: '冷冻'
}]

const statusConfig = [{
    key: 0,
    name: '未启用'
}, {
    key: 1,
    name: '启用'
}, {
    key: -1,
    name: '禁用'
}]

//节点排序
const handleCategorySort = (prop) => {
    return function (obj1, obj2) {
        var val1 = obj1[prop];
        var val2 = obj2[prop];
        if (val1 < val2) {
            return -1;
        } else if (val1 > val2) {
            return 1;
        } else {
            return 0;
        }
    }
}

class itemStore {
    @observable loading = false
    @observable exportLoading = false
    @observable submitLoading = false
    @observable total = 0
    @observable page = 1
    @observable pageSize = 10
    @observable productList = []
    @observable productItem = {}
    @observable categoryList = []
    @observable categoryListMenu = []
    @observable cacheData = {}
    @observable menuOpenKeys = []
    @observable retailFormatList = []
    @observable warehouseGoodsList = []

    @action('获取商品主档列表') getProductList = async (params) => {
        this.loading = true
        if (params && params.pageSize) {
            this.pageSize = params.pageSize
        }
        if (params && params.page) {
            this.page = params.page
        } else {
            this.page = 1
        }
        const data = encodeURIComponent(JSON.stringify({
            page: 1,
            pageSize: this.pageSize,
            ...params
        }))
        this.cacheData = params;
        try {
            const result = await fetchData(urlConfig.getProductList, 'get', data)
            runInAction(() => {
                this.loading = false
                if (result.data.data) {
                    result.data.data.forEach(item => {
                        item.key = item.itemCode + '-' + item.retailFormatId
                        item.itemTypeName = itemTypeConfig.find(v => v.key === item.itemType).name
                        item.statusName = statusConfig.find(v => v.key === item.status).name
                    })
                    this.productList = result.data.data
                    if (result.data.total !== -1) {
                        this.total = result.data.total
                    }
                } else {
                    this.productList = []
                    this.total = 0
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.productList = []
                this.loading = false
            })
        }
    }

    @action('缓存数据') saveCacheData = (params) => {
        for (var key in params) {
            this[key] = params[key]
        }
    }

    @action('清除缓存数据') deleteCacheData = () => {
        this.page = 1
        this.pageSize = 10
        this.cacheData = {}
        this.productList = []
        this.categoryListMenu = []
        this.menuOpenKeys = []
        this.total = 0
    }

    @action('获取商品主档信息') getProductItem = async (params) => {
        this.loading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.getProductItem, 'get', data)
            runInAction(() => {
                this.loading = false
                if (result.data.data) {
                    this.productItem = result.data.data
                } else {
                    message.error('数据加载失败')
                    this.productItem = []
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.loading = false
                this.productItem = []
            })
        }
    }

    @action('更新商品主档信息') updateProductItem = async (params) => {
        this.submitLoading = true
        const data = encodeURIComponent(`[${JSON.stringify(params)}]`)
        try {
            const result = await fetchData(urlConfig.updateProductItem, 'post', data)
            runInAction(() => {
                this.submitLoading = false
            })
            return result.data
        } catch (e) {
            message.error('操作失败')
        }

    }


    @action('获取类别清单') getCategoryList = async (params) => {
        const userName = JSON.parse(window.localStorage.getItem("riderMain")).UserId
        const data = encodeURIComponent(JSON.stringify(Object.assign({
            userName: userName
        }, params)))
        try {
            const result = await fetchData(urlConfig.getCategoryList, 'get', data)
            runInAction(() => {
                if (result.data.data) {
                    this.categoryList = result.data.data
                } else {
                    this.categoryList = []
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.categoryList = []
            })
        }
    }

    @action('获取类别清单菜单') getCategoryListMenu = async (params) => {
        const userName = JSON.parse(window.localStorage.getItem("riderMain")).UserId
        const data = encodeURIComponent(JSON.stringify(Object.assign({
            userName: userName
        }, params)))
        try {
            const result = await fetchData(urlConfig.getCategoryList, 'get', data)
            runInAction(() => {
                if (result.data.data) {
                    this.categoryListMenu = result.data.data
                } else {
                    this.categoryListMenu = []
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.categoryListMenu = []
            })
        }
    }

    @action('将返回数组转成树状结构') handleCategoryData = (data, disable) => {
        let _data = []
        data = data.slice()
        data.forEach(item => {
            item.label = item.categoryName
            item.value = item.categoryId + ''
            if (disable) {
                item.disabled = item.flag === 0 ? true : false
            }
            if (item.level === 1) {
                item.children = data.filter(_item => _item.parentCategoryId === item.categoryId)
                item.children.sort(handleCategorySort('seqNo'))
                _data.push(item)
            }
        })
        return _data.sort(handleCategorySort('seqNo'))
    }

    @action('获取业态模板') getRetailFormatList = async () => {
        //this.shopLoading = true
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await fetchData(urlConfig.getRetailFormatList, 'get', data)
            runInAction(() => {
                //this.shopLoading = false
                if (result.data.data) {
                    this.retailFormatList = result.data.data
                } else {
                    this.retailFormatList = []
                }
            })
        } catch (e) {
            message.error('获取业态模板失败')
            runInAction(() => {
                //this.shopLoading = false
                this.retailFormatList = []
            })
        }
    }

    @action('获取前仓商品列表') getWarehouseGoodsList = async (params) => {
        //this.shopLoading = true
        this.loading = true
        if (params && params.pageSize) {
            this.pageSize = params.pageSize
        }
        if (params && params.page) {
            this.page = params.page
        } else {
            this.page = 1
        }
        const data = encodeURIComponent(JSON.stringify({
            page: 1,
            pageSize: this.pageSize,
            ...params
        }))
        this.cacheData = params;
        try {
            const result = await fetchData(urlConfig.getWarehouseGoodsList, 'get', data)
            runInAction(() => {
                this.loading = false
                if (result.data.data) {
                    result.data.data.forEach(item => {
                        item.key = item.itemCode + '-' + item.retailFormatId
                        item.itemTypeName = itemTypeConfig.find(v => v.key === item.itemType).name
                        item.statusName = statusConfig.find(v => v.key === item.status).name
                    })
                    this.warehouseGoodsList = result.data.data
                    if (result.data.total !== -1) {
                        this.total = result.data.total
                    }
                } else {
                    this.warehouseGoodsList = []
                    this.total = 0
                }
            })
        } catch (e) {
            message.error('获取前仓商品列表失败')
            runInAction(() => {
                this.warehouseGoodsList = []
                this.loading = false
            })
        }
    }
}

export default new itemStore()