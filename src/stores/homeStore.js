import {observable , action, runInAction} from 'mobx'
import axios from 'axios'
import {message} from 'antd'
import {apiUrl} from '../api/constant'

class homeStore{
    @observable channelList
    @observable totalList

    constructor() {
        this.channelList = []
        this.totalList = []
    }
    @action.bound getData = async() =>{
        const data = encodeURIComponent(JSON.stringify())
        const result = await axios({
            method: 'get',
            url:`${apiUrl}/user/getIndex?data=${data}`
        })
        if(result.status !==200){
            message.error('数据加载失败')
            this.channelList=[]
            this.totalList=[]
        }
        runInAction(() =>{
            if(result.data.data){
                this.channelList = result.data.data.channelData
                this.totalList = result.data.data.totalData
            }
        })
    }
}

export default new homeStore()