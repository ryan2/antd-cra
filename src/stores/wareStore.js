import {
    observable,
    action,
    runInAction
} from 'mobx'
import axios from 'axios'
import {
    message
} from 'antd'
import {
    apiUrl
} from '../api/constant'
import {
    fetchData
} from '../api/datasource'

const urlConfig = {
    getWarehouseGoodsList: '/warehouse/getWarehouseGoodsList'
}

class wareStore {
    @observable loading = false
    @observable exportLoading = false
    @observable submitLoading = false
    @observable total = 0
    @observable page = 1
    @observable pageSize = 10
    @observable cacheData = {}
    @observable warehouseGoodsList = []
    @observable cityList = []
    @observable cityshopList = []
    @observable shopList = []

    @action('缓存数据') saveCacheData = (params) => {
        for (var key in params) {
            this[key] = params[key]
        }
    }

    @action('清除缓存数据') deleteCacheData = () => {
        this.page = 1
        this.pageSize = 10
        this.cacheData = {}
        this.total = 0
    }

    @action('获取前仓商品列表') getWarehouseGoodsList = async (params) => {
        //this.shopLoading = true
        this.loading = true
        if (params && params.pageSize) {
            this.pageSize = params.pageSize
        }
        if (params && params.page) {
            this.page = params.page
        } else {
            this.page = 1
        }
        const data = encodeURIComponent(JSON.stringify({
            page: 1,
            pageSize: this.pageSize,
            ...params
        }))
        this.cacheData = params;
        // const result = {
        //     data: {
        //         "status": 200,
        //         "total": null,
        //         "err": null,
        //         "data": [{
        //             "itemId": 100,
        //             "shopId": "114",
        //             "shopName": "古北店",
        //             "itemCode": "10101575",
        //             "goodsName": "三得利橙汁1.25L",
        //             "isDs": 0
        //         }]
        //     }
        // }
        try {
            const result = await fetchData(urlConfig.getWarehouseGoodsList, 'get', data)
            runInAction(() => {
                this.loading = false;
                if (result.data.data) {
                    // result.data.data.forEach(item => {
                    //     item.key = item.shopId + '-' + item.itemCode
                    //     // item.itemTypeName = itemTypeConfig.find(v => v.key === item.itemType).name
                    //     // item.statusName = statusConfig.find(v => v.key === item.status).name
                    // })
                    this.warehouseGoodsList = result.data.data;
                    if (result.data.total !== -1) {
                        this.total = result.data.total
                    }
                } else {
                    this.warehouseGoodsList = []
                    this.total = 0
                }
            })
        } catch (e) {
            message.error('获取前仓商品列表失败')
            runInAction(() => {
                this.warehouseGoodsList = []
                this.loading = false
            })
        }
    }
    @action('卸载城市门店list') clearShopList = () => {
        this.cityList = []
        this.cityshopList = []
    }
    //获取城市对应门店
    // @action.bound getShopListByCity = async (city) => {
    //     const data = encodeURIComponent(JSON.stringify({
    //         city: city
    //     }))
    //     try {
    //         const result = await axios({
    //             method: 'get',
    //             url: `${apiUrl}/shop/getShopListByCity?data=${data}`
    //         })
    //         runInAction(() => {
    //             if (result.data.data) {
    //                 this.cityshopList = result.data.data
    //             } else {
    //                 this.cityshopList = []
    //             }
    //         })
    //     } catch (error) {

    //     }
    // }
    @action('根据city查门店') getShopListByCityList = async (params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await axios({
                method: 'get',
                url: `${apiUrl}/shop/getShopListByCityList?data=${data}`
            })
            runInAction(() => {
                if (result.data.data) {
                    this.shopList = result.data.data
                }
            })
        } catch (error) {
            message.error('获取门店失败')
            runInAction(() => {
                this.shopList = []
            })
        }
    }
    //获取城市列表
    @action.bound getCityList = async () => {
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await axios({
                method: 'get',
                url: `${apiUrl}/shop/getCityList?data=${data}`
            })
            runInAction(() => {
                if (result.data.data) {
                    this.cityList = result.data.data
                } else {
                    this.cityList = []
                }
            })
        } catch (error) {
            message.error('数据加载失败')
            runInAction(() => {
                this.cityList = []
            })
        }
    }
}

export default new wareStore()