import {observable , action, runInAction} from 'mobx'
import axios from 'axios'
import { message } from 'antd'
import {apiUrl} from '../api/constant'
import {fetchData} from '../api/datasource'

const urlConfig = {
    getReportList: '/o2oGoodsFilter/getO2oGoodsFilterSheetHead',
    getNameById: '/o2oGoodsFilter/getSheetGoodsNameById',
    getShopById: '/shop/getShopListByUserName',
    addItemReport: '/o2oGoodsFilter/insertO2oGoodsFilterSheet',
    getItemDetail: '/o2oGoodsFilter/getO2oGoodsFilterSheetDetail',
    changeItemDetail: '/o2oGoodsFilter/updateO2oGoodsFilterSheetDetail',
    saveItemReport: '/o2oGoodsFilter/updateO2oGoodsFilterSheetHead',
    exportReportDetail: '/o2oGoodsFilter/exportO2oGoodsFilterSheetDetail',
    getStatusMsg: '/o2oGoodsFilter/getStatusMsg',
}

class reportStore {

    @observable loading = false
    @observable cardLoading = false
    @observable sheetHeadLoading = false
    @observable addStatus = false
    @observable addLoading = false
    @observable changeDetailStatus = false
    @observable changeDetailLoading = false
    @observable saveStatus = false
    @observable reportList = []
    @observable total = 0
    @observable detailTotal = 0
    @observable goodsName = ''
    @observable imageUrl = ''
    @observable goodsName = ''    
    @observable shopList = []
    @observable sheetHead = {}
    @observable sheetDetail = []
    @observable stepsStatus = undefined
    @observable sheetId = ''
    @observable statusMsg = []

    @action('获取清单列表') getReportList = async (params) => {
        this.loading = true 
        const data = encodeURIComponent(JSON.stringify(Object.assign({
            page: 1,
            pageSize: 10
        }, params)))
        try {
            const result = await fetchData(urlConfig.getReportList, 'get', data)
            runInAction( () => {
                if (result.data.data) {
                    result.data.data.forEach((item, index) => {
                        item.key = index
                    })
                    this.reportList = result.data.data
                    this.total = result.data.total
                    this.loading = false
                } else {
                    this.reportList = []
                    this.total = 0
                    this.loading = false
                }
            })
        } catch (error) {
            console.log(error)
            message.error('数据加载失败')
            runInAction(() => {
                this.loading = false
                this.reportList = []
            })
        }
    }
    
    @action('根据商品编码获取商品名称') getNameById = async(itemCode) => {
        const data = encodeURIComponent(JSON.stringify({itemCode: itemCode}))
        try {
            const result = await fetchData(urlConfig.getNameById, 'get', data)
            runInAction(() => {
                if(result.data.data){
                    this.goodsName = result.data.data.goodsName
                    this.imageUrl = result.data.data.imageUrl
                    this.barcode = result.data.data.barcode
                }else{
                    this.goodsName = ''
                    this.imageUrl = ''
                    this.barcode = ''
                }
            })
        } catch (error) {
            console.log(error)
            message.error('获取商品名称失败')
            runInAction(() => {
                this.goodsName = ''
            })
        }
    }

    @action('根据用户名获取门店') getShopById = async(userName) => {
        const data = encodeURIComponent(JSON.stringify({userName: userName}))
        try {
            const result = await fetchData(urlConfig.getShopById, 'get', data)
            runInAction(() => {
                if(result.data.data){
                    this.shopList = result.data.data
                }else{
                    this.shopList = []
                }
            })
        } catch (error) {
            console.log(error)
            message.error('获取门店失败')
            runInAction(() => {
                this.shopList = []
            })
        }
    }

    @action('获取点击的sheetId') getSheetId = (sheetId) =>{
        this.sheetId = sheetId
    }

    @action('新增商品提报单') addItemReport = async(params) => {
        this.addLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.addItemReport, 'post', data)
            runInAction(() => {
                if(result.data.status === 200){
                    message.success('新增成功')
                    this.addStatus = true
                    this.addLoading = false
                }else{
                    message.error(result.data.err)
                    this.addStatus = false
                    this.addLoading = false
                }
            })
        } catch (error) {
            console.log(error)
            message.error('新增失败')
            runInAction(() => {
                this.addStatus = false
                this.addLoading = false
            })
        }
    }

    @action('获取商品提报单明细') getItemDetail = async(params) => {
        this.cardLoading = true
        if(params.sheetId !== this.sheetId){
            this.sheetHeadLoading = true
        }
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.getItemDetail, 'get', data)
            runInAction(() => {
                if(result.data.data){
                    if(params.sheetId !== this.sheetId){
                        this.sheetHeadLoading = false
                        this.sheetId = params.sheetId
                        this.sheetHead = result.data.data.sheetHead
                    }
                    this.cardLoading = false
                    this.detailTotal = result.data.total
                    result.data.data.sheetDetail.forEach((item,index) => {
                        //item.key = index
                        item.key = item.rowNo
                    })
                    this.sheetDetail = result.data.data.sheetDetail
                    switch (result.data.data.sheetHead.flag){
                        case '新建':
                            this.stepsStatus = 0 ;
                            break
                        case '提报':
                            this.stepsStatus = 1
                            break
                        case '生效':
                            this.stepsStatus = 2
                            break
                        default:
                            this.stepsStatus = undefined
                    }
                }else{
                    this.sheetHead = {}
                    this.sheetDetail = []
                    this.stepsStatus = undefined
                    this.cardLoading = false
                    this.detailTotal = 0                
                }
            })
        } catch (error) {
            console.log(error)
            message.error('获取明细失败')
            runInAction(() => {
                this.sheetHead = {}
                this.sheetDetail = []
                this.stepsStatus = undefined
                this.cardLoading = false 
                this.detailTotal = 0                 
            })
        }
    }

    @action('获取拒绝原因') getStatusMsg = async() => {
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await fetchData(urlConfig.getStatusMsg, 'get', data)
            runInAction(() => {
                if(result.data.data){
                    this.statusMsg = result.data.data
                }else{
                    this.statusMsg = []
                }
            })
        } catch (error) {
            message.error('获取拒审原因失败')
            runInAction(() => {
                this.statusMsg = []
            })
        }
    }

    @action('拒审单据') refuseCheck = async(params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        return fetchData(urlConfig.changeItemDetail, 'post', data)
    }

    @action('审核通过单据') passCheck = async(params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        return fetchData(urlConfig.saveItemReport, 'post', data)
    }

    @action('初始化sheetId') initSheetId = () => {
        this.sheetId = ''
        this.sheetHead = {}
        this.sheetDetail = []
    }

    @action('修改提报单') changeItemDetail = async(params) => {
        this.changeDetailLoading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.changeItemDetail, 'post', data)
            runInAction(() =>{
                if(result.data.data){
                    message.success('修改成功')
                    this.changeDetailLoading = false
                    this.changeDetailStatus = true
                }else{
                    message.error(result.data.err)
                    this.changeDetailLoading = false 
                    this.changeDetailStatus = false               
                }  
            })
        } catch (error) {
            console.log(error)
            message.error('修改失败')
            runInAction(() =>{
                this.changeDetailLoading = false
                this.changeDetailStatus = false 
            })                                    
        }        
    }

    @action('提交提报单') saveItemReport = async(params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.saveItemReport, 'post', data)
            runInAction(() => {
                console.log(result)
                if(result.data.status === 200){
                    message.success('操作成功')
                    this.saveStatus = true
                }else{
                    message.error(result.data.err)
                    this.saveStatus = false
                }
            })        
        } catch (error) {
            console.log(error)
            message.error('操作失败')
            runInAction(() => {
                this.saveStatus = false
            })
        }
    }

    @action('导出提报单明细') exportReportDetail = async(params) => {
        const data =  encodeURIComponent(JSON.stringify(params))
        try {
            window.location.href = `${apiUrl}${urlConfig.exportReportDetail}?data=${data}`
        } catch (error) {
            console.log(error)
            message.error('导出失败')
        }
    }
}

export default new reportStore()