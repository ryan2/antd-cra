import {
    observable,
    action,
    runInAction
} from 'mobx'
import axios from 'axios'
import {
    message
} from 'antd'
import {
    apiUrl
} from '../api/constant'
import {
    fetchData
} from '../api/datasource'

const urlConfig = {
    getO2oGoodsLockPriceList: '/o2oGoodsLockPrice/getO2oGoodsLockPriceList',
    getO2oGoodsLockPriceByItemCode: '/o2oGoodsLockPrice/getO2oGoodsLockPriceByItemCode'
}

class lockPriceStore {
    @observable loading = false
    @observable submitLoading = false
    @observable total = 0
    @observable page = 1
    @observable pageSize = 10
    @observable lockPriceList = []
    @observable lockPrice = {}
    @observable cacheData = {}
    @observable lockPrices = []
    @observable channelList = []
    @observable cityList = []
    @observable cityshopList = []
    @observable shopList = []

    @action('查看商品锁价记录') getO2oGoodsLockPriceByItemCode = async (params) => {
        this.loading = true
        console.log(params)
        console.log(typeof params)
        const data = encodeURIComponent(JSON.stringify({
            ...params
        }))
        try {
            const result = await fetchData(urlConfig.getO2oGoodsLockPriceByItemCode, 'get', data)
            runInAction(() => {
                this.loading = false
                if (result.data.data) {
                    result.data.data.forEach(item => {
                        item.key = item.key
                    })
                    this.lockPrices = result.data.data
                } else {
                    this.lockPrices = []
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.lockPrices = []
                this.loading = false
            })
        }
    }

    @action('获取商品锁价列表') getO2oGoodsLockPriceList = async (params) => {
        this.loading = true
        if (params && params.pageSize) {
            this.pageSize = params.pageSize
        }
        if (params && params.page) {
            this.page = params.page
        } else {
            this.page = 1
        }
        const data = encodeURIComponent(JSON.stringify({
            page: 1,
            pageSize: this.pageSize,
            ...params
        }))
        this.cacheData = params;
        try {
            const result = await fetchData(urlConfig.getO2oGoodsLockPriceList, 'get', data)
            runInAction(() => {
                this.loading = false
                if (result.data.data) {
                    result.data.data.forEach(item => {
                        item.key = item.key
                    })
                    this.lockPriceList = result.data.data
                    if (result.data.total !== -1) {
                        this.total = result.data.total
                    }
                } else {
                    this.lockPriceList = []
                    this.total = 0
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.lockPriceList = []
                this.loading = false
            })
        }
    }

    @action('缓存数据') saveCacheData = (params) => {
        for (var key in params) {
            this[key] = params[key]
        }
    }
    //获取渠道和门店列表
    @action.bound getChannelShopsList = async () => {
        const data = encodeURIComponent(JSON.stringify({}))
        const result = await axios({
            method: 'get',
            url: `${apiUrl}/channel/getAllChannelList?data=${data}`
        })
        if (result.status !== 200) {
            message.error('数据加载失败')
            return []
        }
        runInAction(() => {
            if (result.data.data) {
                this.channelList = result.data.data;
            }
        })
    }
    @action('清除缓存数据') deleteCacheData = () => {
        this.page = 1
        this.pageSize = 10
        this.cacheData = {}
        this.productList = []
        this.categoryListMenu = []
        this.menuOpenKeys = []
        this.total = 0
    }
    @action('根据city查门店') getShopListByCityList = async (params) => {
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await axios({
                method: 'get',
                url: `${apiUrl}/shop/getShopListByCityList?data=${data}`
            })
            runInAction(() => {
                if (result.data.data) {
                    this.shopList = result.data.data
                }
            })
        } catch (error) {
            message.error('获取门店失败')
            runInAction(() => {
                this.shopList = []
            })
        }
    }
    //获取城市列表
    @action.bound getCityList = async () => {
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await axios({
                method: 'get',
                url: `${apiUrl}/shop/getCityList?data=${data}`
            })
            runInAction(() => {
                if (result.data.data) {
                    this.cityList = result.data.data
                } else {
                    this.cityList = []
                }
            })
        } catch (error) {
            message.error('数据加载失败')
            runInAction(() => {
                this.cityList = []
            })
        }
    }
    @action('卸载城市门店list') clearShopList = () => {
        // this.cityList = []
        this.shopList = []
    }
}

export default new lockPriceStore()