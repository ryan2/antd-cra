import {observable , action, runInAction} from 'mobx'
import axios from 'axios'
import { message } from 'antd'
import {apiUrl} from '../api/constant'
import {fetchData} from '../api/datasource'

const urlConfig = {
    getFilterList: '/o2oGoodsFilter/getO2oGoodsFilterList',
    addItemFilter: '/o2oGoodsFilter/insertO2oGoodsFilter',
    getCityList: '/shop/getCityList',
    getShopListByCity: '/shop/getShopListByCity',
}

const statusConfig = [
    {
        key: 0,
        name: '未启用'
    }, {
        key: 1,
        name: '启用'
    }, {
        key: -1,
        name: '禁用'
    }
]

//节点排序
const handleCategorySort = (prop) => {
    return function (obj1, obj2) {
        var val1 = obj1[prop];
        var val2 = obj2[prop];
        if (val1 < val2) {
            return -1;
        } else if (val1 > val2) {
            return 1;
        } else {
            return 0;
        }
    }
}

class itemFilterStore {

    @observable loading = false
    @observable total = 0
    @observable page = 1
    @observable pageSize = 10
    @observable filterList = []
    @observable cityList = []
    @observable cityshopListSearch = []
    @observable cityshopListAdd = []

    @action('初始化数据') initData = (params) => {
        this.filterList = []
        this.total = 0
    }

    @action('获取商品主档筛选列表') getFilterList = async (params) => {
        this.loading = true
        if (params && params.pageSize) {
            this.pageSize = params.pageSize
        }
        if (params && params.page) {
            this.page = params.page
        } else {
            this.page = 1
        }
        const data = encodeURIComponent(JSON.stringify({
            page: 1,
            pageSize: this.pageSize,
            ...params
        }))
        //this.cacheData = params;
        try {
            const result = await fetchData(urlConfig.getFilterList, 'get', data )
            runInAction(() => {
                this.loading = false
                if (result.data.data) {
                    result.data.data.forEach((item, index) => {
                        item.key = index
                        //item.itemTypeName = itemTypeConfig.find(v => v.key === item.itemType).name
                        //item.statusName = statusConfig.find(v => v.key === item.status).name
                    })
                    this.filterList = result.data.data
                    if(result.data.total !== -1) {
                        this.total = result.data.total
                    }
                } else {
                    this.filterList = []
                    this.total = 0
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.filterList = []
                this.loading = false
            })
        }
    }

    @action('获取城市列表') getCityList = async () =>{
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await fetchData(urlConfig.getCityList, 'get', data )
            runInAction( () =>{
                if(result.data.data){
                    this.cityList = result.data.data
                }else{
                    this.cityList = []
                }
            })
        } catch (error) {
            message.error('数据加载失败')
            runInAction( () =>{
                this.cityList = []
            })
        }
    }

    @action('获取城市门店列表') getShopListByCity = async (params, type) =>{
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.getShopListByCity, 'get', data )
            runInAction( () =>{
                if(result.data.data){
                    this[type] = result.data.data
                }else{
                    this[type] = []
                }
            })
        } catch (error) {
            message.error('数据加载失败')
            runInAction( () =>{
                this[type] = []
            })
        }
    }

    @action('重置城市门店列表') initShopListByCity = async (type) =>{
        this[type] = []
    }

    @action('新增商品主档筛选信息') addItemFilter = async (params) =>{
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.addItemFilter, 'post', data )
            return result.data
        } catch (e) {
            message.error('操作失败')
            return false
        }
    }
}

export default new itemFilterStore()