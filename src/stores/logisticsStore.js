import {
    observable,
    action,
    runInAction
} from 'mobx'
import {
    message
} from 'antd'
import {
    fetchData
} from '../api/datasource'

const urlConfig = {
    getTypeList: '/logistics/getLogisticsTypeList',
    editTypeItem: '/logistics/updateLogisticsType',
    saveTypeItem: '/logistics/insertLogisticsType',
    getChargeList: '/logistics/geLogisticsFreeShippingList',
    editChargeItem: '/logistics/updateLogisticsFreeShipping',
    saveChargeItem: '/logistics/insertLogisticsFreeShipping',
    deleteChargeItem: '/logistics/deleteLogisticsFreeShipping',
    getCityList: '/shop/getCityList',
    getShopListByCityList: '/shop/getShopListByCityList',
    getShopLogisticsList: '/shopLogistics/getShopLogisticsList',
    saveShopLogistics: '/shopLogistics/insertShopLogistics',
    updataShopLogistics: '/shopLogistics/updateShopLogistics',
    getIntervalList: '/shopLogistics/getShopLogisticsTimeList',
    editIntervalList: '/shopLogistics/updateShopLogisticsTime',
    saveIntervalList: '/shopLogistics/insertShopLogisticsTime',
    deleteIntervalList: '/shopLogistics/deleteShopLogisticsTime',
    getDistrict: '/logistics/getDistrictByGeo',
    getNoDistrict: '/logistics/getLogisticsDistrict',
    editNoDistrict: '/logistics/insertLogisticsDistrict',
}
const returnRejectPromise = text => {
    return new Promise((reslove, reject) => reject(text));
}
//校验是否为两位数以内小数
const checkDecimal = numberData => {
    // return !Number.isNaN(Number.parseFloat(numberData)) && !Number.isFinite(numberData) && Number.parseFloat(numberData) * 100 % 1 === 0
    return /^\d+(\.\d{0,2})?$/.test(numberData)
}
class logisticsStore {
    @observable loading = false
    @observable exportLoading = false
    @observable submitLoading = false
    @observable total = 0
    @observable page = 1
    @observable pageSize = 10
    @observable cacheData = {}
    @observable typeList = [] //物流类型列表  
    @observable typeCouldChange = true //物流类型是否可以新增
    @observable chargeList = [] //物流费用定义列表
    @observable chargeCouldChange = true //物流费用定义是否可以新增
    @observable shopList = [] //城市门店列表
    @observable cityList = [] //城市列表
    @observable shopLogisticsList = [] //门店物流配置列表
    @observable intervalList = [] //门店物流配置时段列表
    @observable editingShop = {
        "shopId": "",
        "logisticsType": "",
        "firstWeight": '',
        "firstFee": '',
        "nextFee": '',
        "limitWeight": '',
        "freeShippingId": '',
        "logisticsTimeId": '',
        "sFee": '',
        "isToday": '',
        "usedTime": '',
        "choseDays": '',
        "gid": ''
    } //当前编辑门店信息
    @observable auxiliary = 1
    @observable isAdding = false
    @observable districtList = []
    @observable noDistrict = {}

    @action('初始化观察数据') initObservableData = () => {
        // this.loading = false
        // this.exportLoading = false
        // this.submitLoading = false
        // this.total = 0
        this.page = 1
        this.pageSize = 10
        // this.cacheData = {}
        // this.typeList = []
        // this.chargeList = []
        // this.shopList = []
        // this.intervalList = []
        this.editingShop = {
            "shopId": "",
            "logisticsType": "",
            "firstWeight": '',
            "firstFee": '',
            "nextFee": '',
            "limitWeight": '',
            "freeShippingId": '',
            "logisticsTimeId": '',
            "sFee": '',
            "isToday": '',
            "usedTime": '',
            "choseDays": '',
            "gid": ''
        } //当前编辑门店信息
    }
    @action('获取物流类型定义列表') getTypeList = async (params) => {
        const data = encodeURIComponent(JSON.stringify({}))
        this.loading = true;
        try {
            const result = await fetchData(urlConfig.getTypeList, 'get', data)
            runInAction(() => {
                this.loading = false;
                if (result.data.data) {
                    result.data.data.forEach(p => {
                        p.key = p.logisticsType
                    });
                    this.typeList = result.data.data;
                    this.typeCouldChange = true;
                } else {
                    this.typeList = [];
                }
            })
        } catch (e) {
            message.error('获取物流类型定义失败')
            runInAction(() => {
                this.typeList = []
                this.loading = false
            })
        }
    }
    @action('编辑类型定义列表') changeTypeList = (newArr, isCancel, afresh) => {
        if (isCancel) {
            if (afresh) this.getTypeList();
            else {
                this.typeCouldChange = true;
                this.typeList = newArr
            }
        } else this.typeList = newArr
    }
    @action('修改/新增物流类型定义') editTypeItem = (item, callback) => {
        let url = '';
        let msg = '';
        if (item.newItem) {
            url = urlConfig.saveTypeItem;
            msg = '物流类型新增成功'
            const checkArr = this.typeList.filter(p => p.logisticsType == item.logisticsType);
            if (checkArr.length > 1) {
                message.error('物流类型编码不可重复')
            }
        } else {
            url = urlConfig.editTypeItem;
            msg = '物流类型条目修改成功'
        }
        if (item.logisticsType.toString().length === 0 || item.logisticsName.toString().length === 0)
            message.error('请填写物流类型编码编码和物流类型名称')
        else {
            const data = encodeURIComponent(JSON.stringify(item));
            this.loading = true;
            fetchData(url, 'post', data).then(res => {
                if (res.data.status == 200) {
                    runInAction(() => {
                        this.loading = false;
                    })
                    if (item.newItem) this.typeCouldChange = true;
                    callback && callback()
                    message.success(msg)
                } else message.error(res.data.err)
            }).catch(err => {
                runInAction(() => {
                    this.loading = false;
                })
                message.error(err)
            })
        }
    }
    @action('本地新增物流类型定义') addTypeItem = () => {
        const newKey = (Math.random() * 100).toFixed(2)
        const newItem = {
            logisticsType: '',
            logisticsName: '',
            isFresh: 0,
            district: '',
            key: this.typeList.length > 0 ? this.typeList[0].key + newKey : newKey,
            editable: true,
            newItem: true
        };
        this.typeList.push(newItem)
        this.typeCouldChange = false;
    }
    @action('获取物流费用定义列表') getChargeList = async (params) => {
        const data = encodeURIComponent(JSON.stringify({}))
        this.loading = true;
        try {
            const result = await fetchData(urlConfig.getChargeList, 'get', data)
            runInAction(() => {
                this.loading = false;
                if (result.data.data) {
                    // result.data.data.forEach(p => {
                    //     p.key = p.logisticsType
                    // });
                    this.chargeList = result.data.data;
                    this.chargeCouldChange = true;
                } else {
                    this.chargeList = [];
                }
            })
        } catch (e) {
            message.error('获取物流费用定义失败')
            runInAction(() => {
                this.chargeList = []
                this.loading = false
            })
        }
    }
    @action('编辑费用定义列表') changeChargeList = (newArr, isCancel, afresh) => {
        if (isCancel) {
            if (afresh) this.getChargeList();
            else {
                this.chargeCouldChange = true;
                this.chargeList = newArr;
            }
        } else this.chargeList = newArr;
    }
    @action('修改/新增物流费用定义') editChargeItem = (item, callback) => {
        let url = '';
        let msg = '';
        if (item.amountMax == null || item.amountMax == undefined) item.amountMax = '';
        if (item.newItem) {
            url = urlConfig.saveChargeItem;
            msg = '物流费用定义新增成功';
        } else {
            url = urlConfig.editChargeItem;
            msg = '物流费用定义条目修改成功'
        }
        if (item.freeShippingName.toString().length === 0) message.error('请填写免邮名称');
        else if (item.logisticsType.toString().length === 0) message.error('请选择物流类型');
        else if (!checkDecimal(item.amount)) message.error('请填写两位小数以内的免邮金额');
        else if (!checkDecimal(item.derateAmount)) message.error('请填写两位小数以内的减免金额');
        else if (!Number.isInteger(Number(item.weight)) || item.weight < 0) message.error('请填写正整数免邮减免重量');
        else if (item.amountMax.toString().length > 0 && !checkDecimal(item.amountMax)) message.error('请填写两位小数以内的最大金额');
        else {
            if (item.newItem) {
                this.chargeCouldChange = true;
                delete item.freeShippingId
            }
            const data = encodeURIComponent(JSON.stringify(item));
            this.loading = true;
            fetchData(url, 'post', data)
                .then(res => {
                    if (res.data.status == 200) {
                        runInAction(() => {
                            this.loading = false;
                        })
                        callback && callback()
                        message.success(msg)
                    } else message.error(res.data.err)
                }).catch(err => {
                    runInAction(() => {
                        this.loading = false;
                    })
                    message.error(err)
                })
        }
    }
    @action('本地新增费用定义') addChargeItem = () => {
        const newItem = {
            freeShippingId: this.chargeList.length > 0 ? this.chargeList[this.chargeList.length - 1].freeShippingId * 1 + 1 : 1,
            logisticsType: '',
            freeShippingName: '',
            amount: '',
            amountMax: '',
            weight: '',
            editable: true,
            newItem: true
        };
        this.chargeList.push(newItem)
        this.chargeCouldChange = false;
    }
    @action('删除物流费用定义') deleteChargeItem = id => {
        const data = encodeURIComponent(JSON.stringify({
            freeShippingId: id
        }));
        this.loading = true;
        fetchData(urlConfig.deleteChargeItem, 'post', data).
        then(res => {
            if (res.data.status === 200) {
                runInAction(() => {
                    this.loading = false;
                })
                this.getChargeList()
                message.success(`删除物流费用定义条目成功`)
            } else message.error(`删除序号为${id}物流费用定义失败,请稍后重新操作`)
        }).catch(err => {
            runInAction(() => {
                this.loading = false;
            })
            message.error(`删除序号为${id}物流费用定义失败`)
        })
    }
    @action('根据city查门店') getShopListByCityList = params => {
        if (typeof params.city === 'string') {
            const newArr = [params.city]
            params.city = newArr
        }
        params.onlyO2O = 0
        fetchData(urlConfig.getShopListByCityList, 'get', encodeURIComponent(JSON.stringify(params))).then(res => {
            runInAction(() => {
                if (res.data.data) {
                    this.shopList = res.data.data
                } else {
                    this.shopList = []
                }
            })
        }).catch(err => {
            message.error('数据加载失败')
            runInAction(() => {
                this.shopList = []
            })
        })
    }
    //获取城市列表
    @action('获取城市列表') getCityList = () => {
        fetchData(urlConfig.getCityList, 'get', encodeURIComponent(JSON.stringify({}))).then(res => {
            runInAction(() => {
                if (res.data.data) {
                    this.cityList = res.data.data
                } else {
                    this.cityList = []
                }
            })
        }).catch(err => {
            message.error('数据加载失败')
            runInAction(() => {
                this.cityList = []
            })
        })
    }
    /**
     * params 查询条件对象
     * isDetail 是否为查询单条数据
     * isAdding 是否是新增门店校验存在性
     */
    @action('获取门店物流列表') getShopLogisticsList = (params, isDetail, isAdding, callback) => {
        this.loading = true;
        fetchData(urlConfig.getShopLogisticsList, 'get', encodeURIComponent(JSON.stringify(params))).then(res => {
            runInAction(() => {
                this.loading = false;
                if (res.data.status == 200) {
                    if (isDetail) {
                        this.editShopLogisticsItem(res.data.data[0])
                        if (isAdding && res.data.data.length > 0) {
                            callback && callback()
                            // message.warning('门店类型已存在,修改请继续,否则请点击返回')
                        }
                        this.getIntervalList()
                    } else {
                        res.data.data.forEach((p, index, arr) => {
                            p.key = index;
                            if (p.rowSpan === undefined) {
                                p.rowSpan = 1;
                                p.children = []
                                for (let i = index + 1, len = arr.length; i < len; i++) {
                                    if (arr[i].shopId === p.shopId) {
                                        arr[i].rowSpan = 0;
                                        p.rowSpan++;
                                        p.children.push(arr[i])
                                    } else break;
                                }
                                p.rowSpans = p.rowSpan;
                                p.rowSpan = 1
                            }
                        })
                        this.shopLogisticsList = res.data.data.filter(p => p.children !== undefined);
                        this.total = res.data.total
                        this.page = params.page
                        this.pageSize = params.pageSize
                    }
                } else {
                    this.shopLogisticsList = []
                    this.total = 0
                }
            })
        }).catch(err => {
            message.error('数据加载失败')
            runInAction(() => {
                this.loading = false;
                this.shopLogisticsList = [];
                this.total = 0
            })
        })
    }
    @action('编辑本地门店信息') editShopLogisticsItem = params => {
        Object.assign(this.editingShop, params)
    }
    @action('新增门店物流配置') addShopLogisticsItem = callback => {
        this.loading = true;
        fetchData(urlConfig.saveShopLogistics, 'post', encodeURIComponent(JSON.stringify(this.editingShop)))
            .then(res => {
                runInAction(() => {
                    this.loading = false;
                })
                if (res.data.status == 200) {
                    message.success('新增门店物流配置成功')
                    // this.getShopLogisticsList(this.editingShop, true);
                    callback && callback()
                } else message.error(res.data.err)
            }).catch(err => {
                runInAction(() => {
                    this.loading = false;
                })
                console.log(err)
            })
    }
    @action('保存搜索条件') changeCacheData = params => Object.assign(this.cacheData, params)
    @action('修改数据库门店物流配置') updataShopLogistics = callback => {
        this.loading = true;
        fetchData(urlConfig.updataShopLogistics, 'post', encodeURIComponent(JSON.stringify(this.editingShop)))
            .then(res => {
                runInAction(() => {
                    this.loading = false;
                })
                if (res.data.data) {
                    message.success('更新门店物流配置成功')
                    callback && callback()
                }
            }).catch(err => {
                runInAction(() => {
                    this.loading = false;
                })
                console.log(err)
            })
    }
    @action('获取门店物流时间段列表') getIntervalList = () => {
        const search = {
            shopId: this.editingShop.shopId,
            logisticsType: this.editingShop.logisticsType
        }
        this.loading = true
        fetchData(urlConfig.getIntervalList, 'get', encodeURIComponent(JSON.stringify(search)))
            .then(res => {
                runInAction(() => {
                    this.loading = false;
                    if (res.data.status == 200) {
                        res.data.data.forEach(p => p.key = p.logisticsTimeId)
                        this.intervalList = [...res.data.data];
                    } else message.error(res.data.err)
                })
            })
            .catch(err => {
                runInAction(() => {
                    this.loading = false
                })
                console.log(err)
            })
    }
    @action('本地添加时间段项') addInterval = () => {
        this.auxiliary++
            this.isAdding = true
        // const key = this.intervalList.length>0?this.intervalList[this.intervalList.length-1].logisticsTimeId+1:1
        this.intervalList.push({
            logisticsTimeName: '',
            logisticsType: this.editingShop.logisticsType,
            shopId: this.editingShop.shopId,
            timeStart: '00:00',
            timeEnd: '00:00',
            fee: '',
            maxOrders: '',
            nextDay: true,
            // key
        })
    }
    @action('修改本地时间段项') editInterval = (data, key, index) => {
        this.intervalList[index][key] = data;
        this.intervalList[index].isEditing = true;
        if (key === 'timeStart' || key === 'timeEnd') {
            const start = this.intervalList[index].timeStart.split(':')
            const end = this.intervalList[index].timeEnd.split(':')
            const startH = start[0]
            const startM = start[1]
            const endH = end[0]
            const endM = end[1]
            if (startH > endH || (startH == endH && startM >= endM)) this.intervalList[index].nextDay = true;
            else this.intervalList[index].nextDay = false;
            this.isRepeat(this.intervalList[index])
        }
    }
    @action('修改时间段项') editIntervalList = index => {
        const edititem = Object.assign({}, this.intervalList[index])
        this.auxiliary++
            /***
             * 赋予默认值
             */
            if (edititem.fee.toString().length == 0) edititem.fee = 0;
        if (edititem.maxOrders.toString().length == 0) edititem.maxOrders = 100;
        /**
         * 判断时间段是否重复
         */
        const hasIinterval = this.intervalList.filter(p => p.timeStart == edititem.timeStart && p.timeEnd == edititem.timeEnd);
        if (hasIinterval.length > 1) {
            message.error('存在相同时间段,请修改时间')
            return
        }
        /**
         * 校验
         */
        if (edititem.logisticsTimeName.length == 0 || edititem.logisticsTimeName.length > 10) {
            message.error('请填写10字以内时段名称')
        } else if (!Number.isInteger(Number(edititem.fee)) || edititem.fee > 99999 || edititem.fee < 0) {
            message.error('请填写至多5位正整数的加价金额')
        } else if (!Number.isInteger(Number(edititem.maxOrders)) || edititem.maxOrders > 99999 || edititem.maxOrders < 0) {
            message.error('请填写至多5位正整数的订单数')
        } else if (!edititem.isEditing) message.info('此项未修改,无需保存')
        else {
            const url = edititem.logisticsTimeId == undefined ? urlConfig.saveIntervalList : urlConfig.editIntervalList;
            const text = edititem.logisticsTimeId == undefined ? '新增时' : '修改';
            // if(edititem.logisticsTimeId == undefined) this.intervalList=[]
            this.loading = true
            fetchData(url, 'post', encodeURIComponent(JSON.stringify(edititem)))
                .then(res => {
                    runInAction(() => {
                        this.loading = false
                        if (res.data.status == 200) {
                            if (edititem.logisticsTimeId === undefined) {
                                this.getIntervalList()
                                this.isAdding = false
                            }
                            message.success(text + '时间段项成功')
                        } else message.error(text + '时间段失败')
                    })
                })
                .catch(err => {
                    runInAction(() => {
                        this.loading = false
                    })
                    console.log(err)
                })
        }
    }
    @action('删除时间段项') deleteIntervalList = index => {
        const deleteItem = Object.assign({}, this.intervalList[index])
        const logisticsTimeId = deleteItem.logisticsTimeId;
        this.auxiliary++
            if (logisticsTimeId !== undefined) fetchData(urlConfig.deleteIntervalList, 'post', encodeURIComponent(JSON.stringify({
                    logisticsTimeId
                })))
                .then(res => {
                    if (res.data.status == 200) {
                        // this.getIntervalList()
                        runInAction(() => this.intervalList.splice(index, 1))
                        this.isRepeat(deleteItem)
                        message.success('删除时间段项成功')
                    } else message.error(res.data.err)
                })
                .catch(err => {
                    console.log(err)
                })
        else {
            this.intervalList.splice(index, 1)
            this.isAdding = false;
            this.isRepeat(deleteItem)
        }
    }
    @action('判断时间段项是否重复') isRepeat = () => {
        this.intervalList.forEach(item => {
            const hasIinterval = this.intervalList.filter(p => p.timeStart == item.timeStart && p.timeEnd == item.timeEnd);
            // this.intervalList.forEach(p => p.hasInterval = false)
            if (hasIinterval.length > 1) hasIinterval.forEach(p => p.hasInterval = true)
            else item.hasInterval = false
        })
    }
    @action('获取城市下所有行政区') getDistrict = (city, callback) => {
        this.loading = true;
        const cityName = city.value
        fetchData(urlConfig.getDistrict, 'get', encodeURIComponent(JSON.stringify({
                cityName
            })))
            .then(res => {
                runInAction(() => {
                    this.loading = false;
                    if (res.data.status == 200) {
                        this.districtList = res.data.data;
                        this.getNoDistrict(city.code, callback)
                    } else message.error(res.data.err)
                })
            }).catch(err => {
                runInAction(() => {
                    this.loading = false;
                })
                console.log(err)
            })
    }
    @action('获取城市下不可配送区域') getNoDistrict = (cityCode, callback) => {
        this.loading = true;
        fetchData(urlConfig.getNoDistrict, 'get', encodeURIComponent(JSON.stringify({
                cityCode
            })))
            .then(res => {
                runInAction(() => {
                    this.loading = false;
                    if (res.data.status == 200) {
                        this.noDistrict = res.data.data;
                        callback && callback()
                    } else message.error(res.data.err)
                })
            }).catch(err => {
                runInAction(() => {
                    this.loading = false;
                })
                console.log(err)
            })
    }
    @action('维护城市下不可匹配送区域') editNoDistrict = params => {
        this.loading = true;
        fetchData(urlConfig.editNoDistrict, 'post', encodeURIComponent(JSON.stringify(params)))
            .then(res => {
                runInAction(() => {
                    this.loading = false;
                    if (res.data.status == 200) {
                        message.success('不可配送区域修改成功')
                    } else message.error(res.data.err)
                })
            }).catch(err => {
                runInAction(() => {
                    this.loading = false;
                })
                console.log(err)
            })
    }
}

export default new logisticsStore();