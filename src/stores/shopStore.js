import {observable, action, runInAction} from 'mobx'
import axios from 'axios'
import {message} from 'antd'
import {apiUrl} from '../api/constant'
class shopStore {
    @observable loading
    @observable channelList 
    @observable shopList
    @observable checkChannelShopId
    @observable action
    @observable cityList
    @observable cityshopList

    constructor() {
        this.loading = false
        this.channelList = []
        this.cityList = []
        this.cityshopList = []
        this.shopList = {}
        this.checkChannelShopId = true
        this.checkTimestatus = false
        this.action  = `${apiUrl}/file/fileUpload`    
    }


    //获取店铺清单
    @action.bound getMsgListData = async (page, pageSize, isOpen, channelId, regionId, city, shopId) => {
        this.loading=true
        const data = encodeURIComponent(JSON.stringify({
            page : page || 1,
            pageSize: pageSize || 10,
            isOpen: isOpen || '',
            channelId: channelId || '',
            regionId: regionId || '',
            city: city ||'',
            shopId: shopId || []
        }))
        const result = await axios({
            method:'get',
            url:`${apiUrl}/channelShop/getChannelShopList?data=${data}`,
            headers:{
                'Content-type': 'application/x-www-form-urlencoded'
            },
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        return result.data
    }

    @action.bound closeLoading = () => {
        this.loading=false
    }

    //获取店铺信息
    @action.bound getShopMsg = async (channelId, channelShopId) => {
        const data = encodeURIComponent(JSON.stringify({
            channelId: channelId,
            channelShopId: channelShopId
        }))
        const result = await axios({
            method: 'get',
            url: `${apiUrl}/channelShop/getChannelShop?data=${data}`,
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        return result.data
    }

    //验证店铺Id
    @action.bound checkShopId = async (channelId, channelShopId) => {
        const data = encodeURIComponent(JSON.stringify({
            channelId: channelId,
            channelShopId: channelShopId
        }))
        const result = await axios({
            method: 'get',
            url: `${apiUrl}/channelShop/getChannelShop?data=${data}`,
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        runInAction(() =>{
            if(result.data.data === null){
                this.checkChannelShopId = true
            }else{
                this.checkChannelShopId = false
            }
        })
    }

    //修改店铺信息
    @action.bound changeShopMsg = async (channelId, channelShopId, shopId, address, serviceTel, contactTel, notice, preOrder) =>{
        const result =await axios({
            method: 'post',
            url:`${apiUrl}/channelShop/updateChannelShop`,
            data: 'data='+encodeURIComponent(JSON.stringify({
                channelId: channelId,
                channelShopId: channelShopId,
                shopId: shopId,
                address: address,
                serviceTel: serviceTel,
                contactTel: contactTel,
                notice: notice || '',
                preOrder: preOrder
            }))
        })
        if(result.status !==200){
            message.error('保存失败')
            return  []
        }
        return result
    }

    //修改店铺营业状态公告
    @action.bound changeShopisOpen = async (data) => {
        const result = await axios({
            method: 'post',
            url: `${apiUrl}/channelShop/updateChannelShops`,
            data: 'data='+encodeURIComponent(JSON.stringify(data))
        })
        if(result.status !==200){
            message.error('保存失败')
            return  []
        }
        return result
    }

    //修改店铺营业时间
    @action.bound changeShopDate = async (data) => {
        const result = await axios({
            method: 'post',
            url: `${apiUrl}/channelShop/updateChannelShopTimes`,
            data: 'data='+JSON.stringify(data)
        })
        if(result.status !==200){
            message.error('保存失败')
            return  []
        }
        return result
    }

    //获取店铺营业时间
    @action.bound getShopTime = async (channelId, channelShopId) =>{
        const data = encodeURIComponent(JSON.stringify({
            channelId: channelId,
            channelShopId: channelShopId
        }))
        const result = await axios({
            method: 'get',
            url:`${apiUrl}/channelShop/getChannelShopTime?data=${data}`
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        return result
    }

    //获取渠道和门店列表
    @action.bound getChannelShopsList = async () =>{
        const data = encodeURIComponent(JSON.stringify({}))
        const result = await axios({
            method: 'get',
            url:`${apiUrl}/channelShop/getChannelShopsList?data=${data}`
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        runInAction(() =>{
            if(result.data.data){
                const channelList = []
                for (let i in result.data.data){
                    channelList.push({
                        channelName: result.data.data[i].channelName,
                        channelId: result.data.data[i].channelId,
                    })
                }
                this.channelList = channelList
                this.shopList = result.data.data
            }
        })
    }

    // 添加店铺
    @action.bound insertChannelShop = async (channelId, shopId, channelShopId, channelShopName) =>{
        const result = await axios({
            method: 'post',
            url:`${apiUrl}/channelShop/insertChannelShop`,
            data: 'data='+encodeURIComponent(JSON.stringify({
                channelId: channelId || '',
                shopId: shopId || '',
                channelShopId: channelShopId || '',
                channelShopName: channelShopName || '',                
            }))
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        return result
    }

    //导出门店列表
    @action.bound exportChannelShopList = async  (isOpen , channelId, regionId, city, channelShopName) =>{
        const data = encodeURIComponent(JSON.stringify({
            isOpen: isOpen || '',
            channelId: channelId || '',
            regionId: regionId || '',
            city: city ||'',
            channelShopName: channelShopName || ''
        }))
        window.location.href = `${apiUrl}/channelShop/exportChannelShopList?data=${data}`
    }

    //获取城市列表
    @action.bound getCityList = async () =>{
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await axios({
                method: 'get',
                url:`${apiUrl}/shop/getCityList?data=${data}`
            })
            runInAction( () =>{
                if(result.data.data){
                    this.cityList = result.data.data
                }else{
                    this.cityList = []
                }
            })
        } catch (error) {
            message.error('数据加载失败')
            runInAction( () =>{
                this.cityList = []
            })
        }
    }

    //获取城市对应门店
    @action.bound getShopListByCity = async (city) =>{
        const data = encodeURIComponent(JSON.stringify({
            city: city
        }))
        try {
            const result = await axios({
                method: 'get',
                url:`${apiUrl}/shop/getShopListByCity?data=${data}`
            })
            runInAction( () =>{
                if(result.data.data){
                    this.cityshopList = result.data.data
                }else{
                    this.cityshopList = []
                }
            })
        } catch (error) {
       
        }
    }
    
    @action('卸载城市门店list') clearShopList = () =>{
        this.cityList = []
        this.cityshopList = []
    }

    @action('设置营业时间') updateChannelShopTime = async(params) =>{
        try {
            const result = await axios({
                method: 'post',
                url: `${apiUrl}/channelShop/updateChannelShopTime`,
                data: 'data='+JSON.stringify(params)
            })
            runInAction( () =>{
                if(result.data.status === 200){
                    this.checkTimestatus = true
                }else{
                    this.checkTimestatus = false
                }
            })
        } catch (error) {
            message.error('操作失败')
        }
    }
}
export default new shopStore()