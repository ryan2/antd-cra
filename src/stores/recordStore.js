import {observable, action, runInAction} from 'mobx'
import axios from 'axios'
import {message} from 'antd'
import {apiUrl} from '../api/constant'

class recordStore {

    @observable importList 
    @observable exportList   
    @observable loading
    

    constructor() {
        this.importList = []
        this.exportList = []
        this.loading = false
    }

    @action.bound closeLoading = () => {
        this.loading=false
    }
    //导入日志查询
    @action.bound getImportJobList = async (page, pageSize, type, operator, startDate, endDate, batchid) =>{
        this.loading = true
        const data = encodeURIComponent(JSON.stringify({
            page : page || 1,
            pageSize: pageSize || 10,
            type : type || '',
            operator: operator || '',
            startDate: startDate || '',
            endDate: endDate || '',
            batchid: batchid
        }))
        const result = await axios({
            method: 'get',
            url: `${apiUrl}/file/getImportJobList?data=${data}`,
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        runInAction (() =>{
            if(result.data){
                this.importList = result.data
            }
        })
    }
    //重置错误
    @action.bound resetlogs = async (fileId) =>{
        this.loading = true
        const data = encodeURIComponent(JSON.stringify({
            batchId: fileId
        }))
        const result = await axios({
            method: 'get',
            url: `${apiUrl}/log/resetO2OGoodsLog?data=${data}`,
        })
        if(result.status !==200){
            message.error('数据加载失败');
        }else{
            message.success('数据重置成功');
        }
        runInAction (() =>{
            if(result.data){
                this.loading = false
            }
        })
    }

    //导出日志查询
    @action.bound getExportJobList = async (page, pageSize, type, operator, startDate, endDate) =>{
        this.loading = true
        const data = encodeURIComponent(JSON.stringify({
            page : page || 1,
            pageSize: pageSize || 10,
            type : type || '',
            operator: operator || '',
            startDate: startDate || '',
            endDate: endDate || ''
        }))
        const result = await axios({
            method: 'get',
            url: `${apiUrl}/file/getExportJobList?data=${data}`,
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        runInAction (() =>{
            if(result.data){
                this.exportList = result.data
            }
        })
    }
}

export default new recordStore()
