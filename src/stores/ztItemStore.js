import {
    observable,
    action,
    runInAction
} from 'mobx'
import {
    message
} from 'antd'
import {
    fetchData
} from '../api/datasource'

const urlConfig = {
    getProductItem: '/ztProduct/getZtProduct',
    updateProductItem: '/ztProduct/updateZtProduct',
    getCategoryList: '/ztCategory/getZtCategoryList',
    getZtlabelList: '/ztLabel/getZtLabelList',
    getProductionTags: '/ztProduct/getZtProductLabelList',
    editProductionTags: '/ztProduct/saveZtProductLabelList',
    addZtTag: '/ztLabel/insertZtLabel',
    deleteZtTag: '/ztLabel/deleteZtLabel',
    editZtTag: '/ztLabel/updateZtLabel',
    getExProductions: '/ztLabel/getExistZtProductListByLabelId',
    getNoProductions: '/ztLabel/getNotExistZtProductListByLabelId',
    bindProductions: '/ztLabel/addZtProductList',
    unbindProductions: '/ztLabel/removeZtProductList',
    getPictures:'/ztProduct/getZtProductImagesList',
    getDescribes:'/ztProduct/getZtProductContentList',
}
const colorList = ['magenta', 'red', 'volcano', 'orange', 'gold', 'lime', 'green', 'cyan', 'blue', 'geekblue', 'purple']
//节点排序
const handleCategorySort = (prop) => {
    return function (obj1, obj2) {
        var val1 = obj1[prop];
        var val2 = obj2[prop];
        if (val1 < val2) {
            return -1;
        } else if (val1 > val2) {
            return 1;
        } else {
            return 0;
        }
    }
}

class itemStore {
    @observable loading = false
    @observable exportLoading = false
    @observable total = 0
    @observable page = 1
    @observable pageSize = 10
    @observable productList = []
    @observable productItem = {}
    @observable categoryList = [] //类别清单
    @observable ztLabelList = [] //后台返回的全部标签组
    @observable productionTagsList = [] //但商品的标签组
    @observable filterLabelList = [] //已经筛选过的标签组
    @observable filterKeyValue = '' //过滤标签的关键词
    @observable temporary = [] //缓存的商品标签id列表
    @observable exProductions = [] //标签已绑定商品列表
    @observable noProductions = [] //标签未绑定商品列表
    @observable noTotal = 0
    @observable exTotal = 0
    @observable exKeyWords = ''
    @observable noKeyWords = ''
    @observable labelId = ''
    @observable pictures = []
    @observable describes = []

    @action('初始化数据') initData = () => {
        this.loading = false
        this.productList = []
        this.productItem = {}
        this.categoryList = [] //类别清单
        this.ztLabelList = [] //后台返回的全部标签组
        this.productionTagsList = [] //但商品的标签组
        this.filterLabelList = [] //已经筛选过的标签组
        this.filterKeyValue = '' //过滤标签的关键词
        this.temporary = [] //缓存的商品标签id列表
        this.exProductions = [] //标签已绑定商品列表
        this.noProductions = [] //标签未绑定商品列表
        this.noTotal = 0
        this.exTotal = 0
        this.labelId = ''
        this.pictures = []
    }
    @action('获取商品主档信息') getProductItem = async (params) => {
        this.loading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.getProductItem, 'get', data)
            runInAction(() => {
                this.loading = false
                if (result.data.status == 200) {
                    this.productItem = result.data.data
                } else {
                    this.productItem = []
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.loading = false
                this.productItem = []
            })
        }
    }

    @action('更新商品主档信息') updateProductItem = async (params) => {
        this.loading = true
        const data = encodeURIComponent(JSON.stringify(params))
        try {
            const result = await fetchData(urlConfig.updateProductItem, 'post', data)
            runInAction(() => {
                this.loading = false
            })
            return result.data
        } catch (e) {
            // message.error('操作失败')
            return new Promise((reslove, reject) => reject('操作失败'))
        }
    }


    @action('获取类别清单') getCategoryList = async (params) => {
        const userName = JSON.parse(window.localStorage.getItem("riderMain")).UserId
        const data = encodeURIComponent(JSON.stringify(Object.assign({
            userName: userName
        }, params)))
        try {
            const result = await fetchData(urlConfig.getCategoryList, 'get', data)
            runInAction(() => {
                if (result.data.data) {
                    this.categoryList = result.data.data
                } else {
                    this.categoryList = []
                }
            })
        } catch (e) {
            message.error('数据加载失败')
            runInAction(() => {
                this.categoryList = []
            })
        }
    }

    @action('将返回数组转成树状结构') handleCategoryData = (data, disable) => {
        let _data = []
        data = data.slice()
        data.forEach(item => {
            item.label = item.categoryName
            item.value = item.categoryId + ''
            if (disable) {
                item.disabled = item.flag === 0 ? true : false
            }
            if (item.level === 1) {
                item.children = data.filter(_item => _item.parentCategoryId === item.categoryId)
                item.children.sort(handleCategorySort('seqNo'))
                _data.push(item)
            }
        })
        return _data.sort(handleCategorySort('seqNo'))
    }
    @action('获取所有标签列表') getZtlabelList = params => {
        this.loading = true;
        fetchData(urlConfig.getZtlabelList, 'get', encodeURIComponent(JSON.stringify({})))
            .then(res => {
                runInAction(() => this.loading = false)
                if (res.data.status == 200) {
                    const colorListLength = colorList.length;
                    res.data.data.forEach((tags, i) => {
                        tags.color = colorList[i % colorListLength]
                    })
                    runInAction(() => this.ztLabelList = res.data.data)
                    if (params) this.getProductionTags(params)
                    this.filterTagList()
                } else message.error('获取标签列表失败')
            })
            .catch(err => {
                runInAction(() => this.loading = false)
                console.log(err)
            })
    }
    @action('获取单个商品的标签') getProductionTags = params => {
        fetchData(urlConfig.getProductionTags, 'get', encodeURIComponent(JSON.stringify(params)))
            .then(res => {
                if (res.data.status == 200) {
                    runInAction(() => {
                        this.temporary = [...res.data.data]
                    })
                    this.filterTags();
                } else message.error('获取商品标签失败')
            })
            .catch(err => {
                console.log(err)
            })
    }
    @action('筛选单商品标签') filterTags = () => {
        const firArray = [];
        this.ztLabelList.forEach(type => {
            const firObj = {
                labelTypeId: type.labelTypeId,
                labelTypeName: type.labelTypeName,
                color: type.color,
                ztLabelList: type.ztLabelList.filter(label => this.temporary.indexOf(label.labelId) !== -1)
            }
            if (firObj.ztLabelList.length > 0) firArray.push(firObj)
        })
        this.productionTagsList = firArray;
    }
    @action('筛选全部标签') filterTagList = value => {
        const firArray = [];
        const targetValue = this.filterKeyValue.toLowerCase()
        this.ztLabelList.forEach(type => {
            const firObj = {
                labelTypeId: type.labelTypeId,
                labelTypeName: type.labelTypeName,
                color: type.color,
                ztLabelList: type.ztLabelList.filter(label => label.labelName.toLowerCase().indexOf(targetValue) !== -1 || label.labelNameEn.toLowerCase().indexOf(targetValue) !== -1)
            }
            if (firObj.ztLabelList.length > 0 || firObj.labelTypeName.toLowerCase().indexOf(targetValue) !== -1) firArray.push(firObj);
        })
        this.filterLabelList = firArray;
    }
    @action('保存商品标签列表') editProductionTags = (params, callback) => {
        this.loading = true;
        fetchData(urlConfig.editProductionTags, 'post', encodeURIComponent(JSON.stringify(params)))
            .then(res => {
                runInAction(() => {
                    this.loading = false;
                    this.temporary = [...params.labelList]
                })
                if (res.data.status == 200) {
                    message.success('修改标签成功')
                    this.filterTags();
                    callback && callback()
                } else message.error(res.data.err)
            })
            .catch(err => {
                runInAction(() => {
                    this.loading = false;
                })
                console.log(err)
            })
    }
    @action('新增标签') addZtTag = params => {
        this.loading = true
        fetchData(urlConfig.addZtTag, 'post', encodeURIComponent(JSON.stringify(params)))
            .then(res => {
                runInAction(() => {
                    this.loading = false
                })
                if (res.data.status == 200) {
                    message.success('新增标签成功');
                    this.getZtlabelList();
                } else message.error(res.data.err)
            })
            .catch(err => {
                runInAction(() => {
                    this.loading = false;
                })
                console.log(err)
            })
    }
    @action('编辑标签') editZtTag = params => {
        this.loading = true
        fetchData(urlConfig.editZtTag, 'post', encodeURIComponent(JSON.stringify(params)))
            .then(res => {
                runInAction(() => {
                    this.loading = false
                })
                if (res.data.status == 200) {
                    this.getZtlabelList();
                    message.success('修改成功')
                } else message.error(res.data.err)
            })
            .catch(err => {
                runInAction(() => {
                    this.loading = false;
                })
                console.log(err)
            })
    }
    @action('删除标签') deleteZtTag = labelId => {
        this.loading = true
        fetchData(urlConfig.deleteZtTag, 'post', encodeURIComponent(JSON.stringify({
                labelId
            })))
            .then(res => {
                runInAction(() => {
                    this.loading = false
                })
                if (res.data.status == 200) {
                    message.success('删除标签成功')
                    this.getZtlabelList()
                } else message.error(res.data.err)
            })
            .catch(err => {
                runInAction(() => {
                    this.loading = false;
                })
                console.log(err)
            })
    }
    @action('获取标签ID') getLabelId = labelId => this.labelId = labelId;
    @action('修改关键词') getKeyWords = (key,keyWords) => this[key] = keyWords;
    @action('获取标签已绑定商品列表') getExProductions = (page, callback) => {
        this.loading = true;
        const params = {
            pageSize: 17,
            labelId: this.labelId,
            page,
            keywords:this.exKeyWords
        }
        fetchData(urlConfig.getExProductions, 'get', encodeURIComponent(JSON.stringify(params)))
            .then(res => {
                runInAction(() => {
                    this.loading = false;
                    if (res.data.status == 200) {
                        this.exProductions = res.data.data;
                        callback && callback()
                    } else message.error(res.data.err)
                    if (res.data.total !== -1) this.exTotal = res.data.total
                })
            }).catch(err => {
                runInAction(() => {
                    this.loading = false
                })
                message.error('获取标签已绑定商品列表失败')
                console.log(err)
            })
    }
    @action('获取标签未绑定商品列表') getNoProductions = (page, callback) => {
        this.loading = true;
        const params = {
            pageSize: 20,
            labelId: this.labelId,
            page,
            keywords:this.noKeyWords
        }
        fetchData(urlConfig.getNoProductions, 'get', encodeURIComponent(JSON.stringify(params)))
            .then(res => {
                runInAction(() => {
                    this.loading = false;
                    if (res.data.status == 200) {
                        this.noProductions = res.data.data;
                        callback && callback()
                    } else message.error(res.data.err)
                    if (res.data.total !== -1) this.noTotal = res.data.total
                })
            }).catch(err => {
                runInAction(() => {
                    this.loading = false
                })
                message.error('获取标签未绑定商品列表失败')
                console.log(err)
            })
    }
    @action('绑定商品') bindProductions = (itemIdList, callback) => {
        this.loading = true;
        const params = {
            itemIdList,
            labelId: this.labelId,
        }
        fetchData(urlConfig.bindProductions, 'post', encodeURIComponent(JSON.stringify(params)))
            .then(res => {
                runInAction(() => {
                    this.loading = false;
                    if (res.data.status == 200) {
                        callback && callback()
                    } else message.error(res.data.err)
                })
            }).catch(err => {
                runInAction(() => {
                    this.loading = false
                })
                message.error('绑定商品失败')
                console.log(err)
            })
    }
    @action('解绑商品') unbindProductions = (itemIdList, callback) => {
        this.loading = true;
        const params = {
            itemIdList,
            labelId: this.labelId,
        }
        fetchData(urlConfig.unbindProductions, 'post', encodeURIComponent(JSON.stringify(params)))
            .then(res => {
                runInAction(() => {
                    this.loading = false;
                    if (res.data.status == 200) {
                        callback && callback()
                    } else message.error(res.data.err)
                })
            }).catch(err => {
                runInAction(() => {
                    this.loading = false
                })
                message.error('解绑商品失败')
                console.log(err)
            })
    }
    @action('获取商品图片') getPictures = itemId =>{
        this.loading = true
        fetchData(urlConfig.getPictures, 'get', encodeURIComponent(JSON.stringify({
                itemId
            })))
            .then(res => {
                runInAction(() => {
                    this.loading = false
                    if (res.data.status == 200) {
                        this.pictures = res.data.data;
                    } else message.error(res.data.err)
                })
            })
            .catch(err => {
                runInAction(() => {
                    this.loading = false;
                })
                message.error(err)
                console.log(err)
            })
    }
    @action('获取文描信息') getDescribes = itemId =>{
        this.loading = true
        fetchData(urlConfig.getDescribes, 'get', encodeURIComponent(JSON.stringify({
                itemId
            })))
            .then(res => {
                runInAction(() => {
                    this.loading = false
                    if (res.data.status == 200) {
                        this.describes = res.data.data;
                    } else message.error(res.data.err)
                })
            })
            .catch(err => {
                runInAction(() => {
                    this.loading = false;
                })
                message.error(err)
                console.log(err)
            })
    }
}

export default new itemStore()