import {
    observable,
    useStrict,
    action,
    runInAction
} from 'mobx'
import {
    getUser,
    getMenu,
    getHelp
} from '../api/datasource'
import {
    apiUrl
} from '../api/constant'
import {
    fetchData
} from '../api/datasource'
import {
    message
} from 'antd'
import {
    CLIENT_RENEG_LIMIT
} from 'tls';

useStrict(true)

const urlConfig = {
    getMenu: '/user/getMenu',
    getHelp: '/role/getModuleList',
    getVerifyCode: '/verifyCode',
    getChannelList: '/channel/getChannelList'
}

//国际化配置
const localeData = [{
        key: 'en',
        name: '中文'
    },
    {
        key: 'zh',
        name: 'EN'
    },
]

class State {
    @observable collapse = false
    @observable userCityList = []
    @observable channelList = []
    @observable shopId = undefined
    @observable menu = []
    @observable helpMessage = ''
    @observable helpLoading = false
    @observable roleModuleList = []

    @action changeCollapse = () => {
        this.collapse = !this.collapse
    }

    //get user info
    @observable baseUser = {
        username: ''
    }

    @action getUserInfo = async (user) => {
        let res = await getUser(user)
        runInAction("update state after fetching data", () => {
            //this.baseUser = res.data.data.user
            if (res.data.data && res.data.data.user) {
                this.baseUser = res.data.data.user
                this.userCityList = res.data.data.cityList || []
                this.roleModuleList = res.data.data.roleModuleList
            }
        })
        return res.data
    }

    @action changeShopId = (shopId) => {
        this.shopId = shopId
    }

    //locale 国际化, 默认中文
    @observable locale = localeData[1]
    @action changeLocale = (key) => {
        localeData.forEach(item => {
            if (item.key !== key) {
                this.locale = item
            }
        })
    }

    @action('获取用户菜单') getMenu = async (userName) => {
        const data = encodeURIComponent(JSON.stringify({
            userName
        }))
        try {
            const result = await fetchData(urlConfig.getMenu, 'get', data)
            runInAction(() => {
                if (result.data.data) {
                    // console.log(result.data)
                    // result.data.data.push({
                    //     class: 1,
                    //     headMenuId: 0,
                    //     moduleName: '中台管理',
                    //     menuName: '中台管理',
                    //     menuId: 1300,
                    //     isModule: 1,
                    //     url: '/ztmanage',
                    //     children: [{
                    //         class: 2,
                    //         headMenuId: 100,
                    //         moduleName: '类别管理',
                    //         menuName: '类别管理',
                    //         menuId: 1301,
                    //         isModule: 1,
                    //         url: '/ztmanage/classify'
                    //     },{
                    //         class: 2,
                    //         headMenuId: 200,
                    //         moduleName: '商品管理',
                    //         menuName: '商品管理',
                    //         menuId: 1302,
                    //         isModule: 1,
                    //         url: '/ztmanage/production'
                    //     // },{
                    //     //     class: 2,
                    //     //     headMenuId: 300,
                    //     //     moduleName: '门店物流配置',
                    //     //     menuName: '门店物流配置',
                    //     //     menuId: 1303,
                    //     //     isModule: 1,
                    //     //     url: '/ztmanage/modify'
                    //     }
                    // ]
                    // })
                    this.menu = result.data.data;
                } else {
                    this.menu = []
                }
            })
            return result.data
        } catch (e) {
            console.log(e, 111111)
            message.error('菜单获取失败')
            runInAction(() => {
                this.menu = []
            })
            return false
        }
    }

    @action('清空用户菜单') initMenu = () => {
        this.menu = []
    }

    @action('获取帮助信息') getHelp = async (moduleId) => {
        this.helpLoading = true
        const data = encodeURIComponent(JSON.stringify({
            moduleId
        }))
        try {
            const result = await fetchData(urlConfig.getHelp, 'get', data)
            runInAction(() => {
                this.helpLoading = false
                if (result.data.data.moduleList && result.data.data.moduleList[0]) {
                    this.helpMessage = result.data.data.moduleList[0].messages
                } else {
                    this.helpMessage = ''
                }
            })
        } catch (e) {
            message.error('获取帮助信息失败')
            runInAction(() => {
                this.helpLoading = false
                this.helpMessage = ''
            })
        }
    }

    @action('清除帮助信息') clearHelp = () => {
        this.helpMessage = ''
    }

    //params为需要判断的权限，值为authOptions的值，返回true表示有该权限，false则没有
    @action('判断用户权限') hasUserAuth = (params) => {
        const authOptions = {
            1: 'query', //查询
            2: 'edit', //编辑
            4: 'delete', //删除
            8: 'check', //审核
            16: 'finalCheck', //终审
            32: 'print', //打印
        }
        let userAuth = JSON.parse(window.localStorage.getItem('userAuth'))
        if (userAuth.find(item => authOptions[item] == params)) {
            return true
        }
        return false
    }

    @action('获取验证码') getVerifyCode = async () => {
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = fetchData(urlConfig.getVerifyCode, 'get', data)
            runInAction(() => {
                console.log(result)
            })
        } catch (error) {
            console.log(error)
            message.error('获取验证码失败')

        }
    }

    @action('获取渠道列表') getChannelList = async () => {
        const data = encodeURIComponent(JSON.stringify({}))
        try {
            const result = await fetchData(urlConfig.getChannelList, 'get', data)
            runInAction(() => {
                if (result.data.status == 200) {
                    this.channelList = result.data.data
                } else {
                    message.error(result.err)
                }
            })
        } catch (error) {
            console.log(error)
            message.error('获取渠道列表失败')
        }
    }
}

export default new State()