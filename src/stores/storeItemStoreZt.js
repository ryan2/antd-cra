import {observable , action, runInAction} from 'mobx'
import axios from 'axios'
import {message} from 'antd'
import {apiUrl} from '../api/constant'

const handleCategorySort = (prop) => {
    return function (obj1, obj2) {
        var val1 = obj1[prop];
        var val2 = obj2[prop];
        if (val1 < val2) {
            return -1;
        } else if (val1 > val2) {
            return 1;
        } else {
            return 0;
        }
    }
}

class storeItemStore {
    @observable loading
    @observable categoryLit
    @observable shopName
    @observable shopId
    @observable shopList

    constructor() {
        this.loading = false
        this.categoryLit = []
        this.shopName = ''
        this.shopId = ''
        this.shopList = []
    }
    
    //获取门店商品列表
    @action.bound getGoodsListData = async (page, pageSize, shopId, channelId, o2oItemId, goodsName, barcode, itemCode, shelved, status, categoryId, isSelf, city) => {
        this.loading = true;
        // console.log(page, pageSize, shopId, channelId, goodsName,  itemCode, shelved, categoryId, isSelf, city);
        // console.log(page, pageSize, shopId, channelId, o2oItemId, goodsName, barcode, itemCode, shelved, status, categoryId, isSelf, city);
        const data = encodeURIComponent (JSON.stringify({
            page: page || 1,
            pageSize: pageSize || 10,
            shopId : shopId || [],
            channelId: channelId || '',
            goodsName: goodsName ||'',
            itemCode: itemCode || '',
            shelved: shelved || '',
            categoryId: categoryId || '',
            isSelf: isSelf,
            city: city
        }))
        //console.log(categoryId);
        const result = await axios({
            method: 'get',
            url : `${apiUrl}/ztProduct/getZtSkuList?data=${data}`
        })
        if(result.status !==200){
            message.error('数据加载失败')
            return []
        }
        return result
    }

    @action.bound closeLoading = () => {
        this.loading=false
    }

    //获取门店商品信息
    @action.bound getGoodsData = async (skuId, channelId, itemId) => {
        const data = encodeURIComponent(JSON.stringify({
            skuId: skuId || '',
            channelId: channelId,
            itemId: itemId || ''
        }))
        const result = await axios({
            method: 'get',
            url : `${apiUrl}/ztProduct/getZtSkuList?data=${data}`
        })
        if(result.status !==200){
            message.error('数据加载失败')
        }
        return result
    }

    //修改门店商品信息
    @action.bound changeGoodsData = async (oldStatus, newStatus,itemId, channelId, shopId, skuId, safeInventory, lockInventory, lockInventoryRefresh, lockPrice, shelved, barcode, categoryId, goodsEnName, goodsName, spec, unit, weight, isSelf, priceRate) => {
        const result = await axios({
            method: 'post',
            url:`${apiUrl}/o2oGoods/updateO2oGoods`,
            data :'data='+encodeURIComponent(JSON.stringify({
                oldStatus: oldStatus,
                newStatus: newStatus,
                itemId: itemId,
                skuId: skuId,
                channelId: channelId,
                shopId: shopId,
                safeInventory: safeInventory ,
                lockInventory: lockInventory ,
                lockInventoryRefresh: lockInventoryRefresh || null,
                lockPrice: lockPrice ,
                shelved : shelved || 0,
                barcode: barcode || '',
                categoryId: categoryId || '',
                goodsEnName: goodsEnName || '',
                goodsName: goodsName || '',
                spec: spec || '',
                unit: unit || '',
                weight: weight || '',
                isSelf: isSelf || 0,
                priceRate:priceRate || ''             
            }))
        })
        if(result.status !==200){
            message.error('数据加载失败')
        }
        return result
    }

    //批量修改发布状态
    @action.bound updateO2oGoodsStatus = async (data) =>{
        const result = await axios({
            method: 'post',
            url: `${apiUrl}/o2oGoods/updateO2oGoodsStatus`,
            data: 'data='+JSON.stringify(data)
        })
        if(result.status !==200){
            message.error('保存失败')
            return  []
        }
        return result
    }

    //批量修改锁定价格
    @action.bound updateO2oGoodsLockPrice = async (data) =>{
        const result = await axios({
            method: 'post',
            url: `${apiUrl}/o2oGoods/updateO2oGoodsLockPrice`,
            data: 'data='+JSON.stringify(data)
        })
        if(result.status !==200){
            message.error('保存失败')
            return  []
        }
        return result
    }

    //批量修改锁定库存
    @action.bound updateO2oGoodsLockInventory = async (data) =>{
        const result = await axios({
            method: 'post',
            url: `${apiUrl}/o2oGoods/updateO2oGoodsLockInventory`,
            data: 'data='+JSON.stringify(data)
        })
        if(result.status !==200){
            message.error('保存失败')
            return  []
        }
        return result
    }

    //批量修改上下架
    @action.bound updateO2oGoodsShelved = async (data) =>{
        const result = await axios({
            method: 'post',
            url: `${apiUrl}/ztProduct/updateShelvedList`,
            data: 'data='+JSON.stringify(data)
        })
        if(result.status !==200){
            message.error('保存失败')
            return  []
        }
        return result
    }

    //导出商品
    @action.bound exportO2oGoodsList = async (shopId, channelId, o2oSkuId, goodsName, barcode, itemCode, shelved, status) => {
        const data = encodeURIComponent (JSON.stringify({
            shopId : shopId || '',
            channelId: channelId || 0,
            o2oSkuId: o2oSkuId || '',
            goodsName: goodsName ||'',
            barcode: barcode || '',
            itemCode: itemCode || '',
            shelved: shelved || '',
            status: status || ''
        }))
        window.location.href = `${apiUrl}/o2oGoods/exportO2oGoodsList?data=${data}`        
    }

    // 获取类别树
    @action.bound getCategoryList = async (params) =>{   
        const userName = JSON.parse(window.localStorage.getItem("riderMain")).UserId
        const data = encodeURIComponent(JSON.stringify(Object.assign({
            userName: userName
        },params)))  
        try {
            const result = await axios({
                method: 'get',
                url:`${apiUrl}/ztCategory/getZtCategoryList?data=${data}`
            })
            runInAction(() =>{
                if(result.data.data){
                    this.categoryList = result.data.data
                }
            })
        } catch (error) {
            message.error('数据加载失败')
            runInAction(() => {
                this.categoryList = []
            })
        }
    }

    //手工触发
    @action.bound insertGoodsLockInventory = async () =>{
        const data = encodeURIComponent(JSON.stringify({}))
        const result = await axios({
            method: 'get',
            url:`${apiUrl}/o2oGoods/insertGoodsLockInventory?data=${data}`
        })
        if(result.status !==200){
            message.error('处理失败')
            return  []
        }
        return result
    }

    @action('将返回数组转成树状结构') handleCategoryData = (data, disable) => {
        let _data = []
        data = data.slice()
        data.forEach(item => {
            item.label =  item.categoryName
            item.value = item.categoryId + ''
            if(disable) {
                item.disabled = item.flag === 0 ? true : false
            }
            if(item.level === 1) {
                item.children = data.filter(_item => _item.parentCategoryId === item.categoryId)
                item.children.sort(handleCategorySort('seqNo'))
                _data.push(item)
            }
        })
        return _data.sort(handleCategorySort('seqNo'))
    }

    @action('根据shopId查店铺名称') getShopNameById = async (shopId) =>{
        const data = encodeURIComponent(JSON.stringify({
            shopId: shopId
        }))        
        try {
            const result = await axios({
                method: 'get',
                url:`${apiUrl}/shop/getShopByShopId?data=${data}`
            })
            runInAction(() =>{
                if(result.data.data){
                    this.shopName = result.data.data.shopNameAlias
                    this.shopId = result.data.data.shopId
                }
            }) 
        } catch (error) {
            message.error('数据加载失败')
            runInAction(() => {
                this.shopName =''
                this.shopId = ''
            })
        }
    }

    @action('根据city查门店') getShopListByCityList = async (params) =>{
        const data = encodeURIComponent(JSON.stringify(params))        
        try {
            const result = await axios({
                method: 'get',
                url:`${apiUrl}/shop/getShopListByCityList?data=${data}`
            })
            runInAction(() =>{
                if(result.data.data){
                    this.shopList = result.data.data
                }
            }) 
        } catch (error) {
            message.error('获取门店失败')
            runInAction(() => {
                this.shopList = []
            })
        }
    }
}
    
export default new storeItemStore()