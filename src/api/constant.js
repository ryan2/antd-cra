// export const baseUrl = 'http://114.80.240.25:8089/mid' //外网地址 114.80.244.23:8089
// export const apiUrl = 'http://114.80.240.25:8089/midapi'
// export const apiUrl = 'http://172.17.9.10:8080/api' //石顺福
// export const apiUrl = 'http://127.0.0.1:8080/api' //本地
// export const apiUrl = 'http://pickup.carrefour.cn/midapi'  //公网正式地址

// export const baseUrl = 'http://www.pickup.cn.carrefour.com/mid' 
export const apiUrl = 'https://pickup.cn.carrefour.com/midapi'

export const Error = {
  PARAM_MISSING : {status: 4000, error: '参数缺失'},
  AUTH_FAIL : {status: 4001, error: '权限验证失败'},
  NO_PERMISSION : {status: 4002, error: '无权限访问'},
  TOKEN_TIMEOUT : {status: 4003, error: 'Token失效'},
  SERVER_ERR : {status: 5000, error: '服务器异常'},  
}  
 